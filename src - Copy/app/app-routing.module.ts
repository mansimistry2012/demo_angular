import { AuthCallbackComponent } from './shared/auth-callback/auth-callback.component';
import { AddCompanyComponent } from './features/companies/add-company/add-company.component';
import { AddQuotesComponent } from './features/quotes/add-quotes/add-quotes.component';
import { ActivityComponent } from './features/activity/activity.component';
import { QuotesComponent } from './features/quotes/quotes.component';
import { OpportunitiesComponent } from './features/opportunities/opportunities.component';
import { ConactsComponent } from './features/conacts/conacts.component';
import { CompaniesComponent } from './features/companies/companies.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './features/dashboard/dashboard.component';
import { ContactlistComponent } from './features/conacts/contactlist/contactlist.component';
import { AddOpportunitiesComponent } from './features/opportunities/add-opportunities/add-opportunities.component';
import { CompaniesDetailsComponent } from './features/companies/companies-details/companies-details.component';
import { SubActivityComponent } from './features/activity/sub-activity/sub-activity.component';
import { ActivityDetailsComponent } from './features/activity/activity-details/activity-details.component';
import { LogoutComponent } from './features/logout/logout.component';
import { LayoutComponent } from './features/layout/layout.component';
import { LoginComponent } from './features/login/login.component';
import { AuthGuard } from './shared/core/authentication/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'companies',
        component: CompaniesComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'contacts',
        component: ConactsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'opportunities',
        component: OpportunitiesComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'quotes',
        component: QuotesComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'activities',
        component: ActivityComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'add-quotes',
        component: AddQuotesComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'contacts-list',
        component: ContactlistComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'add-opportunities',
        component: AddOpportunitiesComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'companies-details',
        component: CompaniesDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'add-company',
        component: AddCompanyComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'sub-activity',
        component: SubActivityComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'activity-details',
        component: ActivityDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'logout',
        component: LogoutComponent
      },
      {
        path: 'auth-callback',
        component: AuthCallbackComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
