import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { LayoutService } from '../../features/layout/layout.service';
import { environment } from '../../../environments/environment'
import { Title } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../core/authentication/auth.service';
import { Location } from '@angular/common';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  activeRoute: any;
  panelOpenState = false;
  sideMenuToggle: boolean = false;
  window1: any;
  window2: any;
  currentApplicationVersion = environment.appVersion;
  // @Output() toggleSideBarForMe: EventEmitter<any> = new EventEmitter();

  constructor(private router: Router, private layoutService: LayoutService, public http: HttpClient,
    private titleService: Title, private authService: AuthService, private location: Location) {
    this.setTitle("Bepoz");
    // this.metaService.updateTag({name: 'description', content: 'This page is all about changing a page title and meta tags using Angular services' });
    this.activeRoute = this.router.url
    this.layoutService.setObservable().subscribe((data: any) => {
      this.activeRoute = data
    })
  }

  public setTitle(newTitle: string) {
    // let first = newTitle.substr(0,1).toUpperCase();
    if (newTitle == 'dashboard') {
      this.titleService.setTitle('Dashboard');
    }
    else if (newTitle == 'companies' || newTitle == 'companies-details' || newTitle == 'add-company') {
      this.titleService.setTitle('Companies');
    }
    else if (newTitle == 'contacts-list' || newTitle == 'contacts') {
      this.titleService.setTitle('Contacts');
    }
    else if (newTitle == 'opportunities' || newTitle == 'add-opportunities') {
      this.titleService.setTitle('Opportunities');
    }
    else if (newTitle == 'quotes' || newTitle == 'add-quotes') {
      this.titleService.setTitle('Quotes');
    }
    else if (newTitle == 'activities' || newTitle == 'activity-details' || newTitle == 'sub-activity') {
      this.titleService.setTitle('Activities');
    }
    else {
      this.titleService.setTitle("Bepoz");
    }
  }
  ngOnInit(): void {
    if (this.router.url) {
      let pageTitle = this.router.url.substr(1)
      this.setTitle(pageTitle)
    }
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    )
      .subscribe((event: any) => {
        if (event) {
          this.activeRoute = event.url;
        }
      });
  }

  gotoPage(page: any) {
    this.router.navigate([page])
    if (!page.includes('/')) {
      this.activeRoute = "/" + page
    } else {
      this.activeRoute = page
    }
    this.setTitle(page)
  }

  LogOut() {
    // this.layoutService.ResetLoginToken();
    // this.router.navigate(["/login"])

    // setTimeout(() => {

    //   this.layoutService.activeRoute = this.router.url;
    //   this.layoutService.getcurrentActivateRoute(
    //     this.layoutService.activeRoute
    //   );
    // }, 50);

    // setTimeout(() => {
    //   window.location.reload();
    // });
    // var self = this;
    // setTimeout(() => {
    //   //this.http.get(Apiurl.LogoutUrl+"0").subscribe((res: any) => {

    //   self.window1 = window.open(Apiurl.LogoutUrl + "0", "_self");
    //   self.window2 = window.open(Apiurl.LogoutUrl + "0", "_self");
    //   setTimeout(() => {
    //     self.window1?.close();
    //     self.window2?.close();
    //   }, 300);

    //   setTimeout(() => {
    //     this.layoutService.ResetLoginToken();
    //     this.router.navigate(["/login"]);
    //   }, 600);

    // })
    // new Code
    setTimeout(() => {
      this.authService.signout();
      this.layoutService.ResetLoginToken();
    }, 500)
  }

  onSidenavToggle() {
    this.sideMenuToggle = !this.sideMenuToggle
    this.layoutService.sendToggleFlag(this.sideMenuToggle)
  }

}
