import { Apiurl } from './route';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { BehaviorSubject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class CommonProvider {
  navigation: any;
  lookupList = new BehaviorSubject<any>([]);
  constructor(
    public http: HttpClient,
    public notifier: NotifierService,
    private SpinnerService: NgxSpinnerService,
    private toastr: ToastrService,
    // private spinner: NgxSpinnerService,
    private router: Router // private SpinnerService: NgxSpinnerService
  ) {
    // this.navigation = navigation;
    this.getAllLookupList();
  }

  showNotifier(type: any, message: any) {
    this.notifier.notify(type, message);
  }

  showConfirm(obj: any): any {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success ',
        cancelButton: 'btn btn-danger'
      },

      buttonsStyling: true
    });

    return swalWithBootstrapButtons.fire({
      title: obj.title,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: obj.confirmButtonText,
      cancelButtonText: obj.cancelButtonText,
      reverseButtons: false
    });
  }

  showErrorMsg(obj: any): any {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        cancelButton: 'btn btn-danger'
      },

      buttonsStyling: true
    });

    return swalWithBootstrapButtons.fire({
      title: obj.title,
      icon: 'warning',
      text: obj.text,
      // showCancelButton: true,
      cancelButtonText: obj.cancelButtonText,
      reverseButtons: false
    });
  }

  //GET METHOD
  GetMethod(url: any, data: any, IsShowLoader = true, LoadingMsg = 'Loading.') {
    // this.spinner.show();
    const httpHeader = new HttpHeaders({
      Authorization: 'bearer ' + localStorage.getItem('LoginToken')
    });
    return new Promise(async (resolve, reject) => {
      if (IsShowLoader) {
        //this.SpinnerService.show();
      }
      return this.http
        .get(Apiurl.RoutePath + url, { params: data, headers: httpHeader })
        .subscribe(
          (data: any) => {
            if (data) {
              resolve(data);
            }
            if (IsShowLoader) {
              this.SpinnerService.hide();
            }
          },
          (error: any) => {
            if (IsShowLoader) {
              this.SpinnerService.hide();
            }
            if (error.status === 401)

              resolve([]);
          }
        );
    });
  }
  //getUserInfo 
  GetMethodWithNoApiUrl(url: any, token: any) {
    // this.spinner.show();
    const httpHeader = new HttpHeaders({
      Authorization: token
    });
    return new Promise(async (resolve, reject) => {

      return this.http
        .get(url, { headers: httpHeader })
        .subscribe(
          (data: any) => {
            if (data) {
              resolve(data);
            }

          },
          (error: any) => {

            if (error.status === 401)

              resolve([]);
          }
        );
    });
  }
  //POST METHOD
  PostMethod(
    url: any,
    data: any,
    IsShowLoader = true,
    LoadingMsg = 'Loading.',
    headers: any
  ) {
    // this.spinner.show();
    if (headers == null || Object.keys(headers).length === 0)
      headers = new HttpHeaders({
        Authorization: 'bearer ' + localStorage.getItem('access_token')
      });
    else
      headers.append(
        'Authorization',
        'bearer ' + localStorage.getItem('access_token')
      );

    return new Promise(async (resolve, reject) => {
      if (IsShowLoader) {
        //this.SpinnerService.show();
      }
      /*  */
      // return this.http.post(Apiurl.RoutePath + url, data, { headers: httpHeader }).subscribe((data) => {
      return this.http
        .post(Apiurl.RoutePath + url, data, { headers })
        .subscribe(
          (data: any) => {
            if (IsShowLoader) {
              this.SpinnerService.hide();
            }
            resolve(data);
          },
          (error: any) => {
            if (IsShowLoader) {
              this.SpinnerService.hide();
            }

          }
        );
    });
  }

  PutMethod(
    url: any,
    data: any,
    IsShowLoader = true,
    LoadingMsg = 'Loading.',
    headers: any
  ) {
    // this.spinner.show();
    if (headers == null || Object.keys(headers).length === 0)
      headers = new HttpHeaders({
        Authorization: 'bearer ' + localStorage.getItem('access_token')
      });
    else
      headers.append(
        'Authorization',
        'bearer ' + localStorage.getItem('access_token')
      );

    return new Promise(async (resolve, reject) => {
      if (IsShowLoader) {
        //this.SpinnerService.show();
      }
      /*  */
      // return this.http.post(Apiurl.RoutePath + url, data, { headers: httpHeader }).subscribe((data) => {
      return this.http.put(Apiurl.RoutePath + url, data, { headers }).subscribe(
        (data: any) => {
          if (IsShowLoader) {
            this.SpinnerService.hide();
          }
          resolve(data);
          // this.spinner.hide();
        },
        (error: any) => {
          if (IsShowLoader) {
            this.SpinnerService.hide();
          }

        }
      );
    });
  }

  //status type function
  getStatusNameFromId(satusId: any) {
    let status;
    if (satusId == 1) {
      status = 'Completed';
    } else if (satusId == 2 || satusId == 0) {
      status = 'Scheduled';
    } else {
      status = '-';
    }
    return status;
  }
  getAllLookupList() {
    return new Promise(async (resolve, reject) => {
      this.GetMethod(Apiurl.getAllLookup, null, true, 'Loading')
        .then((res: any) => {
          if (res) {
            this.lookupList.next(res);
          } else {
            reject(true);
          }
        });
    })
  }
  sortAlphabetically(array: any) {
    return array.sort((a: any, b: any) => {
      var textA = a.companyName.toUpperCase();
      var textB = b.companyName.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
  }
  showSuccess(msg: any, content: any = "") {
    this.toastr.success(msg, content, {
      closeButton: false,
      timeOut: 10000,
    });
  }

  showError(msg: any, content: any = "") {
    this.toastr.error(msg, content, {
      closeButton: false,
      timeOut: 10000,
    });
  }
  // Only Numbers
  keyPressNumbers(event: any) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode >= 48 && charCode <= 57)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
}
