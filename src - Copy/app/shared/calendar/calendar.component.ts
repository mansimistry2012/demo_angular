import { ExportService } from '../../shared/export.service';
import { ThemePalette } from '@angular/material/core';
import { Validators, FormGroup } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { isSameMonth, isSameDay } from 'date-fns';
import { Subject } from 'rxjs';
import { SpeedDialFabPosition } from './../speed-dial-fab/speed-dial-fab.component';
import { NavigationExtras, Router } from '@angular/router';
import { Apiurl } from './../route';
import { CommonProvider } from '../../shared/common';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Component, Input, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView } from 'angular-calendar';
import * as _ from 'underscore';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { orderBy, SortDescriptor, State } from '@progress/kendo-data-query';
import { process } from "@progress/kendo-data-query";
import * as moment from 'moment';

import { ChangeDetectorRef } from '@angular/core';
interface eventTypeList {
  value: any;
}
interface statusTypeList {
  id: any;
  value: any;
}

const colors: any = {
  orange: {
    primary: '#ff9800',
    secondary: '#ff9800'
  },
  blue: {
    primary: '#2196f3',
    secondary: '#2196f3'
  },
  green: {
    primary: '#65c8c6',
    secondary: '#65c8c6'
  },
  purpule: {
    primary: '#7E38FB  ',
    secondary: '#7E38FB '
  }
};

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
  // @Input() listLookups: any = [];
  @Input() subActivityForm: FormGroup;
  @Input() from: any;
  @Input() InitialView: any;
  @Input() CompanyId: any;
  @Input() ContactId: any;
  @Input() contactDetails: any;
  @Input() getCompanyDetails: any;
  @Input() pageLength: any;
  @Input() tableHeight: any;
  @Input() showExportIcons: boolean = false;
  @Input() salesRepIdContact: any;
  @Input() getCompanySalesRepId: any;

  pageIndex: number = 1;
  ViewFlag: boolean = false;
  headerTitle: any = 'Add Activity';
  activeBtn: any = null;
  relatedCompanyList: any = [];
  endDateError: boolean = false;
  endTimeError: boolean = false;
  selectedView: any = 'Agenda';
  view: any = 'Agenda';
  // selectedView: any = 'Recent Activities';
  // view: any = 'Recent Activities';
  // view: any = CalendarView.Month
  isSheduled: Boolean = true;
  isCompleted: Boolean = false;
  recentActiVities: any = [];
  showWorkingWeeksOnly: boolean = false;
  dataSource: any;
  mainDataSource: any = [];
  duplicateDataSource: any = [];
  searchText: any = null;
  eventType: any;
  statusType: any;
  salesRepId: any;
  events: any;
  activeDayIsOpen: boolean = true;
  filterType: any;
  relatedCompany: any;
  relatedTransaction: any;
  relatedContact: any;
  relatedContactList: any = [];
  SpeedDialFabPosition = SpeedDialFabPosition;
  speedDialFabColumnDirection = 'column';
  speedDialFabPosition = SpeedDialFabPosition.Top;
  speedDialFabPositionClassName = 'speed-dial-container-top';
  viewDate: Date = new Date();
  selectedDate: Date = new Date();
  CalendarView = CalendarView;
  weekStartsOn = 1;
  excludeDays: number[] = [0, 6];
  showDetailFields: boolean = false;
  selectedActivityType: any;
  showForm: boolean = false;
  listTransactions: any = [];
  actList: any = [];
  companyListSearch: any = [];
  searchdropdown: any;
  listContacts: any = [];
  listMainContacts: any = []
  listContactIds: any = [];
  listParticipants: any = [];
  showProgressBar: boolean = false;
  userDetailsStr: any;
  color: ThemePalette = 'primary';
  mode: ProgressBarMode = 'indeterminate';
  userDetail: any;
  value = 50;
  bufferValue = 75;

  netsuiteId: number = 0;
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;
  // @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  refresh: Subject<any> = new Subject();
  showFilter: boolean = false;
  listLookups: any = [];
  state: State = {
    sort: [{
      field: "startDate",
      dir: "asc",
    }],
  }
  public sort: SortDescriptor[] = [
    {
      field: "documentNumber",
      dir: "asc",
    },
  ];
  speedDialFabButtons = [
    {
      icon: 'note',
      tooltip: 'Task',
      class: 'note-color',
    },
    {
      icon: 'event',
      tooltip: 'Event',
      class: 'event-color',
    },
    {
      icon: 'phone',
      tooltip: 'Phone',
      class: 'phone-color',
    }
  ];

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-pencil c-edit" aria-hidden="true" title="Edit"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-eye c-view" aria-hidden="true" title="View"></i>',
      a11yLabel: 'View',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('View', event);
      }
    },
    // {
    //   label: '<i class="fa fa-trash c-trash" aria-hidden="true" title="Delete"></i>',
    //   a11yLabel: 'Delete',
    //   onClick: ({ event }: { event: CalendarEvent }): void => {
    //     this.events = this.events.filter((iEvent: any) => iEvent !== event);
    //     this.handleEvent('Deleted', event);
    //   }
    // }
  ];

  modalData: {
    action: string;
    event: CalendarEvent;
  };
  eventList: eventTypeList[] = [
    { value: 'Event' },
    { value: 'Phone' },
    { value: 'Task' }
  ];
  statusTypeList: statusTypeList[] = [
    { id: 1, value: 'Completed' },
    { id: 2, value: 'Scheduled' }
  ];

  displayedColumns: string[] = [
    'type',
    'title',
    'startDate',
    'endDate',
    'duration',
    'salesRep',
    'statusId',
    'relatedCustomer',
    'relatedTransaction',
    'relatedContact',
    'phone'
  ];
  defaultSalesRepId: any;
  userDetails: any;
  userIsAdmin: boolean = false;
  constructor(
    public common: CommonProvider,
    private router: Router,
    private modal: NgbModal,
    private SpinnerService: NgxSpinnerService,
    private commonService: CommonProvider
    , private exportService: ExportService,
    private cdref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.getAllLookups();
    this.getUserDetailsFromStorage()
    if (localStorage.getItem('activityView')) {
      let view = localStorage.getItem('activityView') as string;
      switch (view.toLowerCase()) {
        case 'agenda':
          this.selectedView = 'Agenda'
          this.view = 'Agenda'
          this.viewAgenda()
          break;
        case 'month':
          this.selectedView = 'Month'
          this.setView(CalendarView.Month)
          break;
        case 'week':
          this.selectedView = 'Week'
          this.setView(CalendarView.Week)
          break;
        case 'working weeks':
          this.selectedView = 'Working Weeks'
          this.setView(CalendarView.Week)
          break;
        case 'day':
          this.selectedView = 'Day'
          this.setView(CalendarView.Day)
          break;

        default:
          this.selectedView = 'Agenda'
          this.view = 'Agenda'
          break;
      }
    }
    if (this.listLookups.company) {
      this.companyListSearch = this.common.sortAlphabetically(this.listLookups.company?.companyList)
      this.relatedCompanyList = this.companyListSearch
    }
    // this.view = this.InitialView

    this.recentActiVities = [
      {
        CreatedOn: new Date(),
        ActivityType: 'Phone',
        ActivityName: 'Test',
        title: 'New Test',
        ActivityDescription: 'In student get api we dont get document details, medical details & other details',
        StartDate: new Date(),
        EndDate: new Date(),

      },
      {
        CreatedOn: new Date(),
        ActivityType: 'Event',
        ActivityName: 'Test',
        title: 'New Test',
        ActivityDescription: 'In student get api we dont get document details, medical details & other details',
        StartDate: new Date(),
        EndDate: new Date(),

      },
      {
        CreatedOn: new Date(),
        ActivityType: 'Task',
        ActivityName: 'Test',
        title: 'New Test',
        ActivityDescription: 'In student get api we dont get document details, medical details & other details',
        StartDate: new Date(),
        EndDate: new Date(),

      }]
  }

  ngAfterContentChecked() {
    this.showProgressBar = false;
    this.cdref.detectChanges();
  }

  getUserDetailsFromStorage() {
    let userDetailsStr: any = '';
    if (localStorage.getItem('UserDetails')) {
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      this.userDetails = JSON.parse(userDetailsStr);
      this.userIsAdmin = this.userDetails.isAdmin;
      if (this.userDetails && !this.userDetails.isAdmin) {
        this.defaultSalesRepId = this.userDetails.salesRepId;
      }
    }
  }
  ngAfterViewInit(): void {
    this.getCalenderEvent()
  }
  //sales repo data
  getAllLookups() {
    this.listLookups = [];
    this.common
      .lookupList.subscribe(async (res: any) => {
        if (res) {
          this.listLookups = res;
        } else {
          await this.common.getAllLookupList();
          this.getAllLookups();
        }
      });
  }
  viewAgenda() {
    this.view = 'Agenda';
    localStorage.setItem('activityView', 'Agenda')
  }

  viewRecentActivity() {
    this.view = 'Recent Activities'
  }

  setView(view: CalendarView, showWhichDays: any = null) {
    this.view = view;
    switch (this.view) {
      case CalendarView.Month:
        localStorage.setItem('activityView', view)
        break;
      case 'week':
        localStorage.setItem('activityView', showWhichDays)
        break;
      case 'working weeks':
        localStorage.setItem('activityView', showWhichDays)
        break;
      case 'day':
        localStorage.setItem('activityView', view)
        break;
      default:
        break;
    }
    this.showWorkingWeeksOnly = false;
    if (showWhichDays == 'Working Weeks') {
      this.showWorkingWeeksOnly = true;
    }
  }

  inputEvent(event: any) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filterTypeChange(filterValue);
  }

  filterTypeChange(event: any) {
    this.dataSource = new MatTableDataSource(this.mainDataSource);
    this.mainDataSource = JSON.parse(JSON.stringify(this.duplicateDataSource))
    if (this.eventType) {
      // this.dataSource = new MatTableDataSource(
      //   _.filter(this.dataSource.filteredData, {
      //     type: this.eventType
      //   })
      // );
      this.mainDataSource = this.mainDataSource.filter((obj: any) => obj.type === this.eventType)
    }
    if (this.statusType) {
      let filterStatus = this.statusType
      if (filterStatus.toLowerCase() === "scheduled") {
        filterStatus = "in progress"
      }
      this.mainDataSource = this.mainDataSource.filter((obj: any) => obj.status.toLowerCase() === filterStatus.toLowerCase())

    }
    if (this.salesRepId || this.defaultSalesRepId) {
      this.mainDataSource = this.mainDataSource.filter((obj: any) => (obj.salesRepId === this.salesRepId) || (obj.salesRepId === this.defaultSalesRepId))
    }
    // this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator
    this.getEvents(this.mainDataSource)
  }

  async clear() {
    this.searchText = null;
    // this.getCalenderEvent();
    if (this.getCompanyDetails) {
      await this.getCompanyDetail();
    } else if (this.contactDetails) {
      await this.getContactDetail()
    } else {
      this.getCalenderEvent()
    }
  }

  getCalenderEvent() {
    this.showProgressBar = true;

    let param: any = {}
    if (this.userDetails && this.userDetails.isAdmin) {
      param.subsidiaryId = this.userDetails.subsidiaryId;
      param.salesRepId = this.userDetails.salesRepId;
    }
    this.common
      .GetMethod(Apiurl.getAllActivties, param, true, 'Loading..')
      .then((res: any) => {
        if (res) {
          if (this.CompanyId && this.getCompanyDetails) {
            this.mainDataSource = [];
            this.dataSource = [];
            if (this.getCompanyDetails.activities) {
              this.getCompanyDetails.activities.map((activity: any) => {
                if (activity.RelatedCustomerId) {
                  activity.relatedCustomer = this.getCustomerName(activity.RelatedCustomerId);
                }
                if (activity.RelatedContactId) {
                  activity.relatedContact = this.getContactName(activity.RelatedContactId);
                }
                if ((!activity.RelatedTransaction || activity.RelatedTransaction == '') && activity.RelatedTransactionId) {
                  if (activity.transactionType.toLowerCase().includes('opp')) {
                    activity.relatedTransaction = 'OPP#' + activity.RelatedTransactionId;
                  }
                  if (activity.transactionType.toLowerCase().includes('quote')) {
                    activity.relatedTransaction = 'ESTBG#' + activity.RelatedTransactionId;
                  }
                }
              })
              this.setStartAndEndTime(this.getCompanyDetails.activities);
              this.actList = JSON.parse(JSON.stringify(this.getCompanyDetails.activities));
              this.duplicateDataSource = JSON.parse(JSON.stringify(this.actList));
              this.mainDataSource = this.getCompanyDetails.activities.splice(0, this.pageLength);
              this.dataSource = new MatTableDataSource(this.mainDataSource);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
              this.getEvents(this.mainDataSource)
              // if (this.defaultSalesRepId) {
              //   this.filterTypeChange(null)
              // }
            }
          }
          else if (this.ContactId && this.contactDetails) {
            this.mainDataSource = [];
            this.dataSource = [];
            if (this.contactDetails.activities) {
              this.contactDetails.activities.map((activity: any) => {
                if ((!activity.relatedTransaction || activity.relatedTransaction == '') && activity.relatedTransactionId) {
                  if (activity.transactionType.toLowerCase().includes('opp')) {
                    activity.relatedTransaction = 'OPP#' + activity.relatedTransactionId;
                  }
                  if (activity.transactionType.toLowerCase().includes('quote')) {
                    activity.relatedTransaction = 'ESTBG#' + activity.relatedTransactionId;
                  }
                }
              })
              this.setStartAndEndTime(this.contactDetails.activities);
              this.actList = JSON.parse(JSON.stringify(this.contactDetails.activities))
              this.duplicateDataSource = JSON.parse(JSON.stringify(this.actList));
              this.mainDataSource = this.contactDetails.activities.splice(0, this.pageLength);
              this.dataSource = new MatTableDataSource(this.mainDataSource);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
              this.getEvents(this.mainDataSource)
              // if (this.defaultSalesRepId) {
              //   this.filterTypeChange(null)
              // }
            }
          }
          else {
            this.mainDataSource = [];
            this.dataSource = [];
            res.map((activity: any) => {
              if ((!activity.relatedTransaction || activity.relatedTransaction == '') && activity.relatedTransactionId) {
                if (activity.transactionType.toLowerCase().includes('opp')) {
                  activity.relatedTransaction = 'OPP#' + activity.relatedTransactionId;
                }
                if (activity.transactionType.toLowerCase().includes('quote')) {
                  activity.relatedTransaction = 'ESTBG#' + activity.relatedTransactionId;
                }
              }
            })
            this.setStartAndEndTime(res);
            this.actList = JSON.parse(JSON.stringify(res))
            this.duplicateDataSource = JSON.parse(JSON.stringify(res));
            this.mainDataSource = res.splice(0, this.pageLength);
            this.dataSource = new MatTableDataSource(this.mainDataSource);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.getEvents(this.mainDataSource)
            // if (this.defaultSalesRepId) {
            //   this.filterTypeChange(null)
            // }
          }
        }
        this.showProgressBar = false;
      });
  }
  setStartAndEndTime(activities: Array<Object>) {
    if (activities && activities.length) {
      activities.map((activity: any) => {
        activity.startTime = activity.startTime.split('+')[0]
        activity.endTime = activity.endTime.split('+')[0]
        if (moment(activity.startTime).format("hh:mm:ss a") === "12:00:00 am" && moment(activity.endTime).format("hh:mm:ss a") === "12:00:00 am") {
          activity.startTime = null;
          activity.endTime = null;
        }
      })
    }
  }
  getEvents(resArray: any) {

    this.events = [];
    if (!resArray && !resArray.length) {
      this.showProgressBar = false;
    }
    resArray.map((event: any) => {
      let end: any;
      if (
        new Date(event.endTime).setHours(0, 0, 0, 0) ==
        new Date().setHours(0, 0, 0, 0)
      ) {
        end = null;
      } else {
        end = new Date(event.endTime);
      }
      let selectedColor = null;
      if ((event.type.toLowerCase()).includes('Phone') || (event.type.toLowerCase()).includes('phone')) {
        selectedColor = colors.orange
      }
      else if ((event.type.toLowerCase()).includes('event')) {
        selectedColor = colors.green
      }
      else if ((event.type.toLowerCase()).includes('task')) {
        selectedColor = colors.blue
      }
      if (event.startTime && event.endTime) {
        this.events.push({
          start: new Date(event.startTime),
          end: new Date(event.endTime),
          id: event.id,
          title: event.title,
          color: selectedColor,
          actions: this.actions,
          resizable: {
            beforeStart: true,
            afterEnd: true
          },
          draggable: true,
          type: event.type.toLowerCase(),
        });
      }
    })
    this.showProgressBar = false;
  }
  async handleEvent(action: string, event: CalendarEvent) {
    this.modalData = { event, action };
    var response = _.filter(this.mainDataSource, { id: this.modalData.event.id })

    if (action == 'View') {
      this.ViewFlag = true;
      this.headerTitle = 'View Activity'
      response[0].for = 'View'
      if ((response[0].relatedCustomerId && response[0].relatedCustomerId != 0) || (response[0].RelatedCustomerId && response[0].RelatedCustomerId != 0)) {
        let customerId = response[0].relatedCustomerId ? response[0].relatedCustomerId : response[0].RelatedCustomerId
        await this.getCompanyDetailById(customerId, response[0], 0, true)
      } else {
        await this.editSetData(response[0])
      }
    }
    else if (action == 'Edited') {
      this.ViewFlag = false;
      this.headerTitle = 'Edit Activity'
      response[0].for = 'Edit'
      if ((response[0].relatedCustomerId && response[0].relatedCustomerId != 0) || (response[0].RelatedCustomerId && response[0].RelatedCustomerId != 0)) {
        let customerId = response[0].relatedCustomerId ? response[0].relatedCustomerId : response[0].RelatedCustomerId
        await this.getCompanyDetailById(customerId, response[0], 2, true)
      } else {
        await this.editSetData(response[0])
      }
    }
  }


  onPositionChange(position: SpeedDialFabPosition) {
    switch (position) {
      case SpeedDialFabPosition.Bottom:
        this.speedDialFabPositionClassName = 'speed-dial-container-bottom';
        this.speedDialFabColumnDirection = 'column-reverse';
        break;
      default:
        this.speedDialFabPositionClassName = 'speed-dial-container-top';
        this.speedDialFabColumnDirection = 'column';
    }
  }

  onSpeedDialFabClicked(btn: any) {
    this.gotoActivities(btn.tooltip)
  }

  async gotoActivities(whereToGo: any) {
    let from = null;
    if (this.from == 'Activity') {
      from = 'activities'
    }
    else if (this.from == 'Dashboard') {
      from = 'dashboard'
    }
    else if (this.from == 'Company') {
      from = 'company-detail'
    }
    else if (this.from == 'Contact') {
      from = 'contact'
    }

    let param: NavigationExtras = {
      state: {
        type: whereToGo,
        id: this.CompanyId,
        contact_id: this.ContactId,
        from: from
      }
    };

    // Set conditon isAdmin and is salesrepid matches
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    this.userDetail = JSON.parse(this.userDetailsStr);
    var salesRepId: any;
    if (this.getCompanySalesRepId) {
      salesRepId = this.getCompanySalesRepId
    } else {
      salesRepId = this.salesRepIdContact;
    }
    if (this.CompanyId && salesRepId !== this.userDetail.salesRepId && !this.userDetail.isAdmin) {
      let obj = {
        title: "Error",
        text: "Activity SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok',

      };
      await this.commonService.showErrorMsg(obj).
        then((resp: any) => { });
    } else {
      this.router.navigate(['sub-activity'], param);
    }
  }

  viewFilterField() {
    this.showFilter = !this.showFilter;
  }

  addEvent(): void {
    this.events = [
      ...this.events,
      {
        title: 'New event',
        start: new Date(),
        end: new Date(),
        color: colors.purpule,
        // 
        // resizable: {
        //   beforeStart: true,
        //   afterEnd: true
        // }
      }
    ];
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter((event: any) => event !== eventToDelete);
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map((iEvent: any) => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    this.mainDataSource = this.mainDataSource.map((iEvent: any) => {
      if (iEvent.id === event.id) {
        return {
          ...iEvent,
          startTime: newStart,
          endTime: newEnd,
          endDate: newEnd,
          startDate: newStart
        };
      }
      return iEvent;
    });
    let data = _.findWhere(this.mainDataSource, { id: event.id })

    let participantIds: any = []
    data.participantIds.split(",").forEach((element: any) => {
      element = parseInt(element)
      if (element && element != 0 && element != '') {
        participantIds.push(element)
      }
    })
    let relatedCustomerId: any = null;
    let relatedTransactionId: any = null;
    let relatedContactId: any = null;

    if (this.from == 'Contact') {
      if (data.companyId != 0) {
        relatedCustomerId = data.companyId
      } else {
        relatedCustomerId = data.relatedCustomerId
      }
      if (data.relatedTransactionId != 0) {
        relatedTransactionId = data.transactionId
      } else {
        relatedTransactionId = data.relatedTransactionId
      }
      if (data.relatedContactId != 0) {
        relatedContactId = data.contactId
      } else {
        relatedContactId = data.relatedContactId
      }
    } else {
      relatedCustomerId = data.relatedCustomerId || data.RelatedCustomerId
      relatedTransactionId = data.relatedTransactionId || data.RelatedTransactionId
      relatedContactId = data.relatedContactId || data.RelatedContactId
    }

    if (data) {
      let obj = {
        "id": data.id,
        "type": data.type,
        "title": data.title,
        "salesRepId": this.userDetails.salesRepId,
        "startDate": data.startDate,
        "endDate": data.endDate,
        "startTime": data.startTime,
        "endTime": data.endTime,
        "relatedCustomerId": relatedCustomerId,
        "relatedContactId": relatedContactId,
        "relatedTransactionId": relatedTransactionId,
        "message": data.message,
        "phone": data.phone,
        "statusId": data.statusId,
        "transactionType": "",
        "participantIds": participantIds,
        "netSuiteId": this.netsuiteId,
        "accountId": 0,
        "createBy": "",
        "createDate": new Date(),
        "updatedBy": "",
        "updatedDate": new Date(),
        "dateCreated": new Date(),
      }

      if (obj) {
        this.common
          .PutMethod(
            Apiurl.getAllActivties + '/' + event.id,
            obj,
            true,
            'Loading..',
            null
          )
          .then((resp: any) => {
            if (resp) {
              if (this.CompanyId) {
                this.getCompanyDetail()
              } else if (this.ContactId) {
                this.getContactDetail()
              } else {
                this.getCalenderEvent()
              }
            }
          })
      }
    }
    // this.handleEvent('Dropped or resized', event);
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
    this.showFilter = false;
    this.listParticipants = [];
  }
  //scheduler end


  clearSearch() {
    if (this.salesRepId || this.statusType || this.eventType) {
      this.filterType = null;
      this.statusType = null;
      this.eventType = null;
      this.statusType = null;
      this.salesRepId = null;
      this.relatedCompany = null;
      this.relatedTransaction = null;
      this.relatedContact = null;
      // this.dataSource = new MatTableDataSource(this.mainDataSource);
      // this.dataSource.sort = this.sort;
      // this.dataSource.paginator = this.paginator
      this.relatedContactList = [];
      this.mainDataSource = JSON.parse(JSON.stringify(this.duplicateDataSource))
      this.getEvents(this.mainDataSource)
    }
  }


  //clear individual filter
  clearIndividualFilter(event: any, filtername: any) {
    switch (filtername) {
      case 'eventType':
        this.eventType = null;
        event.stopPropagation();
        this.filterTypeChange(null)
        break;
      case 'statusType':
        this.statusType = null;
        event.stopPropagation();
        this.filterTypeChange(null)
        break;
      case 'salesRep':
        this.salesRepId = null;
        event.stopPropagation();
        this.filterTypeChange(null)
        break;
      default:
        break;
    }
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator
  }
  dayClicked(action: any, { date, events }: { date: Date; events: CalendarEvent[] }, event: any): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
    if (event.sourceEvent.target.id == 'eventdivparent') {
      this.activeBtn = null;
      this.headerTitle = 'Add Activity'
      if (action == 'add-Activity') {
        this.close()
        this.selectedDate = date;
        let obj = {
          tooltip: 'Task'
        }
        if (this.subActivityForm) {
          this.subActivityForm.reset()
          this.subActivityForm.controls['startDate'].setValue(this.selectedDate);
          this.subActivityForm.controls['startDate'].updateValueAndValidity()
          this.subActivityForm.controls['endDate'].setValue(this.selectedDate);
          this.subActivityForm.controls['endDate'].updateValueAndValidity()
          if (this.CompanyId) {
            let obj = {
              companyId: this.CompanyId
            }
            this.setCompany(obj)
          }
        }
        this.onClickFab(obj, true)
        this.editActivity(false)
        if (this.from == 'Activity') {
          let param: NavigationExtras = {
            state: {
              type: 'Task',
              activity_id: null,
              from: 'activities',
              for: 'add',
              selectedDate: this.selectedDate,
              selected: 'date'
            }
          };
          this.router.navigate(['sub-activity'], param);
        } else {
          this.modal.open(this.modalContent, { size: 'lg' });
        }
      }
    }
    if (event.sourceEvent.target.id.includes('edit_')) {
      let id = parseInt(event.sourceEvent.target.id.split('_')[1])
      if (id) {
        let obj = _.findWhere(events, { id: id })
        if (obj) {
          this.handleEvent('Edited', obj);
        }
      }
    }
    if (event.sourceEvent.target.id.includes('view_')) {
      let id = parseInt(event.sourceEvent.target.id.split('_')[1])
      if (id) {
        let obj = _.findWhere(events, { id: id })
        if (obj) {
          this.handleEvent('View', obj);
        }
      }
    }
  }

  showdetails(value: any) {
    if (!value) { return }
    if (this.from === 'Activity') {
      let param: NavigationExtras = {
        state: {
          type: value.type,
          activity_id: value.id,
          from: 'activities'
        }
      };
      this.router.navigate(['/activity-details'], param);
    }
    else {
      this.handleEvent('View', value);
    }
  }

  //add activity
  showDetailRecord() {
    this.showDetailFields = true;
  }
  close() {
    this.showDetailFields = false;
    this.showForm = false;
    this.selectedDate = new Date()
    this.listContacts = [];
    this.listContactIds = [];
    this.listParticipants = [];
    this.listMainContacts = []
  }
  hideDetailRecord() {
    this.showDetailFields = false;
  }

  searchCompany(event: any) {
    if (event.target.value != '') {
      this.companyListSearch = this.relatedCompanyList.filter(
        (s: any) =>
          s.companyName
            .toLowerCase()
            .indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.companyListSearch = this.relatedCompanyList;
    }
  }
  setCompany(obj: any) {
    this.subActivityForm.controls['companyId'].setValue(obj.companyId);
    let data = _.findWhere(this.relatedCompanyList, { id: obj.companyId });
    if (data) {
      this.subActivityForm.controls['salesRepId'].setValue(data.salesRepId);
    }
    this.getCompanyDetailById(obj.companyId, null, 1, false)
  }

  getCompanyDetailById(id: any, obj: any = null, newAdd: number = 0, openPopUp: boolean = false) {
    this.showProgressBar = true;
    this.listTransactions = []
    this.SpinnerService.show()
    this.common
      .GetMethod(Apiurl.getAllCompanies + '/' + id, null, true, 'Loading')
      .then((res: any) => {
        if (res) {
          this.SpinnerService.hide()
          if (res.opportunities && res.opportunities.length != 0) {
            if (res.opportunities.length == 1) {
              res.opportunities[0].type = 'Opportunities' + '-' + res.opportunities[0].id;
              if (res.opportunities[0].documentNumber != '' && res.opportunities[0].documentNumber != null) {
                res.opportunities[0].typeval = res.opportunities[0].documentNumber + ' - ' + 'Opportunities';
              } else {
                res.opportunities[0].typeval = 'OPP' + res.opportunities[0].id + ' - ' + 'Opportunities';
              }
              this.listTransactions.push(res.opportunities[0]);
              if (this.subActivityForm) {
                this.subActivityForm.controls['relatedTransactionId'].setValue(res.opportunities[0].id)
              }
            } else {
              for (let i = 0; i < res.opportunities.length; i++) {
                res.opportunities[i].type = 'Opportunities' + '-' + res.opportunities[i].id;
                if (res.opportunities[i].documentNumber != '' && res.opportunities[i].documentNumber != null) {
                  res.opportunities[i].typeval = res.opportunities[i].documentNumber + ' - ' + 'Opportunities';
                } else {
                  res.opportunities[i].typeval = 'OPP' + res.opportunities[i].id + ' - ' + 'Opportunities';
                }
                this.listTransactions.push(res.opportunities[i]);
              }
            }
          }
          if (res.quotes && res.quotes.length != 0) {
            if (res.quotes.length == 1) {
              res.quotes[0].type = 'Quotes' + '-' + res.quotes[0].id;
              if (res.quotes[0].documentNumber != '' && res.quotes[0].documentNumber != null) {
                res.quotes[0].typeval = res.quotes[0].documentNumber + ' - ' + 'Quotes';
              } else {
                res.quotes[0].typeval = 'QUOTE' + res.quotes[0].id + ' - ' + 'Quotes';
              }
              this.listTransactions.push(res.quotes[0]);
              if (this.subActivityForm) {
                this.subActivityForm.controls['relatedTransactionId'].setValue(res.quotes[0].id)
              }
            } else {
              for (let i = 0; i < res.quotes.length; i++) {
                res.quotes[i].type = 'Quotes' + '-' + res.quotes[i].id;
                if (res.quotes[i].documentNumber != '' && res.quotes[i].documentNumber != null) {
                  res.quotes[i].typeval = res.quotes[i].documentNumber + ' - ' + 'Quotes';
                } else {
                  res.quotes[i].typeval = 'QUOTE' + res.quotes[i].id + ' - ' + 'Quotes';
                }
                this.listTransactions.push(res.quotes[i]);
              }
            }
          }
          if (this.listTransactions.length != 0) {
            if (history.state.qoute_id) {
              let data = _.findWhere(this.listTransactions, { id: history.state.qoute_id })

              if (data) {
                this.selectedTransectionType(data)
              }
            }
          }
          this.listParticipants = [];
          this.listContacts = [];
          this.listMainContacts = []
          if (res.relatedContacts && res.relatedContacts.length != 0) {
            if (res.relatedContacts.length == 1) {
              res.relatedContacts[0].isPrimary = false;
              if (res.relatedContacts[0].role == 'Primary Contact') {
                res.relatedContacts[0].isPrimary = true;
              }
              this.listContacts.push(res.relatedContacts[0])
              this.listMainContacts.push(res.relatedContacts[0])

              this.listParticipants.push(res.relatedContacts[0].id);
              if (this.subActivityForm) {
                this.subActivityForm.controls['contact'].setValue(res.relatedContacts[0].id);
              }
            } else {

              if (this.ContactId) {
                for (let i = 0; i < res.relatedContacts.length; i++) {
                  if (this.ContactId == res.relatedContacts[i].id) {
                    if (this.subActivityForm) {
                      this.subActivityForm.controls['contact'].setValue(res.relatedContacts[i].id);
                      this.subActivityForm.controls['contact'].disable()
                    }
                  }
                }
              }
              if (newAdd == 1) {
                for (let i = 0; i < res.relatedContacts.length; i++) {
                  res.relatedContacts[i].isPrimary = false;
                  if (res.relatedContacts[i].role == 'Primary Contact') {
                    res.relatedContacts[i].isPrimary = true;
                    if (this.subActivityForm) {
                      this.subActivityForm.controls['contact'].setValue(res.relatedContacts[i].id);
                    }
                    this.listParticipants.push(res.relatedContacts[i].id);
                  }
                  this.listContacts.push(res.relatedContacts[i])
                  this.listMainContacts.push(res.relatedContacts[i])

                }
              } else if (newAdd == 0) {
                res.relatedContacts.forEach((element1: any) => {
                  element1.isPrimary = false;
                  if (obj.participantIds) {
                    obj.participantIds.split(",").forEach((element: any) => {
                      element = parseInt(element)
                      if (element1.id == element) {
                        element1.isPrimary = true;
                        this.listParticipants.push(element1.id);
                        this.listContacts.push(element1)
                      }
                    });
                  }
                  this.listMainContacts.push(element1)
                });
              } else if (newAdd == 2) {
                res.relatedContacts.forEach((element1: any) => {
                  element1.isPrimary = false;
                  if (obj.participantIds) {
                    obj.participantIds.split(",").forEach((element: any) => {
                      element = parseInt(element)
                      if (element1.id == element) {
                        element1.isPrimary = true;
                        this.listParticipants.push(element1.id);
                      }
                    });
                  }
                  this.listContacts.push(element1)
                  this.listMainContacts.push(element1)
                });
              }
            }

          }
          if (this.subActivityForm) {
            this.subActivityForm.controls['salesRepId'].setValue(res.salesRepId)
          }
          if (obj && openPopUp) {
            this.editSetData(obj, true)
          }
          if (newAdd == 1) {
            if (this.CompanyId && this.subActivityForm) {
              this.subActivityForm.controls['companyId'].disable()
            }
          }
        }
        this.showProgressBar = false;
      })
  }

  editSetData(obj: any, companyGet: boolean = false) {

    let relatedCustomerId: any = null;
    let relatedTransactionId: any = null;
    let relatedContactId: any = null;
    this.netsuiteId = obj.netSuiteId || 0

    if (this.from == 'Contact') {
      if (obj.companyId && obj.companyId != 0) {
        relatedCustomerId = obj.companyId
      } else {
        relatedCustomerId = obj.relatedCustomerId
      }
      if ((obj.relatedTransactionId && obj.relatedTransactionId != 0) || (obj.RelatedTransactionId && obj.RelatedTransactionId != 0)) {
        relatedTransactionId = obj.transactionId
      } else {
        relatedTransactionId = obj.relatedTransactionId ? obj.relatedTransactionId : obj.RelatedTransactionId
      }
      if (obj.relatedContactId && obj.relatedContactId != 0) {
        relatedContactId = obj.contactId
      } else {
        relatedContactId = obj.relatedContactId ? obj.relatedContactId : obj.RelatedContactId
      }
    } else {
      relatedCustomerId = obj.relatedCustomerId ? obj.relatedCustomerId : obj.RelatedCustomerId
      relatedTransactionId = obj.relatedTransactionId ? obj.relatedTransactionId : obj.RelatedTransactionId
      relatedContactId = obj.relatedContactId ? obj.relatedContactId : obj.RelatedContactId
    }

    this.companyListSearch = [];
    this.relatedCompanyList = [];
    this.companyListSearch = this.listLookups.company.companyList
    this.relatedCompanyList = this.companyListSearch
    if (this.subActivityForm) {
      this.subActivityForm.reset()
      this.subActivityForm.controls['id'].setValue(obj.id)
      this.subActivityForm.controls['companyId'].setValue(relatedCustomerId)
      this.subActivityForm.controls['relatedTransactionId'].setValue(relatedTransactionId)
      this.subActivityForm.controls['salesRepId'].setValue(obj.salesRepId)
      this.subActivityForm.controls['contact'].setValue(relatedContactId)
      this.subActivityForm.controls['title'].setValue(obj.title)
      this.subActivityForm.controls['description'].setValue(obj.message)
      this.subActivityForm.controls['phoneNumber'].setValue(obj.phone)
      this.subActivityForm.controls['transactionType'].setValue(obj.transactionType)
      this.subActivityForm.controls['statusId'].setValue(obj.statusId)
      this.subActivityForm.controls['startDate'].setValue(new Date(obj.startDate))
      this.subActivityForm.controls['endDate'].setValue(new Date(obj.endDate))
      this.subActivityForm.controls['startTime'].setValue(new Date(obj.startTime))
      this.subActivityForm.controls['endTime'].setValue(new Date(obj.endTime))
      this.subActivityForm.controls['id'].enable()
      this.subActivityForm.controls['companyId'].enable()
      this.subActivityForm.controls['relatedTransactionId'].enable()
      this.subActivityForm.controls['salesRepId'].enable()
      this.subActivityForm.controls['contact'].enable()
      this.subActivityForm.controls['title'].enable()
      this.subActivityForm.controls['description'].enable()
      this.subActivityForm.controls['phoneNumber'].enable()
      this.subActivityForm.controls['transactionType'].enable()
      this.subActivityForm.controls['statusId'].enable()
      this.subActivityForm.controls['startDate'].enable()
      this.subActivityForm.controls['endDate'].enable()
      this.subActivityForm.controls['startTime'].enable()
      this.subActivityForm.controls['endTime'].enable()
    }
    if (obj.for == 'Edit') {
      if (obj.type == 'Call') {
        obj.type = 'Phone'
      }
      let newobj = {
        tooltip: obj.type,
      }
      this.onClickFab(newobj, false)
      if (this.from == 'Activity') {
        let param: NavigationExtras = {
          state: {
            type: obj.type,
            activity_id: obj.id,
            from: 'activities',
          }
        };
        this.router.navigate(['sub-activity'], param);
      } else {
        this.modal.open(this.modalContent, { size: 'lg' });
      }

    }
    else if (obj.for == 'View') {
      if (this.subActivityForm) {
        this.subActivityForm.controls['id'].disable()
        this.subActivityForm.controls['companyId'].disable()
        this.subActivityForm.controls['relatedTransactionId'].disable()
        this.subActivityForm.controls['salesRepId'].disable()
        this.subActivityForm.controls['contact'].disable()
        this.subActivityForm.controls['title'].disable()
        this.subActivityForm.controls['description'].disable()
        this.subActivityForm.controls['phoneNumber'].disable()
        this.subActivityForm.controls['transactionType'].disable()
        this.subActivityForm.controls['statusId'].disable()
        this.subActivityForm.controls['startDate'].disable()
        this.subActivityForm.controls['endDate'].disable()
        this.subActivityForm.controls['startTime'].disable()
        this.subActivityForm.controls['endTime'].disable()
      }
      if (obj.type == 'Call') {
        obj.type = 'Phone'
      }
      let newobj = {
        tooltip: obj.type,
      }
      this.onClickFab(newobj, false)
      if (this.from == 'Activity') {
        let param: NavigationExtras = {
          state: {
            type: obj.type,
            activity_id: obj.id,
            from: 'activities',
          }
        };
        this.router.navigate(['activity-details'], param);
      } else {
        this.modal.open(this.modalContent, { size: 'lg' });
      }
    }
  }
  clearCompanyDetails() {
    this.searchdropdown = '';
    this.companyListSearch = this.relatedCompanyList;
  }

  SelectParticipant(contactInfo: any) {
    contactInfo.isPrimary = !contactInfo.isPrimary
  }
  // add activity close

  //activity add
  onClickFab(event: any, isReset = false) {
    if (this.subActivityForm) {
      this.subActivityForm.controls['phoneNumber'].setValidators([])
      this.subActivityForm.controls['phoneNumber'].updateValueAndValidity()
    }

    const endTime = new Date(this.selectedDate);
    const startTime = new Date(this.selectedDate);

    startTime.setHours(new Date().getHours())
    startTime.setMinutes(new Date().getMinutes());
    startTime.setSeconds(new Date().getSeconds());

    endTime.setHours(new Date().getHours() + 1);
    endTime.setMinutes(new Date().getMinutes());
    endTime.setSeconds(new Date().getSeconds());
    if (isReset) {
      if (this.subActivityForm) {
        this.subActivityForm.controls['id'].setValue(0)
        this.subActivityForm.controls['startDate'].setValue(new Date(this.selectedDate))
        this.subActivityForm.controls['endDate'].setValue(new Date(this.selectedDate))
        this.subActivityForm.controls['startTime'].setValue(startTime)
        this.subActivityForm.controls['endTime'].setValue(endTime)
        this.subActivityForm.controls['salesRepId'].setValue(0)
        this.subActivityForm.controls['statusId'].setValue(2)
        this.subActivityForm.controls['phoneNumber'].setValue('')
        this.subActivityForm.controls['transactionType'].setValue('')
      }
    }
    this.activeBtn = event.tooltip
    if (event.tooltip == "Task" || event.tooltip == "Tasks") {
      this.showForm = true;
      this.selectedActivityType = "Task"
    } else if (event.tooltip == "Event" || event.tooltip == "Events") {
      this.showForm = true;
      this.selectedActivityType = "Event"
    }
    else if (event.tooltip == "Phone") {
      this.showForm = true;
      this.selectedActivityType = "Phone"
      if (this.subActivityForm) {
        this.subActivityForm.controls['phoneNumber'].setValidators(Validators.required)
        this.subActivityForm.controls['phoneNumber'].updateValueAndValidity()
      }
    }
  }

  datechange(event: any, from: any) {
    if (from == 'startDate') {
      this.subActivityForm.controls['startDate'].setValue(event._d)
      if (this.headerTitle == 'Edit Activity') {
        this.subActivityForm.controls['startTime'].setValue(this.setEndTime(this.subActivityForm.controls['startDate'].value, this.subActivityForm.controls['startTime'].value))
        this.subActivityForm.controls['endTime'].setValue(this.setEndTimeOnePlus(this.subActivityForm.controls['startDate'].value, this.subActivityForm.controls['endTime'].value))
      } else {
        this.subActivityForm.controls['startTime'].setValue(this.setEndTime(this.subActivityForm.controls['startDate'].value, this.subActivityForm.controls['startTime'].value))
        this.subActivityForm.controls['endTime'].setValue(this.setEndTimeOnePlus(this.subActivityForm.controls['startDate'].value, this.subActivityForm.controls['startTime'].value))
      }
    }
    else if (from == 'endDate') {
      this.subActivityForm.controls['endDate'].setValue(event._d)
      this.subActivityForm.controls['endTime'].setValue(this.setEndTime(this.subActivityForm.controls['endDate'].value, this.subActivityForm.controls['endTime'].value))
      if (new Date(this.subActivityForm.controls['startDate'].value).setHours(0, 0, 0, 0) > new Date(this.subActivityForm.controls['endDate'].value).setHours(0, 0, 0, 0)) {
        this.endDateError = true;
      } else {
        this.endDateError = false;
      }
    }
  }

  timeChange(from: any) {
    if (from == 'endTime') {
      if (new Date(this.subActivityForm.controls['startTime'].value.getTime()) > new Date(this.subActivityForm.controls['endTime'].value.getTime())) {
        this.endTimeError = true;
      } else {
        this.endTimeError = false;
      }
    }
  }

  addActivity() {
    this.listParticipants = [];
    this.SpinnerService.show()

    for (let i = 0; i < this.listContacts.length; i++) {
      if (this.listContacts[i].isPrimary == true) {
        this.listParticipants.push(this.listContacts[i].id);
      }
    }
    if (this.subActivityForm.controls['id'].value == null) {
      this.subActivityForm.controls['id'].setValue(0)
    }

    if (new Date(this.subActivityForm.controls['startDate'].value).setHours(0, 0, 0, 0) > new Date(this.subActivityForm.controls['endDate'].value).setHours(0, 0, 0, 0)) {
      this.endDateError = true;
      return false;
    } else if (new Date(this.subActivityForm.controls['startTime'].value.getTime()) > new Date(this.subActivityForm.controls['endTime'].value.getTime())) {
      this.endTimeError = true;
      return false;
    } else {
      var params = {
        // salesRepId: this.subActivityForm.get('salesRepId')?.value ? this.subActivityForm.get('salesRepId')?.value : 0,
        salesRepId: this.userDetails.salesRepId,
        type: this.selectedActivityType,
        title: this.subActivityForm.get('title')?.value,
        startDate: this.subActivityForm.get('startDate')?.value,
        startTime: this.subActivityForm.get('startTime')?.value,
        endTime: this.subActivityForm.get('endTime')?.value,
        relatedCustomerId: this.subActivityForm.get('companyId')?.value
          ? this.subActivityForm.get('companyId')?.value
          : 0,
        relatedContactId: this.subActivityForm.get('contact')?.value
          ? this.subActivityForm.get('contact')?.value
          : 0,
        relatedTransactionId:
          this.subActivityForm.get('relatedTransactionId')?.value
            ? this.subActivityForm.get('relatedTransactionId')?.value
            : 0,
        message: this.subActivityForm.get('description')?.value,
        statusId: this.subActivityForm.get('statusId')?.value
          ? this.subActivityForm.get('statusId')?.value
          : 2,
        transactionType: this.subActivityForm.get('transactionType')?.value,
        participantIds: this.listParticipants,
        netSuiteId: this.netsuiteId,
        accountId: 0,
        createBy: '',
        createDate: new Date(),
        updatedBy: '',
        updatedDate: new Date(),
        dateCreated: new Date(),
        endDate: this.subActivityForm.get('endDate')?.value,
        phone: this.subActivityForm.get('phoneNumber')?.value ? this.subActivityForm.get('phoneNumber')?.value : "",
        id: this.subActivityForm.controls['id'].value ? this.subActivityForm.controls['id'].value : 0
      };
      if (this.subActivityForm.controls['id'].value == 0) {
        this.common
          .PostMethod(Apiurl.saveActivity, params, false, 'Loading.', null)
          .then(async (responsedata: any) => {
            this.SpinnerService.hide();
            if (responsedata > 0) {
              // Swal.fire('Success', 'Activity Saved successfully', 'success').then(
              //   async () => {
              this.common.showSuccess('Activity Saved successfully');
              this.close();
              this.modal.dismissAll();
              this.clear()
              if (this.getCompanyDetails) {
                this.getCompanyDetail();
              }
              if (this.contactDetails) {
                await this.getContactDetail()
              }
              this.getCalenderEvent();
              //   }
              // );
            } else {
              this.common.showError('Failed to Save Activity');
              // Swal.fire(
              //   'Error',
              //   'Failed to Save Activity',
              //   'error'
              // ).then(() => { });
            }
          });
      } else {
        this.common
          .PutMethod(
            Apiurl.getAllActivties + '/' + this.subActivityForm.get('id')?.value,
            params,
            true,
            'Loading..',
            null
          )
          .then(async (resp: any) => {
            if (resp) {
              this.SpinnerService.hide();
              this.common.showSuccess('Activity Updated successfully');
              // Swal.fire('Success', 'Activity Updated successfully', 'success').then(
              //   async () => {
              this.close();
              this.modal.dismissAll();
              this.clear()
            } else {
              this.common.showError('Failed to Update Activity');
              // Swal.fire(
              //   'Error',
              //   'Failed to Update Activity',
              //   'error'
              // ).then(() => { });
            }
          })
      }
      return true;
    }
  }

  editActivity(IsEdit: boolean) {
    if (this.subActivityForm) {
      this.subActivityForm.controls['id'].enable()
      this.subActivityForm.controls['companyId'].enable()
      this.subActivityForm.controls['relatedTransactionId'].enable()
      this.subActivityForm.controls['salesRepId'].enable()
      this.subActivityForm.controls['contact'].enable()
      this.subActivityForm.controls['title'].enable()
      this.subActivityForm.controls['description'].enable()
      this.subActivityForm.controls['phoneNumber'].enable()
      this.subActivityForm.controls['transactionType'].enable()
      this.subActivityForm.controls['statusId'].enable()
      this.subActivityForm.controls['startDate'].enable()
      this.subActivityForm.controls['endDate'].enable()
      this.subActivityForm.controls['startTime'].enable()
      this.subActivityForm.controls['endTime'].enable()
    }
    if (IsEdit == true) {
      this.ViewFlag = false;
      this.headerTitle = 'Edit Activity';
      if (this.subActivityForm.controls['companyId'].value != 0) {
        var response = _.filter(this.mainDataSource, { id: this.subActivityForm.controls['id'].value })
        if (response.length != 0) {
          this.getCompanyDetailById(this.subActivityForm.controls['companyId'].value, response[0], 2, false)
        }
      }
    }
    //  else {
    //   
    //   if (this.CompanyId) {
    //     var response = _.filter(this.mainDataSource, { id: this.subActivityForm.controls['id'].value })
    //     if (response.length != 0) {
    //       this.subActivityForm.controls['companyId'].setValue(this.CompanyId)
    //       this.getCompanyDetailById(this.subActivityForm.controls['companyId'].value, response[0], 1, false)
    //     }
    //   }
    // }
  }

  //week view click
  dayHeaderClicked(event: any) {

  }

  hourSegmentClicked(event: any) {
    this.close()
    this.selectedDate = event;
    let obj = {
      tooltip: 'Task'
    }
    this.onClickFab(obj, true)
    this.editActivity(false)
    const endTime = new Date(event);
    endTime.setHours(event.getHours() + 1);
    endTime.setMinutes(event.getMinutes());
    endTime.setSeconds(event.getSeconds());
    if (this.subActivityForm) {
      this.subActivityForm.reset()
      this.subActivityForm.controls['startDate'].setValue(event);
      this.subActivityForm.controls['endDate'].setValue(new Date());
      this.subActivityForm.controls['startTime'].setValue(event);
      this.subActivityForm.controls['endTime'].setValue(endTime);
      if (this.CompanyId) {
        let obj = {
          companyId: this.CompanyId
        }
        this.headerTitle = 'Add Activity';
        this.setCompany(obj)
      }
      this.modal.open(this.modalContent, { size: 'lg' });
    } else {
      if (this.from == 'Activity') {
        let param: NavigationExtras = {
          state: {
            type: 'Task',
            activity_id: null,
            from: 'activities',
            for: 'add',
            selectedDate: event,
            selected: 'time'
          }
        };
        this.router.navigate(['sub-activity'], param);
      }
    }
  }

  setEndTime(startDate: any, startTime: any) {
    const endTime = new Date(startDate);
    endTime.setHours(startTime.getHours());
    endTime.setMinutes(startTime.getMinutes());
    endTime.setSeconds(startTime.getSeconds());
    return endTime;
  }
  setEndTimeOnePlus(startDate: any, startTime: any) {
    const endTime = new Date(startDate);
    endTime.setHours(startTime.getHours() + 1);
    endTime.setMinutes(startTime.getMinutes());
    endTime.setSeconds(startTime.getSeconds());
    return endTime;
  }

  getCompanyDetail() {
    let self = this;
    return new Promise(function (resolve, reject) {
      self.common
        .GetMethod(Apiurl.getAllCompanies + '/' + self.CompanyId, null, true, 'Loading')
        .then((response: any) => {
          if (response) {
            self.getCompanyDetails = response;
            self.getCalenderEvent()
            resolve(true);
          } else {
            reject(true);
          }
        })
    })

  }

  getContactDetail() {
    let self = this;
    return new Promise(function (resolve, reject) {
      self.common
        .GetMethod(Apiurl.getContactList + '/' + self.ContactId, null, true, 'Loading')
        .then((response: any) => {
          if (response) {
            self.contactDetails = response;
            self.getCalenderEvent()
            resolve(true);
          } else {
            reject(true);
          }
        })
    })


  }
  selectedTransectionType(obj: any) {

    if (obj) {
      this.subActivityForm.controls['relatedTransactionId'].setValue(obj.id);
      if (obj.typeval.toLowerCase().includes('opp')) {
        this.subActivityForm.controls['transactionType'].setValue('Opportunities');
      }
      if (obj.typeval.toLowerCase().includes('quote')) {
        this.subActivityForm.controls['transactionType'].setValue('Quotes');
      }
    }
  }

  getSalesRep(id: any) {
    let salesRep = _.findWhere(this.listLookups?.opportunity?.salesRep, { id: id })
    if (salesRep) {
      return salesRep.salesRepDescription;
    };
  }
  sortActivityList(sort: any) {
    this.sort = sort;
    this.mainDataSource = {
      data: orderBy(this.actList, this.sort),
      total: this.actList.length,
    };
    if (this.mainDataSource.data && this.mainDataSource.data.length) {
      this.mainDataSource = [...this.mainDataSource.data];
    }
  }
  public rowCallback(context: any) {
    return {
      dragging: context.dataItem.dragging
    };
  }
  public exportToExcel(grid: any): void {
    grid.saveAsExcel();
  }
  public onFilter(inputValue: string): void {
    this.mainDataSource = process(this.duplicateDataSource, {
      filter: {
        logic: "or",
        filters: [
          {
            field: 'type',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'title',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'status',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'salesRep',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'relatedTransaction',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'relatedContact',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'phone',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'relatedCustomer',
            operator: 'contains',
            value: inputValue
          }
        ],
      }
    });
    if (inputValue == '' || inputValue == null) {
      this.mainDataSource = process(this.duplicateDataSource, this.state)
    }
    if (this.mainDataSource?.data?.length) {
      this.mainDataSource = [...this.mainDataSource.data]
    }
    // this.dataBinding.skip = 0;
  }
  onChangePage(ev: PageEvent) {
    this.actList = JSON.parse(JSON.stringify(this.duplicateDataSource));
    this.pageIndex = ev.pageIndex + 1;
    localStorage.setItem('pageLength', ev.pageSize.toString())
    this.pageLength = ev.pageSize;
    this.mainDataSource = this.actList.splice((this.pageIndex - 1) * this.pageLength, this.pageLength);
  }
  refreshTable() {
    this.clearCompanyDetails()
    this.clearSearch();
    this.clear();
  }
  exportToCsv(): void {
    let exportObj = {
      'Type': null,
      'Title': null,
      'Company': null,
      'Start Date': null,
      'End Date': null,
      'Duration': '',
      'Sales Rep.': null,
      'Status': null,
      'Transaction': null,
      'Contact': null,
    }
    var exportData: any = [];
    this.mainDataSource.forEach((data: any) => {
      exportObj = {
        'Type': data.type,
        'Title': data.title,
        'Company': data.relatedCustomer,
        'Start Date': data.startDate,
        'End Date': data.endDate,
        'Duration': data.startTime ? data.startTime + '-' : '-' + data.endTime ? data.endTime : '',
        'Sales Rep.': data.salesRep,
        'Status': data.status,
        'Transaction': data.relatedTransaction && data.relatedTransactionId ? data.relatedTransaction : !data.relatedTransaction && data.relatedTransactionId && data.transactionType?.toLowerCase().includes('quote') ? 'QUO#' + data.relatedTransactionId : !data.relatedTransaction && data.relatedTransactionId && data.transactionType?.toLowerCase().includes('opp') ? 'OPP#' + data.relatedTransactionId : '-',
        'Contact': data.relatedContact,
      };
      exportData.push(exportObj)
    })
    // return
    this.exportService.exportToCsv(exportData, 'Activities');
  }
  setActivityStatus(id: any) {
    if (this.ViewFlag) {
      return
    }
    this.subActivityForm.controls['statusId'].setValue(+id);
    if (id == 2) {
      this.isSheduled = true;
      this.isCompleted = false;
    } else {
      this.isSheduled = false;
      this.isCompleted = true;
    }
  }
  getCustomerName(customerId: any) {
    if (this.listLookups && this.listLookups.company && this.listLookups.company.companyList && this.listLookups.company.companyList.length) {
      let relatedCustomer = this.listLookups.company.companyList.filter((customer: any) => customer.companyId === customerId);
      if (relatedCustomer && relatedCustomer.length) {
        return relatedCustomer[0].companyName;
      } else {
        return '';
      }
    }
  }
  getContactName(contactId: any) {
    if (this.listLookups && this.listLookups.contact && this.listLookups.contact.contactList && this.listLookups.contact.contactList.length) {
      let relatedcontact = this.listLookups.contact.contactList.filter((contact: any) => contact.contactId === contactId);
      if (relatedcontact && relatedcontact.length) {
        return relatedcontact[0].name;
      } else {
        return '';
      }
    }
  }
}
