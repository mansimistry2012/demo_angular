import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserManager, UserManagerSettings, User, WebStorageStateStore } from 'oidc-client';
import { BehaviorSubject } from 'rxjs';
import { AuthCoreUrl } from '../core.config';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // Observable navItem source
  private _authNavStatusSource = new BehaviorSubject<boolean>(false);
  // Observable navItem stream
  authNavStatus$ = this._authNavStatusSource.asObservable();
  userStore = new WebStorageStateStore({ store: window.localStorage })
  public manager = new UserManager(getClientSettings());
  private user: any;
  userInformation: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor(private http: HttpClient, private router: Router) {

    this.manager.getUser().then((user: any) => {
      this.user = user;
      this._authNavStatusSource.next(this.isAuthenticated());
    });

  }

  login() {
    return this.manager.signinRedirect();
  }

  getUserDetails() {
    // Call SignIn Method of OIDC-client to get the user login details
    this.manager.processSigninResponse().then((res) => {
      localStorage.setItem("loginResponse", JSON.stringify(res))
      console.log("Response", res)
      this.userInformation.next(res)
    })
    return this.user;
  }

  async completeAuthentication() {
    // Fetch url and set the data into the user
    var obj: any = {};
    if (this.router.url != "/login?redirect=%2Fdashboard") {
      var url = this.router.url
      url = url.replace("/auth-callback#", "")
      var pairs = url.split('&');
      for (var i in pairs) {
        var split = pairs[i].split('=');
        obj[decodeURIComponent(split[0])] = decodeURIComponent(split[1]);
      }
      if (obj) {
        this.user = obj;
        localStorage.setItem("LoginToken", obj.access_token);
      }
    }
  }

  isAuthenticated(): boolean {
    return true;
  }

  get authorizationHeaderValue(): string {
    if (this.user != null)
      return `${this.user.token_type} ${this.user.access_token}`;
    else
      return "";
  }

  get name(): any {

    return (this.user != null) ? this.user.profile.name : '';
  }

  async signout() {
    await this.manager.signoutRedirect().then((resp: any) => {
      this.router.navigate(['login']);
      setTimeout(() => {
        this.router.navigate([''])
        setTimeout(() => {
          window.location.reload();
        }, 100);
      }, 1500);
    });
  }
}

export function getClientSettings(): UserManagerSettings {
  return {

    authority: AuthCoreUrl.Authority,
    client_id: AuthCoreUrl.ClientId,
    redirect_uri: AuthCoreUrl.RedirectUri,
    post_logout_redirect_uri: AuthCoreUrl.Post_Logout_Redirect_Uri,
    response_type: AuthCoreUrl.ResponseType,
    scope: AuthCoreUrl.Scope,
    filterProtocolClaims: AuthCoreUrl.FilterProtocolClaims,
    loadUserInfo: AuthCoreUrl.LoadUserInfo,
    automaticSilentRenew: AuthCoreUrl.AutomaticSilentRenew,
    silent_redirect_uri: AuthCoreUrl.Silent_Redirect_Uri
  };
}
