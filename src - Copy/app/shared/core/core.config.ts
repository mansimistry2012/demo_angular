let Authority = 'https://salesapp-auth-dev.nexware.app'; //http://localhost:5000
let ClientId = 'salesapp.portal';
let RedirectUri = 'https://bepoz-dev.nexware.app/auth-callback'; //http://localhost:3000/auth-callback
let Post_Logout_Redirect_Uri = 'https://bepoz-dev.nexware.app/#/signout-oidc'; //http://localhost:3000/login
let ResponseType = 'id_token token';
let Scope = 'openid gateway.salesapp.portal';
let FilterProtocolClaims = true;
let LoadUserInfo = true;
let AutomaticSilentRenew = true;
let Silent_Redirect_Uri = 'https://bepoz-dev.nexware.app/#/signin-oidc'; //http://localhost:3000/
if (window.location && window.location.hostname == "localhost") {
  Authority = 'https://salesapp-auth-dev.nexware.app';
  RedirectUri = 'http://localhost:3000/auth-callback';
  Post_Logout_Redirect_Uri = 'http://localhost:3000/#/signout-oidc';
  Silent_Redirect_Uri = 'http://localhost:3000/#/signin-oidc';
}
if (window.location && window.location.hostname == "bepoz.nexware.app") {
  Authority = 'https://salesapp-auth.nexware.app';
  RedirectUri = 'https://bepoz.nexware.app/auth-callback';
  Post_Logout_Redirect_Uri = 'https://bepoz.nexware.app/#/signout-oidc';
  Silent_Redirect_Uri = 'https://bepoz.nexware.app/#/signin-oidc';
}



export const AuthCoreUrl = {

  Authority: Authority,
  ClientId: ClientId,
  RedirectUri: RedirectUri,
  Post_Logout_Redirect_Uri: Post_Logout_Redirect_Uri,
  ResponseType: ResponseType,
  Scope: Scope,
  FilterProtocolClaims: FilterProtocolClaims,
  LoadUserInfo: LoadUserInfo,
  AutomaticSilentRenew: AutomaticSilentRenew,
  Silent_Redirect_Uri: Silent_Redirect_Uri,

}