let RoutePath = 'https://bepoz-api-dev.nexware.app/v1/';
let LogoutUrl = 'https://salesapp-auth-dev.nexware.app/employeeaccount/logout/';
let UserInfo = 'https://salesapp-auth-dev.nexware.app/employeeaccount/getuserinfo';
//let RoutePath = 'https://bepoz-api.nexware.app/v1/';
// let LogoutUrl = 'https://salesapp-auth.nexware.app/employeeaccount/logout/'
// let UserInfo = 'https://salesapp-auth.nexware.app/employeeaccount/getuserinfo';
if (window.location && window.location.hostname == "bepoz.nexware.app") {
  RoutePath = 'https://bepoz-api.nexware.app/v1/';
  LogoutUrl = 'https://salesapp-auth.nexware.app/employeeaccount/logout/'
  UserInfo = 'https://salesapp-auth.nexware.app/employeeaccount/getuserinfo';
}
export const Apiurl = {
  RoutePath: RoutePath,
  LogoutUrl: LogoutUrl,
  UserInfo: UserInfo,
  //USER DATA
  getUserData: 'http://ec2-52-63-135-184.ap-southeast-2.compute.amazonaws.com:8091/v1/Employees/Donny.f%40bepoz.com.au',
  //quotes methods
  getAllQuotes: 'Quotes/v1/Quotes',
  getAllTemplates: 'product-template/v1/ProductTemplate',
  getAllItems: 'Items/v1/Items',

  //company methods
  getAllCompanies: 'companies/v1/Company',

  //save Company
  saveCompany: 'companies/v1/Company',

  //lookup
  getAllLookup: 'lookup/v1/Lookup',
  //Contact List
  getContactList: 'contact/v1/Contacts',

  //opportunities
  getAllOpportunityList: 'Opportunities/v1/Opportunity',

  //activities

  getAllActivties: 'activity/v1/Activity',

  //save activities
  saveActivity: 'activity/v1/Activity',

  //GetAllActivities
  GetAllActivities: 'activity/v1/Activity',

};
