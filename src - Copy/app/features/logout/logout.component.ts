import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LayoutService } from '../../features/layout/layout.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router, private layoutService: LayoutService) { }

  ngOnInit(): void {
  }

  logout() {
    this.layoutService.ResetLoginToken();
    this.router.navigate(["/login"])
  }
}
