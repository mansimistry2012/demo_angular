import { ExportService } from '../../shared/export.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { NavigationExtras, Router } from '@angular/router';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { CommonProvider } from '../../shared/common';
import { Apiurl } from '../../shared/route';
import * as moment from 'moment';
import * as _ from 'underscore';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { ThemePalette } from '@angular/material/core';
import { orderBy, SortDescriptor, State } from '@progress/kendo-data-query';
import { PageChangeEvent } from '@progress/kendo-angular-grid';
import { process } from "@progress/kendo-data-query";
@Component({
  selector: 'app-opportunities',
  templateUrl: './opportunities.component.html',
  styleUrls: ['./opportunities.component.scss']
})
export class OpportunitiesComponent implements OnInit {
  displayedColumns: string[] = [
    'id',
    'name',
    'title',
    'date',
    'status',
    'probability',
    'expectedClose',
    'actualClose',
    'salesRep',
    'projectedTotal',
    'forecastType',
    'amountAfterDiscount'
  ];
  dataSource: any;
  mainDataSource: any = [];
  showFilter: boolean = false;
  // @ViewChild(MatSort) sort: any;
  pageIndex: number = 1;
  pageLength: number = 100;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  panelOpenState = false;
  searchText: any = '';
  Company: any;
  OpportunityStatus: any;
  companyList: any = [];
  salesRepList: any = [];
  companyListSearch: any = [];
  OpportunityStatusList: any = [];
  startDate: any = '';
  endDate: any = '';
  expectedClose: any = '';
  salesRep: any;
  defaultSalesRep: any;
  TodayDate: any = new Date().toDateString();
  pageSize = 100;
  skip = 0;
  listLookups: any;
  showProgressBar: boolean = false;
  color: ThemePalette = 'primary';
  mode: ProgressBarMode = 'indeterminate';
  value = 50;
  bufferValue = 75;
  duplicateDataSource: any = [];
  public sort: SortDescriptor[] = [
    {
      field: "documentNumber",
      dir: "asc",
    },
  ];
  oppList: any = [];
  state: State = {
    sort: [{
      field: "documentNumber",
      dir: "asc",
    }],
  }
  userIsAdmin: boolean = false;
  userDetails: any;
  constructor(
    private commonService: CommonProvider,
    private router: Router,
    private exportService: ExportService
  ) {

  }
  ngOnInit() {
    this.init()
  }
  async init() {
    await this.getAllLookupList();
    this.getUserDetailsFromStorage();
    this.getAllQuotes();
  }
  manageChartsFilters() {
    if (history.state.endDate && history.state.setMonth) {
      this.startDate = moment(new Date(moment().year(), history.state.setMonth, 1));
      this.endDate = moment(new Date(moment().year(), history.state.setMonth, history.state.endDate - 1));
      this.showFilter = true;
      this.filterTypeChange();
    }
  }

  getAllLookupList() {
    let self = this;
    return new Promise(async function (resolve, reject) {
      if (self.commonService.lookupList) {
        self.commonService.lookupList.subscribe(async (list: any) => {
          if (list) {
            self.listLookups = list;
            if (self.listLookups && self.listLookups.opportunity) {
              self.OpportunityStatusList = self.listLookups.opportunity.opportunityStatus
              self.companyList = self.commonService.sortAlphabetically(self.listLookups.company?.companyList);
              self.companyListSearch = JSON.parse(JSON.stringify(self.companyList))
              self.salesRepList = self.listLookups.opportunity.salesRep
            }
            resolve(true)
          } else {
            reject(true);
          }
        });
      } else {
        await self.commonService.getAllLookupList();
        resolve(true)
      }
    })
  }
  getUserDetailsFromStorage() {
    let userDetailsStr: any = '';
    if (localStorage.getItem('UserDetails')) {
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      this.userDetails = JSON.parse(userDetailsStr);
      this.userIsAdmin = this.userDetails.isAdmin;
      if (this.userDetails && !this.userDetails.isAdmin) {
        this.defaultSalesRep = this.getSalesRep(this.userDetails.salesRepId);
      }
    }
  }
  getAllQuotes() {
    this.showProgressBar = true;

    let param: any = {}
    if (this.userDetails && !this.userDetails.isAdmin) {
      param.subsidiaryId = this.userDetails.subsidiaryId;
      param.salesRepId = this.userDetails.salesRepId;
    }
    this.commonService
      .GetMethod(Apiurl.getAllOpportunityList, param, true, 'Loading')
      .then((resp: any) => {
        if (resp) {
          if (resp.length) {
            resp.forEach((items: any) => {
              let daysInMonth = this.getDaysInMonth(new Date(items.date).getUTCMonth() + 1, new Date(items.date).getFullYear())
              items.opportunityItems.forEach((oppItem: any, index: any) => {
                if (oppItem.segmentId === 3) {
                  items.amountAfterDiscount = items.amountAfterDiscount + daysInMonth * (oppItem.rate / 7);
                }
              })
            })
            this.oppList = JSON.parse(JSON.stringify(resp));
            this.duplicateDataSource = JSON.parse(JSON.stringify(resp))
            this.mainDataSource = resp.splice(0, this.pageLength);
            this.dataSource = new MatTableDataSource(resp);
            // this.dataSource.sort = this.sort;
            // this.dataSource.paginator = this.paginator;
            this.manageChartsFilters()
            // if (this.defaultSalesRep) {
            //   this.filterTypeChange()
            // }
          }
        }
        this.showProgressBar = false;
      });
  }
  onChangePage(ev: PageEvent) {
    console.log(this.duplicateDataSource)
    this.oppList = JSON.parse(JSON.stringify(this.duplicateDataSource));
    this.pageIndex = ev.pageIndex + 1;
    localStorage.setItem('pageLength', ev.pageSize.toString())
    this.pageLength = ev.pageSize;
    this.mainDataSource = this.oppList.splice((this.pageIndex - 1) * this.pageLength, this.pageLength);
  }
  inputEvent(event: any) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filterTypeChange();
  }
  openFilter() {
    this.showFilter = !this.showFilter;
  }
  viewRow(row: any) {

    var navigationExtras: NavigationExtras = {
      state: { isEdit: true, qoute_id: row.id }
    };
    this.router.navigate(['add-opportunities'], navigationExtras);
  }
  filterTypeChange() {
    this.mainDataSource = JSON.parse(JSON.stringify(this.duplicateDataSource))
    // this.mainDataSource = process(this.duplicateDataSource, this.state)
    if (this.Company) {
      this.mainDataSource = this.mainDataSource.filter((data: any) => data.customerId === this.Company);
    }
    if (this.OpportunityStatus) {
      this.mainDataSource = this.mainDataSource.filter((data: any) => data.opportunityStatusId === this.OpportunityStatus)
    }
    if (this.salesRep || this.defaultSalesRep) {
      this.mainDataSource = this.mainDataSource.filter((data: any) => (data.salesRep == this.salesRep) || (data.salesRep == this.defaultSalesRep))
    }

    if (this.startDate && this.endDate) {
      let x: any = [];
      _.map(this.mainDataSource, function (res) {
        x.push({ ...res, date: moment(res.date).format('DD MMM YYYY') });
      });
      this.mainDataSource = x;
      var setStartDate = moment(new Date(this.startDate), 'DD MMM YYYY');
      var setEndDate = moment(new Date(this.endDate), 'DD MMM YYYYY');
      let filterData = this.mainDataSource.filter(
        (data: any) =>
          moment(data.date, 'DD MMM YYYY').isSameOrAfter(
            moment(setStartDate, 'DD MMM YYYY')
          ) &&
          moment(data.date, 'DD MMM YYYY').isSameOrBefore(
            moment(setEndDate, 'DD MMM YYYY')
          )
      );
      this.mainDataSource = filterData;
    }
    if (this.expectedClose) {
      let x: any = [];
      _.map(this.mainDataSource, function (res) {
        x.push({ ...res, date: moment(res.date).format('DD MMM YYYY') });
      });
      this.mainDataSource = x;
      let filterData = this.mainDataSource.filter(
        (data: any) =>
          moment(data.expectedClose).format('DD MMM YYYY') ==
          moment(this.expectedClose).format('DD MMM YYYY')
      );
      this.mainDataSource = filterData;
    }
  }
  clearSearch() {
    this.Company = null;
    this.OpportunityStatus = null;
    this.startDate = null;
    this.expectedClose = null;
    this.salesRep = null;
    this.defaultSalesRep = null;
    history.state.endDate = null;
    history.state.startDate = null;
    this.endDate = null;
    this.getAllQuotes()
  }

  sample() {
    alert('yes');
  }

  addOpportunities() {
    this.router.navigate(['add-opportunities']);
  }

  clearFilterCompany(event: any) {
    this.Company = null;
    event.stopPropagation();
    this.filterTypeChange();
  }
  clearFilterSalesRep(event: any) {
    this.salesRep = null;
    this.defaultSalesRep = null;
    event.stopPropagation();
    this.filterTypeChange();
  }
  clearFilterOppStatus(event: any) {
    this.OpportunityStatus = null;
    event.stopPropagation();
    this.filterTypeChange();
  }
  clearFilterDatePicker(event: any) {
    this.startDate = null;
    this.endDate = null;
    event.stopPropagation();
    this.filterTypeChange();
  }
  clearFilterExpectedDatePicker(event: any) {
    this.expectedClose = null;
    event.stopPropagation();
    this.filterTypeChange();
  }
  clear() {
    this.searchText = '';
    this.getAllQuotes();
  }
  clearCompanyDetails() {
    this.companyListSearch = this.companyList;
  }
  searchCompany(event: any) {
    if (event.target.value != '') {
      this.companyListSearch = this.companyList.filter(
        (s: any) =>
          s.companyName
            .toLowerCase()
            .indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.companyListSearch = this.companyList;
    }
  }
  getSalesRep(id: any) {
    let salesRep = _.findWhere(this.listLookups?.opportunity?.salesRep, { id: id })
    if (salesRep) {
      return salesRep.salesRepDescription;
    };
  }

  getOppStatus(id: any) {
    let oppStatus = _.findWhere(this.listLookups?.opportunity?.opportunityStatus, { id: id })
    if (oppStatus) {
      return oppStatus.statusDescription;
    };
  }
  getDaysInMonth(month: number, year: number) {
    return new Date(year, month, 0).getDate();
  };
  public rowCallback(context: any) {
    return {
      dragging: context.dataItem.dragging
    };
  }
  public exportToExcel(grid: any): void {
    grid.saveAsExcel();
  }
  sortOpportunityList(sort: any) {
    this.sort = sort;
    this.mainDataSource = {
      data: orderBy(this.oppList, this.sort),
      total: this.oppList.length,
    };
  }
  public pageChange(event: PageChangeEvent): void {
    this.oppList = JSON.parse(JSON.stringify(this.duplicateDataSource));
    this.skip = event.skip;
    this.mainDataSource = {
      data: this.oppList.slice(this.skip, this.skip + this.pageSize),
      total: this.oppList.length,
    };
  }
  public onFilter(inputValue: string): void {
    this.mainDataSource = process(this.duplicateDataSource, {
      filter: {
        logic: "or",
        filters: [
          {
            field: 'documentNumber',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'salesRep',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'name',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'title',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'opportunityStatus',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'probability',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'marketIndustry',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'tiers',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'salesRep',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'projectedTotal',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'forecastType',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'amountAfterDiscount',
            operator: 'contains',
            value: inputValue
          },
        ],
      }
    });
    if (inputValue == '' || inputValue == null) {
      this.mainDataSource = process(this.duplicateDataSource, this.state)
    }
    if (this.mainDataSource.data && this.mainDataSource.data.length) {
      this.mainDataSource = [...this.mainDataSource]
    }
    // this.dataBinding.skip = 0;
  }
  refreshGrid() {
    this.ngOnInit()
  }

  exportToCsv(): void {
    let exportObj = {
      'Opportunity No.': null,
      'Customer': null,
      'Title': null,
      'date': null,
      'Status': null,
      'Probability': null,
      'Expected Close': null,
      'Actual Close': null,
      'Market Indusrty': null,
      'Tier': null,
      'Sales Rep.': null,
      'Projected Total': null,
      'Forecast Type': null,
      'Total Amount': null
    }
    var exportData: any = [];
    this.mainDataSource.forEach((data: any) => {
      exportObj = {
        'Opportunity No.': data.documentNumber ? data.documentNumber : "OPP#" + data.id || '-',
        'Customer': data.name,
        'Title': data.title,
        'date': data.date,
        'Status': data.opportunityStatus,
        'Probability': data.probability,
        'Expected Close': data.expectedClose,
        'Actual Close': data.actualClose,
        'Market Indusrty': data.marketIndustry,
        'Tier': data.tiers,
        'Sales Rep.': data.salesRep,
        'Projected Total': data.projectedTotal,
        'Forecast Type': data.forecastType,
        'Total Amount': data.amountAfterDiscount
      };
      exportData.push(exportObj)
    })
    // return
    this.exportService.exportToCsv(exportData, 'Opportunities');
  }
}
