import { SpeedDialFabPosition } from './../../../shared/speed-dial-fab/speed-dial-fab.component';
import { Location } from '@angular/common';
import { LayoutService } from '../../../features/layout/layout.service';
import { NavigationExtras, Router } from '@angular/router';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatAccordion } from '@angular/material/expansion';
import { CommonProvider } from '../../../shared/common';
import { Apiurl } from '../../../shared/route';
import { SelectionModel } from '@angular/cdk/collections';
import * as _ from 'underscore';
import { NgxSpinnerService } from 'ngx-spinner';
import { ThemePalette } from '@angular/material/core';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { MatTabChangeEvent } from '@angular/material/tabs';
import * as moment from 'moment';
import { NgSelectComponent } from '@ng-select/ng-select';
@Component({
  selector: 'app-add-opportunities',
  templateUrl: './add-opportunities.component.html',
  styleUrls: ['./add-opportunities.component.scss']
})
export class AddOpportunitiesComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;
  @ViewChild('ngSelectComponent') ngSelectComponent: NgSelectComponent;
  @ViewChild('searchInputElement') searchInputElement: ElementRef;
  listCompanies: any = [];
  listLookups: any = [];
  listopportunities: any = [];
  listTemplates: any = [];
  mainlistTemplates: any = [];
  listItems: any;
  mainlistItems: any;
  ContactList: any = [];
  duplicateListItems: any = [];
  opportunitiesForm: FormGroup;
  activeTab: string = 'templates';
  displayedColumns: string[] = ['select', 'name', 'basePrice'];
  displayOpportunityItems: string[] = ['name', 'rate', 'amount', 'quantity'];
  displayOpportunityItemsInEdit: string[] = ['name', 'quantity', 'amount'];
  selection = new SelectionModel<any>(true, []);
  totalHardwareAmount: number = 0;
  totalServiceAmount: number = 0;
  totalSubscriptionAmount: number = 0;
  totalSelectedCheckBox: number = 0;
  opportunityItems: any = [];
  showSavedItemsList: boolean = false;
  opportunityItemsID: any;
  rowData: any;
  isShowData: boolean = false;
  isViewData: boolean = false;
  contactListSearch: any;
  showExtras: boolean = false;
  showDisCountPage: boolean = false;
  openBlock: number = 1;
  discountType: string = 'dollar';
  contactData: any = [];
  activityList: any = [];
  serachInput: any = '';
  ContactSearch: any = [];
  opportunityList: any = [];
  relatedActivityArray: any = [];
  companyListSearch: any = [];
  searchdropdown: any;
  searchtext: any = '';
  submitted: boolean = false;
  forcastTypeName: any = '';
  selectedIndex = 0;
  showItemList?: boolean = false;
  hideSpinnerofTemplate?: boolean = false;
  editOpportunities: any;

  /*--LOADER FLAG--*/
  showActivitiesLoader = true;
  showActivitiesNoRecMsg = false;
  showContactsLoader = true;
  showContactsNoRecMsg = false;

  speedDialFabButtons = [
    {
      icon: 'note',
      tooltip: 'Task',
      class: 'note-color'
    },
    {
      icon: 'event',
      tooltip: 'Event',
      class: 'event-color'
    },
    {
      icon: 'phone',
      tooltip: 'Phone',
      class: 'phone-color'
    }
  ];
  SpeedDialFabPosition = SpeedDialFabPosition;
  speedDialFabColumnDirection = 'column';
  speedDialFabPosition = SpeedDialFabPosition.Top;
  speedDialFabPositionClassName = 'speed-dial-container-top';
  TodayDate: any = new Date().toDateString();

  showProgressBar: boolean = false;
  color: ThemePalette = 'primary';
  mode: ProgressBarMode = 'indeterminate';
  value = 50;
  bufferValue = 75;
  userDetailsStr: any = '';
  userDetails: any;

  constructor(
    private commonService: CommonProvider,
    private fb: FormBuilder,
    private router: Router,
    private location: Location,
    private layoutService: LayoutService,
    private SpinnerService: NgxSpinnerService
  ) { }

  async ngOnInit() {
    this.createOpportunitiesForm();
    await this.getAllLookupList();
    this.fetchData();
  }

  onPositionChange(position: SpeedDialFabPosition) {
    switch (position) {
      case SpeedDialFabPosition.Bottom:
        this.speedDialFabPositionClassName = 'speed-dial-container-bottom';
        this.speedDialFabColumnDirection = 'column-reverse';
        break;
      default:
        this.speedDialFabPositionClassName = 'speed-dial-container-top';
        this.speedDialFabColumnDirection = 'column';
    }
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.listItems.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.listItems.map((x: any) => {
        x.selected = false;
      });
      return;
    }

    this.selection.select(...this.listItems);
  }
  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'
      } row ${row.position + 1}`;
  }

  onSpeedDialFabClicked(btn: any) {
    this.gotoActivity(btn.tooltip);
  }

  createOpportunitiesForm() {
    if (localStorage.getItem('UserDetails')) {
      this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      this.userDetails = JSON.parse(this.userDetailsStr);
    }
    // let relatedContactIds: any = [];
    var newDate = moment().add(1, 'month');
    this.opportunitiesForm = this.fb.group({
      netSuiteId: [0],
      customerId: [null, Validators.required],
      title: [''],
      date: [new Date()],
      expectedClose: [newDate],
      actualClose: [newDate],
      opportunityStatusId: [8, Validators.required],
      probability: [70],
      opportunityItems: [null],
      subsidiaryId: [
        this.userDetails && this.userDetails.subsidiaryId
          ? this.userDetails.subsidiaryId
          : 6
      ],
      winLossReasonId: [null, Validators.required],
      salesRepId: [null, Validators.required],
      leadSourceId: [null, Validators.required],
      memo: [''],
      forecastTypeId: [null, Validators.required],
      marketIndustryId: [null, Validators.required],
      productCategoryId: [null, Validators.required],
      tiersId: [null, Validators.required],
      originalAmount: [null],
      amountAfterDiscount: [null],
      discountAmount: [null],
      dollarDiscount: [null],
      percentageDiscount: [null],
      relatedContactIds: [null],
      totalAmount: [null],
      weightedTotal: [{ value: null, disabled: true }],
      projectedTotal: [null],
      rangeHigh: [null],
      rangeLow: [null],
      documentNumber: [null]
    });
  }

  async fetchData() {
    await this.getAllOpportunity();
    if (history.state.navigationId != 1) {
      if (history.state && history.state.contact_id && history.state.qoute_id) {
        this.isShowData = true;
        this.getAllContact();
      } else if (history.state && history.state.activity_id) {
        this.isShowData = true;
        this.getAllContact();
        this.getAllActivityList();
      } else if (history.state && history.state.isEdit) {
        this.isShowData = history.state.isEdit;
        this.getAllContact();
        this.getAllActivityList();
      }
    } else {
      this.router.navigate(['dashboard']);
      setTimeout(() => {
        this.layoutService.activeRoute = this.router.url;
        this.layoutService.getcurrentActivateRoute(
          this.layoutService.activeRoute
        );
      }, 50);
    }
    await this.getAllCompaniesList();
  }

  async getAllOpportunity() {

    let userDetailsStr: any = '';
    let userDetails: any;

    if (localStorage.getItem('UserDetails')) {
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      userDetails = JSON.parse(userDetailsStr);
    }
    let param: any = {};
    if (userDetails && !userDetails.isAdmin) {
      param.subsidiaryId = userDetails.subsidiaryId;
      param.salesRepId = userDetails.salesRepId;
    }
    await this.commonService
      .GetMethod(Apiurl.getAllOpportunityList, param, true, 'Loading')
      .then((res: any) => {
        if (res) {
          this.opportunityList = res;
          this.getAllActivityList();
        }
      });
  }

  getAllContact() {
    this.opportunityItemsID = this.opportunityItemsID
      ? this.opportunityItemsID
      : history.state.qoute_id;

    this.commonService
      .GetMethod(
        Apiurl.getAllOpportunityList + '/' + this.opportunityItemsID,
        null,
        true,
        'Loading'
      )
      .then(async (res: any) => {
        if (res) {
          this.editOpportunities = (res.salesRepId == this.userDetails.salesRepId) ? true : false;
          this.rowData = res;
          this.forcastTypeName = _.findWhere(
            this.listLookups.opportunity?.forecastType,
            {
              id: this.rowData.forecastTypeId
            }
          );
          if (this.forcastTypeName) {
            this.forcastTypeName = this.forcastTypeName.forecastTypeDescription;

          }
          if (this.rowData.documentNumber) {
            this.rowData.documentId = this.rowData.documentNumber;

          } else {
            this.rowData.documentId = 'OPP#' + this.rowData.id;
          }

          this.getAllCompaniesList();
          this.getItemList(res.id);
          this.opportunityItemsID = history.state.qoute_id;
          if (
            history.state.contact_id != null &&
            history.state.contact_id != undefined
          ) {
            if (!res?.relatedContactIds?.includes(history.state.contact_id)) {
              let contactIDList: any = [];
              this.patchValues();
              if (this.opportunitiesForm.controls['relatedContactIds'].value) {
                contactIDList = this.opportunitiesForm.controls[
                  'relatedContactIds'
                ].value;
              }
              contactIDList.push(history.state.contact_id);
              this.opportunitiesForm.controls['relatedContactIds'].setValue(
                contactIDList
              );
            }
            this.updateQuotes();
          }
          this.getAllContactList();
          this.patchValues();
        }
      });
  }

  getAllCompaniesList() {
    let self = this;
    return new Promise(function (resolve, reject) {
      if (self.listLookups) {
        if (
          self.listLookups.company &&
          self.listLookups.company.companyList.length
        ) {
          self.listCompanies = self.listLookups.company.companyList;
          self.listCompanies = self.commonService.sortAlphabetically(
            self.listCompanies
          );
          self.companyListSearch = JSON.parse(
            JSON.stringify(self.listCompanies)
          );
        }
        if (self.rowData) {
          let company = _.findWhere(self.listCompanies, {
            companyId: self.rowData.customerId
          });
          let statusName = _.findWhere(self.listLookups.quotes.quotesStatus, {
            id: self.rowData.statusId
          });
          self.rowData.companyName = company?.companyName;
          self.rowData.status = statusName?.statusDescription;
        }
        self.setCompanyDetails();
        resolve(true);
      } else {
        reject(true);
      }
    });
  }

  async setCompanyDetails() {
    if (history.state && history.state.id && history.state.id) {
      let companyName = _.findWhere(this.listCompanies, {
        companyId: this.rowData?.customerId
          ? this.rowData?.customerId
          : history.state.id
      });
      this.opportunitiesForm.controls['customerId'].setValue(history.state.id);
      await this.setCompanySubDetails(companyName);
    }
  }


  async setCompanySubDetails(selectedCompanyDetail: any) {

    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    // Set User details from localStorage
    var userDetails = JSON.parse(this.userDetailsStr);
    if (!userDetails.isAdmin && selectedCompanyDetail.salesRepId !== JSON.parse(this.userDetailsStr).salesRepId) {
      let obj = {
        title: "Error",
        text: "Opportunities SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok',
      };
      await this.commonService.showErrorMsg(obj).
        then((resp: any) => {
          this.opportunitiesForm.controls['customerId'].setValue('');
        });
    }
    else {
      if (selectedCompanyDetail) {
        this.opportunitiesForm.controls['salesRepId'].setValue(
          selectedCompanyDetail?.salesRepId
        );
        this.opportunitiesForm.controls['leadSourceId'].setValue(
          selectedCompanyDetail?.leadSourceId
        );
        this.opportunitiesForm.controls['marketIndustryId'].setValue(
          selectedCompanyDetail?.marketIndustryId
        );
        this.opportunitiesForm.controls['marketIndustryId'].setValue(
          selectedCompanyDetail?.marketIndustryId
        );
        this.opportunitiesForm.controls['productCategoryId'].setValue(
          selectedCompanyDetail?.productCategoryId
        );
        this.opportunitiesForm.controls['tiersId'].setValue(
          selectedCompanyDetail?.tiersId
        );

        if (selectedCompanyDetail && selectedCompanyDetail.subsidiaryId) {
          await this.getAllTemplateList(selectedCompanyDetail.subsidiaryId);
          await this.getAllItemList(selectedCompanyDetail.subsidiaryId);
        }
      } else {
        this.showItemList = false;
      }
    }
  }

  getAllActivityList() {
    this.showActivitiesLoader = true;
    this.showActivitiesNoRecMsg = false;
    let userDetailsStr: any = '';
    let userDetails: any;

    if (localStorage.getItem('UserDetails')) {
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      userDetails = JSON.parse(userDetailsStr);
    }
    let param: any = {};
    if (userDetails && !userDetails.isAdmin) {
      param.subsidiaryId = userDetails.subsidiaryId;
      param.salesRepId = userDetails.salesRepId;
    }
    this.commonService
      .GetMethod(Apiurl.getAllActivties, param, true, 'Loading..')
      .then((resp: any) => {
        if (resp.length != 0) {
          this.activityList = resp;
          this.relatedActivityArray = [];
          for (let index = 0; index < this.activityList.length; index++) {
            if (
              this.activityList[index].transactionType.includes(
                'opportunities'
              ) ||
              this.activityList[index].transactionType.includes(
                'Opportunities'
              ) ||
              this.activityList[index].transactionType.includes('opp') ||
              this.activityList[index].transactionType.includes('quote') ||
              this.activityList[index].transactionType.includes('Quote') ||
              this.activityList[index].transactionType.includes('Quotes') ||
              this.activityList[index].transactionType.includes('quotes')
            ) {
              if (this.activityList[index].relatedTransactionId != 0) {
                let data = _.findWhere(this.opportunityList, {
                  id: this.activityList[index].relatedTransactionId
                });
                if (data) {
                  if (data.id == history.state.qoute_id) {
                    this.relatedActivityArray.push(this.activityList[index]);
                  }
                }
              }
            }
          }
          this.showActivitiesLoader = false;
          if (this.relatedActivityArray.length == 0) {
            this.showActivitiesNoRecMsg = true;
          }
        } else {
          this.showActivitiesNoRecMsg = true;
          this.showActivitiesLoader = false;
        }
      });
  }



  getAllContactList() {
    this.showContactsLoader = true;
    this.showContactsNoRecMsg = false;
    this.commonService.lookupList.subscribe((resp: any) => {
      if (resp.length != 0) {
        this.ContactList = resp?.contact?.contactList;
        this.contactListSearch = JSON.parse(JSON.stringify(this.ContactList));
        let data: any = [];
        if (
          this.rowData.relatedContactIds &&
          this.rowData.relatedContactIds.length
        ) {
          this.contactData = [];
          data = this.rowData.relatedContactIds.forEach((id: any, i: any) => {
            let contacts = _.findWhere(this.ContactList, { contactId: id });
            if (contacts) {
              this.contactData.push(contacts);
            }
            if (i == this.rowData.relatedContactIds.length - 1) {
              this.showContactsLoader = false;
              if (this.contactData.length == 0) {
                this.showContactsNoRecMsg = true;
              } else {
                this.showContactsNoRecMsg = false;
              }
            }
          });
        } else {
          this.showContactsLoader = false;
          if (this.contactData.length == 0) {
            this.showContactsNoRecMsg = true;
          } else {
            this.showContactsNoRecMsg = false;
          }
        }
        this.ContactSearch = this.contactData;
      }
    });
  }

  getAllLookupList() {
    let self = this;
    return new Promise(function (resolve, reject) {
      self.commonService.lookupList.subscribe((list: any) => {
        if (list) {
          self.listLookups = list;
          console.log("LISTLOGGGGGGGG", self.listLookups)
          resolve(true);
        } else {
          reject(true);
        }
      });
    });
  }

  getAllTemplateList(subsidiaryId: any) {
    this.SpinnerService.show();
    let SubsidiaryId = '?subsidiaryId=' + subsidiaryId;
    this.commonService
      .GetMethod(Apiurl.getAllTemplates + SubsidiaryId, null, true, 'Loading')
      .then((res: any) => {
        this.SpinnerService.hide();
        if (res) {
          this.hideSpinnerofTemplate = true;
          this.listTemplates = res.filter((item: any) => {
            if (item.templateName) {
              item.selected = false;
              item.added = false;
              return true;
            } else {
              return false;
            }
          });
          this.mainlistTemplates = this.listTemplates;
        }
      });
  }

  getAllItemList(subsidiaryId: any) {
    this.SpinnerService.show();
    let self = this;
    return new Promise(function (resolve, reject) {
      let SubsidiaryId = '?subsidiaryId=' + subsidiaryId;
      self.commonService
        .GetMethod(Apiurl.getAllItems + SubsidiaryId, null, true, 'Loading')
        .then((res: any) => {
          self.SpinnerService.hide();
          self.showItemList = true;
          if (res) {
            self.duplicateListItems = res;
            self.duplicateListItems.map((x: any) => {
              x.selected = false;
            });
            resolve(true);
          } else {
            reject(true);
          }
        });
    });
  }

  onSubmit(form: FormGroup) { }

  onTabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.selectedIndex = tabChangeEvent.index;
  }

  saveAndContinue(shouldRoute?: boolean) {
    if (this.opportunitiesForm.invalid) {
      if (
        this.opportunitiesForm.controls.customerId.status == 'INVALID' ||
        this.opportunitiesForm.controls.opportunityStatusId.status ==
        'INVALID' ||
        this.opportunitiesForm.controls.subsidiaryId.status == 'INVALID' ||
        this.opportunitiesForm.controls.winLossReasonId.status == 'INVALID' ||
        this.opportunitiesForm.controls.salesRepId.status == 'INVALID' ||
        this.opportunitiesForm.controls.leadSourceId.status == 'INVALID'
      ) {
        this.selectedIndex = 0;
      } else if (
        this.opportunitiesForm.controls.forecastTypeId.status == 'INVALID'
      ) {
        this.selectedIndex = 1;
      } else if (
        this.opportunitiesForm.controls.productCategoryId.status == 'INVALID' ||
        this.opportunitiesForm.controls.marketIndustryId.status == 'INVALID' ||
        this.opportunitiesForm.controls.tiersId.status == 'INVALID'
      ) {
        this.selectedIndex = 2;
      }
      this.opportunitiesForm.markAllAsTouched();
      return;
    }
    if (this.opportunityItems.length == 0) {
      this.commonService.showError(
        'Please select at least one Item to add Opportunity'
      );
      return;
    }
    this.setCurrencyValue();
    this.checkNullForAmounts();
    if (this.opportunityItemsID) {
      this.updateQuotes();
      return;
    }
    if (history.state.contact_id) {
      let contactIDList: any = [];
      // contactIDList = this.opportunitiesForm.controls['relatedContactIds']
      //   .value;

      // Added condition to check whether the form control has value or not
      if (this.opportunitiesForm.controls['relatedContactIds'].value) {
        contactIDList = this.opportunitiesForm.controls['relatedContactIds']
          .value;
      }
      contactIDList.push(history.state.contact_id);

      this.opportunitiesForm.controls['relatedContactIds'].setValue(
        contactIDList
      );
    }
    this.opportunityItems.map((quotes: any) => {
      delete quotes?.segmentId;
      delete quotes?.name;
    });
    this.opportunitiesForm.controls['opportunityItems'].setValue(
      this.opportunityItems
    );
    this.SpinnerService.show();
    this.commonService
      .PostMethod(
        Apiurl.getAllOpportunityList,
        this.opportunitiesForm.getRawValue(),
        true,
        'Loading',
        null
      )
      .then(async (res: any) => {
        this.SpinnerService.hide();
        if (!res) {
          this.commonService.showError(
            'Something went wrong Opportunity was not saved, Please try again'
          );
          return;
        }
        if (res) {
          if (res?.code?.includes('Duplicate Error')) {
            this.commonService.showError(res.message);
            return;
          }
          this.commonService.showSuccess('Opportunity Added successfully');
          this.opportunityItemsID = res;
          if (history.state.from == 'company-detail') {
            let param: NavigationExtras = {
              state: {
                id: history.state.id,
                quote_id: this.opportunityItemsID
              }
            };
            this.router.navigate(['companies-details'], param);
            setTimeout(() => {
              this.layoutService.activeRoute = this.router.url;
              this.layoutService.getcurrentActivateRoute(
                this.layoutService.activeRoute
              );
            }, 50);
          } else if (history.state.from == 'contacts') {
            let param: NavigationExtras = {
              state: {
                id: history.state.id,
                contact_id: history.state.contact_id,
                quote_id: this.opportunityItemsID
              }
            };
            this.router.navigate(['contacts'], param);
            setTimeout(() => {
              this.layoutService.activeRoute = this.router.url;
              this.layoutService.getcurrentActivateRoute(
                this.layoutService.activeRoute
              );
            }, 50);
          } else if (!history.state.isEdit) {
            this.isShowData = true;
            this.showDisCountPage = false;
            this.showSavedItemsList = false;
            this.getAllContact();
            return;
          }
          await this.getItemList(this.opportunityItemsID);
        }
      });
  }

  saveAndNext() {
    this.opportunityItems.map((quotes: any, i: any) => {
      var items = _.findWhere(this.duplicateListItems, { id: quotes.itemId });
      if (!items) return;
      quotes.name = items.name;
      quotes.segmentId = items.segmentId;
    });
    this.showSavedItemsList = true;
    this.calculateSummarForQuotes();
  }

  saveOppItems() {
    this.showSavedItemsList = false;
    this.showDisCountPage = true;
  }

  setCurrencyValue() {
    this.opportunitiesForm.value.amountAfterDiscount = this.opportunitiesForm
      .value.amountAfterDiscount
      ? this.opportunitiesForm.value.amountAfterDiscount
      : 0;
    this.opportunitiesForm.value.originalAmount = this.opportunitiesForm.value
      .originalAmount
      ? this.opportunitiesForm.value.originalAmount
      : 0;
    this.opportunitiesForm.value.discountAmount = this.opportunitiesForm.value
      .discountAmount
      ? this.opportunitiesForm.value.discountAmount
      : 0;
    this.opportunitiesForm.value.totalAmount = this.opportunitiesForm.value
      .totalAmount
      ? this.opportunitiesForm.value.totalAmount
      : 0;
    this.opportunitiesForm.getRawValue().weightedTotal = this.opportunitiesForm.getRawValue()
      .weightedTotal
      ? this.opportunitiesForm.value.weightedTotal
      : 0;
    this.opportunitiesForm.value.projectedTotal = this.opportunitiesForm.value
      .projectedTotal
      ? this.opportunitiesForm.value.projectedTotal
      : 0;
    this.opportunitiesForm.value.rangeHigh = this.opportunitiesForm.value
      .rangeHigh
      ? this.opportunitiesForm.value.rangeHigh
      : 0;
    this.opportunitiesForm.value.rangeLow = this.opportunitiesForm.value
      .rangeLow
      ? this.opportunitiesForm.value.rangeLow
      : 0;
  }

  getItemList(id: any) {
    this.showProgressBar = true;
    let self = this;
    return new Promise(function (resolve, reject) {
      self.commonService
        .GetMethod(
          Apiurl.getAllOpportunityList + '/' + id,
          null,
          true,
          'Loading'
        )
        .then((res: any) => {
          if (res) {
            self.showSavedItemsList = true;
            self.opportunityItems = [];
            if (res.opportunityItems?.length) {
              // res.opportunityItems.forEach((quotes: any, i: any) => {
              // if (self.duplicateListItems && self.duplicateListItems.length != 0) {
              //   var items = _.findWhere(self.duplicateListItems, {
              //     id: quotes.itemId
              //   });
              //   console.log(items)
              //   if (!items) return;
              //   quotes.name = items.name;
              //   quotes.segmentId = items.segmentId;
              //   self.opportunityItems.push(quotes);
              //   console.log("self.opportunityItems", self.opportunityItems)
              // } else {
              // var items = _.findWhere(res, {
              //   id: quotes.itemId
              // });
              // console.log(items)
              // if (!items) return;
              //   quotes.name = items.name;
              // quotes.segmentId = items.segmentId;
              // quotes.segmentId = items.segmentId;
              // }
              // });
              self.opportunityItems = res.opportunityItems
              self.showProgressBar = false;
              if (self.isShowData) {
                self.listTemplates.forEach((template: any) => {
                  let templateItemsIds: any = [],
                    opportunityItemsIds: any = [];
                  template.items = template.items.filter(
                    (item: any) => item.segmentId != 0
                  );
                  templateItemsIds = _.pluck(template.items, 'itemId');
                  templateItemsIds = templateItemsIds.filter(
                    (ids: number) => ids != 0
                  );
                  opportunityItemsIds = _.pluck(res.opportunityItems, 'itemId');
                  var check =
                    JSON.stringify(templateItemsIds) ===
                    JSON.stringify(opportunityItemsIds);
                  if (check) {
                    template.selected = true;
                  } else {
                    template.selected = false;
                  }
                });
              }
            }
            self.showProgressBar = false;
            self.calculateSummarForQuotes();
            resolve(true);
          } else {
            self.showProgressBar = false;
            reject(true);
          }
        })
        .catch(err => {
          self.showProgressBar = false;
          reject(true);
        });
    });
  }

  calculateSummarForQuotes() {
    // if (this.isShowData) {
    this.totalHardwareAmount = 0;
    this.totalServiceAmount = 0;
    this.totalSubscriptionAmount = 0;
    let opportunityItems: any = [];
    this.opportunityItems.forEach((y: any) => {
      var x = _.findWhere(this.duplicateListItems, { id: y.itemId });
      if (x.segmentId == 1) {
        x.selected = true;
        y.name = x.name;
        this.totalHardwareAmount = this.totalHardwareAmount + y.amount;
        opportunityItems.push(y);
      } else if (x.segmentId == 2) {
        y.name = x.name;
        x.selected = true;
        this.totalServiceAmount = this.totalServiceAmount + y.amount;
        opportunityItems.push(y);
      } else if (x.segmentId == 3) {
        x.selected = true;
        y.name = x.name;
        this.totalSubscriptionAmount = this.totalSubscriptionAmount + y.amount;
        opportunityItems.push(y);
      } else {
        x.selected = false;
      }
      this.opportunityItems = opportunityItems;
    });
    // }
    this.opportunitiesForm.controls['originalAmount'].setValue(
      this.totalHardwareAmount + this.totalServiceAmount
    );
    let amountAfterDiscount;
    if (this.opportunitiesForm.controls['discountAmount'].value) {
      amountAfterDiscount =
        this.totalHardwareAmount +
        this.totalServiceAmount -
        this.opportunitiesForm.controls['discountAmount'].value;
    } else {
      amountAfterDiscount = this.totalHardwareAmount + this.totalServiceAmount;
    }
    this.opportunitiesForm.controls['amountAfterDiscount'].setValue(
      amountAfterDiscount
    );
  }

  goTo(tab: string) {
    this.activeTab = tab;
    let segmentId: any;
    switch (this.activeTab) {
      case 'hardware':
        segmentId = 1;
        break;
      case 'services':
        segmentId = 2;
        break;
      case 'subscriptions':
        segmentId = 3;
        break;
      default:
        break;
    }
    this.mainlistItems = this.duplicateListItems.filter(
      (item: any) => item.segmentId == segmentId
    );
    this.listItems = this.duplicateListItems.filter(
      (item: any) => item.segmentId == segmentId
    );
    this.listItems.sort((a: any, b: any) => b.selected - a.selected);
    this.listItems = [...this.listItems];
  }

  calculateTotalValue(items: any) {
    items.selected = !items.selected;
    if (items.selected) {
      items.items.forEach((object: any) => {
        object.selected = true;
      });
      this.totalSelectedCheckBox++;
      items.added = false;
      this.selectItems(items);
    } else {
      items.items.forEach((object: any) => {
        object.selected = false;
      });
      this.totalSelectedCheckBox--;
      items.added = true;
      this.selectItems(items);
    }
  }

  calculateHardwareItems(items: any) {
    let params = {
      amount: 0,
      itemId: 0,
      quantity: 0,
      rate: 0
    };
    items.selected = !items.selected;
    this.listTemplates.forEach((x: any) => {
      var itemInTemplates = _.findWhere(x.items, { itemId: items.id });
      if (itemInTemplates) {
        itemInTemplates.selected = !itemInTemplates.selected;
      }
      var selectedItems = x.items.filter(
        (object: any) => object.selected === true
      );
      if (selectedItems.length === x.items.length) {
        x.selected = true;
      } else {
        x.selected = false;
      }
    });
    if (items.selected) {
      items.quantity = 1;
      params.quantity = items.quantity;
      params.amount = items.basePrice * items.quantity;
      params.itemId = items.id;
      params.rate = items.basePrice;
      this.opportunityItems.push(params);
      this.totalHardwareAmount =
        Math.round(this.totalHardwareAmount * 100 + Number.EPSILON) / 100 +
        items.basePrice;
    } else {
      let index = this.opportunityItems.findIndex(
        (x: any) => x.itemId == items.id
      );
      if (index < 0) return;
      items.quantity = 0;
      items.selected = false;
      this.totalHardwareAmount =
        Math.round(this.totalHardwareAmount * 100 + Number.EPSILON) / 100 -
        items.basePrice;
      this.opportunityItems.splice(index, 1);
    }
    this.opportunitiesForm.controls['originalAmount'].setValue(
      this.totalHardwareAmount + this.totalServiceAmount
    );
    this.opportunitiesForm.controls['amountAfterDiscount'].setValue(
      this.totalHardwareAmount + this.totalServiceAmount
    );
    this.listItems.sort((a: any, b: any) => b.selected - a.selected);
    this.listItems = [...this.listItems];
    if (
      !this.opportunitiesForm.controls['rangeLow'].value ||
      history.state.isEdit
    ) {
      this.setRangesAsPerTotalAmnt();
    }
  }

  calculateServiceItems(items: any) {
    items.selected = !items.selected;
    let params = {
      amount: 0,
      itemId: 0,
      quantity: 0,
      rate: 0
    };
    this.listTemplates.forEach((x: any) => {
      var itemInTemplates = _.findWhere(x.items, { itemId: items.id });
      if (itemInTemplates) {
        itemInTemplates.selected = !itemInTemplates.selected;
      }
      var selectedItems = x.items.filter(
        (object: any) => object.selected === true
      );
      if (selectedItems.length === x.items.length) {
        x.selected = true;
      } else {
        x.selected = false;
      }
    });
    if (items.selected) {
      items.quantity = 1;
      params.quantity = items.quantity;
      params.amount = items.basePrice * items.quantity;
      params.itemId = items.id;
      params.rate = items.basePrice;
      this.opportunityItems.push(params);
      this.totalServiceAmount =
        Math.round(this.totalServiceAmount * 100 + Number.EPSILON) / 100 +
        items.basePrice;
    } else {
      let index = this.opportunityItems.findIndex(
        (x: any) => x.itemId == items.id
      );
      if (index < 0) return;
      items.quantity = 0;
      items.selected = false;
      this.totalServiceAmount =
        Math.round(this.totalServiceAmount * 100 + Number.EPSILON) / 100 -
        items.basePrice;
      this.opportunityItems.splice(index, 1);
    }
    this.opportunitiesForm.controls['originalAmount'].setValue(
      this.totalHardwareAmount + this.totalServiceAmount
    );
    this.opportunitiesForm.controls['amountAfterDiscount'].setValue(
      this.totalHardwareAmount + this.totalServiceAmount
    );
    this.listItems.sort((a: any, b: any) => b.selected - a.selected);
    this.listItems = [...this.listItems];
    if (!this.opportunitiesForm.controls['rangeLow'].value) {
      this.setRangesAsPerTotalAmnt();
    }
  }

  calculateSubscriptionItems(items: any) {
    items.selected = !items.selected;
    let params = {
      amount: 0,
      itemId: 0,
      quantity: 0,
      rate: 0
    };
    this.listTemplates.forEach((x: any) => {
      var itemInTemplates = _.findWhere(x.items, { itemId: items.id });
      if (itemInTemplates) {
        itemInTemplates.selected = !itemInTemplates.selected;
      }
      var selectedItems = x.items.filter(
        (object: any) => object.selected === true
      );
      if (selectedItems.length === x.items.length) {
        x.selected = true;
      } else {
        x.selected = false;
      }
    });
    if (items.selected) {
      items.quantity = 1;
      params.quantity = items.quantity;
      params.amount = 7 * items.basePrice * items.quantity;
      params.itemId = items.id;
      params.rate = 7 * items.basePrice;
      this.opportunityItems.push(params);
      this.totalSubscriptionAmount =
        Math.round(this.totalSubscriptionAmount * 100 + Number.EPSILON) / 100 +
        7 * items.basePrice;
    } else {
      let index = this.opportunityItems.findIndex(
        (x: any) => x.itemId == items.id
      );
      if (index < 0) return;
      items.quantity = 0;
      items.selected = false;
      this.totalSubscriptionAmount =
        Math.round(this.totalSubscriptionAmount * 100 + Number.EPSILON) / 100 -
        7 * items.basePrice;
      this.opportunityItems.splice(index, 1);
    }
    this.listItems.sort((a: any, b: any) => b.selected - a.selected);
    this.listItems = [...this.listItems];
    if (
      !this.opportunitiesForm.controls['rangeLow'].value ||
      history.state.isEdit
    ) {
      this.setRangesAsPerTotalAmnt();
    }
  }

  setRangesAsPerTotalAmnt() {
    let rangeLow = this.opportunitiesForm.controls['amountAfterDiscount'].value;
    this.opportunitiesForm.controls['rangeLow'].setValue(rangeLow);
    this.opportunitiesForm.controls['rangeHigh'].setValue(rangeLow);
    this.opportunitiesForm.controls['projectedTotal'].setValue(rangeLow);
    let weightedTotal =
      rangeLow * (this.opportunitiesForm.controls['probability'].value / 100);
    this.opportunitiesForm.controls['weightedTotal'].setValue(weightedTotal);
  }

  selectItems(templateItems: any) {
    let index;
    templateItems.items.forEach((objs: any) => {
      let params = {
        amount: 0,
        itemId: 0,
        quantity: 0,
        rate: 0
      };
      var items = _.findWhere(this.duplicateListItems, { id: objs.itemId });
      if (items) {
        if (items.segmentId === 1) {
          if (templateItems.selected) {
            items.quantity = 1;
            params.quantity = items.quantity;
            params.amount = items.basePrice * items.quantity;
            params.itemId = objs.itemId;
            params.rate = items.basePrice;
            items.selected = true;
            this.totalHardwareAmount =
              this.totalHardwareAmount + items.basePrice;
            this.opportunityItems.push(params);
          } else {
            items.selected = false;
            if (this.totalHardwareAmount > 0) {
              this.totalHardwareAmount =
                this.totalHardwareAmount - items.basePrice;
            }
            index = this.opportunityItems.findIndex(
              (x: any) => x.id == items.id
            );
            this.opportunityItems.splice(index, 1);
          }
        }
        if (items.segmentId === 2) {
          if (templateItems.selected) {
            items.quantity = 1;
            params.quantity = items.quantity;
            params.amount = items.basePrice * items.quantity;
            params.itemId = objs.itemId;
            params.rate = items.basePrice;
            items.selected = true;
            this.totalServiceAmount = this.totalServiceAmount + items.basePrice;
            this.opportunityItems.push(params);
          } else {
            items.selected = false;
            if (this.totalServiceAmount > 0) {
              this.totalServiceAmount =
                this.totalServiceAmount - items.basePrice;
            }
            index = this.opportunityItems.findIndex(
              (x: any) => x.id == items.id
            );
            this.opportunityItems.splice(index, 1);
          }
        }
        if (items.segmentId === 3) {
          if (templateItems.selected) {
            items.quantity = 1;
            params.quantity = items.quantity;
            params.amount = 7 * items.basePrice * items.quantity;
            params.itemId = objs.itemId;
            params.rate = 7 * items.basePrice;
            items.selected = true;
            this.totalSubscriptionAmount =
              this.totalSubscriptionAmount + 7 * items.basePrice;

            this.opportunityItems.push(params);
          } else {
            items.selected = false;
            if (this.totalSubscriptionAmount > 0) {
              this.totalSubscriptionAmount =
                this.totalSubscriptionAmount - 7 * items.basePrice;
            }

            index = this.opportunityItems.findIndex(
              (x: any) => x.id == items.id
            );
            this.opportunityItems.splice(index, 1);
          }
        }
      }
    });
    this.opportunitiesForm.controls['originalAmount'].setValue(
      this.totalHardwareAmount +
      this.totalServiceAmount -
      this.opportunitiesForm.value.discountAmount
    );
    this.opportunitiesForm.controls['amountAfterDiscount'].setValue(
      this.totalHardwareAmount + this.totalServiceAmount
    );
    if (!this.opportunitiesForm.controls['rangeLow'].value) {
      this.setRangesAsPerTotalAmnt();
    }
  }

  increaseItemQuantity(item: any) {
    if (item.segmentId === 1) {
      item.quantity++;
      item.amount = item.rate * item.quantity;
      this.totalHardwareAmount =
        Math.round(this.totalHardwareAmount * 100 + Number.EPSILON) / 100 +
        Math.round(item.rate * 100 + Number.EPSILON) / 100;
    }
    if (item.segmentId === 2) {
      item.quantity++;
      item.amount = item.rate * item.quantity;
      this.totalServiceAmount =
        Math.round(this.totalServiceAmount * 100 + Number.EPSILON) / 100 +
        Math.round(item.rate * 100 + Number.EPSILON) / 100;
    }
    if (item.segmentId === 3) {
      item.quantity++;
      item.amount = item.rate * item.quantity;
      this.totalSubscriptionAmount =
        Math.round(this.totalSubscriptionAmount * 100 + Number.EPSILON) / 100 +
        Math.round(item.rate * 100 + Number.EPSILON) / 100;
    }
    let index = this.opportunityItems.findIndex(
      (x: any) => x.itemId == item.itemId
    );
    if (index >= 0 && item.quantity > 0) {
      this.opportunityItems[index] = item;
      this.opportunityItems = [...this.opportunityItems];
    }
    if (!this.opportunitiesForm.controls['rangeLow'].value) {
      this.setRangesAsPerTotalAmnt();
    }
    this.checkTotalAmountAndDiscount();
  }

  checkTotalAmountAndDiscount() {
    let totalAmount = this.totalHardwareAmount + this.totalServiceAmount;
    this.opportunitiesForm.controls['amountAfterDiscount'].setValue(
      totalAmount
    );
    this.opportunitiesForm.controls['originalAmount'].setValue(totalAmount);
    this.addDiscount(this.discountType);
  }

  decreaseItemQuantity(item: any) {
    if (item.quantity <= 1) {
      let index = this.opportunityItems.findIndex(
        (x: any) => x.itemId == item.itemId
      );
      if (index >= 0) {
        this.opportunityItems[index].selected = false;
        this.opportunityItems.splice(index, 1);
        this.opportunityItems = [...this.opportunityItems];
      }
    }
    if (item.segmentId === 1) {
      item.quantity--;
      item.amount = item.rate * item.quantity;
      if (this.totalHardwareAmount > 0) {
        this.totalHardwareAmount = this.totalHardwareAmount - item.rate;
      }
    }
    if (item.segmentId === 2) {
      item.quantity--;
      item.amount = item.rate * item.quantity;
      this.totalServiceAmount = this.totalServiceAmount - item.rate;
    }
    if (item.segmentId === 3) {
      item.quantity--;
      item.amount = item.rate * item.quantity;
      this.totalSubscriptionAmount = this.totalSubscriptionAmount - item.rate;
    }
    let index = this.opportunityItems.findIndex(
      (x: any) => x.itemId == item.itemId
    );
    if (index >= 0 && item.quantity > 0) {
      this.opportunityItems[index] = item;
      this.opportunityItems = [...this.opportunityItems];
    }
    this.checkTotalAmountAndDiscount();
  }

  goBackToTabs() {
    this.showSavedItemsList = false;
  }

  goBackToSelectionList() {
    this.showSavedItemsList = false;
    this.showDisCountPage = false;
  }

  goBackToSavedItemsList() {
    this.showDisCountPage = false;
    this.showSavedItemsList = true;
    // this.opportunityItems.forEach((quotes: any) => {
    //   var items = _.findWhere(this.duplicateListItems, { id: quotes.itemId });
    //   if (!items) return;
    //   quotes.name = items.name
    //   quotes.segmentId = items.segmentId
    //   this.opportunityItems.push(quotes);
    // })
  }

  updateQuotes() {

    // this.showDisCountPage = true;
    // return
    this.setCurrencyValue();
    this.checkNullForAmounts();
    this.opportunityItems.map((quotes: any) => {
      delete quotes?.segmentId;
      delete quotes?.name;
      delete quotes?.id;
    });

    if (
      !this.opportunitiesForm.controls['rangeLow'].value ||
      history.state.isEdit
    ) {
      this.setRangesAsPerTotalAmnt();
    }
    this.opportunitiesForm.controls['opportunityItems'].setValue(
      this.opportunityItems
    );
    this.checkNullForAmounts();
    this.SpinnerService.show();
    this.commonService
      .PutMethod(
        Apiurl.getAllOpportunityList + '/' + this.opportunityItemsID,
        this.opportunitiesForm.getRawValue(),
        true,
        'Loading',
        null
      )
      .then((res: any) => {
        this.SpinnerService.hide();
        if (!res) {
          this.commonService.showError(
            'Error updating Opportunity, Please try again'
          );
          return;
        }
        if (res) {
          if (res?.code?.includes('Duplicate Error')) {
            this.commonService.showError(res.message);
            return;
          }
          this.commonService.showSuccess('Opportunity Updated successfully');
          // this.getAllLookupList();
          //call this funcation for getting contact after add 
          this.commonService.getAllLookupList();
          //call this api for getting opportunity record after update
          this.commonService
            .GetMethod(
              Apiurl.getAllOpportunityList + '/' + this.opportunityItemsID,
              null,
              true,
              'Loading'
            )
            .then(async (res: any) => {
              this.rowData = res;
            })
          if (history.state.from == 'company-detail') {
            let param: NavigationExtras = {
              state: {
                id: history.state.id,
                quote_id: this.opportunityItemsID
              }
            };
            this.router.navigate(['companies-details'], param);
            setTimeout(() => {
              this.layoutService.activeRoute = this.router.url;
              this.layoutService.getcurrentActivateRoute(
                this.layoutService.activeRoute
              );
            }, 50);
          } else if (
            history.state.from == 'contacts' &&
            this.showDisCountPage
          ) {
            let param: NavigationExtras = {
              state: {
                id: history.state.id,
                contact_id: history.state.contact_id,
                quote_id: this.opportunityItemsID
              }
            };
            this.router.navigate(['contacts'], param);
            setTimeout(() => {
              this.layoutService.activeRoute = this.router.url;
              this.layoutService.getcurrentActivateRoute(
                this.layoutService.activeRoute
              );
            }, 50);
          } else {
            this.isShowData = true;
            this.showDisCountPage = false;
            this.showSavedItemsList = false;
            // this.getAllContact();
            return;
          }
        }
      });
  }

  checkNullForAmounts() {
    this.opportunitiesForm.controls['percentageDiscount'].value == null
      ? this.opportunitiesForm.controls['percentageDiscount'].setValue(0)
      : '';
    this.opportunitiesForm.controls['dollarDiscount'].value == null
      ? this.opportunitiesForm.controls['dollarDiscount'].setValue(0)
      : '';
    this.opportunitiesForm.controls['discountAmount'].value == null
      ? this.opportunitiesForm.controls['discountAmount'].setValue(0)
      : '';
    this.opportunitiesForm.controls['totalAmount'].value == null
      ? this.opportunitiesForm.controls['totalAmount'].setValue(0)
      : '';
    this.opportunitiesForm.controls['projectedTotal'].value == null
      ? this.opportunitiesForm.controls['projectedTotal'].setValue(0)
      : '';
    this.opportunitiesForm.controls['rangeHigh'].value == null
      ? this.opportunitiesForm.controls['rangeHigh'].setValue(0)
      : '';
    this.opportunitiesForm.controls['rangeLow'].value == null
      ? this.opportunitiesForm.controls['rangeLow'].setValue(0)
      : '';
  }

  OpenBlock(id: number) {
    this.openBlock = id;
  }

  addQuotes() {
    this.createOpportunitiesForm();
    this.duplicateListItems.map((x: any) => {
      x.selected = false;
    });
    this.totalHardwareAmount = 0;
    this.totalSubscriptionAmount = 0;
    this.totalServiceAmount = 0;
    this.opportunityItems = [];
    this.isShowData = false;
    this.showExtras = false;
    this.showDisCountPage = false;
    this.showSavedItemsList = false;
  }

  async editQuotes() {
    if (this.editOpportunities) {
      this.isShowData = false;
      this.isViewData = true;
      this.showExtras = true;
      this.patchValues();
    } else {
      // Swal.fire('Warning', "You don't have permission to edit data.")
      let obj = {
        title: "Error",
        text: "Opportunities SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok',

      };
      await this.commonService.showErrorMsg(obj).
        then((resp: any) => { });
    }

  }

  async patchValues() {
    let obj = this.rowData;
    this.opportunitiesForm.patchValue({
      customerId: obj.customerId,
      title: obj.title,
      date: obj.date,
      opportunityStatusId: obj.opportunityStatusId,
      probability: obj.probability,
      //  salesRepId: obj.salesRepId,

      // Set SalesrepId from the Obj passed
      salesRepId: this.userDetails.salesRepId,
      memo: obj.memo,
      subsidiaryId: obj.subsidiaryId,
      opportunity: obj.opportunity,
      leadSourceId: obj.leadSourceId,
      forecastTypeId: obj.forecastTypeId,
      originalAmount: obj.originalAmount,
      amountAfterDiscount: obj.amountAfterDiscount,
      discountAmount: obj.discountAmount,
      dollarDiscount: obj.dollarDiscount,
      relatedContactIds: obj.relatedContactIds,
      percentageDiscount: obj.percentageDiscount,
      documentNumber: obj.documentNumber ? obj.documentNumber : '',
      winLossReasonId: obj.winLossReasonId,
      marketIndustryId: obj.marketIndustryId,
      productCategoryId: obj.productCategoryId,
      tiersId: obj.tiersId,
      totalAmount: obj.totalAmount,
      weightedTotal: obj.weightedTotal,
      projectedTotal: obj.projectedTotal,
      rangeHigh: obj.rangeHigh,
      rangeLow: obj.rangeLow,
      actualClose: obj.actualClose,
      netSuiteId: obj.netSuiteId
    });
    if (this.opportunitiesForm.controls['dollarDiscount'].value > 0) {
      this.discountType = 'dollar';
    } else if (
      this.opportunitiesForm.controls['percentageDiscount'].value > 0
    ) {
      this.discountType = 'percent';
    }
    // if (!this.opportunitiesForm.controls['documentNumber'].value) {
    //   this.opportunitiesForm.controls['documentNumber'].setValue(
    //     'Opp' + this.rowData.id
    //   );
    // }
    let amountAfterDiscount;
    if (this.opportunitiesForm.controls['discountAmount'].value) {
      amountAfterDiscount =
        this.totalHardwareAmount +
        this.totalServiceAmount -
        this.opportunitiesForm.controls['discountAmount'].value;
    } else {
      amountAfterDiscount = this.totalHardwareAmount + this.totalServiceAmount;
    }
    this.opportunitiesForm.controls['amountAfterDiscount'].setValue(
      amountAfterDiscount
    );
    // if (this.opportunitiesForm.controls['amountAfterDiscount'].value <= 0 || this.opportunitiesForm.controls['amountAfterDiscount'].value > (this.totalHardwareAmount + this.totalServiceAmount)) {
    //   let discountAmnt = this.opportunitiesForm.controls['discountAmount'].value > (this.totalHardwareAmount + this.totalServiceAmount) ? 0 : this.opportunitiesForm.controls['discountAmount'].value
    //   let amountAfterDiscount = (this.totalHardwareAmount + this.totalServiceAmount) - discountAmnt;
    //   this.opportunitiesForm.controls['amountAfterDiscount'].setValue((amountAfterDiscount))
    // }

    if (obj.subsidiaryId) {
      await this.getAllTemplateList(obj.subsidiaryId);
      await this.getAllItemList(obj.subsidiaryId);
    }
  }

  async cancelEditing() {
    let obj = {
      title: 'Are you sure you want to Cancel?',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    };
    await this.commonService.showConfirm(obj).then((resp: any) => {
      if (resp.isConfirmed) {
        if (history.state.isEdit) {
          this.isShowData = true;
          this.getAllContact();
        } else {
          this.location.back();
        }
      }
    });
  }

  navigateTo(route: string) {
    this.router.navigate([route]);
  }

  inputEvent(event: any) {
    this.ContactList = this.ContactSearch;
    var evens = _.filter(this.ContactSearch, function (contact) {
      return (
        contact.name.toLowerCase().indexOf(event.target.value.toLowerCase()) >
        -1
      );
    });

    this.ContactList = evens;
  }

  async addContact() {
    //redirct to add-contact page if login salesrepid is same as company salesrepid
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    if ((this.rowData.salesRepId !== JSON.parse(this.userDetailsStr).salesRepId) && !JSON.parse(this.userDetailsStr).isAdmin) {
      let obj = {
        title: "Error",
        text: "Contact SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok',

      };
      await this.commonService.showErrorMsg(obj).
        then((resp: any) => { });
    } else {
      let param: NavigationExtras = {
        state: {
          contact_id: 0,
          from: 'opportunity',
          id: this.rowData.customerId,
          quote_id: this.rowData.id
        }
      };
      this.router.navigate(['contacts'], param);
    }

  }

  addDiscount(discountType: string) {
    let totalAmount = this.totalHardwareAmount + this.totalServiceAmount;
    let amountAfterDiscount = 0;
    let discountAmount;
    switch (discountType) {
      case 'dollar':
        if (
          this.opportunitiesForm.controls['dollarDiscount'].value > totalAmount
        ) {
          this.opportunitiesForm.controls['dollarDiscount'].setValue(
            totalAmount
          );
        }
        discountAmount = this.opportunitiesForm.controls['dollarDiscount']
          .value;
        amountAfterDiscount = totalAmount - discountAmount;
        if (amountAfterDiscount < 0) {
          amountAfterDiscount = 0.0;
        }
        break;
      case 'percent':
        if (this.opportunitiesForm.controls['percentageDiscount'].value > 100) {
          this.opportunitiesForm.controls['percentageDiscount'].setValue(100);
        }
        discountAmount =
          totalAmount *
          (this.opportunitiesForm.controls['percentageDiscount'].value / 100);
        amountAfterDiscount = totalAmount - discountAmount;
        if (amountAfterDiscount < 0) {
          amountAfterDiscount = 0;
        }
        break;
      default:
        break;
    }
    this.opportunitiesForm.controls['amountAfterDiscount'].setValue(
      +amountAfterDiscount.toFixed(2)
    );
    this.opportunitiesForm.controls['discountAmount'].setValue(
      +discountAmount.toFixed(2)
    );
  }

  async gotoActivity(type: string) {
    //check login salesrepid is same as company salesrepid then redirect to subasctivity page
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    var userDetails = JSON.parse(this.userDetailsStr)
    if (this.rowData.salesRepId !== userDetails.salesRepId && !userDetails.isAdmin) {
      let obj = {
        title: "Error",
        text: "Activity SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok',

      };
      await this.commonService.showErrorMsg(obj).
        then((resp: any) => { });
    } else {
      let param: NavigationExtras = {
        state: {
          contact_id: 0,
          from: 'opportunity',
          id: this.rowData.customerId,
          qoute_id: this.rowData.id,
          relatedContactIds: this.rowData.relatedContactIds,
          type: type
        }
      };
      this.router.navigate(['sub-activity'], param);
    }


  }

  setSelectedContact(item: any) {
    // history.state.contact_id=item.
    // this.getAllContact()
  }

  selectDiscountType(discountType: string) {
    this.discountType = discountType;
    let totalAmount = this.totalHardwareAmount + this.totalServiceAmount;
    let amountAfterDiscount =
      totalAmount - this.opportunitiesForm.controls['discountAmount'].value;
    this.opportunitiesForm.controls['amountAfterDiscount'].setValue(
      amountAfterDiscount
    );
    this.opportunitiesForm.controls['percentageDiscount'].setValue(null);
    this.opportunitiesForm.controls['dollarDiscount'].setValue(null);
    this.opportunitiesForm.controls['discountAmount'].setValue(null);
  }

  searchCompany(event: any) {
    if (event.target.value != '') {
      this.companyListSearch = this.listCompanies.filter(
        (s: any) =>
          s.companyName
            .toLowerCase()
            .indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.companyListSearch = this.listCompanies;
    }
  }

  clearCompanyDetails() {
    this.searchdropdown = '';
    this.companyListSearch = this.listCompanies;
  }

  seach(event: any, activeTab: string) {
    let val =
      event && event.target && event.target.value
        ? event.target.value.toLowerCase()
        : null;
    if (activeTab === 'templates' && val) {
      this.listTemplates = this.mainlistTemplates.filter(
        (s: any) => s.templateName.toLowerCase().indexOf(val) !== -1
      );
    } else if (val) {
      this.listItems = this.mainlistItems.filter(
        (s: any) => s.name.toLowerCase().indexOf(val) !== -1
      );
    } else {
      this.listTemplates = this.mainlistTemplates;
      this.listItems = this.mainlistItems;
    }
    this.listItems.sort((a: any, b: any) => b.selected - a.selected);
    this.listItems = [...this.listItems];
  }

  serachContacts(event: any) {
    this.contactData = this.ContactSearch.filter(
      (s: any) =>
        s.name.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1
    );
  }

  gotoContact(data: any) {
    let param: NavigationExtras = {
      state: {
        contact_id: data.contactId,
        id: data.companyId,
        from: 'opportunity',
        quote_id: this.rowData.id
      }
    };
    this.router.navigate(['contacts'], param);
  }

  showdetails(value: any) {
    let param: NavigationExtras = {
      state: {
        type: value.type,
        activity_id: value.id
      }
    };
    this.router.navigate(['/activity-details'], param);
  }

  searchParent(event: any) {
    if (event.target.value != '') {
      this.contactListSearch = this.ContactList.filter(
        (s: any) =>
          s.name.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.contactListSearch = this.ContactList;
    }
  }

  updateContacts(obj: any) {
    if (this.rowData && !this.rowData.relatedContactIds) {
      this.rowData.relatedContactIds = [];
    }
    if (
      obj &&
      this.rowData &&
      !this.rowData.relatedContactIds?.includes(obj.contactId)
    ) {
      this.showContactsLoader = true;
      this.rowData.relatedContactIds.push(obj.contactId);
      this.rowData.relatedContactIds = this.rowData.relatedContactIds.filter(
        (ids: any, index: any) =>
          this.rowData.relatedContactIds.indexOf(ids) === index
      );
      this.commonService
        .PutMethod(
          Apiurl.getAllOpportunityList + '/' + this.opportunityItemsID,
          this.rowData,
          true,
          'Loading',
          null
        )
        .then(async (res: any) => {
          this.ngSelectComponent.clearModel();
          this.ngSelectComponent.blur();
          await this.commonService.getAllLookupList();
          this.getAllContact();
          this.showContactsLoader = false;
        });
    } else if (obj) {
      this.commonService.showError('This contact is already added');
    }
  }

  makeQuote() {
    this.rowData.quoteItems = this.rowData.opportunityItems;
    let params: NavigationExtras = {
      state: {
        oppData: this.rowData,
        makeQuotes: true
      }
    };
    this.router.navigate(['add-quotes'], params);
  }

  setRanges(val: any) {
    this.opportunitiesForm.controls['rangeLow'].setValue(val);
    this.opportunitiesForm.controls['rangeHigh'].setValue(val);
    this.opportunitiesForm.controls['projectedTotal'].setValue(val);
    let weightedTotal =
      val * (this.opportunitiesForm.controls['probability'].value / 100);
    this.opportunitiesForm.controls['weightedTotal'].setValue(weightedTotal);
  }

  back() {
    this.location.back();
  }

  gotoCompany(item: any) {
    let param: NavigationExtras = {
      state: {
        id: item.customerId
      }
    };
    this.router.navigate(['companies-details'], param);
    setTimeout(() => {
      this.layoutService.activeRoute = this.router.url;
      this.layoutService.getcurrentActivateRoute(
        this.layoutService.activeRoute
      );
    }, 50);
  }

  clearSearchInput() {
    this.searchInputElement.nativeElement.value = null;
    this.seach(null, this.activeTab);
  }

  getSalesRep(id: any) {
    let salesRep = _.findWhere(this.listLookups?.opportunity?.salesRep, {
      id: id
    });
    if (salesRep) {
      return salesRep.salesRepDescription;
    }
  }

  changeWeightedTotal() {
    let weightedTotal =
      this.opportunitiesForm.controls['projectedTotal'].value *
      (this.opportunitiesForm.controls['probability'].value / 100);
    this.opportunitiesForm.controls['weightedTotal'].setValue(weightedTotal);
  }
}
