import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Apiurl } from './../../shared/route';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CommonProvider } from './../../shared/common';
import { NavigationExtras, Router } from '@angular/router';
import * as _ from 'underscore';
import { NgxSpinnerService } from 'ngx-spinner';
import { LayoutService } from '../../features/layout/layout.service';
import { ChartComponent } from 'ng-apexcharts';
import * as moment from 'moment';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { CalendarEvent, } from 'angular-calendar';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CalendarSchedulerViewComponent } from 'angular-calendar-scheduler';
import { ThemePalette } from '@angular/material/core';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { AuthService } from './../../shared/core/authentication/auth.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  showWorkingWeeksOnly: boolean = false;
  showDetailFields: boolean = false;
  userInfo: any;
  userDetails: any;
  selectedActivityType: any;
  showForm: boolean = false;
  listTransactions: any = [];
  companyListSearch: any = [];
  searchdropdown: any;
  listContacts: any = [];
  listContactIds: any = [];
  listParticipants: any = [];
  speedDialFabButtons = [
    {
      icon: 'note',
      tooltip: 'Task',
      class: 'note-color',
    },
    {
      icon: 'event',
      tooltip: 'Event',
      class: 'event-color',
    },
    {
      icon: 'phone',
      tooltip: 'Phone',
      class: 'phone-color',
    }
  ];
  quoteItems: Array<object> = [];
  quotesWon: Array<object> = [];
  oppItems: Array<object> = [];
  totalQuotesAmountPerMonth: Array<any> = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  totalOppsAmountPerMonth: Array<any> = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  totalQuotesWonPerMonth: Array<any> = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  companyId: any;
  cardList: any = [];
  TodayDate: any = new Date().toDateString();
  @ViewChild('chart') chart: ChartComponent;
  public chartOptions: any;
  public chartOptions1: any;
  public chartOptions2: any;
  filterTypeList: any = [];

  statusName: any
  viewDate: Date = new Date();
  modalData: {
    action: string;
    event: CalendarEvent;
  };
  activeDayIsOpen: boolean = true;
  searchInput: any;
  statusList: any = [];
  relatedCompanyList: any = [];
  relatedContactList: any = [];

  @ViewChild(CalendarSchedulerViewComponent)
  calendarScheduler: CalendarSchedulerViewComponent;

  amountDataForSeries: Array<object> = [];
  listLookups: any = [];
  eventTitle: any;
  dayStartHour = 9
  subActivityForm: FormGroup;
  showProgressBar1: boolean = true;
  showProgressBar2: boolean = true;
  showProgressBar3: boolean = true;
  color: ThemePalette = 'primary';
  mode: ProgressBarMode = 'indeterminate';
  value = 50;
  bufferValue = 75;
  selectedDate: Date = new Date();
  showActivitiesLoader: Boolean = true;
  constructor(
    public common: CommonProvider,
    private router: Router,
    private SpinnerService: NgxSpinnerService,
    private layoutService: LayoutService,
    private modal: NgbModal,
    private fb: FormBuilder,
    private authService: AuthService,
    private cdr: ChangeDetectorRef
  ) {
    this.getUserDetailsFromStorage()
    this.cardList = [
      {
        value: 0,
        name: 'New Leads',
        Image: '../../../assets/img/New_Lead.svg',
        url: 'companies'
      },
      {
        value: 0,
        name: 'New Prospects',
        Image: '../../../assets/img/New_Orispects.svg',
        url: 'contacts-list'
      }


      // Developer : - sumit meghrajani 
      // Date : - 06 - 06 - 2022 12:12 PM
      // hidden the Opportunities and  Quotes for new changes Phase 1 



      // ,
      // {
      //   value: 0,
      //   name: 'Total opportunities',
      //   Image: '../../../assets/img/Total_opp.svg',
      //   url: 'opportunities'
      // },
      // {
      //   value: 0,
      //   name: 'Total Quotes',
      //   Image: '../../../assets/img/Total_quotes.svg',
      //   url: 'quotes'
      // }
    ];
    const myDateValue = this.selectedDate;
    myDateValue.setHours(myDateValue.getHours() + 1);
    this.subActivityForm = this.fb.group({
      id: [''],
      companyId: [null],
      contact: ['0'],
      title: ['', Validators.required],
      description: [''],
      phoneNumber: ['', [Validators.pattern('^[\\d() +-]+$')]],
      startDate: [this.selectedDate, Validators.required],
      endDate: [this.selectedDate, Validators.required],
      startTime: [this.selectedDate, Validators.required],
      endTime: [myDateValue, Validators.required],
      transactionType: [''],
      relatedTransactionId: [''],
      salesRepId: [0],
      statusId: [2]
    });

    // Developer: - sumit meghrajani
    // Date: - 06 - 06 - 2022 11: 50 AM
    //     hidden the Opportunities and Quotes part for new changes Phase 1 
    // this.getAllQuotes();
    // this.getAllOpportunity();
  }


  async ngOnInit(): Promise<void> {
    localStorage.removeItem('activityView')
    this.fetchData();
    this.getAllLookups();
    this.getContactList();
  }
  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  fetchData() {
    // Fetch user details and set in the localStorage
    this.authService.userInformation.subscribe(res => {
      var loginResponse: any = (localStorage.getItem("loginResponse"))
      console.log("loginR3sponse", JSON.parse(loginResponse))
      var loginRes = JSON.parse(loginResponse)
      // if (res != null) {
      this.showActivitiesLoader = false
      this.userInfo = res == null ? loginRes.profile : res.profile;
      localStorage.setItem("Email", this.userInfo.email);
      this.getUserDetails();
      this.calculateTotalLeadsCustomer();
      this.calculateTotalProspectCustomer();
      this.loadOpportunitiesChart();
      this.loadQuotesChart();
      this.loadWonQuotes();
      this.filterTypeList = [
        {
          value: 1,
          name: 'Type'
        },
        {
          value: 1,
          name: 'Phone'
        },
        {
          value: 2,
          name: 'Event'
        },
        {
          value: 3,
          name: 'Task'
        },
        {
          value: 4,
          name: 'Note'
        }
      ];
      // }

    })
  }
  getUserDetails() {

    let token = localStorage.getItem("LoginToken");
    let email = localStorage.getItem("Email");

    this.common.GetMethodWithNoApiUrl(Apiurl.UserInfo + "/" + email, token).then((res) => {
      this.userDetails = res;
      //this.userDetails.isAdmin = true;
      localStorage.setItem("UserDetails", JSON.stringify(this.userDetails));
    });
  }
  getContactList() {
    var companyId = this.subActivityForm.get('companyId')?.value;
    this.SpinnerService.show();
    let param: any = {};
    if (this.userDetails && !this.userDetails.isAdmin) {
      param.subsidiaryId = this.userDetails.subsidiaryId;
      param.salesRepId = this.userDetails.salesRepId;
    } else {
      param = null;
    }
    this.common
      .GetMethod(Apiurl.getContactList, param, true, 'Loading')
      .then((res: any) => {
        if (res) {
          // if (this.showFilter == true) {
          for (let i = 0; i < res.length; i++) {
            if (res[i].companyId == companyId) {
              this.relatedContactList.push(res[i]);
            }
          }
          // }
          for (let i = 0; i < res.length; i++) {
            if (history.state.relatedContactIds) {
              let self = this;
              function forLoop(j: any) {
                if (j >= history.state.relatedContactIds.length) return;
                let id = history.state.relatedContactIds[j];
                if (id === res[i].id) {
                  res[i].isSelect = false;
                  self.listContacts.push(res[i]);
                  self.listContactIds.push(res[i].id);
                  // if (!self.editflag) {
                  if (res[i].role == 'Primary Contact') {
                    self.subActivityForm.controls['contact'].setValue(
                      res[i].id
                    );
                    // }
                  }
                } else {
                  forLoop(j + 1);
                }
              }
              forLoop(0);
            } else if (res[i].companyId == companyId) {
              res[i].isSelect = false;
              this.listContacts.push(res[i]);
              this.listContactIds.push(res[i].id);
              // if (!this.editflag) {
              if (
                history.state.contact_id != null &&
                history.state.contact_id != undefined
              ) {
                var getRefContactInfo: any;
                getRefContactInfo = _.filter(this.listContacts, {
                  id: history.state.contact_id
                });
                var phoneNum = getRefContactInfo[0].phone;
                if (phoneNum != '') {
                  this.subActivityForm.controls['phoneNumber'].setValue(
                    phoneNum
                  );
                }
              }
              if (res[i].role == 'Primary Contact') {
                this.subActivityForm.controls['contact'].setValue(
                  res[i].id
                );
              }
              // }
            }
          }
          if (this.listContacts.length == 1) {
            this.subActivityForm.controls['contact'].setValue(
              this.listContacts[0].id
            );
          }
          this.SpinnerService.hide();
        }
      });
  }

  getAllOpportunity() {
    var companyId = this.subActivityForm.get('companyId')?.value;
    let userDetailsStr: any = '';
    let userDetails: any;

    if (localStorage.getItem('UserDetails')) {
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      userDetails = JSON.parse(userDetailsStr);
    }
    let param: any = {}
    if (userDetails && !userDetails.isAdmin) {
      param.subsidiaryId = userDetails.subsidiaryId;
      param.salesRepId = userDetails.salesRepId;
    }
    this.common
      .GetMethod(Apiurl.getAllOpportunityList, param, true, 'Loading')
      .then((res: any) => {
        if (res) {
          this.SpinnerService.hide();
          this.oppItems = res;

          //     Developer : - sumit meghrajani
          //   Date: - 06 - 06 - 2022 12:14 PM
          // hidden the Opportunities part for new changes Phase 1 

          // this.cardList[2].value = res.length;
          this.oppItems = this.oppItems.filter((opp: any) => opp.opportunityStatus && opp.opportunityStatus.toLowerCase() != 'lost' && opp.opportunityStatus.toLowerCase() != 'closed' && opp.opportunityStatus.toLowerCase() != 'inactive')
          this.calculateOppsAmountForGraph();
          for (let i = 0; i < res.length; i++) {
            if (
              res[i].customerId == companyId &&
              res[i].documentNumber != null
            ) {
              res[i].type = 'Opportunities' + '-' + res[i].id;
              if (res[i].documentNumber != '') {
                res[i].typeval = res[i].documentNumber + '-' + 'Opportunities';
              } else {
                res[i].typeval = 'OPP' + res[i].id + '-' + 'Opportunities';
              }
              this.listTransactions.push(res[i]);
            }
          }
        }
      });
  }

  getAllQuotes() {
    this.SpinnerService.show();
    var companyId = this.subActivityForm.get('companyId')?.value;
    let userDetailsStr: any = '';
    let userDetails: any;

    if (localStorage.getItem('UserDetails')) {
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      userDetails = JSON.parse(userDetailsStr);
    }
    let param: any = {}
    if (userDetails && !userDetails.isAdmin) {
      param.subsidiaryId = userDetails.subsidiaryId;
      param.salesRepId = userDetails.salesRepId;
    }
    this.common
      .GetMethod(Apiurl.getAllQuotes, param, true, 'Loading')
      .then((res: any) => {
        if (res) {
          this.SpinnerService.hide();
          this.quoteItems = res;
          //     Developer : - sumit meghrajani
          //   Date: - 06 - 06 - 2022 12:14 PM
          // hidden the Quotes part for new changes Phase 1 

          //this.cardList[3].value = res.length;
          this.quoteItems = this.quoteItems.filter((quote: any) => quote.status && quote.status.toLowerCase() != 'lost' && quote.status.toLowerCase() != 'closed')
          for (let i = 0; i < res.length; i++) {
            if (
              res[i].customerId == companyId &&
              res[i].documentNumber != null
            ) {
              res[i].type = 'Quotes' + '-' + res[i].id;
              if (res[i].documentNumber != '') {
                res[i].typeval = res[i].documentNumber + '-' + 'Quotes';
              } else {
                res[i].typeval = 'QUOTE' + res[i].id + '-' + 'Quotes';
              }
              this.listTransactions.push(res[i]);
            }
          }
          if (this.listTransactions) {
            for (let index = 0; index < this.listTransactions.length; index++) {
              if (
                history.state.qoute_id != null &&
                history.state.qoute_id != undefined &&
                this.listTransactions[index].id == history.state.qoute_id
              ) {
                this.subActivityForm.controls['relatedTransactionId'].setValue(
                  history.state.qoute_id
                );
                this.subActivityForm.controls[
                  'relatedTransactionId'
                ].updateValueAndValidity();
                this.subActivityForm.controls['transaction'].setValue(
                  this.listTransactions[index].type
                );
                this.subActivityForm.controls[
                  'transaction'
                ].updateValueAndValidity();
                this.subActivityForm.controls['transactionType'].setValue(
                  'Quotes'
                );
                this.subActivityForm.controls[
                  'transactionType'
                ].updateValueAndValidity();
              }
            }
          }
          this.calculateQuotesAmountForGraph();
        }
      });
  }
  calculateQuotesAmountForGraph() {
    // this.showProgressBar1 = false;
    this.quoteItems.sort((a: any, b: any) => (new Date(a.date).getTime() - new Date(b.date).getTime()));
    this.quoteItems = this.quoteItems.filter((quote: any) => new Date(quote.date).getFullYear() === new Date().getFullYear())
    this.quotesWon = JSON.parse(JSON.stringify(this.quoteItems));
    var groupedByMonth = _.groupBy(this.quoteItems, function (item: any) {
      return item.date.substring(0, 7);
    });
    Object.entries(groupedByMonth).forEach((obj: any) => {
      obj[1].total = 0;
      obj[1].forEach((item: any) => {
        let daysInMonth = this.getDaysInMonth(new Date(item.date).getUTCMonth() + 1, new Date(item.date).getFullYear())
        if (item.quoteItems && item.quoteItems.length) {
          item.quoteItems.forEach((quoteItem: any, index: any) => {
            if (quoteItem.segmentId === 3) {
              item.amountAfterDiscount += parseFloat((daysInMonth * (parseFloat((quoteItem.amount / 7).toFixed(2)))).toFixed(2))
            }
            if (index == (item.quoteItems.length - 1)) {
              obj[1].total += item.amountAfterDiscount;
            }
          })
        }
      })
      if (obj[1].total >= 0) {
        let index = +obj[0].split('-')[1]
        this.totalQuotesAmountPerMonth[index - 1] = parseFloat((obj[1].total.toFixed(2)));
      }
    })
    this.loadQuotesChart();
    this.calculateQuotesWonForGraph();
    this.showProgressBar1 = false;
  }
  calculateOppsAmountForGraph() {
    this.oppItems.sort((a: any, b: any) => (new Date(a.date).getTime() - new Date(b.date).getTime()));
    this.oppItems = this.oppItems.filter((quote: any) => new Date(quote.date).getFullYear() === new Date().getFullYear())

    var groupedByMonth = _.groupBy(this.oppItems, function (item: any) {
      return item.date.substring(0, 7);
    });
    Object.entries(groupedByMonth).forEach((obj: any) => {
      obj[1].total = 0;
      obj[1].forEach((item: any) => {
        let daysInMonth = this.getDaysInMonth(new Date(item.date).getUTCMonth() + 1, new Date(item.date).getFullYear())
        if (item.opportunityItems.length) {
          item.opportunityItems.forEach((oppItem: any, index: any) => {
            if (oppItem.segmentId === 3) {
              item.amountAfterDiscount += parseFloat((daysInMonth * (parseFloat((oppItem.amount / 7).toFixed(2)))).toFixed(2))
            }
            if (index == (item.opportunityItems.length - 1)) {
              obj[1].total += item.amountAfterDiscount;
            }
          })
        }
      })
      if (obj[1].total >= 0) {
        let index = +obj[0].split('-')[1]
        this.totalOppsAmountPerMonth[index - 1] = parseFloat(obj[1].total.toFixed(2));
      }
    })
    this.loadOpportunitiesChart();
    this.showProgressBar2 = false;
  }
  calculateQuotesWonForGraph() {
    this.quotesWon = this.quotesWon.filter((quote: any) => quote.status.toLowerCase() === 'won')
    var groupedByMonth = _.groupBy(this.quotesWon, function (item: any) {
      return item.date.substring(0, 7);
    });

    Object.entries(groupedByMonth).forEach((obj: any) => {
      obj[1].total = 0;
      obj[1].forEach((item: any) => {
        let daysInMonth = this.getDaysInMonth(new Date(item.date).getUTCMonth() + 1, new Date(item.date).getFullYear())
        if (item.quoteItems && item.quoteItems.length) {
          item.quoteItems.forEach((quoteItem: any, index: any) => {
            if (quoteItem.segmentId === 3) {
              item.amountAfterDiscount += parseFloat((daysInMonth * (parseFloat((quoteItem.amount / 7).toFixed(2)))).toFixed(2))
            }
            if (index == (item.quoteItems.length - 1)) {
              obj[1].total += item.amountAfterDiscount;
            }
          })
        }
      })
      if (obj[1].total >= 0) {
        let index = +obj[0].split('-')[1]
        this.totalQuotesWonPerMonth[index - 1] = parseFloat((obj[1].total.toFixed(2)));
      }
    })
    this.loadWonQuotes();
    this.showProgressBar3 = false;
  }
  loadQuotesChart() {
    let self = this;
    this.chartOptions = {
      series: [
        {
          name: 'Total',
          data: this.totalQuotesAmountPerMonth,
        }
      ],
      colors: ['#151050'],
      fill: {
        type: "gradient",
        gradient: {
          shade: "dark",
          shadeIntensity: 0.5,
          type: "vertical",
          gradientToColors: ["#7e38fb"],
          opacityFrom: 1,
          opacityTo: 1,
          stops: [13.19, 110.57]
        }
      },
      plotOptions: {
        bar: {
          borderRadius: 7
        }
      },
      chart: {
        height: 350,
        type: 'bar',
        zoom: {
          enabled: false
        }, events: {
          click(event: any, chartContext: any, config: any) {
            let params: NavigationExtras = {
              state: {
                setMonth: config.globals.selectedDataPoints[0][0],
                endDate: self.getDaysInMonth(config.globals.selectedDataPoints[0][0])
              }
            }
            self.router.navigate(['quotes'], params);
          }
        }
      },
      dataLabels: {
        enabled: false
      },
      title: {
        text: 'Open Quotes',
        align: 'left'
      },
      xaxis: {
        title: 'Month',
        categories: [
          'Jan',
          'Feb',
          'Mar',
          'Apr',
          'May',
          'Jun',
          'Jul',
          'Aug',
          'Sep',
          'Oct',
          'Nov',
          'Dec'
        ]
      },
      yaxis: {
        labels: {
          formatter: (val: any, index: any) => {
            return this.kFormatter(val);
          }
        }
      }
    };
    // this.showProgressBar1 = false;
  }

  loadOpportunitiesChart() {
    let self = this;
    this.chartOptions1 = {
      series: [
        {
          name: 'Total',
          data: this.totalOppsAmountPerMonth,
        }
      ],
      colors: ['#151050'],
      fill: {
        type: "gradient",
        gradient: {
          shade: "dark",
          shadeIntensity: 0.5,
          type: "vertical",
          gradientToColors: ["#7e38fb"],
          opacityFrom: 1,
          opacityTo: 1,
          stops: [13.19, 110.57]
        }
      },
      plotOptions: {
        bar: {
          borderRadius: 7,
        }
      },
      chart: {
        height: 350,
        type: 'bar',
        zoom: {
          enabled: false
        }, events: {
          click(event: any, chartContext: any, config: any) {
            let params: NavigationExtras = {
              state: {
                setMonth: config.globals.selectedDataPoints[0][0],
                endDate: self.getDaysInMonth(config.globals.selectedDataPoints[0][0])
              }
            }

            self.router.navigate(['opportunities'], params);
          }
        }
      },

      dataLabels: {
        enabled: false
      },
      title: {
        text: 'Open Opportunities',
        align: 'left'
      },
      xaxis: {
        title: 'Month',
        categories: [
          'Jan',
          'Feb',
          'Mar',
          'Apr',
          'May',
          'Jun',
          'Jul',
          'Aug',
          'Sep',
          'Oct',
          'Nov',
          'Dec'
        ]
      },
      yaxis: {
        labels: {
          /**
          * Allows users to apply a custom formatter function to yaxis labels.
          *
          * @param { String } value - The generated value of the y-axis tick
          * @param { index } index of the tick / currently executing iteration in yaxis labels array
          */
          formatter: (val: any, index: any) => {
            return this.kFormatter(val);
          }
        }
      },
    };
  }

  loadWonQuotes() {
    let self = this;
    this.chartOptions2 = {
      series: [
        {
          name: 'Total',
          data: this.totalQuotesWonPerMonth,
        }
      ],
      colors: ['#151050'],
      fill: {
        type: "gradient",
        gradient: {
          shade: "dark",
          shadeIntensity: 0.5,
          type: "vertical",
          gradientToColors: ["#7e38fb"],
          opacityFrom: 1,
          opacityTo: 1,
          stops: [13.19, 110.57]
        }
      },
      plotOptions: {
        bar: {
          borderRadius: 7
        }
      },
      chart: {
        height: 350,
        type: 'bar',
        zoom: {
          enabled: false
        }, events: {
          click(event: any, chartContext: any, config: any) {
            let params: NavigationExtras = {
              state: {
                quotesStatus: 'Won',
                setMonth: config.globals.selectedDataPoints[0][0],
                endDate: self.getDaysInMonth(config.globals.selectedDataPoints[0][0])
              }
            }
            self.router.navigate(['quotes'], params);
          }
        }
      },

      dataLabels: {
        enabled: false
      },
      title: {
        text: 'Won Quotes',
        align: 'left'
      },
      xaxis: {
        title: 'Month',
        categories: [
          'Jan',
          'Feb',
          'Mar',
          'Apr',
          'May',
          'Jun',
          'Jul',
          'Aug',
          'Sep',
          'Oct',
          'Nov',
          'Dec'
        ]
      },
      yaxis: {
        labels: {
          /**
          * Allows users to apply a custom formatter function to yaxis labels.
          *
          * @param { String } value - The generated value of the y-axis tick
          * @param { index } index of the tick / currently executing iteration in yaxis labels array
          */
          formatter: (val: any, index: any) => {
            return this.kFormatter(val);
          }
        }
      }
    };
    // this.showProgressBar3 = false;
  }


  statusTypeChange(event: any) { }

  gotoActivities(whereToGo: any) {
    let param: NavigationExtras = {
      state: {
        type: whereToGo,
        id: history.state.id,
        from: 'dashboard'
      }
    };
    this.router.navigate(['sub-activity'], param);
  }

  gotoPage(item: any) {
    let param: NavigationExtras = {
      state: {
        stage: item.name == 'New Leads' ? 'Lead' : 'Prospect',
        status:
          item.name == 'New Leads'
            ? 'LEAD-Not Contacted'
            : 'PROSPECT-Negotiation/Review',
        from: 'dashboard'
      }
    };
    if (item.url == 'contacts-list' || item.url == 'companies') {
      this.router.navigate(['companies'], param);
    } else {
      this.router.navigate([item.url]);
    }
    setTimeout(() => {
      this.layoutService.activeRoute = this.router.url;
      this.layoutService.getcurrentActivateRoute(
        this.layoutService.activeRoute
      );
    }, 50);
  }

  //sales repo data
  getAllLookups() {
    this.listLookups = [];
    this.statusList = [];
    this.common
      .lookupList.subscribe(async (res: any) => {
        if (res) {
          this.listLookups = res;
          if (this.listLookups.company && this.listLookups.company.companyList) {
            this.companyListSearch = this.listLookups.company.companyList
            this.relatedCompanyList = this.companyListSearch
            this.statusList = res.activity.statuses;
          }
        } else {
          await this.common.getAllLookupList();
          this.getAllLookups();
        }
      });
  }

  kFormatter(num: any) {
    let number = parseInt(num)
    if (number > 999 && number < 1000000) {
      return '$' + (number / 1000).toFixed(1) + 'K'; // convert to K for numberber from > 1000 < 1 million 
    } else if (number >= 1000000) {
      return '$' + (number / 1000000).toFixed(1) + 'M'; // convert to M for numberber from > 1 million
    } else if (number < 900 && number > 0) {
      return '$' + number; // if value < 1000, nothing to do
    } else {
      return parseInt(number.toFixed(2))
    }

  }

  drop(event: CdkDragDrop<any>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }

  //add activity
  showDetailRecord() {
    this.showDetailFields = true;
  }
  close() {
    this.showDetailFields = false;
    this.showForm = false;
    this.selectedDate = new Date()
  }
  hideDetailRecord() {
    this.showDetailFields = false;
  }
  searchCompany(event: any) {
    if (event.target.value != '') {
      this.companyListSearch = this.relatedCompanyList.filter(
        (s: any) =>
          s.companyName
            .toLowerCase()
            .indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.companyListSearch = this.relatedCompanyList;
    }
  }
  setCompany(obj: any) {
    this.subActivityForm.controls['companyId'].setValue(obj.companyId);
    let data = _.findWhere(this.relatedCompanyList, { id: obj.companyId });
    if (data) {
      this.subActivityForm.controls['salesRepId'].setValue(data.salesRepId);
    }
  }
  clearCompanyDetails() {
    this.searchdropdown = '';
    this.companyListSearch = this.relatedCompanyList;
  }
  getCompanyBaseData() {
    this.listTransactions = [];
    // Developer: - sumit meghrajani
    // Date: - 06 - 06 - 2022 11: 50 AM
    //     hidden the Opportunities and Quotes part for new changes Phase 1 



    // this.getAllOpportunity();
    // this.getAllQuotes();
  }

  SelectParticipant(contactInfo: any) {
    if (contactInfo.isSelect == false) {
      contactInfo.isSelect = true;
    } else if (contactInfo.isSelect == true) {
      contactInfo.isSelect = false;
    }
  }
  // add activity close
  calculateTotalLeadsCustomer() {
    let param: any = {
      st: 'Lead'
    }
    if (this.userDetails && !this.userDetails.isAdmin) {
      param.subsidiaryId = this.userDetails.subsidiaryId;
      param.salesRepId = this.userDetails.salesRepId;
    } else {
      delete param.subsidiaryId
      delete param.salesRepId
    }
    this.common
      .GetMethod(Apiurl.getAllCompanies + '/' + 1 + '/' + 2, param, true, 'Loading')
      .then((response: any) => {
        if (response && response.length)
          this.cardList[0].value = response[0].totalCount;
        console.log("cardList", this.cardList)
      })
  }
  calculateTotalProspectCustomer() {
    let param: any = {
      st: 'Prospect'
    }
    if (this.userDetails && !this.userDetails.isAdmin) {
      param.subsidiaryId = this.userDetails.subsidiaryId;
      param.salesRepId = this.userDetails.salesRepId;
    } else {
      delete param.subsidiaryId
      delete param.salesRepId
    }
    this.common
      .GetMethod(Apiurl.getAllCompanies + '/' + 1 + '/' + 2, param, true, 'Loading')
      .then((response: any) => {
        if (response && response.length)
          this.cardList[1].value = response[0].totalCount;
      })
  }
  getDaysInMonth(month: number, year: number = moment().year()) {
    return new Date(year, month, 0).getDate();
  };
  ngOnDestroy() {
    localStorage.removeItem('activityView')
  }
  getUserDetailsFromStorage() {
    let userDetailsStr: any = '';
    if (localStorage.getItem('UserDetails')) {
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      this.userDetails = JSON.parse(userDetailsStr);
    }
  }
  getSalesRep(id: any) {
    let salesRep = _.findWhere(this.listLookups?.opportunity?.salesRep, { id: id })
    if (salesRep) {
      return salesRep.salesRepDescription;
    };
  }
}
