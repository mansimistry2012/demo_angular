import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment'
import { AuthService } from './../../shared/core/authentication/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  currentApplicationVersion = environment.appVersion;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.Authlogin();
  }

  Authlogin() {
    // fetch user details
    if (!this.authService.getUserDetails()) {
      this.authService.login();
    }

  }
}
