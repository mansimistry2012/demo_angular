import { ExportService } from '../../shared/export.service';
import { Apiurl } from '../../shared/route';
import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonProvider } from '../../shared/common';
import { NavigationExtras, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import * as moment from 'moment';
import * as _ from 'underscore';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { ThemePalette } from '@angular/material/core';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { orderBy, SortDescriptor, State } from '@progress/kendo-data-query';
import { PageChangeEvent } from '@progress/kendo-angular-grid';
import { process } from "@progress/kendo-data-query";
@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.scss']
})
export class QuotesComponent implements OnInit {
  displayedColumns: string[] = [
    'id',
    'name',
    'title',
    'date',
    'expectedClose',
    'status',
    'probability',
    'salesRep',
    'opportunity',
    'forecastType',
    'amountAfterDiscount'
  ];
  Company: any;
  salesRep: any;
  defaultSalesRep: any;
  QuoteStatus: any;
  companyList: any = [];
  salesRepList: any = [];
  companyListSearch: any = [];
  QuoteStatusList: any = [];
  lookupList: any = [];
  searchText: any = '';
  dataSource: any;
  mainDataSource: any = [];
  duplicateDataSource: any = [];
  showFilter: boolean = false;
  quoteList: any = [];
  startDate: any = '';
  endDate: any = '';
  pageIndex: number = 1;
  pageLength: number = 100;
  expectedClose: any = '';
  // @ViewChild(MatSort) sort: any;
  @ViewChild(MatPaginator) paginator: any;
  panelOpenState = false;
  showProgressBar: boolean = false;
  color: ThemePalette = 'primary';
  mode: ProgressBarMode = 'indeterminate';
  value = 50;
  bufferValue = 75;
  skip = 0;
  pageSize = 100;
  public sort: SortDescriptor[] = [
    {
      field: "documentNumber",
      dir: "asc",
    },
  ];
  state: State = {
    sort: [{
      field: "documentNumber",
      dir: "asc",
    }],
  }
  userDetails: any;
  userIsAdmin: boolean = false;
  constructor(private commonService: CommonProvider, private router: Router, private exportService: ExportService) { }
  ngOnInit(): void {
    this.init();
  }
  async init() {
    await this.getAllLookupList();
    this.getUserDetailsFromStorage();
    this.getAllQuotes();
    localStorage.removeItem('rowData');
  }
  manageChartsFilters() {
    if (history.state.endDate && history.state.setMonth) {
      if (history.state.quotesStatus) {
        this.QuoteStatus = history.state.quotesStatus;
      }
      this.startDate = moment(new Date(moment().year(), history.state.setMonth, 1));
      this.endDate = moment(new Date(moment().year(), history.state.setMonth, history.state.endDate - 1));
      this.showFilter = true;
      this.filterTypeChange(null);
    }
  }

  getAllLookupList() {
    let self = this;
    return new Promise(function (resolve, reject) {
      self.commonService.lookupList.subscribe(async (list: any) => {
        if (list) {
          self.lookupList = list;
          if (self.lookupList && self.lookupList.quotes && self.lookupList.quotes.quotesStatus.length) {
            self.QuoteStatusList = self.lookupList?.quotes?.quotesStatus;
            self.companyList = self.commonService.sortAlphabetically(self.lookupList.company?.companyList);
            self.companyListSearch = JSON.parse(JSON.stringify(self.companyList));
            self.salesRepList = self.lookupList.opportunity.salesRep
          }
          resolve(true)
        } else {
          reject(true);
        }
      });
    })
  }
  getUserDetailsFromStorage() {
    let userDetailsStr: any = '';
    if (localStorage.getItem('UserDetails')) {
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      this.userDetails = JSON.parse(userDetailsStr);
      this.userIsAdmin = this.userDetails.isAdmin;
      if (this.userDetails && !this.userDetails.isAdmin) {
        this.defaultSalesRep = this.getSalesRep(this.userDetails.salesRepId);
      }
    }
  }
  getAllQuotes() {
    this.showProgressBar = true;
    let userDetailsStr: any = '';
    let userDetails: any;

    if (localStorage.getItem('UserDetails')) {
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      userDetails = JSON.parse(userDetailsStr);
    }
    let param: any = {}
    if (userDetails && !userDetails.isAdmin) {
      param.subsidiaryId = userDetails.subsidiaryId;
      param.salesRepId = userDetails.salesRepId;
    }
    this.commonService
      .GetMethod(Apiurl.getAllQuotes, param, true, 'Loading')
      .then((resp: any) => {
        if (resp) {
          if (resp.length) {
            resp.forEach((items: any) => {
              let daysInMonth = this.getDaysInMonth(new Date(items.date).getUTCMonth() + 1, new Date(items.date).getFullYear())
              items.quoteItems.forEach((oppItem: any, index: any) => {
                if (oppItem.segmentId === 3) {
                  items.amountAfterDiscount = items.amountAfterDiscount + daysInMonth * (oppItem.rate / 7);
                }
              })
            })
            this.quoteList = JSON.parse(JSON.stringify(resp));
            this.duplicateDataSource = JSON.parse(JSON.stringify(resp))
            this.mainDataSource = resp.splice(0, this.pageLength);
            this.dataSource = new MatTableDataSource(resp);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            this.manageChartsFilters()
            // if (this.defaultSalesRep) {
            //   this.filterTypeChange(null)
            // }
          }
        }
        this.showProgressBar = false;
      });
  }
  addQuotes() {
    this.router.navigate(['add-quotes']);
  }
  inputEvent(event: any) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filterTypeChange(filterValue);
  }
  openFilter() {
    this.showFilter = !this.showFilter;
  }
  viewRow(row: any) {

    var navigationExtras: NavigationExtras = {
      state: { isEdit: true, qoute_id: row.id }
    };
    this.router.navigate(['add-quotes'], navigationExtras);
  }
  clearFilterCompany(event: any) {
    this.Company = null;
    event.stopPropagation();
    this.dataSource = new MatTableDataSource(this.mainDataSource);
    if (this.QuoteStatus != null) {
      this.dataSource = new MatTableDataSource(
        _.filter(this.dataSource.filteredData, { status: this.QuoteStatus })
      );
    }
    if (this.startDate && this.endDate) {
      let x: any = [];
      _.map(this.dataSource.filteredData, function (res) {
        x.push({ ...res, date: moment(res.date).format('DD MMM YYYY') });
      });
      this.dataSource = new MatTableDataSource(x);

      let filterData = this.dataSource.filteredData.filter((data: any) => moment(data.date, 'DD MMM YYYY').isAfter(moment(this.startDate, 'DD MMM YYYY')) && moment(data.date, 'DD MMM YYYY').isBefore(moment(this.endDate, 'DD MMM YYYY')))
      this.dataSource = new MatTableDataSource(
        filterData
      );
    }
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  clearFilterExpectedDatePicker(event: any) {
    this.expectedClose = null;
    event.stopPropagation();
    this.filterTypeChange(event);
  }
  clearFilterQuoteStatus(event: any) {
    this.QuoteStatus = null;
    event.stopPropagation();
    this.dataSource = new MatTableDataSource(this.mainDataSource);
    if (this.Company != null) {
      this.dataSource = new MatTableDataSource(
        _.filter(this.dataSource.filteredData, { name: this.Company })
      );
    }

    if (this.startDate && this.endDate) {
      let x: any = [];
      _.map(this.dataSource.filteredData, function (res) {
        x.push({ ...res, date: moment(res.date).format('DD MMM YYYY') });
      });
      this.dataSource = new MatTableDataSource(x);

      let filterData = this.dataSource.filteredData.filter((data: any) => moment(data.date, 'DD MMM YYYY').isAfter(moment(this.startDate, 'DD MMM YYYY')) && moment(data.date, 'DD MMM YYYY').isBefore(moment(this.endDate, 'DD MMM YYYY')))
      this.dataSource = new MatTableDataSource(
        filterData
      );
    }
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  clearFilterstartDate(event: any) {
    this.startDate = null;
    this.endDate = null;
    event.stopPropagation();
    this.dataSource = new MatTableDataSource(this.mainDataSource);
    if (this.Company != null) {
      this.dataSource = new MatTableDataSource(
        _.filter(this.dataSource.filteredData, { name: this.Company })
      );
    }
    if (this.QuoteStatus != null) {
      this.dataSource = new MatTableDataSource(
        _.filter(this.dataSource.filteredData, { status: this.QuoteStatus })
      );
    }
  }
  filterTypeChange(event: any) {
    // this.mainDataSource = process(this.duplicateDataSource, this.state)
    this.mainDataSource = JSON.parse(JSON.stringify(this.duplicateDataSource))
    // if (this.searchText !== '') {
    //   this.dataSource=  this.searchText.trim().toLowerCase();
    // }
    if (this.QuoteStatus) {
      this.mainDataSource = this.mainDataSource.filter((data: any) => data.statusId === this.QuoteStatus);
    }
    if (this.Company) {
      this.mainDataSource = this.mainDataSource.filter((data: any) => data.customerId === this.Company);
    }
    if (this.salesRep || this.defaultSalesRep) {
      this.mainDataSource = this.mainDataSource.filter((data: any) => (data.salesRep == this.salesRep) || (data.salesRep == this.defaultSalesRep))
    }

    if (this.startDate && this.endDate) {
      let x: any = [];
      _.map(this.mainDataSource, function (res) {
        x.push({ ...res, date: moment(res.date).format('DD MMM YYYY') });
      });
      this.mainDataSource = x;
      var setStartDate = moment(new Date(this.startDate), 'DD MMM YYYY');

      var setEndDate = moment(new Date(this.endDate), 'DD MMM YYYYY');
      let filterData = this.mainDataSource.filter(
        (data: any) =>
          moment(data.date, 'DD MMM YYYY').isSameOrAfter(
            moment(setStartDate, 'DD MMM YYYY')
          ) &&
          moment(data.date, 'DD MMM YYYY').isSameOrBefore(
            moment(setEndDate, 'DD MMM YYYY')
          )
      );
      this.mainDataSource = filterData;
    }
    if (this.expectedClose) {
      let x: any = [];
      _.map(this.mainDataSource, function (res) {
        x.push({ ...res, date: moment(res.date).format('DD MMM YYYY') });
      });
      this.mainDataSource = x;
      let filterData = this.mainDataSource.filter(
        (data: any) =>
          moment(data.expectedClose).format('DD MMM YYYY') ==
          moment(this.expectedClose).format('DD MMM YYYY')
      );
      this.mainDataSource = filterData;
    }
  }
  clearFilterSalesRep(event: any) {
    this.salesRep = null;
    this.defaultSalesRep = null;
    event.stopPropagation();
    this.filterTypeChange(event);
  }
  onChangePage(ev: PageEvent) {
    this.quoteList = JSON.parse(JSON.stringify(this.duplicateDataSource));
    this.pageIndex = ev.pageIndex + 1;
    localStorage.setItem('pageLength', ev.pageSize.toString());
    this.pageLength = ev.pageSize;
    this.mainDataSource = this.quoteList.splice((this.pageIndex - 1) * this.pageLength, this.pageLength);
  }
  clearSearch() {
    this.Company = null;
    this.QuoteStatus = null;
    this.startDate = null;
    this.endDate = null;
    this.expectedClose = null;
    this.salesRep = null;
    this.defaultSalesRep = null;
    history.state.endDate = null;
    history.state.startDate = null;
    this.mainDataSource = process(this.duplicateDataSource, this.state)
    // this.dataSource = new MatTableDataSource(this.mainDataSource);
    // this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
  }
  clearCompanyDetails() {
    this.companyListSearch = this.companyList;
  }
  searchCompany(event: any) {
    if (event.target.value != '') {
      this.companyListSearch = this.companyList.filter(
        (s: any) =>
          s.companyName
            .toLowerCase()
            .indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.companyListSearch = this.companyList;
    }
  }
  clear() {
    this.searchText = '';
    this.getAllQuotes();
  }
  getDaysInMonth(month: number, year: number) {
    return new Date(year, month, 0).getDate();
  };
  public rowCallback(context: any) {
    return {
      dragging: context.dataItem.dragging
    };
  }
  public exportToExcel(grid: any): void {
    grid.saveAsExcel();
  }
  sortQuoteList(sort: any) {
    this.sort = sort;
    this.mainDataSource = {
      data: orderBy(this.quoteList, this.sort),
      total: this.quoteList.length,
    };
  }
  public pageChange(event: PageChangeEvent): void {
    this.quoteList = JSON.parse(JSON.stringify(this.duplicateDataSource));
    this.skip = event.skip;
    this.mainDataSource = {
      data: this.quoteList.slice(this.skip, this.skip + this.pageSize),
      total: this.quoteList.length,
    };
  }
  public onFilter(inputValue: string): void {
    this.mainDataSource = process(this.duplicateDataSource, {
      filter: {
        logic: "or",
        filters: [
          {
            field: 'documentNumber',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'salesRep',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'name',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'title',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'opportunityStatus',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'probability',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'marketIndustry',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'tiers',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'salesRep',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'projectedTotal',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'forecastType',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'amountAfterDiscount',
            operator: 'contains',
            value: inputValue
          },
        ],
      }
    });
    if (inputValue == '' || inputValue == null) {
      this.mainDataSource = process(this.duplicateDataSource, this.state)
    }
    if (this.mainDataSource.data && this.mainDataSource.data.length) {
      this.mainDataSource = [...this.mainDataSource]
    }
    // this.dataBinding.skip = 0;
  }
  refreshGrid() {
    this.ngOnInit();
  }
  exportToCsv(): void {
    let exportObj = {
      'Quote No.': null,
      'Customer': null,
      'Title': null,
      'Date': null,
      'Sales Rep.': null,
      'Status': null,
      'Probability': null,
      'Expected Close': null,
      'Opportunity': '',
      'Forecast Type': null,
      'Total Amount': null,
    }
    var exportData: any = [];
    this.mainDataSource.forEach((data: any) => {
      exportObj = {
        'Quote No.': data.documentNumber ? data.documentNumber : "ESTBG#" + data.id || '-',
        'Customer': data.name,
        'Title': data.title,
        'Date': data.date,
        'Sales Rep.': data.salesRep,
        'Status': data.status,
        'Probability': data.probability,
        'Expected Close': data.expectedClose,
        'Opportunity': data.opportunity ? 'OPP#' + data.opportunity : '-',
        'Forecast Type': data.forecastType,
        'Total Amount': data.amountAfterDiscount,
      };
      exportData.push(exportObj)
    })
    // return
    this.exportService.exportToCsv(exportData, 'Quotes');
  }
  getForeCastTypeById(id: any) {
    if (this.lookupList && this.lookupList.quotes && this.lookupList.quotes.forecastType) {
      let foreCastTypes = this.lookupList.quotes.forecastType
      let forecastType = foreCastTypes.filter((forecast: any) => forecast.id === id)
      return forecastType[0].forecastTypeDescription
    }
  }
  getSalesRep(id: any) {
    let salesRep = _.findWhere(this.lookupList?.opportunity?.salesRep, { id })
    if (salesRep) {
      return salesRep.salesRepDescription;
    };
  }
}