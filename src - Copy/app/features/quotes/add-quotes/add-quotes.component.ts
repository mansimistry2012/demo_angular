import { SpeedDialFabPosition } from './../../../shared/speed-dial-fab/speed-dial-fab.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { Location } from '@angular/common';
import { Router, NavigationExtras } from '@angular/router';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatAccordion } from '@angular/material/expansion';
import { CommonProvider } from '../../../shared/common';
import { Apiurl } from '../../../shared/route';
import { SelectionModel } from '@angular/cdk/collections';
import * as _ from 'underscore';
import { LayoutService } from '../../../features/layout/layout.service';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { ThemePalette } from '@angular/material/core';
import { ProgressBarMode } from '@angular/material/progress-bar';
import * as moment from 'moment';
import { NgSelectComponent } from '@ng-select/ng-select';
@Component({
  selector: 'app-add-quotes',
  templateUrl: './add-quotes.component.html',
  styleUrls: ['./add-quotes.component.scss']
})
export class AddQuotesComponent implements OnInit {
  selectedIndex = 0;
  showProgressBar: boolean = false;
  @ViewChild(MatAccordion) accordion: MatAccordion;
  @ViewChild('ngSelectComponent') ngSelectComponent: NgSelectComponent;
  @ViewChild('searchInputElement') searchInputElement: ElementRef;
  listCompanies: any = [];
  listLookups: any = [];
  listopportunities: any = [];
  listTemplates: any = [];
  mainlistTemplates: any = [];
  listItems: any;
  mainlistItems: any;
  ContactList: any = [];
  duplicateListItems: any = [];
  quotesForm: FormGroup;
  activeTab: string = 'templates';
  displayedColumns: string[] = ['select', 'name', 'quantity', 'basePrice'];
  displayedSubscriptionColumns: string[] = ['select', 'name', 'basePrice'];
  displayQuoteItems: string[] = ['name', 'rate', 'amount', 'quantity'];
  displayQuoteItemsInEdit: string[] = ['name', 'quantity', 'amount'];
  selection = new SelectionModel<any>(true, []);
  totalHardwareAmount: number = 0;
  totalServiceAmount: number = 0;
  totalSubscriptionAmount: number = 0;
  totalSelectedCheckBox: number = 0;
  quoteItems: any = [];
  showSavedItemsList: boolean = false;
  quoteItemsID: any;
  rowData: any;
  isShowData: boolean = false;
  isViewData: boolean = false;
  showExtras: boolean = false;
  openBlock: number = 1;
  showDisCountPage: boolean = false;
  discountType: string = 'dollar';
  activityList: any = [];
  serachInput: any = '';
  ContactSearch: any = [];
  quoteList: any = [];
  relatedActivityArray: any = [];
  companyListSearch: any = [];
  searchdropdown: any;
  searchtext: any = '';
  TodayDate: any = new Date().toDateString();
  contactData: any = [];
  forcastTypeName: any = '';
  showItemList?: boolean = false;
  hideSpinnerofTemplate?: boolean = false;
  isAllowEdit: any;
  /*--LOADER FLAG--*/
  showActivitiesLoader = true;
  showActivitiesNoRecMsg = false;
  showContactsLoader = true;
  showContactsNoRecMsg = false;

  speedDialFabButtons = [
    {
      icon: 'note',
      tooltip: 'Task',
      class: 'note-color'
    },
    {
      icon: 'event',
      tooltip: 'Event',
      class: 'event-color'
    },
    {
      icon: 'phone',
      tooltip: 'Phone',
      class: 'phone-color'
    }
  ];
  SpeedDialFabPosition = SpeedDialFabPosition;
  speedDialFabColumnDirection = 'column';
  speedDialFabPosition = SpeedDialFabPosition.Top;
  speedDialFabPositionClassName = 'speed-dial-container-top';
  contactListSearch: any;

  color: ThemePalette = 'primary';
  mode: ProgressBarMode = 'indeterminate';
  value = 50;
  bufferValue = 75;
  userDetailsStr: any = '';
  userDetails: any;

  constructor(
    private commonService: CommonProvider,
    private fb: FormBuilder,
    private router: Router,
    private location: Location,
    private layoutService: LayoutService,
    private SpinnerService: NgxSpinnerService
  ) {
    if (localStorage.getItem('UserDetails')) {
      this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      this.userDetails = JSON.parse(this.userDetailsStr);
    }
  }

  async ngOnInit(): Promise<void> {
    this.createQuotesForm();
    await this.getAllLookupList();
    this.getAllOpportunityList();
    this.fetchData();
  }

  onPositionChange(position: SpeedDialFabPosition) {
    switch (position) {
      case SpeedDialFabPosition.Bottom:
        this.speedDialFabPositionClassName = 'speed-dial-container-bottom';
        this.speedDialFabColumnDirection = 'column-reverse';
        break;
      default:
        this.speedDialFabPositionClassName = 'speed-dial-container-top';
        this.speedDialFabColumnDirection = 'column';
    }
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.listItems.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.listItems.map((x: any) => {
        x.selected = false;
      });
      this.calculateHardwareItemsForMasterToggle();
      return;
    }
    this.listItems.map((x: any) => {
      x.selected = true;
    });
    this.selection.select(...this.listItems);
    this.calculateHardwareItemsForMasterToggle();
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'
      } row ${row.position + 1}`;
  }

  onSpeedDialFabClicked(btn: any) {
    this.gotoActivity(btn.tooltip);
  }

  createQuotesForm() {
    let relatedContactIds: any = [];
    var newDate = moment().add(1, 'month');
    this.quotesForm = this.fb.group({
      customerId: [null, Validators.required],
      title: [''],
      date: [new Date()],
      dueDateRecieveBy: [newDate],
      statusId: [9, Validators.required],
      probability: [70],
      salesRepId: [null, Validators.required],
      expectedClose: [newDate],
      memo: [''],
      subsidiaryId: [
        this.userDetails && this.userDetails.subsidiaryId
          ? this.userDetails.subsidiaryId
          : 6
      ],
      quoteItems: [],
      opportunity: [0],
      leadSourceId: [null, Validators.required],
      forecastTypeId: [2],
      originalAmount: [0],
      amountAfterDiscount: [0],
      discountAmount: [0],
      relatedContactIds: [relatedContactIds],
      dollarDiscount: [null],
      percentageDiscount: [null],
      documentNumber: [{ value: '', disabled: true }],
      netSuiteId: [0]
    });
  }

  async fetchData() {
    this.getAllActivityList();
    this.getAllQuotes();
    if (history.state.navigationId != 1) {
      if (history.state && history.state.contact_id && history.state.qoute_id) {
        this.isShowData = true;
        this.getAllContact();
      } else if (history.state && history.state.activity_id) {
        this.isShowData = true;
        this.getAllContact();
      } else if (history.state && history.state.isEdit) {
        this.isShowData = history.state.isEdit;
        this.getAllContact();
      } else if (history.state.makeQuotes && history.state.oppData) {
        this.rowData = history.state.oppData;
        this.makeQuote();
      } else {
      }
    } else {
      this.router.navigate(['dashboard']);
      setTimeout(() => {
        this.layoutService.activeRoute = this.router.url;
        this.layoutService.getcurrentActivateRoute(
          this.layoutService.activeRoute
        );
      }, 50);
    }
    await this.getAllCompaniesList();
  }

  getAllQuotes() {
    let userDetailsStr: any = '';
    let userDetails: any;

    if (localStorage.getItem('UserDetails')) {
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      userDetails = JSON.parse(userDetailsStr);
    }
    let param: any = {};
    if (userDetails && !userDetails.isAdmin) {
      param.subsidiaryId = userDetails.subsidiaryId;
      param.salesRepId = userDetails.salesRepId;
    }
    this.commonService
      .GetMethod(Apiurl.getAllQuotes, param, true, 'Loading')
      .then((res: any) => {
        if (res) {
          this.quoteList = res;
          this.getAllActivityList();
        }
      });
  }

  async makeQuote() {
    this.patchValues();
    this.getDocumentNumberFromRowData();
    // await this.getAllCompaniesList();
    this.manageQuoteItems(this.rowData);
    this.quotesForm.controls['opportunity'].setValue(this.rowData.id);
    // this.quoteItemsID = history.state.qoute_id;
  }

  getAllContact() {
    this.quoteItemsID = this.quoteItemsID
      ? this.quoteItemsID
      : history.state.qoute_id;
    if (!this.quoteItemsID) return;
    this.commonService
      .GetMethod(
        Apiurl.getAllQuotes + '/' + this.quoteItemsID,
        null,
        true,
        'Loading'
      )
      .then(async (res: any) => {
        this.isAllowEdit =
          res.salesRepId == this.userDetails.salesRepId ? true : false;
        if (res) {
          this.rowData = res;
          this.forcastTypeName = _.findWhere(
            this.listLookups.quotes?.forecastType,
            {
              id: this.rowData.forecastTypeId
            }
          );
          if (this.forcastTypeName) {
            this.forcastTypeName = this.forcastTypeName.forecastTypeDescription;
          }

          this.getDocumentNumberFromRowData();
          // this.getAllCompaniesList();
          this.getItemList(res.id);
          if (
            history.state.contact_id != null &&
            history.state.contact_id != undefined
          ) {
            if (!res?.relatedContactIds?.includes(history.state.contact_id)) {
              let contactIDList: any = [];

              this.patchValues();
              //condition for if relatedContactIds is null
              if (this.quotesForm.controls['relatedContactIds'].value) {
                contactIDList = this.quotesForm.controls['relatedContactIds']
                  .value;
              }
              contactIDList?.push(history.state.contact_id);
              this.quotesForm.controls['relatedContactIds'].setValue(
                contactIDList
              );
              this.updateQuotes();
            }
          }
          this.getAllContactList();
          this.patchValues();
        }
      });
  }

  getDocumentNumberFromRowData() {
    if (this.rowData.documentNumber) {
      this.rowData.documentId = this.rowData.documentNumber;
    } else {
      this.rowData.documentId = 'QUO#' + this.rowData.id;
    }
  }

  getAllCompaniesList() {
    let self = this;
    return new Promise(function (resolve, reject) {
      if (self.listLookups) {
        self.listCompanies = self.listLookups.company.companyList;
        self.listCompanies = self.commonService.sortAlphabetically(
          self.listCompanies
        );
        self.companyListSearch = JSON.parse(JSON.stringify(self.listCompanies));
        if (self.rowData) {
          let company = _.findWhere(self.listCompanies, {
            companyId: self.rowData.customerId
          });
          let statusName = _.findWhere(self.listLookups.quotes.quotesStatus, {
            id: self.rowData.statusId
          });
          self.rowData.companyName = company?.companyName;
          self.rowData.status = statusName?.statusDescription;
        }
        self.setCompanyDetails();
        resolve(true);
      } else {
        reject(true);
      }
    });
  }

  setCompanyDetails() {
    if (history.state && history.state.id) {
      let company = _.findWhere(this.listCompanies, {
        companyId: this.rowData?.customerId
          ? this.rowData?.customerId
          : history.state.id
      });
      this.quotesForm.patchValue({
        customerId: company.companyId,
        salesRepId: company?.salesRepId,
        leadSourceId: company?.leadSourceId
      })
      this.setCompany(company)
    }
  }

  getAllActivityList() {
    this.showActivitiesLoader = true;
    this.showActivitiesNoRecMsg = false;
    let userDetailsStr: any = '';
    let userDetails: any;

    if (localStorage.getItem('UserDetails')) {
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      userDetails = JSON.parse(userDetailsStr);
    }
    let param: any = {};
    if (userDetails && !userDetails.isAdmin) {
      param.subsidiaryId = userDetails.subsidiaryId;
      param.salesRepId = userDetails.salesRepId;
    }
    this.commonService
      .GetMethod(Apiurl.getAllActivties, param, true, 'Loading..')
      .then((resp: any) => {
        //
        if (resp.length != 0) {
          this.activityList = resp;
          this.relatedActivityArray = [];
          for (let index = 0; index < this.activityList.length; index++) {
            if (
              this.activityList[index].transactionType.includes('quote') ||
              this.activityList[index].transactionType.includes('Quote') ||
              this.activityList[index].transactionType.includes('Quotes') ||
              this.activityList[index].transactionType.includes('quotes')
            ) {
              if (this.activityList[index].relatedTransactionId != 0) {
                let data = _.findWhere(this.quoteList, {
                  id: this.activityList[index].relatedTransactionId
                });
                if (
                  this.activityList[index].relatedTransactionId ==
                  history.state.qoute_id
                ) {
                  //
                }
                if (data) {
                  if (data.id == history.state.qoute_id) {
                    this.relatedActivityArray.push(this.activityList[index]);
                  }
                }
              }
            }
          }
          this.showActivitiesLoader = false;
          if (this.relatedActivityArray.length == 0) {
            this.showActivitiesNoRecMsg = true;
          }
        } else {
          this.showActivitiesNoRecMsg = true;
          this.showActivitiesLoader = false;
        }
      });
  }

  getAllContactList() {
    this.showContactsLoader = true;
    this.showContactsNoRecMsg = false;
    this.commonService.lookupList.subscribe((resp: any) => {
      if (resp.length != 0) {
        this.ContactList = resp?.contact?.contactList;
        this.contactListSearch = JSON.parse(JSON.stringify(this.ContactList));
        let data: any = [];
        if (
          this.rowData.relatedContactIds &&
          this.rowData.relatedContactIds.length
        ) {
          this.contactData = [];
          data = this.rowData.relatedContactIds.forEach((id: any, i: any) => {
            let contacts = _.findWhere(this.ContactList, { contactId: id });
            if (contacts) {
              this.contactData.push(contacts);
            }
            if (i == this.rowData.relatedContactIds.length - 1) {
              this.showContactsLoader = false;
              if (this.contactData.length == 0) {
                this.showContactsNoRecMsg = true;
              } else {
                this.showContactsNoRecMsg = false;
              }
            }
          });
        } else {
          this.showContactsLoader = false;
          if (this.contactData.length == 0) {
            this.showContactsNoRecMsg = true;
          } else {
            this.showContactsNoRecMsg = false;
          }
        }
        this.ContactSearch = this.contactData;
      }
      // this.showContactsLoader = false;
      // if (this.contactData.length == 0) {
      //   this.showContactsNoRecMsg = true;
      // }
    });
  }

  getAllLookupList() {
    let self = this;
    return new Promise(function (resolve, reject) {
      self.commonService.lookupList.subscribe((list: any) => {
        if (list) {
          self.listLookups = list;

          resolve(true);
        } else {
          reject(true);
        }
      });
    });
  }

  getAllOpportunityList() {
    let userDetailsStr: any = '';
    let userDetails: any;

    if (localStorage.getItem('UserDetails')) {
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      userDetails = JSON.parse(userDetailsStr);
    }
    let param: any = {};
    if (userDetails && !userDetails.isAdmin) {
      param.subsidiaryId = userDetails.subsidiaryId;
      param.salesRepId = userDetails.salesRepId;
    }
    this.commonService
      .GetMethod(Apiurl.getAllOpportunityList, param, true, 'Loading')
      .then((res: any) => {
        if (res) {
          this.listopportunities = res;
        }
      });
  }

  getAllTemplateList(subsidiaryId: any) {
    this.SpinnerService.show();
    let SubsidiaryId = '?subsidiaryId=' + subsidiaryId;
    this.commonService
      .GetMethod(Apiurl.getAllTemplates + SubsidiaryId, null, true, 'Loading')
      .then((res: any) => {
        this.SpinnerService.hide();
        if (res) {
          this.hideSpinnerofTemplate = true;
          this.listTemplates = res.filter((item: any) => {
            if (item.templateName) {
              item.selected = false;
              item.added = false;
              return true;
            } else {
              return false;
            }
          });
          this.mainlistTemplates = this.listTemplates;
        }
      });
  }

  getAllItemList(subsidiaryId: any) {
    this.SpinnerService.show();
    let self = this;
    return new Promise(function (resolve, reject) {
      let SubsidiaryId = '?subsidiaryId=' + subsidiaryId;
      self.commonService
        .GetMethod(Apiurl.getAllItems + SubsidiaryId, null, true, 'Loading')
        .then((res: any) => {
          self.SpinnerService.hide();
          self.showItemList = true;
          if (res) {
            self.duplicateListItems = res;
            self.duplicateListItems.map((x: any) => {
              x.selected = false;
            });
            resolve(true);
          } else {
            reject(true);
          }
        });
    });
  }

  onSubmit(form: FormGroup) { }

  saveAndContinue(shouldRoute?: boolean) {
    // this.showSavedItemsList = true
    // return

    if (this.quotesForm.invalid) {
      if (
        this.quotesForm.controls.customerId.status == 'INVALID' ||
        this.quotesForm.controls.statusId.status == 'INVALID' ||
        this.quotesForm.controls.salesRepId.status == 'INVALID'
      ) {
        this.selectedIndex = 0;
      } else if (this.quotesForm.controls.leadSourceId.status == 'INVALID') {
        this.selectedIndex = 1;
      }
      this.quotesForm.markAllAsTouched();
      return;
    }
    if (this.quoteItems?.length == 0) {
      this.commonService.showError(
        'Please select at least one Item to add Quote'
      );
      return;
    }
    if (this.quoteItemsID) {
      this.updateQuotes(true);
      return;
    }
    if (history.state.contact_id) {
      let contactIDList: any = [];
      contactIDList = this.quotesForm.controls['relatedContactIds'].value;
      contactIDList?.push(history.state.contact_id);
      this.quotesForm.controls['relatedContactIds'].setValue(contactIDList);
    }
    if (this.quoteItems) {
      this.quoteItems.map((quotes: any) => {
        delete quotes?.segmentId;
        delete quotes?.name;
        delete quotes?.id;
        delete quotes?.opportunityId;
      });
    }
    this.quotesForm.controls['quoteItems'].setValue(this.quoteItems);
    this.quotesForm.controls['dollarDiscount'].setValue(0);
    this.quotesForm.controls['percentageDiscount'].setValue(0);
    this.quotesForm.controls['quoteItems'].setValue(this.quoteItems);
    // }
    !this.quotesForm.value.percentageDiscount
      ? this.quotesForm.controls['percentageDiscount'].setValue(0)
      : '';
    !this.quotesForm.value.dollarDiscount
      ? this.quotesForm.controls['dollarDiscount'].setValue(0)
      : '';
    !this.quotesForm.value.discountAmount
      ? this.quotesForm.controls['discountAmount'].setValue(0)
      : '';
    !this.quotesForm.value.opportunity
      ? this.quotesForm.controls['opportunity'].setValue(0)
      : '';
    this.SpinnerService.show();
    this.commonService
      .PostMethod(
        Apiurl.getAllQuotes,
        this.quotesForm.value,
        true,
        'Loading',
        null
      )
      .then(async (res: any) => {
        this.SpinnerService.hide();
        if (!res || res.internalServerError) {
          // Swal.fire("Error", "Error updating contacts, Please try again", "error").then(() => {
          // });
          this.commonService.showError(
            'Something went wrong Quote was not saved, Please try again'
          );
          return;
        }
        if (res?.code?.includes('Duplicate Error')) {
          // Swal.fire(res?.code, res.message, "error").then(() => {
          // });
          this.commonService.showError(res.message);
          return;
        }
        if (res) {
          this.commonService.showSuccess('Quote Added successfully');
          if (history.state.makeQuotes) {
            this.rowData.opportunityStatusId = 13;
            this.commonService
              .PutMethod(
                Apiurl.getAllOpportunityList + '/' + this.rowData.id,
                this.rowData,
                true,
                'Loading',
                null
              )
              .then((res: any) => { });
          }
          this.quoteItemsID = res;
          // this.getItemList(res);
          if (history.state.from == 'company-detail') {
            let param: NavigationExtras = {
              state: {
                id: history.state.id,
                quote_id: this.quoteItemsID
              }
            };
            this.router.navigate(['companies-details'], param);
            setTimeout(() => {
              this.layoutService.activeRoute = this.router.url;
              this.layoutService.getcurrentActivateRoute(
                this.layoutService.activeRoute
              );
            }, 50);
          } else if (history.state.from == 'contacts') {
            let param: NavigationExtras = {
              state: {
                id: history.state.id,
                contact_id: history.state.contact_id,
                quote_id: this.quoteItemsID
              }
            };
            this.router.navigate(['contacts'], param);
            setTimeout(() => {
              this.layoutService.activeRoute = this.router.url;
              this.layoutService.getcurrentActivateRoute(
                this.layoutService.activeRoute
              );
            }, 50);
          } else if (!history.state.isEdit || history.state.makeQuotes) {
            this.isShowData = true;
            this.showDisCountPage = false;
            this.showSavedItemsList = false;
            this.getAllContact();
            return;
          }
          // if (this.showSavedItemsList) {
          //   this.showSavedItemsList = false;
          //   this.showDisCountPage = true;
          // } else if (this.showDisCountPage) {
          //   this.isShowData = true;
          //   this.showDisCountPage = false;
          //   this.showSavedItemsList = false;
          //   this.getAllContact();
          // } else {
          //   this.showSavedItemsList = true;
          //   this.showDisCountPage = false;
          await this.getItemList(this.quoteItemsID);
          // }
        }
      });
  }

  saveAndNext() {
    this.showProgressBar = true;
    this.quoteItems.map((quotes: any, i: any) => {
      var items = _.findWhere(this.duplicateListItems, { id: quotes.itemId });
      if (!items) return;
      quotes.name = items.name;
      quotes.segmentId = items.segmentId;
    });
    this.showSavedItemsList = true;
    this.calculateSummarForQuotes();
  }

  saveOppItems() {
    this.showSavedItemsList = false;
    this.showDisCountPage = true;
  }

  getItemList(id: any) {
    return new Promise((resolve, reject) => {
      this.commonService
        .GetMethod(Apiurl.getAllQuotes + '/' + id, null, true, 'Loading')
        .then((res: any) => {
          if (res) {
            this.showSavedItemsList = true;
            this.quoteItems = [];
            this.manageQuoteItems(res);
            resolve(true);
          } else {
            reject(true);
          }
        });
    });
  }
  manageQuoteItems(res: any) {
    // res.quoteItems.forEach((quotes: any, i: any) => {
    //   var items = _.findWhere(this.duplicateListItems, {
    //     id: quotes.itemId
    //   });
    //   if (items) {
    //     quotes.name = items.name;
    //     quotes.segmentId = items.segmentId;
    //     this.quoteItems.push(quotes);
    //   }
    // });
    this.quoteItems = res.quoteItems;
    if (this.isShowData) {
      this.listTemplates.forEach((template: any) => {
        let templateItemsIds: any = [],
          quoteItemsIds: any = [];
        template.items = template.items.filter(
          (item: any) => item.segmentId != 0
        );
        templateItemsIds = _.pluck(template.items, 'itemId');
        templateItemsIds = templateItemsIds.filter((ids: number) => ids != 0);
        quoteItemsIds = _.pluck(res.quoteItems, 'itemId');
        var check =
          JSON.stringify(templateItemsIds) === JSON.stringify(quoteItemsIds);
        if (check) {
          template.selected = true;
        } else {
          template.selected = false;
        }
      });
    }
    this.calculateSummarForQuotes();
  }

  calculateSummarForQuotes() {
    // this.showProgressBar = true;
    // if (this.isShowData) {
    this.totalHardwareAmount = 0;
    this.totalServiceAmount = 0;
    this.totalSubscriptionAmount = 0;
    let quoteItems: any = [];
    this.quoteItems?.forEach((y: any, i: any) => {
      var x = _.findWhere(this.duplicateListItems, { id: y.itemId });
      if (x?.segmentId == 1) {
        x.selected = true;
        y.name = x.name;
        // x.quantity = y.quantity;
        // x.amount = y.amount;
        this.totalHardwareAmount = this.totalHardwareAmount + y.amount;
        quoteItems.push(y);
      } else if (x?.segmentId == 2) {
        // x.quantity = y.quantity;
        // x.amount = y.amount;
        y.name = x.name;
        x.selected = true;
        this.totalServiceAmount = this.totalServiceAmount + y.amount;
        quoteItems.push(y);
      } else if (x?.segmentId == 3) {
        x.selected = true;
        y.name = x.name;
        // x.quantity = y.quantity;
        // x.amount = y.amount;
        this.totalSubscriptionAmount = this.totalSubscriptionAmount + y.amount;
        quoteItems.push(y);
      } else {
        x.selected = false;
      }
      if (i === this.quoteItems?.length - 1) {
        this.showProgressBar = false;
      }
    });

    // this.quoteItems = quoteItems;
    // }
    let amountAfterDiscount;
    if (this.quotesForm.controls['discountAmount'].value) {
      amountAfterDiscount =
        this.totalHardwareAmount +
        this.totalServiceAmount -
        this.quotesForm.controls['discountAmount'].value;
    } else {
      amountAfterDiscount = this.totalHardwareAmount + this.totalServiceAmount;
    }
    this.quotesForm.controls['amountAfterDiscount'].setValue(
      amountAfterDiscount
    );
  }

  goTo(tab: string) {
    this.activeTab = tab;
    let segmentId: any;
    switch (this.activeTab) {
      case 'hardware':
        segmentId = 1;
        break;
      case 'services':
        segmentId = 2;
        break;
      case 'subscriptions':
        segmentId = 3;
        break;
      default:
        break;
    }
    this.mainlistItems = this.duplicateListItems.filter(
      (item: any) => item.segmentId == segmentId
    );
    this.listItems = this.mainlistItems;
    this.listItems.sort((a: any, b: any) => b.selected - a.selected);
    this.listItems = [...this.listItems];
  }

  calculateTotalValue(items: any) {
    items.selected = !items.selected;
    if (items.selected) {
      items.items.forEach((object: any) => {
        object.selected = true;
      });
      this.totalSelectedCheckBox++;
      items.added = false;
      this.selectItems(items);
    } else {
      items.items.forEach((object: any) => {
        object.selected = false;
      });
      this.totalSelectedCheckBox--;
      items.added = true;
      this.selectItems(items);
    }
  }

  calculateHardwareItems(items: any) {
    let params = {
      amount: 0,
      itemId: 0,
      quantity: 0,
      rate: 0
    };
    items.selected = !items.selected;
    this.listTemplates.forEach((x: any) => {
      var itemInTemplates = _.findWhere(x.items, { itemId: items.id });
      if (itemInTemplates) {
        itemInTemplates.selected = !itemInTemplates.selected;
      }
      var selectedItems = x.items.filter(
        (object: any) => object.selected === true
      );
      if (selectedItems.length === x.items.length) {
        x.selected = true;
      } else {
        x.selected = false;
      }
    });
    if (items.selected) {
      items.quantity = 1;
      params.quantity = items.quantity;
      params.amount = items.basePrice * items.quantity;
      params.itemId = items.id;
      params.rate = items.basePrice;
      this.quoteItems.push(params);
      this.totalHardwareAmount =
        Math.round(this.totalHardwareAmount * 100 + Number.EPSILON) / 100 +
        items.basePrice;
    } else {
      let index = this.quoteItems.findIndex((x: any) => x.itemId == items.id);
      if (index < 0) return;
      items.quantity = 0;
      items.selected = false;
      this.totalHardwareAmount =
        Math.round(this.totalHardwareAmount * 100 + Number.EPSILON) / 100 -
        items.basePrice;
      this.quoteItems.splice(index, 1);
    }
    this.quotesForm.controls['originalAmount'].setValue(
      this.totalHardwareAmount + this.totalServiceAmount
    );
    this.quotesForm.controls['amountAfterDiscount'].setValue(
      this.totalHardwareAmount + this.totalServiceAmount
    );
    this.listItems.sort((a: any, b: any) => b.selected - a.selected);
    this.listItems = [...this.listItems];
  }

  calculateServiceItems(items: any) {
    items.selected = !items.selected;
    let params = {
      amount: 0,
      itemId: 0,
      quantity: 0,
      rate: 0
    };
    this.listTemplates.forEach((x: any) => {
      var itemInTemplates = _.findWhere(x.items, { itemId: items.id });
      if (itemInTemplates) {
        itemInTemplates.selected = !itemInTemplates.selected;
      }
      var selectedItems = x.items.filter(
        (object: any) => object.selected === true
      );
      if (selectedItems.length === x.items.length) {
        x.selected = true;
      } else {
        x.selected = false;
      }
    });
    if (items.selected) {
      items.quantity = 1;
      params.quantity = items.quantity;
      params.amount = items.basePrice * items.quantity;
      params.itemId = items.id;
      params.rate = items.basePrice;
      this.quoteItems.push(params);
      this.totalServiceAmount =
        Math.round(this.totalServiceAmount * 100 + Number.EPSILON) / 100 +
        items.basePrice;
    } else {
      let index = this.quoteItems.findIndex((x: any) => x.itemId == items.id);
      if (index < 0) return;
      items.quantity = 0;
      items.selected = false;
      this.totalServiceAmount =
        Math.round(this.totalServiceAmount * 100 + Number.EPSILON) / 100 -
        items.basePrice;
      this.quoteItems.splice(index, 1);
    }
    this.quotesForm.controls['originalAmount'].setValue(
      this.totalHardwareAmount + this.totalServiceAmount
    );
    this.quotesForm.controls['amountAfterDiscount'].setValue(
      this.totalHardwareAmount + this.totalServiceAmount
    );
    this.listItems.sort((a: any, b: any) => b.selected - a.selected);
    this.listItems = [...this.listItems];
  }

  calculateSubscriptionItems(items: any) {
    items.selected = !items.selected;
    let params = {
      amount: 0,
      itemId: 0,
      quantity: 0,
      rate: 0
    };
    this.listTemplates.forEach((x: any) => {
      var itemInTemplates = _.findWhere(x.items, { itemId: items.id });
      if (itemInTemplates) {
        itemInTemplates.selected = !itemInTemplates.selected;
      }
      var selectedItems = x.items.filter(
        (object: any) => object.selected === true
      );
      if (selectedItems.length === x.items.length) {
        x.selected = true;
      } else {
        x.selected = false;
      }
    });
    if (items.selected) {
      items.quantity = 1;
      params.quantity = items.quantity;
      params.amount = 7 * items.basePrice * items.quantity;
      params.itemId = items.id;
      params.rate = 7 * items.basePrice;
      this.quoteItems.push(params);
      this.totalSubscriptionAmount =
        Math.round(this.totalSubscriptionAmount * 100 + Number.EPSILON) / 100 +
        7 * items.basePrice;
    } else {
      let index = this.quoteItems.findIndex((x: any) => x.itemId == items.id);
      if (index < 0) return;
      items.quantity = 0;
      items.selected = false;
      this.totalSubscriptionAmount =
        Math.round(this.totalSubscriptionAmount * 100 + Number.EPSILON) / 100 -
        7 * items.basePrice;
      this.quoteItems.splice(index, 1);
    }
    this.listItems.sort((a: any, b: any) => b.selected - a.selected);
    this.listItems = [...this.listItems];
  }

  selectItems(templateItems: any) {
    let index;

    templateItems.items.forEach((objs: any) => {
      let params = {
        amount: 0,
        itemId: 0,
        quantity: 0,
        rate: 0
      };
      var items = _.findWhere(this.duplicateListItems, { id: objs.itemId });
      if (items) {
        if (items.segmentId === 1) {
          if (templateItems.selected) {
            items.quantity = 1;
            params.quantity = items.quantity;
            params.amount = items.basePrice * items.quantity;
            params.itemId = objs.itemId;
            params.rate = items.basePrice;
            items.selected = true;
            this.totalHardwareAmount =
              this.totalHardwareAmount + items.basePrice;
            this.quoteItems.push(params);
          } else {
            items.selected = false;
            if (this.totalHardwareAmount > 0) {
              this.totalHardwareAmount =
                this.totalHardwareAmount - items.basePrice;
            }
            index = this.quoteItems.findIndex((x: any) => x.id == items.id);
            this.quoteItems.splice(index, 1);
          }
        }
        if (items.segmentId === 2) {
          if (templateItems.selected) {
            items.quantity = 1;
            params.quantity = items.quantity;
            params.amount = items.basePrice * items.quantity;
            params.itemId = objs.itemId;
            params.rate = items.basePrice;
            items.selected = true;
            this.totalServiceAmount = this.totalServiceAmount + items.basePrice;
            //check if quoteitem is null
            if (this.quoteItems) {
              this.quoteItems.push(params);
            }
          } else {
            items.selected = false;
            if (this.totalServiceAmount > 0) {
              this.totalServiceAmount =
                this.totalServiceAmount - items.basePrice;
            }
            index = this.quoteItems.findIndex((x: any) => x.id == items.id);
            this.quoteItems.splice(index, 1);
          }
        }
        if (items.segmentId === 3) {
          if (templateItems.selected) {
            items.quantity = 1;
            params.quantity = items.quantity;
            params.amount = 7 * items.basePrice * items.quantity;
            params.itemId = objs.itemId;
            params.rate = 7 * items.basePrice;
            items.selected = true;
            this.totalSubscriptionAmount =
              this.totalSubscriptionAmount + 7 * items.basePrice;

            //check if quoteitem is null
            if (this.quoteItems) {
              this.quoteItems.push(params);
            }
            this.quoteItems.push(params);
          } else {
            items.selected = false;
            if (this.totalSubscriptionAmount > 0) {
              this.totalSubscriptionAmount =
                this.totalSubscriptionAmount - 7 * items.basePrice;
            }

            index = this.quoteItems.findIndex((x: any) => x.id == items.id);
            this.quoteItems.splice(index, 1);
          }
        }
      }
    });
    this.quotesForm.controls['originalAmount'].setValue(
      this.totalHardwareAmount + this.totalServiceAmount
    );
    this.quotesForm.controls['amountAfterDiscount'].setValue(
      this.totalHardwareAmount + this.totalServiceAmount
    );
  }

  increaseItemQuantity(item: any) {
    if (item.segmentId === 1) {
      item.quantity++;
      item.amount = item.rate * item.quantity;
      this.totalHardwareAmount =
        Math.round(this.totalHardwareAmount * 100 + Number.EPSILON) / 100 +
        Math.round(item.rate * 100 + Number.EPSILON) / 100;
    }
    if (item.segmentId === 2) {
      item.quantity++;
      item.amount = item.rate * item.quantity;
      this.totalServiceAmount =
        Math.round(this.totalServiceAmount * 100 + Number.EPSILON) / 100 +
        Math.round(item.rate * 100 + Number.EPSILON) / 100;
    }
    if (item.segmentId === 3) {
      item.quantity++;
      item.amount = item.rate * item.quantity;
      this.totalSubscriptionAmount =
        Math.round(this.totalSubscriptionAmount * 100 + Number.EPSILON) / 100 +
        Math.round(item.rate * 100 + Number.EPSILON) / 100;
    }
    let index = this.quoteItems.findIndex((x: any) => x.itemId == item.itemId);
    if (index >= 0) {
      this.quoteItems[index] = item;
      this.quoteItems = [...this.quoteItems];
    }
    this.checkTotalAmountAndDiscount();
  }

  decreaseItemQuantity(item: any) {
    if (item.quantity <= 1) {
      let index = this.quoteItems.findIndex(
        (x: any) => x.itemId == item.itemId
      );
      if (index >= 0) {
        this.quoteItems[index].selected = false;
        this.quoteItems.splice(index, 1);
        this.quoteItems = [...this.quoteItems];
      }
    }
    if (item.segmentId === 1) {
      item.quantity--;
      item.amount = item.rate * item.quantity;
      if (this.totalHardwareAmount > 0) {
        this.totalHardwareAmount = this.totalHardwareAmount - item.rate;
      }
    }
    if (item.segmentId === 2) {
      item.quantity--;
      item.amount = item.rate * item.quantity;
      this.totalServiceAmount = this.totalServiceAmount - item.rate;
    }
    if (item.segmentId === 3) {
      item.quantity--;
      item.amount = item.rate * item.quantity;
      this.totalSubscriptionAmount = this.totalSubscriptionAmount - item.rate;

      // this.totalSubscriptionAmount =
      //   Math.round(this.totalSubscriptionAmount * 100 + Number.EPSILON) / 100 -
      //   Math.round(item.rate * 100 + Number.EPSILON) / 100;
      // this.totalSubscriptionAmount = this.totalSubscriptionAmount;
    }
    let index = this.quoteItems.findIndex((x: any) => x.itemId == item.itemId);
    if (index >= 0) {
      this.quoteItems[index] = item;
      this.quoteItems = [...this.quoteItems];
    }
    this.checkTotalAmountAndDiscount();
  }

  goBackToSelectionList() {
    this.showSavedItemsList = false;
    this.showDisCountPage = false;
  }

  goBackToSavedItemsList() {
    this.showDisCountPage = false;
    this.showSavedItemsList = true;
    // this.quoteItems.forEach((quotes: any) => {
    //   var items = _.findWhere(this.duplicateListItems, { id: quotes.itemId });
    //   if (!items) return;
    //   quotes.name = items.name;
    //   quotes.segmentId = items.segmentId;
    //   this.quoteItems.push(quotes);
    // });
  }

  updateQuotes(showSavedItemsList: boolean = false) {
    // set condition if quoteitem getting null

    if (this.quoteItems) {
      this.quoteItems.map((quotes: any) => {
        delete quotes?.segmentId;
        delete quotes?.name;
        delete quotes?.id;
        delete quotes?.opportunityId;
      });
    }

    // if (this.rowData && !this.quoteItems.length) {
    //   this.quotesForm.controls['quoteItems'].setValue(this.rowData.quoteItems);
    // } else {
    this.quotesForm.controls['quoteItems'].setValue(this.quoteItems);
    // }
    !this.quotesForm.value.percentageDiscount
      ? this.quotesForm.controls['percentageDiscount'].setValue(0)
      : '';
    !this.quotesForm.value.dollarDiscount
      ? this.quotesForm.controls['dollarDiscount'].setValue(0)
      : '';
    !this.quotesForm.value.discountAmount
      ? this.quotesForm.controls['discountAmount'].setValue(0)
      : '';
    !this.quotesForm.value.opportunity
      ? this.quotesForm.controls['opportunity'].setValue(0)
      : '';
    let url = '';
    if (this.quoteItemsID) {
      url = Apiurl.getAllQuotes + '/' + this.quoteItemsID;
    } else {
      url = Apiurl.getAllQuotes;
    }
    this.SpinnerService.show();
    this.commonService
      .PutMethod(url, this.quotesForm.value, true, 'Loading', null)
      .then((res: any) => {
        this.SpinnerService.hide();
        if (!res) {
          this.commonService.showError(
            'Error updating Quote, Please try again'
          );
          return;
        }
        if (res) {
          this.commonService.showSuccess('Quote Updated successfully');
          //call this funcation for getting contact after add 
          this.commonService.getAllLookupList();
          //call this api for getting quote record after update
          this.commonService
            .GetMethod(
              Apiurl.getAllQuotes + '/' + this.quoteItemsID,
              null,
              true,
              'Loading'
            )
            .then(async (res: any) => { this.rowData = res; })
          if (history.state.from == 'company-detail') {
            let param: NavigationExtras = {
              state: {
                id: history.state.id,
                quote_id: this.quoteItemsID
              }
            };
            this.router.navigate(['companies-details'], param);
            setTimeout(() => {
              this.layoutService.activeRoute = this.router.url;
              this.layoutService.getcurrentActivateRoute(
                this.layoutService.activeRoute
              );
            }, 50);
          } else if (history.state.from == 'contacts') {
            let param: NavigationExtras = {
              state: {
                id: history.state.id,
                contact_id: history.state.contact_id,
                quote_id: this.quoteItemsID
              }
            };
            this.router.navigate(['contacts'], param);
            setTimeout(() => {
              this.layoutService.activeRoute = this.router.url;
              this.layoutService.getcurrentActivateRoute(
                this.layoutService.activeRoute
              );
            }, 50);
          } else if (
            history.state.from != 'company-detail' &&
            history.state.from != 'contacts'
          ) {
            this.isShowData = true;
            this.showDisCountPage = false;
            this.showSavedItemsList = false;
            // this.getAllContact();
            return;
          }
        }
      });
  }

  OpenBlock(id: number) {
    this.openBlock = id;
  }

  addQuotes() {
    this.createQuotesForm();
    this.quoteItems = [];
    this.duplicateListItems.map((x: any) => {
      x.selected = false;
    });
    this.totalHardwareAmount = 0;
    this.totalSubscriptionAmount = 0;
    this.totalServiceAmount = 0;
    this.isShowData = false;
    this.showExtras = false;
    this.showDisCountPage = false;
    this.showSavedItemsList = false;
  }

  async editQuotes() {
    this.showSavedItemsList = false;
    //Set Admin and salesRepId validation
    let userDetailsStr: any = '';
    // Set Validation for isAdmin and salesRepId
    userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    this.userDetails = JSON.parse(userDetailsStr);
    if (this.rowData.salesRepId !== this.userDetails.salesRepId && !this.userDetails.isAdmin) {
      let obj = {
        title: 'Error',
        text: "Quotes SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => { });
    } else {
      this.isShowData = false;
      this.isViewData = true;
      this.showExtras = true;
      this.patchValues();
    }
  }

  async patchValues() {
    var newDate = moment().add(1, 'month');
    let obj = this.rowData;
    this.quotesForm.patchValue({
      customerId: obj.customerId,
      title: obj.title,
      date: obj.date,
      dueDateRecieveBy: obj.dueDate ? obj.dueDate : newDate,
      statusId: obj.statusId,
      probability: obj.probability,
      // set Sales Rep Id
      salesRepId: obj.salesRepId,
      expectedClose: obj.expectedClose ? obj.expectedClose : newDate,
      memo: obj.memo,
      subsidiaryId: obj.subsidiaryId,
      opportunity: obj.opportunity,
      leadSourceId: obj.leadSourceId,
      forecastTypeId: obj.forecastTypeId,
      originalAmount: obj.originalAmount,
      amountAfterDiscount: obj.amountAfterDiscount,
      discountAmount: obj.discountAmount,
      dollarDiscount: obj.dollarDiscount,
      relatedContactIds: obj.relatedContactIds,
      percentageDiscount: obj.percentageDiscount,
      documentNumber: obj.documentNumber,
      netSuiteId: obj.netSuiteId
    });
    if (this.quotesForm.controls['dollarDiscount'].value > 0) {
      this.discountType = 'dollar';
    } else if (this.quotesForm.controls['percentageDiscount'].value > 0) {
      this.discountType = 'percent';
    }
    let amountAfterDiscount;
    if (this.quotesForm.controls['discountAmount'].value) {
      amountAfterDiscount =
        this.totalHardwareAmount +
        this.totalServiceAmount -
        this.quotesForm.controls['discountAmount'].value;
    } else {
      amountAfterDiscount = this.totalHardwareAmount + this.totalServiceAmount;
    }
    this.quotesForm.controls['amountAfterDiscount'].setValue(
      amountAfterDiscount
    );
    if (obj.subsidiaryId) {
      await this.getAllTemplateList(obj.subsidiaryId);
      await this.getAllItemList(obj.subsidiaryId);
    }
  }
  async cancelEditing() {
    let obj = {
      title: 'Are you sure you want to Cancel?',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    };
    await this.commonService.showConfirm(obj).then((resp: any) => {
      if (resp.isConfirmed) {
        if (history.state.isEdit) {
          this.isShowData = true;
          // this.getAllContact();
        } else {
          this.location.back();
        }
      }
    });
  }

  navigateTo(route: string) {
    this.router.navigate([route]);
  }

  inputEvent(event: any) {
    this.ContactList = this.ContactSearch;
    var evens = _.filter(this.ContactSearch, function (contact) {
      return (
        contact.name.toLowerCase().indexOf(event.target.value.toLowerCase()) >
        -1
      );
    });

    this.ContactList = evens;
  }

  async addContact() {
    //check if login saleasrepid is same as company salesrepid then redirect to contact page
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    if ((this.rowData.salesRepId !== JSON.parse(this.userDetailsStr).salesRepId) && !JSON.parse(this.userDetailsStr).isAdmin) {
      let obj = {
        title: 'Error',
        text: "Contact SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => { });

    } else {
      let param: NavigationExtras = {
        state: {
          contact_id: 0,
          from: 'qoute',
          id: this.rowData.customerId,
          qoute_id: this.rowData.id
        }
      };
      this.router.navigate(['contacts'], param);
    }
  }

  addDiscount(discountType: string) {
    let totalAmount = this.totalHardwareAmount + this.totalServiceAmount;
    let amountAfterDiscount = 0;
    let discountAmount;
    switch (discountType) {
      case 'dollar':
        if (this.quotesForm.controls['dollarDiscount'].value > totalAmount) {
          this.quotesForm.controls['dollarDiscount'].setValue(totalAmount);
        }
        discountAmount = this.quotesForm.controls['dollarDiscount'].value;
        amountAfterDiscount = totalAmount - discountAmount;
        if (amountAfterDiscount < 0) {
          amountAfterDiscount = 0.0;
        }
        break;
      case 'percent':
        if (this.quotesForm.controls['percentageDiscount'].value > 100) {
          this.quotesForm.controls['percentageDiscount'].setValue(100);
        }
        discountAmount =
          totalAmount *
          (this.quotesForm.controls['percentageDiscount'].value / 100);
        amountAfterDiscount = totalAmount - discountAmount;
        if (amountAfterDiscount < 0) {
          amountAfterDiscount = 0;
        }
        break;
      default:
        break;
    }
    this.quotesForm.controls['amountAfterDiscount'].setValue(
      +amountAfterDiscount.toFixed(2)
    );
    this.quotesForm.controls['discountAmount'].setValue(
      +discountAmount.toFixed(2)
    );
  }

  checkTotalAmountAndDiscount() {
    let totalAmount = this.totalHardwareAmount + this.totalServiceAmount;
    this.quotesForm.controls['amountAfterDiscount'].setValue(totalAmount);
    this.quotesForm.controls['originalAmount'].setValue(totalAmount);
    this.addDiscount(this.discountType);
  }

  async gotoActivity(type: string) {
    //check if loging salesrepid is same as company salesrep id then redirect to sub-activity page
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    var userDetails = JSON.parse(this.userDetailsStr)
    //check login salesrepid is same as company salesrepid then redirect to subasctivity page && if user is admin then also redirect to subactivity page
    if ((this.rowData.salesRepId !== userDetails.salesRepId) && !userDetails.isAdmin) {
      let obj = {
        title: 'Error',
        text: "Activity SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => { });

    } else {
      let param: NavigationExtras = {
        state: {
          contact_id: 0,
          from: 'qoute',
          id: this.rowData.customerId,
          qoute_id: this.rowData.id,
          relatedContactIds: this.rowData.relatedContactIds,
          type: type
        }
      };
      this.router.navigate(['sub-activity'], param);
    }
  }

  setSelectedContact(item: any) {
    //
    // history.state.contact_id=item.
    // this.getAllContact()
  }

  selectDiscountType(discountType: string) {
    this.discountType = discountType;
    let totalAmount = this.totalHardwareAmount + this.totalServiceAmount;
    this.quotesForm.controls['amountAfterDiscount'].setValue(totalAmount);
    this.quotesForm.controls['percentageDiscount'].setValue(null);
    this.quotesForm.controls['dollarDiscount'].setValue(null);
    this.quotesForm.controls['discountAmount'].setValue(null);
  }

  searchCompany(event: any) {
    if (event.target.value != '') {
      this.companyListSearch = this.listCompanies.filter(
        (s: any) =>
          s.companyName
            .toLowerCase()
            .indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.companyListSearch = this.listCompanies;
    }
  }

  clearCompanyDetails() {
    this.searchdropdown = '';
    this.companyListSearch = this.listCompanies;
  }

  seach(event: any, activeTab: string) {
    let val =
      event && event.target && event.target.value
        ? event.target.value.toLowerCase()
        : null;
    if (activeTab === 'templates' && val) {
      this.listTemplates = this.mainlistTemplates.filter(
        (s: any) => s.templateName.toLowerCase().indexOf(val) !== -1
      );
    } else if (val) {
      this.listItems = this.mainlistItems.filter(
        (s: any) => s.name.toLowerCase().indexOf(val) !== -1
      );
    } else {
      this.listTemplates = this.mainlistTemplates;
      this.listItems = this.mainlistItems;
    }
    this.listItems.sort((a: any, b: any) => b.selected - a.selected);
    this.listItems = [...this.listItems];
  }

  serachContacts(event: any) {
    this.contactData = this.ContactSearch.filter(
      (s: any) =>
        s.name.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1
    );
  }

  gotoContact(data: any) {
    let param: NavigationExtras = {
      state: {
        contact_id: data.contactId,
        id: data.companyId,
        from: 'quote'
      }
    };
    this.router.navigate(['contacts'], param);
  }

  showdetails(value: any) {
    ////
    let param: NavigationExtras = {
      state: {
        type: value.type,
        activity_id: value.id
      }
    };
    this.router.navigate(['/activity-details'], param);
  }

  searchParent(event: any) {
    if (event.target.value != '') {
      this.contactListSearch = this.ContactList.filter(
        (s: any) =>
          s.name.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.contactListSearch = this.ContactList;
    }
  }

  onTabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.selectedIndex = tabChangeEvent.index;
  }

  updateContacts(obj: any) {
    if (this.rowData && !this.rowData.relatedContactIds) {
      this.rowData.relatedContactIds = [];
    }
    if (obj && !this.rowData.relatedContactIds.includes(obj.contactId)) {
      this.showContactsLoader = true;
      this.rowData.relatedContactIds.push(obj.contactId);
      this.rowData.relatedContactIds = this.rowData.relatedContactIds.filter(
        (ids: any, index: any) =>
          this.rowData.relatedContactIds.indexOf(ids) === index
      );

      this.commonService
        .PutMethod(
          Apiurl.getAllQuotes + '/' + this.quoteItemsID,
          this.rowData,
          true,
          'Loading',
          null
        )
        .then(async (res: any) => {
          this.ngSelectComponent.clearModel();
          this.ngSelectComponent.blur();
          await this.commonService.getAllLookupList();
          this.getAllContact();
          this.showContactsLoader = false;
        });
    } else if (obj) {
      this.commonService.showError('This contact is already added');
      //Swal.fire('This contact is already added');
    }
  }

  gotoCompany(item: any) {
    let param: NavigationExtras = {
      state: {
        id: item.customerId
      }
    };
    this.router.navigate(['companies-details'], param);
    setTimeout(() => {
      this.layoutService.activeRoute = this.router.url;
      this.layoutService.getcurrentActivateRoute(
        this.layoutService.activeRoute
      );
    }, 50);
  }

  async setCompany(obj: any) {
    //get userinfo from localstorage
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    var userDetails = JSON.parse(this.userDetailsStr)
    //check login salesrepid is same as company salesrepid if not display popup message and blank dropdown && also check user is admin or not
    if (obj.salesRepId !== userDetails.salesRepId && !userDetails.isAdmin) {
      let obj = {
        title: 'Error',
        text: "Quotes SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => {
        this.quotesForm.controls['customerId'].setValue('');
      });
    }
    else {
      if (obj) {
        this.quotesForm.controls['salesRepId'].setValue(obj.salesRepId)
        this.quotesForm.controls['customerId'].setValue(obj.companyId);
        let data = _.findWhere(this.listCompanies, {
          companyId: obj.companyId
        });
        this.quotesForm.controls['leadSourceId'].setValue(data.leadSourceId);
        if (obj && obj.subsidiaryId) {
          await this.getAllItemList(obj.subsidiaryId);
          await this.getAllTemplateList(obj.subsidiaryId);
        }
      } else {
        this.showItemList = false;
      }
    }
  }

  clearSearchInput() {
    this.searchInputElement.nativeElement.value = null;
    this.seach(null, this.activeTab);
  }

  calculateHardwareItemsForMasterToggle() {
    let params = {
      amount: 0,
      itemId: 0,
      quantity: 0,
      rate: 0
    };
    this.listItems.forEach((items: any, id: any) => {
      if (items.segmentId === 1) {
        this.listTemplates.forEach((x: any) => {
          var itemInTemplates = _.findWhere(x.items, { itemId: items.id });
          if (itemInTemplates) {
            itemInTemplates.selected = !itemInTemplates.selected;
          }
          var selectedItems = x.items.filter(
            (object: any) => object.selected === true
          );
          if (selectedItems.length === x.items.length) {
            x.selected = true;
          } else {
            x.selected = false;
          }
        });
        if (items.selected) {
          items.quantity = 1;
          params.quantity = items.quantity;
          params.amount = items.basePrice * items.quantity;
          params.itemId = items.id;
          params.rate = items.basePrice;
          this.quoteItems.push(params);
          this.totalHardwareAmount =
            Math.round(this.totalHardwareAmount * 100 + Number.EPSILON) / 100 +
            items.basePrice;
        } else {
          let index = this.quoteItems.findIndex((x: any) => {
            x.itemId == items.id;
          });
          if (index < 0) return;
          items.quantity = 0;
          items.selected = false;
          this.totalHardwareAmount =
            Math.round(this.totalHardwareAmount * 100 + Number.EPSILON) / 100 -
            items.basePrice;
          this.quoteItems.splice(index, 1);
        }
        this.quotesForm.controls['originalAmount'].setValue(
          this.totalHardwareAmount + this.totalServiceAmount
        );
        this.quotesForm.controls['amountAfterDiscount'].setValue(
          this.totalHardwareAmount + this.totalServiceAmount
        );
      }
    });
    this.listItems.sort((a: any, b: any) => b.selected - a.selected);
    this.listItems = [...this.listItems];
  }

  calculateServiceItemsForMasterToggle() {
    let params = {
      amount: 0,
      itemId: 0,
      quantity: 0,
      rate: 0
    };
    this.listItems.forEach((items: any) => {
      if (items.segmentId === 2) {
        this.listTemplates.forEach((x: any) => {
          var itemInTemplates = _.findWhere(x.items, { itemId: items.id });
          if (itemInTemplates) {
            itemInTemplates.selected = !itemInTemplates.selected;
          }
          var selectedItems = x.items.filter(
            (object: any) => object.selected === true
          );
          if (selectedItems.length === x.items.length) {
            x.selected = true;
          } else {
            x.selected = false;
          }
        });
        if (items.selected) {
          items.quantity = 1;
          params.quantity = items.quantity;
          params.amount = items.basePrice * items.quantity;
          params.itemId = items.id;
          params.rate = items.basePrice;
          this.quoteItems.push(params);
          this.totalServiceAmount =
            Math.round(this.totalServiceAmount * 100 + Number.EPSILON) / 100 +
            items.basePrice;
        } else {
          let index = this.quoteItems.findIndex(
            (x: any) => x.itemId == items.id
          );
          if (index < 0) return;
          items.quantity = 0;
          items.selected = false;
          this.totalServiceAmount =
            Math.round(this.totalServiceAmount * 100 + Number.EPSILON) / 100 -
            items.basePrice;
          this.quoteItems.splice(index, 1);
        }
        this.quotesForm.controls['originalAmount'].setValue(
          this.totalHardwareAmount + this.totalServiceAmount
        );
        this.quotesForm.controls['amountAfterDiscount'].setValue(
          this.totalHardwareAmount + this.totalServiceAmount
        );
      }
    });
    this.listItems.sort((a: any, b: any) => b.selected - a.selected);
    this.listItems = [...this.listItems];
  }

  calculateSubscriptionItemsForMasterToggle() {
    let params = {
      amount: 0,
      itemId: 0,
      quantity: 0,
      rate: 0
    };
    this.listItems.forEach((items: any) => {
      if (items.segmentId === 3) {
        this.listTemplates.forEach((x: any) => {
          var itemInTemplates = _.findWhere(x.items, { itemId: items.id });
          if (itemInTemplates) {
            itemInTemplates.selected = !itemInTemplates.selected;
          }
          var selectedItems = x.items.filter(
            (object: any) => object.selected === true
          );
          if (selectedItems.length === x.items.length) {
            x.selected = true;
          } else {
            x.selected = false;
          }
        });
        if (items.selected) {
          items.quantity = 1;
          params.quantity = items.quantity;
          params.amount = 7 * items.basePrice * items.quantity;
          params.itemId = items.id;
          params.rate = 7 * items.basePrice;
          this.quoteItems.push(params);
          this.totalSubscriptionAmount =
            Math.round(this.totalSubscriptionAmount * 100 + Number.EPSILON) /
            100 +
            7 * items.basePrice;
        } else {
          let index = this.quoteItems.findIndex(
            (x: any) => x.itemId == items.id
          );
          if (index < 0) return;
          items.quantity = 0;
          items.selected = false;
          this.totalSubscriptionAmount =
            Math.round(this.totalSubscriptionAmount * 100 + Number.EPSILON) /
            100 -
            7 * items.basePrice;
          this.quoteItems.splice(index, 1);
        }
      }
    });
    this.listItems.sort((a: any, b: any) => b.selected - a.selected);
    this.listItems = [...this.listItems];
  }
}
