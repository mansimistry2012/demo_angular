import { LayoutService } from '../../features/layout/layout.service';
import { Component, OnInit, ViewChild, TemplateRef, ElementRef, NgZone } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { CommonProvider } from './../../shared/common';
import { Apiurl } from './../../shared/route';
import * as _ from 'underscore';
import { Subject } from 'rxjs';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { CalendarView, CalendarEvent } from 'angular-calendar';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectComponent } from '@ng-select/ng-select';
import { ApiService, Maps } from '../../api.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-conacts',
  templateUrl: './conacts.component.html',
  styleUrls: ['./conacts.component.scss']
})
export class ConactsComponent implements OnInit {
  salesRepId: any;
  pageIndex: number = 1;
  pageLength: number = 10;
  totalCount: number = 0;
  defaultCompanyIdDuringPost: any;
  showCal: boolean = false;
  onSpeedDialFabClicked(btn: any) {
    this.gotoActivities(btn.tooltip);
  }
  @ViewChild('ngSelectComponent') ngSelectComponent: NgSelectComponent;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild('searchAdress')
  public searchElementRefBaddress: ElementRef;
  ContactForm: any;
  selectedIndex = 0;
  searchdropdown: any;
  CompanyList: any[] = [];
  RoleList: any[] = [];
  SubsidiariesList: any[] = [];
  StateList: any[] = [];
  CountryList: any[] = [];
  accountId: any;
  companyId: any;
  netSuiteId: any;
  openBlock: any = 1;
  ContactId: any = 0;
  companyDetails: any = [];
  IsEditClick: boolean = false;
  StateName: any;
  CountryName: any;
  phoneLength = 0;
  phoneData: any;
  postCodeLength = 0;
  postCodeData: any;
  filterTypeList: any = [];
  selectedView: any = 'Agenda';
  dataSource: Array<any> = [];
  companyListSearch: any;
  companyType: any;
  agendaList: any = [];
  /*--LOADER FLAG--*/
  showActivitiesLoader = true;
  showActivitiesNoRecMsg = false;
  showCompaniesLoader = true;
  showCompaniesNoRecMsg = false;
  showOppertunitiesLoader = true;
  showOppertunitiesNoRecMsg = false;
  showQuoteLoader = true;
  showQuoteNoRecMsg = false;
  agendaTableColumn: string[] = [
    'type',
    'title',
    'startDate',
    'duration',
    'salesRep',
    'statusId',
    'relatedCustomer',
    'relatedTransaction',
    'relatedContact',
    'phone'
  ];
  mainagendaList: any = [];
  relatedCompanyList: any = [];
  relatedContactList: any = [];
  //serching & filteration
  searchText: any = '';
  statusType: any;
  eventType: any;
  eventTitle: any;
  relatedCompany: any = [];
  relatedTransaction: any;
  relatedContact: any = [];
  //scheduler
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;

  view: any = 'Agenda';
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  modalData: {
    action: string;
    event: CalendarEvent;
  };
  relatedcompanies: any;
  listLookups: any = [];

  refresh: Subject<any> = new Subject();
  showFilter: boolean = false;
  events: CalendarEvent[];
  filterType: any;
  activeDayIsOpen: boolean = true;
  searchInput: any;
  statusList: any = [];
  searchedInput: any;

  oppertunityData: any = [];
  quoteData: any = [];

  customer_id: any;
  selectedDate: Date = new Date();
  subActivityForm: FormGroup;
  listContacts: any = [];
  listContactIds: any = [];
  userDetailsStr: any = '';
  userDetails: any;
  salesRepIdContact: any;
  constructor(
    private formBuilder: FormBuilder,
    public common: CommonProvider,
    public router: Router,
    private modal: NgbModal,
    private route: ActivatedRoute,
    private commonService: CommonProvider,
    private layoutService: LayoutService,
    private apiService: ApiService,
    private ngZone: NgZone,
    private SpinnerService: NgxSpinnerService // private location: Location
  ) {
    if (localStorage.getItem('UserDetails')) {
      this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      this.userDetails = JSON.parse(this.userDetailsStr);
    }
    this.ContactForm = this.formBuilder.group({
      firstName: [
        '',
        Validators.compose([
          Validators.pattern(
            '^[A-Za-zs ! @#$%^&*()_+=-`~\\][{}|;:/.,? >< A-Za-z ]*$'
          ),
          Validators.required
        ])
      ],
      middlename: [
        '',
        Validators.compose([
          Validators.pattern(
            '^[A-Za-zs ! @#$%^&*()_+=-`~\\][{}|;:/.,? >< A-Za-z ]*$'
          )
        ])
      ],
      lastName: [
        '',
        Validators.compose([
          Validators.pattern(
            '^[A-Za-zs ! @#$%^&*()_+=-`~\\][{}|;:/.,? >< A-Za-z ]*$'
          ),
          Validators.required
        ])
      ],
      companyId: [null, Validators.compose([Validators.required])],
      phone: ['', [Validators.pattern('^[\\d() +-]+$')]],
      mainphone: ['', [Validators.pattern('^[\\d() +-]+$')]],
      email: [
        '',
        Validators.compose([Validators.maxLength(100), Validators.email])
      ],
      jobTitle: [''],
      roleId: [''],
      subsidiaryId: [
        this.userDetails && this.userDetails.subsidiaryId
          ? this.userDetails.subsidiaryId
          : 6
      ],
      dateCreated: [''],
      addressLine1: [''],
      addressLine2: [''],
      city: [''],
      idRes: [''],
      id: [''],
      stateId: [''],
      postCode: [
        '',
        Validators.compose([
          Validators.maxLength(6),
          Validators.pattern('[0-9]{0,6}')
        ])
      ],
      countryId: [1],
      ContactId: [''],
      relatedCompanyIds: [''],
      companyType: ['', Validators.required]
    });
  }

  initAutocomplete(maps: Maps) {
    //Billing Search Region
    let autocompleteBilling = new maps.places.Autocomplete(
      this.searchElementRefBaddress?.nativeElement
    );
    autocompleteBilling.addListener('place_changed', () => {
      this.ngZone.run(() => {
        this.ContactForm.controls['addressLine1'].setValue(
          autocompleteBilling.getPlace().address_components[0]['long_name'] +
          ', ' +
          autocompleteBilling.getPlace().address_components[1]['long_name']
        );
        this.ContactForm.controls['addressLine2'].setValue(
          autocompleteBilling.getPlace().address_components[2]['long_name']
        );
        this.ContactForm.controls['city'].setValue(
          autocompleteBilling.getPlace().address_components[3]['short_name']
        );
        this.ContactForm.controls['postCode'].setValue(
          autocompleteBilling.getPlace().address_components[6]['long_name']
            ? autocompleteBilling.getPlace().address_components[6]['long_name']
            : 0
        );
        if (isNaN(this.ContactForm.controls['postCode'].value)) {
          this.ContactForm.controls['postCode'].setValue(0);
        }

        this.listLookups?.address.states.forEach((element: any) => {
          if (
            element.stateDescription ==
            autocompleteBilling.getPlace().address_components[4]['long_name']
          ) {
            this.ContactForm.controls['stateId'].setValue(parseInt(element.id));
          }
        });
        this.locationFromPlace(autocompleteBilling.getPlace());
      });
    });
  }

  locationFromPlace(place: any) {
    const components = place.address_components;
    if (components === undefined) {
      return;
    }
    const areaLevel3 = this.getShort(components, 'administrative_area_level_3');
    const locality = this.getLong(components, 'locality');
    const cityName = locality || areaLevel3;
    const countryName = this.getLong(components, 'country');
    const countryCode = this.getShort(components, 'country');
    const stateCode = this.getShort(components, 'administrative_area_level_1');
    const name = place.name !== cityName ? place.name : null;
  }
  getLong(components: any, name: string) {
    const component = this.getComponent(components, name);
    return component && component.long_name;
  }

  getShort(components: any, name: string) {
    const component = this.getComponent(components, name);
    return component && component.short_name;
  }

  getComponent(components: any, name: string) {
    return components.filter(
      (component: any) => component.types[0] === name
    )[0];
  }
  ngOnInit(): void {
    const myDateValue = this.selectedDate;
    myDateValue.setHours(myDateValue.getHours() + 1);
    this.subActivityForm = this.formBuilder.group({
      id: [''],
      companyId: [null],
      contact: ['0'],
      title: ['', Validators.required],
      description: [''],
      phoneNumber: [''],
      startDate: [this.selectedDate, Validators.required],
      endDate: [this.selectedDate, Validators.required],
      startTime: [this.selectedDate, Validators.required],
      endTime: [myDateValue, Validators.required],
      transactionType: [''],
      relatedTransactionId: [''],
      salesRepId: [0],
      statusId: [0]
    });
    this.getAllContacts();

    if (history.state.navigationId != 1) {
      console.log("History", history.state)
      if (history && history.state) {
        if (
          history.state.contact_id != null &&
          history.state.contact_id != undefined
        ) {
          this.ContactId = history.state.contact_id;
          this.IsEditClick = this.ContactId == 0 ? true : false;

          if (this.ContactId != 0) {
            if (
              history.state.Company_id &&
              history.state.from &&
              history.state.from == 'add-company'
            ) {
              this.customer_id = history.state.Company_id;
            } else {
            }
          }
          if (this.ContactId == 0) {
            this.clearContactFormData();
          }
        }

        if (history.state.activity_id && history.state.contact_id) {
          // this.getCalenderEvent();
        }
      }
    } else {
      this.router.navigate(['dashboard']);
      setTimeout(() => {
        this.layoutService.activeRoute = this.router.url;
        this.layoutService.getcurrentActivateRoute(
          this.layoutService.activeRoute
        );
      }, 50);
    }
    this.getAllLookups();

    // this.getAllQuotes();

    // this.getCalenderEvent();
    this.getAllCompaniesList();
  }

  ngAfterViewInit() {
    this.apiService.api.then((maps: any) => {
      this.initAutocomplete(maps);
    });
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  statusTypeChange(event: any) { }
  filterTypeChange(event: any) {
    this.agendaList = new MatTableDataSource(this.mainagendaList);
    this.agendaList.sort = this.sort;
    if (this.searchText !== '') {
      this.agendaList.filter = this.searchText.trim().toLowerCase();
    }
    if (this.eventType) {
      this.agendaList = new MatTableDataSource(
        _.filter(this.agendaList.filteredData, {
          type: this.eventType
        })
      );
    }

    if (this.statusType) {
      this.agendaList = new MatTableDataSource(
        _.filter(this.agendaList.filteredData, {
          statusId: parseInt(this.statusType)
        })
      );
    }
    if (this.salesRepId) {
      this.agendaList = new MatTableDataSource(
        _.filter(this.agendaList.filteredData, { salesRepId: this.salesRepId })
      );
    }

    this.agendaList.sort = this.sort;
  }
  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
    this.showFilter = false;
  }
  clearSearch() {
    if (this.eventType || this.statusType || this.salesRepId) {
      this.searchInput = '';
      this.eventType = null;
      this.statusType = null;
      this.salesRepId = null;
      this.agendaList = new MatTableDataSource(this.mainagendaList);
    }
  }
  clearContactFormData() {
    this.ContactForm.patchValue({
      firstName: '',
      lastName: '',
      companyId: null,
      email: '',
      phone: '',
      main: '',
      jobTitle: '',
      // subsidiaryId: [6],
      roleId: '',
      netSuiteId: 0,
      accountId: '',
      contactId: '',
      addressLine1: '',
      addressLine2: '',
      city: '',
      stateId: '',
      postCode: '',
      countryId: '',
      idRes: '',
      id: '',
      relatedCompanyIds: ''
    });
    // this.ContactForm.reset();
    if (history.state.id) {
      this.ContactForm.controls['companyId'].setValue(history.state.id);
      this.ContactForm.controls['companyId'].updateValueAndValidity();
    }
  }
  CompanyDDLChange() {
    this.accountId = this.CompanyList.filter(
      x => x.id == this.ContactForm.value.companyId
    ).map(x => x.accountId)[0];
    this.netSuiteId = this.CompanyList.filter(
      x => x.id == this.ContactForm.value.companyId
    ).map(x => x.netSuiteId)[0];
  }
  search(event: any, from: any) {
    if (from == 'phone') {
      // if (event.target.value.length <= 10) {
      //   this.phoneLength = event.target.value.length;
      //   return;
      // }
      this.phoneData = event.target.value;
      // this.ContactForm.controls['phone'].setValue(this.phoneData);
      // this.phoneData = event.target.value;
      this.ContactForm.controls['phone'].setValue(this.phoneData);
    }
    // if (event.target.value.length <= 10) {
    //   this.phoneLength = event.target.value.length;
    //   return;
    // }
    //   this.phoneData = event.target.value;
    // this.ContactForm.controls['mainphone'].setValue(this.phoneData);
    else if (from == 'mainphone') {
      this.phoneData = event.target.value;
      this.ContactForm.controls['mainphone'].setValue(this.phoneData);
    }
    if (from == 'postCode') {
      if (event.target.value.length <= 6) {
        this.postCodeLength = event.target.value.length;
        this.postCodeData = event.target.value;
        return;
      }
      this.ContactForm.controls['postCode'].setValue(this.postCodeData);
    }
  }
  async FillDropDowns(obj?: any) {
    let companyId = obj && obj.companyId ? obj.companyId : history.state.id;
    if (companyId) {
      await this.common
        .GetMethod(
          Apiurl.getAllCompanies + '/' + companyId,
          null,
          true,
          'Loading..'
        )
        .then((data: any) => {
          this.salesRepIdContact = data;
          if (data) {
            this.defaultCompanyIdDuringPost = data.companyIdRel;
            this.ContactForm.controls['companyId'].setValue(data.id);
            this.ContactForm.controls['companyId'].updateValueAndValidity();

            this.ContactForm.controls['companyType'].setValue(data.tradingName);
            this.ContactForm.controls['companyType'].updateValueAndValidity();

            if (this.ContactId == 0) {
              this.ContactForm.controls['addressLine1'].setValue(
                data.billingAddress.addressLine1
              );
              this.ContactForm.controls[
                'addressLine1'
              ].updateValueAndValidity();

              this.ContactForm.controls['addressLine2'].setValue(
                data.billingAddress.addressLine2
              );
              this.ContactForm.controls[
                'addressLine2'
              ].updateValueAndValidity();

              this.ContactForm.controls['city'].setValue(
                data.billingAddress.city
              );
              this.ContactForm.controls['city'].updateValueAndValidity();

              this.ContactForm.controls['postCode'].setValue(
                data.billingAddress.postCode
              );
              this.ContactForm.controls['postCode'].updateValueAndValidity();
              this.postCodeLength =
                data.billingAddress &&
                  data.billingAddress.postCode &&
                  data.billingAddress.postCode.toString().length
                  ? data.billingAddress.postCode.toString().length
                  : 0;
              this.ContactForm.controls['countryId'].setValue(
                data.billingAddress.countryId
              );
              this.ContactForm.controls['countryId'].updateValueAndValidity();

              this.ContactForm.controls['stateId'].setValue(
                data.billingAddress.stateId
              );
              this.ContactForm.controls['stateId'].updateValueAndValidity();
            }
          }
          if (
            history.state.Company_id &&
            history.state.from &&
            history.state.from == 'add-company'
          ) {
            this.customer_id = history.state.Company_id;
            this.getContactDetailBynewCustomerId();
          } else if (this.ContactId) {
            this.getContactDetailById();
          }
        });
    }
  }
  submitbtnClick(): any {
    if (!this.ContactForm.valid) {
      if (
        this.ContactForm.controls.firstName.status == 'INVALID' ||
        this.ContactForm.controls.lastName.status == 'INVALID' ||
        this.ContactForm.controls.companyType.status == 'INVALID'
      ) {
        this.selectedIndex = 0;
      }
      if (this.ContactForm.controls.postCode.status == 'INVALID') {
        this.selectedIndex = 3;
      }

      this.ContactForm.markAllAsTouched();
      return false;
    } else {
      //

      if (this.ContactId > 0) {
        let relatedCompanyIds = [];
        if (!this.relatedCompany) {
          this.relatedCompany = [];
        }
        if (this.relatedCompany.length) {
          for (let index = 0; index < this.relatedCompany.length; index++) {
            relatedCompanyIds.push(this.relatedCompany[index].id);
          }
          if (
            !relatedCompanyIds.includes(
              Number(this.ContactForm.value.companyId)
            )
          ) {
            relatedCompanyIds.push(Number(this.ContactForm.value.companyId));
          }
        }

        let param = {
          companyId: Number(this.ContactForm.value.companyId),
          id: Number(this.ContactForm.value.id),
          netSuiteId: this.netSuiteId ? this.netSuiteId : 0,
          companyIdRel: this.defaultCompanyIdDuringPost,
          firstName: this.ContactForm.value.firstName,
          middlename: this.ContactForm.value.middlename,
          lastName: this.ContactForm.value.lastName,
          name:
            this.ContactForm.value.firstName +
            ' ' +
            this.ContactForm.value.lastName,
          email: this.ContactForm.value.email,
          jobTitle: this.ContactForm.value.jobTitle,
          phone: this.ContactForm.value.phone.toString(),
          mainphone: this.ContactForm.value.mainphone.toString(),
          subsidiaryId: Number(this.ContactForm.value.subsidiaryId),
          dateCreated: new Date().toISOString(),
          roleId: Number(this.ContactForm.value.roleId),
          accountId: this.accountId,
          relatedCompanyIds: [Number(this.ContactForm.value.companyId)],
          residentialAddress: {
            id: Number(this.ContactForm.value.idRes),
            contactId: this.ContactId > 0 ? Number(this.ContactId) : 0,
            addressLine1: this.ContactForm.value.addressLine1,
            addressLine2: this.ContactForm.value.addressLine2,
            city: this.ContactForm.value.city,
            stateId: Number(this.ContactForm.value.stateId),
            postCode: Number(this.ContactForm.value.postCode),
            countryId: Number(this.ContactForm.value.countryId)
          },
          createBy: '1',
          createDate: new Date().toISOString(),
          updatedBy: '1',
          updatedDate: new Date().toISOString()
        };
        this.SpinnerService.show();
        this.common
          .PutMethod(
            Apiurl.getContactList + '/' + this.ContactId,
            param,
            true,
            'Loading..',
            null
          )
          .then((resp: any) => {
            this.SpinnerService.hide();
            if (resp > 0) {
              this.commonService.getAllLookupList();
              this.commonService.showSuccess('Contact updated successfully');
              // Swal.fire(
              //   'Success',
              //   'Contact updated successfully',
              //   'success'
              // ).then(() => {
              history.state;

              if (history && history.state.id) {
                let id = history.state.id;
                if (history.state.from == 'company-detail') {
                  id = history.state.id;
                }
                let qoute_id = null;
                if (history.state.quote_id) {
                  qoute_id = history.state.quote_id;
                }
                let param: NavigationExtras = {
                  state: {
                    id: id,
                    contact_id: this.ContactId,
                    qoute_id: qoute_id,
                    isEdit: true
                  }
                };
                if (history.state.from == 'qoute') {
                  this.router.navigate(['add-quotes'], param);
                } else if (history.state.from == 'company-detail') {
                  this.router.navigate(['companies-details'], param);
                } else if (history.state.from == 'opportunity') {
                  this.router.navigate(['add-opportunities'], param);
                } else {
                  this.ngOnInit();
                }
              } else {
                this.router.navigate(['contacts-list']);
              }
              // });
            } else {
              //Swal.fire(resp.code, resp.message, 'error');
              this.commonService.showError(resp.message);
            }
          });
      } else {
        let param = {
          id: Number(this.ContactForm.value.id),
          netSuiteId: this.netSuiteId,
          companyId: Number(this.ContactForm.value.companyId),
          firstName: this.ContactForm.value.firstName,
          middlename: this.ContactForm.value.middlename,
          lastName: this.ContactForm.value.lastName,
          name:
            this.ContactForm.value.firstName +
            ' ' +
            this.ContactForm.value.middlename +
            ' ' +
            this.ContactForm.value.lastName,
          email: this.ContactForm.value.email,
          jobTitle: this.ContactForm.value.jobTitle,
          phone: this.ContactForm.value.phone
            ? this.ContactForm.value.phone.toString()
            : '',
          mainphone: this.ContactForm.value.mainphone
            ? this.ContactForm.value.mainphone.toString()
            : '',
          subsidiaryId: Number(this.ContactForm.value.subsidiaryId),
          dateCreated: new Date().toISOString(),
          roleId: Number(this.ContactForm.value.roleId),
          accountId: this.accountId,
          relatedCompanyIds: [Number(this.ContactForm.value.companyId)],
          residentialAddress: {
            id: Number(this.ContactForm.value.idRes),
            contactId: this.ContactId > 0 ? Number(this.ContactId) : 0,
            addressLine1: this.ContactForm.value.addressLine1,
            addressLine2: this.ContactForm.value.addressLine2,
            city: this.ContactForm.value.city,
            stateId: Number(this.ContactForm.value.stateId),
            postCode: Number(this.ContactForm.value.postCode),
            countryId: Number(this.ContactForm.value.countryId)
          },
          createBy: '1',
          createDate: new Date().toISOString(),
          updatedBy: '1',
          updatedDate: new Date().toISOString()
        };
        this.SpinnerService.show();
        this.common
          .PostMethod(Apiurl.getContactList, param, true, 'Loading..', null)
          .then((resp: any) => {
            if (resp) {
              this.SpinnerService.hide();
              this.commonService.getAllLookupList();
              this.commonService.showSuccess('Contact saved successfully');
              // Swal.fire(
              //   'Success',
              //   'Contact saved successfully',
              //   'success'
              // ).then(() => {
              if (history && history.state.id) {
                let id = history.state.id;
                if (history.state.from == 'company-detail') {
                  id = history.state.id;
                }
                let qoute_id = null;
                if (history.state.quote_id || history.state.qoute_id) {
                  qoute_id = history.state.quote_id
                    ? history.state.quote_id
                    : history.state.qoute_id;
                }
                let param: NavigationExtras = {
                  state: {
                    id: id,
                    contact_id: resp,
                    qoute_id: qoute_id,
                    isEdit: true
                  }
                };
                if (history.state.from == 'qoute') {
                  this.router.navigate(['add-quotes'], param);
                } else if (history.state.from == 'company-detail') {
                  this.router.navigate(['companies-details'], param);
                } else if (history.state.from == 'opportunity') {
                  this.router.navigate(['add-opportunities'], param);
                } else {
                  this.router.navigate(['contacts-list']);
                }
              } else {
                if (history.state.from == 'contacts-list') {
                  history.state.id = Number(this.ContactForm.value.companyId);
                  history.state.contact_id = resp;
                  this.ngOnInit();
                } else {
                  this.router.navigate(['contacts-list']);
                }
              }
              // });
            } else {
              this.commonService.showError(resp.message);
              //Swal.fire(resp.code, resp.message, 'error');
            }
          });
      }
    }
  }
  isRequired(controlName: string) {
    if (!controlName) {
      return false;
    }
    return this.ContactForm.controls[controlName].validator('').required;
  }
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.ContactForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  async ViewContact() {
    let obj = {
      title: 'Are you sure you want to Cancel?',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    };
    await this.common.showConfirm(obj).then((resp: any) => {
      if (resp.isConfirmed) {
        history.state;
        if (history && history.state.id) {
          let id = history.state.id;
          if (history.state.from == 'company-detail') {
            id = history.state.id;
          }
          let qoute_id = null;
          if (history.state.quote_id || history.state.qoute_id) {
            qoute_id = history.state.quote_id
              ? history.state.quote_id
              : history.state.qoute_id;
          }
          let param: NavigationExtras = {
            state: {
              id: id,
              contact_id: resp,
              qoute_id: qoute_id,
              isEdit: true
            }
          };
          if (history.state.from == 'qoute') {
            this.router.navigate(['add-quotes'], param);
          } else if (history.state.from == 'company-detail') {
            this.router.navigate(['companies-details'], param);
          } else if (history.state.from == 'opportunity') {
            this.router.navigate(['add-opportunities'], param);
          } else {
            this.router.navigate(['contacts-list']);
          }
          setTimeout(() => {
            this.layoutService.activeRoute = this.router.url;
            this.layoutService.getcurrentActivateRoute(
              this.layoutService.activeRoute
            );
          }, 100);
        } else {
          if (history.state.from == 'contacts-list') {
            history.state.id = Number(this.ContactForm.value.companyId);
            history.state.contact_id = resp;
            this.ngOnInit();
          } else {
            this.router.navigate(['contacts-list']);
            setTimeout(() => {
              this.layoutService.activeRoute = this.router.url;
              this.layoutService.getcurrentActivateRoute(
                this.layoutService.activeRoute
              );
            }, 100);
          }
        }
      }
    });
  }

  async goToPage() {
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    var userDetails = JSON.parse(this.userDetailsStr)
    if (
      this.salesRepIdContact.salesRepId !==
      userDetails.salesRepId && !userDetails.isAdmin
    ) {
      let obj = {
        title: 'Error',
        text: "company SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => { });
    } else {
      let id: NavigationExtras = {
        state: {
          id: history.state.id,
          contact_id: history.state.contact_id,
          from: 'contacts'
        }
      };
      this.router.navigate(['add-company'], id);
    }
  }

  getAllLookups() {
    this.listLookups = [];
    this.commonService.lookupList.subscribe((res: any) => {
      this.listLookups = res;
      if (
        this.listLookups.contact != undefined &&
        this.listLookups.contact.roles.length > 0
      ) {
        this.RoleList = this.listLookups.contact.roles;
      }
      if (
        this.listLookups.company != undefined &&
        this.listLookups.company.subsidiaries.length > 0
      ) {
        this.SubsidiariesList = this.listLookups.company.subsidiaries;
      }
      if (
        this.listLookups.address != undefined &&
        this.listLookups.address.states.length > 0
      ) {
        this.StateList = this.listLookups.address.states;
      }
      if (
        this.listLookups.address != undefined &&
        this.listLookups.address.countries.length > 0
      ) {
        this.CountryList = this.listLookups.address.countries;
        if (this.CountryList.length == 1) {
          this.ContactForm.patchValue({
            countryId: this.CountryList[0].id
          });
        }
      }
    });
  }
  async getContactDetailById() {
    this.showCompaniesLoader = true;
    this.showCompaniesNoRecMsg = false;
    await this.common
      .GetMethod(
        Apiurl.getContactList + '/' + this.ContactId,
        null,
        true,
        'Loading'
      )
      .then((res: any) => {
        res;
        if (res) {
          this.companyDetails = [];
          this.companyDetails = res;
          console.log("ComapnyData", this.companyDetails)
          this.showCal = true;
          this.netSuiteId = res.netSuiteId;
          this.ContactForm.patchValue({
            firstName: res.firstName,
            middlename: res.middleName,
            lastName: res.lastName,
            companyId: res.companyId,
            email: res.email,
            phone: res.phone,
            jobTitle: res.jobTitle,
            subsidiaryId: res.subsidiaryId,
            roleId: res.roleId,
            netSuiteId: res.netSuiteId,
            accountId: res.accountId,
            contactId: res.residentialAddress?.contactId,
            addressLine1: res.residentialAddress?.addressLine1,
            addressLine2: res.residentialAddress?.addressLine2,
            city: res.residentialAddress?.city,
            stateId: res.residentialAddress?.stateId,
            postCode: !res.residentialAddress?.postCode
              ? 0
              : res.residentialAddress?.postCode,
            countryId: res.residentialAddress?.countryId,
            idRes: res.residentialAddress?.id,
            id: res.id,
            relatedCompanyIds: res.relatedCompanyIds
          });
          this.relatedCompany = [];
          this.relatedCompany = res.relatedCompanies;
          this.showCompaniesLoader = false;
          if (this.relatedCompany && this.relatedCompany.length == 0) {
            this.showCompaniesNoRecMsg = true;
          }
        }
        this.getAllQuotes(res);
        this.getAllOppertunities(res);
      });
  }
  getContactDetailBynewCustomerId() {
    this.common
      .GetMethod(
        Apiurl.getContactList + '/' + this.ContactId,
        null,
        true,
        'Loading'
      )
      .then((res: any) => {
        res.relatedCompanyIds.push(history.state.Company_id);
        this.common
          .PutMethod(
            Apiurl.getContactList + '/' + this.ContactId,
            res,
            true,
            'Loading..',
            null
          )
          .then((resp: any) => {
            if (resp) {
              this.getContactDetailById();
            }
          });
      });
  }
  viewFilterField() {
    this.showFilter = !this.showFilter;
  }
  OpenBlock(id: any) {
    this.openBlock = id;
    this.StateName = this.StateList.filter(
      x => x.id == this.companyDetails.residentialAddress.stateId
    ).map(x => x.stateDescription)[0];
    this.CountryName = this.CountryList.filter(
      x => x.id == this.companyDetails.residentialAddress.countryId
    ).map(x => x.countryDescription)[0];
  }

  async EditClick() {
    //Set Admin and salesRepId validation
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    if (
      this.salesRepIdContact.salesRepId !==
      JSON.parse(this.userDetailsStr).salesRepId && !JSON.parse(this.userDetailsStr).isAdmin
    ) {
      let obj = {
        title: 'Error',
        text: "Contacts SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => { });
    } else {
      this.IsEditClick = true;
      this.FillDropDowns();
    }
  }

  AddNewContact() {
    this.ContactForm.reset();
    this.IsEditClick = true;
    this.ContactId = 0;
  }
  viewAgenda() {
    this.view = 'Agenda';
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  gotoActivities(whereToGo: any) {
    let param: NavigationExtras = {
      state: {
        type: whereToGo,
        id: history.state.id,
        contact_id: history.state.contact_id,
        from: 'contact'
      }
    };
    this.router.navigate(['sub-activity'], param);
  }
  searchCompany(event: any) {
    if (event.target.value != '') {
      this.companyListSearch = this.CompanyList.filter(
        (s: any) =>
          s.companyName
            .toLowerCase()
            .indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.companyListSearch = this.CompanyList;
    }
  }
  setCompany(obj: any) {
    this.ContactForm.controls['companyId'].setValue(obj.companyId);
    this.ContactForm.controls['companyType'].setValue(obj.companyName);
    this.accountId = this.CompanyList.filter(
      x => x.companyId == this.ContactForm.value.companyId
    ).map(x => x.accountId)[0];
    this.netSuiteId = this.CompanyList.filter(
      x => x.companyId == this.ContactForm.value.companyId
    ).map(x => x.netSuiteId)[0];
  }

  gotoCompany(item: any, isFromContactView: boolean = false) {
    console.log("Item", item)
    var id = 0;
    if (isFromContactView) {
      id = item.companyId;
    } else {
      id = item.id;
    }
    let param: NavigationExtras = {
      state: {
        id: id
      }
    };
    this.router.navigate(['companies-details'], param);
    setTimeout(() => {
      this.layoutService.activeRoute = this.router.url;
      this.layoutService.getcurrentActivateRoute(
        this.layoutService.activeRoute
      );
    }, 50);
  }



  async setSelectedcompany(item: any) {
    //set to check selected company salesrepid is same as login 
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    if (
      item.salesRepId ==
      JSON.parse(this.userDetailsStr).salesRepId
    ) {
      if (!this.relatedCompany) {
        this.relatedCompany = [];
      }
      this.common
        .GetMethod(
          Apiurl.getContactList + '/' + this.ContactId,
          null,
          true,
          'Loading'
        )
        .then((res: any) => {
          if (!res.relatedCompanyIds.includes(item.companyId)) {
            res.relatedCompanyIds.push(item.companyId);
            this.common
              .PutMethod(
                Apiurl.getContactList + '/' + this.ContactId,
                res,
                true,
                'Loading..',
                null
              )
              .then((resp: any) => {
                if (resp) {
                  history.state.from = 'contacts';
                  this.getContactDetailById();
                  this.getAllLookups();
                }
                this.ngSelectComponent.clearModel();
                this.ngSelectComponent.blur();
              });
          } else if (res.relatedCompanyIds.includes(item.companyId)) {
            this.commonService.showError('This contact is already added');
            // Swal.fire('This contact is already added')
          }
        });
      this.searchedInput = '';
    } else {
      let obj = {
        title: 'Error',
        text: "Company SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => { });
    }
  }

  searchParent(event: any) {
    this.relatedCompany;
    if (event.target.value != '') {
      this.companyListSearch = this.CompanyList.filter(
        (s: any) =>
          s.companyName
            .toLowerCase()
            .indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.companyListSearch = this.CompanyList;
    }
  }

  getAllCompaniesList() {
    let self = this;

    if (self.listLookups) {
      self.CompanyList = self.listLookups.company?.companyList;
      if (self.CompanyList) {
        self.CompanyList = self.commonService.sortAlphabetically(
          self.CompanyList
        );
        self.companyListSearch = JSON.parse(JSON.stringify(self.CompanyList));
      }
      this.FillDropDowns();
    }
  }

  getContactList(id: any) {
    this.common
      .GetMethod(Apiurl.getContactList, null, true, 'Loading')
      .then((res: any) => {
        if (res) {
          for (let i = 0; i < res.length; i++) {
            if (res[i].companyId == id) {
              this.relatedContactList.push(res[i]);
            }
          }
        }
      });
  }

  inputEvent(event: any) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filterTypeChange(filterValue);
  }
  onTabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.selectedIndex = tabChangeEvent.index;
  }
  clear() {
    this.searchText = '';
    // this.getCalenderEvent();
  }
  clearCompanyDetails() {
    this.searchdropdown = '';
    this.companyListSearch = this.CompanyList;
  }

  //clear individual filter
  clearIndividualFilter(filtername: any) {
    switch (filtername) {
      case 'eventType':
        this.eventType = null;
        this.filterTypeChange(null);
        break;
      case 'statusType':
        this.statusType = null;
        this.filterTypeChange(null);
        break;
      case 'salesRep':
        this.salesRepId = null;
        this.filterTypeChange(null);
        break;
      default:
        break;
    }
  }
  //oppertunity and quotes data
  getAllOppertunities(res: any) {
    this.showOppertunitiesLoader = true;
    this.showOppertunitiesNoRecMsg = false;
    if (res) {
      res;
      this.oppertunityData = [];
      let data: any = [];
      if (res.opportunities) {
        res.opportunities.map((opp: any) => {
          if (opp.documentNumber) {
            opp.opp = opp.documentNumber;
          } else {
            opp.opp = 'OPP#' + opp.id;
          }
          data.push(opp);
        });
      }
      this.oppertunityData = data;
      this.showOppertunitiesLoader = false;
      if (!this.oppertunityData || !this.oppertunityData.length) {
        this.showOppertunitiesNoRecMsg = true;
      }
    }
  }

  getAllQuotes(res: any) {
    this.showQuoteLoader = true;
    if (res) {
      this.quoteData = [];
      let data: any = [];
      if (res.quotes) {
        res.quotes.map((quote: any) => {
          if (quote.documentNumber) {
            quote.opp = quote.documentNumber;
          } else {
            quote.opp = 'ESTBG' + quote.id;
          }
          data.push(quote);
        });
      }
      this.quoteData = data;
      this.showQuoteLoader = false;
      if (!this.quoteData || !this.quoteData.length) {
        this.showQuoteNoRecMsg = true;
      }
    }
    // this.quoteData = res.quotes;
  }

  navigateTo(page: any) {
    let param: NavigationExtras = {
      state: {
        id: history.state.id,
        contact_id: history.state.contact_id,
        from: 'contacts'
      }
    };
    switch (page) {
      case 'Contacts':
        this.router.navigate(['contacts'], param);
        break;
      case 'Quotes':
        this.router.navigate(['add-quotes'], param);
        setTimeout(() => {
          this.layoutService.activeRoute = this.router.url;
          this.layoutService.getcurrentActivateRoute(
            this.layoutService.activeRoute
          );
        }, 50);
        break;
      case 'Opportunities':
        this.router.navigate(['add-opportunities'], param);
        setTimeout(() => {
          this.layoutService.activeRoute = this.router.url;
          this.layoutService.getcurrentActivateRoute(
            this.layoutService.activeRoute
          );
        }, 50);
        break;

      default:
        break;
    }
  }
  gotoOpportunity(item: any) {
    let param: NavigationExtras = {
      state: {
        id: history.state.id,
        qoute_id: item.id,
        from: 'contact',
        isEdit: true,
        item
      }
    };
    this.router.navigate(['add-opportunities'], param);
  }

  gotoQuote(item: any) {
    let param: NavigationExtras = {
      state: {
        id: history.state.id,
        qoute_id: item.id,
        from: 'contact',
        isEdit: true,
        item
      }
    };
    this.router.navigate(['add-quotes'], param);
  }
  getState(id: any) {
    let data = _.findWhere(this.listLookups?.address?.states, { id: id });
    if (data) {
      return data.stateDescription;
    } else {
      return;
    }
  }
  getCountry(id: any) {
    let data = _.findWhere(this.listLookups?.address?.countries, { id: id });
    if (data) {
      return data.countryDescription;
    } else {
      return;
    }
  }

  getAllContacts() {
    var companyId = this.subActivityForm.get('companyId')?.value;
    return new Promise((resolve: any, reject: any) => {
      this.commonService
        .GetMethod(Apiurl.getContactList, null, false, 'Loading..')
        .then((res: any) => {
          if (res) {
            resolve(true);
            for (let i = 0; i < res.length; i++) {
              if (res[i].companyId == companyId) {
                this.relatedContactList.push(res[i]);
              }
            }
            for (let i = 0; i < res.length; i++) {
              if (history.state.relatedContactIds) {
                let self = this;
                function forLoop(j: any) {
                  if (j >= history.state.relatedContactIds.length) return;
                  let id = history.state.relatedContactIds[j];
                  if (id === res[i].id) {
                    res[i].isSelect = false;
                    self.listContacts.push(res[i]);
                    self.listContactIds.push(res[i].id);
                    if (res[i].role == 'Primary Contact') {
                      self.subActivityForm.controls['contact'].setValue(
                        res[i].id
                      );
                    }
                  } else {
                    forLoop(j + 1);
                  }
                }
                forLoop(0);
              } else if (res[i].companyId == companyId) {
                res[i].isSelect = false;
                this.listContacts.push(res[i]);
                this.listContactIds.push(res[i].id);
                if (
                  history.state.contact_id != null &&
                  history.state.contact_id != undefined
                ) {
                  var getRefContactInfo: any;
                  getRefContactInfo = _.filter(this.listContacts, {
                    id: history.state.contact_id
                  });
                  this.listContacts;
                  if (getRefContactInfo && getRefContactInfo.length) {
                    var phoneNum = getRefContactInfo[0].phone;
                    if (phoneNum != '') {
                      this.subActivityForm.controls['phoneNumber'].setValue(
                        phoneNum
                      );
                    }
                  }
                }
                if (res[i].role == 'Primary Contact') {
                  this.subActivityForm.controls['contact'].setValue(res[i].id);
                }
              }
            }
            if (this.listContacts.length == 1) {
              this.subActivityForm.controls['contact'].setValue(
                this.listContacts[0].id
              );
            }
          } else {
            resolve(false);
          }
        });
    });
  }

  //redirct to add-quote page if login salesrepid is same as company salesrepid
  async setRedirctQuote() {
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    var userDetails = JSON.parse(this.userDetailsStr)
    if (this.salesRepIdContact.salesRepId !==
      userDetails.salesRepId && !userDetails.isAdmin) {
      let obj = {
        title: 'Error',
        text: "Quotes SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => { });
    } else {
      this.navigateTo('Quotes');
    }
  }

  //redirct to add-Opportunities page if login salesrepid is same as company salesrepid
  async setRedirctopportunity() {
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    var userDetails = JSON.parse(this.userDetailsStr)
    // set validation for isAdmin and salesRepId
    if (this.salesRepIdContact.salesRepId !==
      userDetails.salesRepId && !userDetails.isAdmin) {
      let obj = {
        title: 'Error',
        text: "Opportunities SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => { });
    } else {
      this.navigateTo('Opportunities');
    }
  }
}
