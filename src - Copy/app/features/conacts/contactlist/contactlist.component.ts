import { ExportService } from '../../../shared/export.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { Apiurl } from './../../../shared/route';
import { CommonProvider } from '../../../shared/common';
import { Router, NavigationExtras } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { ProgressBarMode } from '@angular/material/progress-bar';
import * as _ from 'underscore';
import { NgxSpinnerService } from 'ngx-spinner';
import { ThemePalette } from '@angular/material/core';
import { SortDescriptor } from '@progress/kendo-data-query';
@Component({
  selector: 'app-contactlist',
  templateUrl: './contactlist.component.html',
  styleUrls: ['./contactlist.component.scss']
})
export class ContactlistComponent implements OnInit {
  displayedColumns: string[] = [
    'firstName',
    'middleName',
    'lastName',
    'companyName',
    'phone',
    'email',
    'jobTitle',
    'role'
  ];
  searchText: any = '';
  sortName: string = '';
  sortBy: any = 'asc';
  dataSource: any;
  // @ViewChild(MatSort) sort: any;
  @ViewChild(MatPaginator) paginator: any;
  panelOpenState = false;
  ContactList: any[] = [];
  companyList: any = [];
  companyListSearch: any = [];
  listLookups: any = [];
  roleList: any = [];
  showFilter: boolean = false;
  Company: any;
  TodayDate: any = new Date().toDateString();
  Role: any;
  mainDataSouce: any = [];
  pageIndex: number = 1;
  pageLength: number = 100;
  totalCount: number = 0;
  showProgressBar: boolean = false;
  color: ThemePalette = 'primary';
  mode: ProgressBarMode = 'indeterminate';
  value = 50;
  bufferValue = 75;
  searchContact = new Subject<string>();
  public sort: SortDescriptor[] = [
    {
      field: 'firstName',
      dir: 'asc'
    }
  ];
  constructor(
    public common: CommonProvider,
    public router: Router,
    private SpinnerService: NgxSpinnerService,
    private exportService: ExportService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    if (localStorage.getItem('pageLengthInContacts')) {
      let pageLength = localStorage.getItem('pageLengthInContacts') as string;
      this.pageLength = parseInt(pageLength);
    }
    this.getAllRole();
    this.searchContact
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(value => {
        this.filterContactList();
      });
  }
  ngAfterViewInit() {
    this.cdr.detectChanges();
  }
  getAllStages() {
    this.companyList = [];
    this.companyList = this.listLookups?.company?.companyList;
    if (this.companyList?.length) {
      this.companyList = this.common.sortAlphabetically(this.companyList);
      this.companyListSearch = JSON.parse(JSON.stringify(this.companyList));
    }
  }

  getAllRole() {
    this.common.lookupList.subscribe((res: any) => {
      this.listLookups = res;
      this.roleList = this.listLookups?.contact?.roles;
      this.getContactList();
      this.getAllStages();
    });
  }
  filterContactList() {
    // this.dataSource = [];
    this.getContactList();
  }
  async getContactList() {
    //this.SpinnerService.show();
    this.showProgressBar = true;
    let userDetailsStr: any = '';
    let userDetails: any;

    if (localStorage.getItem('UserDetails')) {
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      userDetails = JSON.parse(userDetailsStr);
    }
    let param: any = {};
    if (this.Company) {
      param.cn = this.Company;
    }
    if (this.Role) {
      param.ro = this.Role;
    }
    if (this.searchText) {
      param.search = this.searchText;
    }
    if (this.sortBy) {
      param.sortBy = this.sortBy;
    }
    if (this.sortName) {
      param.sortName = this.sortName;
    }
    if (userDetails && !userDetails.isAdmin) {
      param.subsidiaryId = userDetails.subsidiaryId;
      param.salesRepId = userDetails.salesRepId;
    }
    this.common
      .GetMethod(
        Apiurl.getContactList + '/' + this.pageIndex + '/' + this.pageLength,
        param,
        true,
        'Loading'
      )
      .then((res: any) => {
        if (res && res.length) {
          this.mainDataSouce = res;
          this.totalCount = res[0].totalCount;
          this.dataSource = res;
          // this.dataSource.sort = this.sort;
          // this.dataSource.paginator = this.paginator;
          this.SpinnerService.hide();
        } else {
          this.mainDataSouce = [];
          this.totalCount = 0;
          this.dataSource.filteredData = [];
          this.dataSource = [];
        }
        this.dataSource;
        this.showProgressBar = false;
      })
      .catch(error => {
        this.showProgressBar = false;
      });
  }
  AddNewContact() {
    let param: NavigationExtras = {
      state: {
        contact_id: 0,
        from: 'contacts-list'
      }
    };
    this.router.navigate(['contacts'], param);
  }
  selection(value: any) {
    let param: NavigationExtras = {
      state: {
        contact_id: value.residentialAddress.contactId,
        id: value.companyId
      }
    };
    this.router.navigate(['contacts'], param);
  }
  openFilter() {
    this.showFilter = !this.showFilter;
  }

  inputEvent(filterValue: any) {
    // const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  clear() {
    this.searchText = '';
    this.getContactList();
  }
  clearFilterCompany(event: any) {
    this.Company = null;
    event.stopPropagation();
    // this.filterTypeChange(event);
    this.filterContactList();
  }
  clearFilterRole(event: any) {
    this.Role = null;
    event.stopPropagation();
    // this.filterTypeChange(event);
    this.filterContactList();
  }
  filterTypeChange(event: any) {
    this.dataSource = new MatTableDataSource(this.mainDataSouce);

    if (this.dataSource.filteredData.length === 0) {
      this.dataSource = new MatTableDataSource(this.mainDataSouce);
    }
    if (this.searchText !== '') {
      this.dataSource.filter = this.searchText.trim().toLowerCase();
    }

    if (this.Company) {
      this.dataSource = new MatTableDataSource(this.mainDataSouce);
      // this.Role = null;
      this.dataSource = new MatTableDataSource(
        _.filter(this.dataSource.filteredData, { companyName: this.Company })
      );
    }
    if (this.Role) {
      this.dataSource = new MatTableDataSource(
        _.filter(this.dataSource.filteredData, { role: this.Role })
      );
    }
    this.dataSource.sort = this.sort;
    this.dataSource;
    // this.dataSource.paginator = this.paginator;
  }
  clearSearch() {
    this.Company = null;
    this.Role = null;
    // this.dataSource = new MatTableDataSource(this.mainDataSouce);
    this.getContactList();
    // this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
    // if (this.Company || this.Role) {
    //   // this.dataSource.paginator = this.paginator;
    // }
  }
  clearCompanyDetails() {
    this.companyListSearch = this.companyList;
  }
  searchCompany(event: any) {
    if (event.target.value != '') {
      this.companyListSearch = this.companyList.filter(
        (s: any) =>
          s.companyName
            .toLowerCase()
            .indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.companyListSearch = this.companyList;
    }
  }
  // setCompany(obj: any) {
  //  this.Company=obj.id;
  // }
  onChangePage(ev: PageEvent) {
    this.pageIndex = ev.pageIndex + 1;
    localStorage.setItem('pageLengthInContacts', ev.pageSize.toString());
    this.pageLength = ev.pageSize;
    this.getContactList();
  }
  sortCompanyList(sort: any) {
    this.sort = sort;
    this.sortName = this.sort[0].field;
    this.sortBy = this.sort[0].dir;
    this.filterContactList();
  }
  public rowCallback(context: any) {
    return {
      dragging: context.dataItem.dragging
    };
  }
  public exportToExcel(grid: any): void {
    grid.saveAsExcel();
  }
  refreshGrid() {
    this.ngOnInit();
  }
  exportToCsv(): void {
    let exportObj = {
      'Contact Name': null,
      'Company name': null,
      Phone: null,
      Email: null,
      'Job Title': null,
      Role: null
    };
    var exportData: any = [];
    this.mainDataSouce.forEach((data: any) => {
      exportObj = {
        'Contact Name': data.firstName
          ? data.firstName + ' '
          : null + data.middleName
            ? data.middleName + ' '
            : null + data.lastName
              ? data.lastName
              : null,
        'Company name': data.companyName,
        Phone: data.phone,
        Email: data.email,
        'Job Title': data.jobTitle,
        Role: data.role
      };
      exportData.push(exportObj);
    });
    // return
    this.exportService.exportToCsv(exportData, 'Contacts');
  }
}
