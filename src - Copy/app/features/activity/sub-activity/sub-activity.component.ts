import { ThemePalette } from '@angular/material/core';
import { LayoutService } from '../../../features/layout/layout.service';
import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { CommonProvider } from '../../../shared/common';
import { Apiurl } from '../../../shared/route';
import * as _ from 'underscore';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProgressBarMode } from '@angular/material/progress-bar';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sub-activity',
  templateUrl: './sub-activity.component.html',
  styleUrls: ['./sub-activity.component.scss']
})
export class SubActivityComponent implements OnInit {
  speedDialFabButtons = [
    {
      icon: 'note',
      tooltip: 'Task',
      class: 'note-color'
    },
    {
      icon: 'event',
      tooltip: 'Event',
      class: 'event-color'
    },
    {
      icon: 'phone',
      tooltip: 'Phone',
      class: 'phone-color'
    }
  ];

  endDateError: boolean = false;
  headerTitle: any = 'Add Activity';
  endTimeError: boolean = false;
  subActivityForm: FormGroup;
  listCompanies: any = [];
  listContacts: any = [];
  listMainContacts: any = [];
  listContactIds: any = [];
  activity_id: any = 0;
  companyListSearch: any = [];
  listQutoes: any = [];
  listopportunities: any = [];
  listTransactions: any = [];
  listParticipants: any = [];
  editflag: Boolean = false;
  listLookups: any = [];
  isSheduled: Boolean = true;
  isCompleted: Boolean = false;
  companyId: any;
  searchdropdown: any;
  selectedDate: Date = new Date();
  type: any;
  mainOpportunityList: any = [];
  mainQuoteList: any = [];
  activityDetailObj: any;
  showOptionBtn: boolean = false;
  activeBtn: any = 'Task';
  showProgressBar: boolean = false;
  color: ThemePalette = 'primary';
  mode: ProgressBarMode = 'indeterminate';
  value = 50;
  bufferValue = 75;
  netSuiteId: any = 0;
  userDetailsStr: any = '';
  userDetails: any;
  constructor(
    private fb: FormBuilder,
    public commonService: CommonProvider,
    private router: Router,
    private route: ActivatedRoute,
    private layoutService: LayoutService,
    private SpinnerService: NgxSpinnerService
  ) { }

  ngOnInit() {
    const endTime = new Date(this.selectedDate);
    endTime.setHours(this.selectedDate.getHours() + 1);
    endTime.setMinutes(this.selectedDate.getMinutes());
    endTime.setSeconds(this.selectedDate.getSeconds());
    this.subActivityForm = this.fb.group({
      id: [''],
      companyId: [''],
      contact: ['0'],
      title: ['', Validators.required],
      description: [''],
      phoneNumber: [''],
      startDate: [this.selectedDate, Validators.required],
      endDate: [this.selectedDate, Validators.required],
      startTime: [this.selectedDate, Validators.required],
      endTime: [endTime, Validators.required],
      transactionType: [''],
      relatedTransactionId: [''],
      salesRepId: [null],
      statusId: [2]
    });
    this.getAllLookups();
    this.getAllOpportunityList();
    this.getAllQuoteList();

    if (history.state.navigationId != 1) {
      if (history && history.state) {
        if (history.state.type != null && history.state.type != undefined) {
          this.type = history.state.type;
          if (
            history.state.activity_id == null ||
            history.state.activity_id == undefined ||
            history.state.activity_id == 0
          ) {
            this.showOptionBtn = false;
            if (history.state.for == 'add') {
              this.showOptionBtn = true;
            }
            if (history.state.selectedDate) {
              const selectedDate = history.state.selectedDate;
              this.subActivityForm.controls['startDate'].setValue(selectedDate);
              this.subActivityForm.controls['endDate'].setValue(selectedDate);
              if (history.state.selected == 'date') {
                this.subActivityForm.controls['startTime'].setValue(
                  this.setEndTime(
                    this.subActivityForm.controls['startDate'].value,
                    new Date()
                  )
                );
                this.subActivityForm.controls['endTime'].setValue(
                  this.setEndTimeOnePlus(
                    this.subActivityForm.controls['startDate'].value,
                    this.subActivityForm.controls['startTime'].value
                  )
                );
              } else if (history.state.selected == 'time') {
                const endTime = new Date(selectedDate);
                endTime.setHours(selectedDate.getHours() + 1);
                endTime.setMinutes(selectedDate.getMinutes());
                endTime.setSeconds(selectedDate.getSeconds());
                this.subActivityForm.controls['startTime'].setValue(
                  selectedDate
                );
                this.subActivityForm.controls['endTime'].setValue(endTime);
              }
            }
            this.showProgressBar = false;
          } else if (
            history.state.activity_id != null &&
            history.state.activity_id != undefined &&
            history.state.activity_id > 0
          ) {
            this.activity_id = history.state.activity_id;
            this.getAllLookups();
            this.editflag = true;
          }
        }
      }
    } else {
      this.router.navigate(['dashboard']);
      setTimeout(() => {
        this.layoutService.activeRoute = this.router.url;
        this.layoutService.getcurrentActivateRoute(
          this.layoutService.activeRoute
        );
      }, 50);
    }
  }

  getAllLookups() {
    this.listLookups = [];
    let self = this;
    self.commonService.lookupList.subscribe((res: any) => {
      if (res) {
        self.listLookups = res;
        this.showProgressBar = true;
        this.getActivityById();
      } else {
      }
    });
  }

  getAllQuoteList() {
    var companyId = this.subActivityForm.get('companyId')?.value;
    this.listQutoes = [];
    let userDetailsStr: any = '';
    let userDetails: any;

    if (localStorage.getItem('UserDetails')) {
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      this.userDetails = JSON.parse(userDetailsStr);
    }
    let param: any = {};
    if (this.userDetails && !this.userDetails.isAdmin) {
      param.subsidiaryId = this.userDetails.subsidiaryId;
      param.salesRepId = this.userDetails.salesRepId;
    }
    this.commonService
      .GetMethod(Apiurl.getAllQuotes, param, true, 'Loading')
      .then((res: any) => {
        if (res) {
          this.mainQuoteList = res;
        }
      });
  }
  getAllOpportunityList() {
    var companyId = this.subActivityForm.get('companyId')?.value;
    this.listopportunities = [];
    let userDetailsStr: any = '';
    let userDetails: any;

    if (localStorage.getItem('UserDetails')) {
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      userDetails = JSON.parse(userDetailsStr);
    }
    let param: any = {};
    if (userDetails && !userDetails.isAdmin) {
      param.subsidiaryId = userDetails.subsidiaryId;
      param.salesRepId = userDetails.salesRepId;
    }
    this.commonService
      .GetMethod(Apiurl.getAllOpportunityList, param, true, 'Loading')
      .then((res: any) => {
        if (res) {
          this.mainOpportunityList = res;
        }
      });
  }

  SelectParticipant(contactInfo: any) {
    contactInfo.isPrimary = !contactInfo.isPrimary;
  }

  SaveData() {
    this.SpinnerService.show();
    this.listParticipants = [];
    for (let i = 0; i < this.listContacts.length; i++) {
      if (this.listContacts[i].isPrimary == true) {
        this.listParticipants.push(this.listContacts[i].id);
      }
    }
    if (
      (this.subActivityForm.get('transactionType')?.value ||
        this.subActivityForm.get('transactionType')?.value == '') &&
      this.subActivityForm.get('relatedTransactionId')?.value
    ) {
      let data = _.findWhere(this.listTransactions, {
        id: this.subActivityForm.get('relatedTransactionId')?.value
      });

      if (data) {
        this.selectedTransectionType(data);
      }
    }

    if (localStorage.getItem('UserDetails')) {
      this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      this.userDetails = JSON.parse(this.userDetailsStr);
    }

    var params = {
      salesRepId: this.userDetails.salesRepId,
      type: this.type,
      title: this.subActivityForm.get('title')?.value,
      startDate: this.subActivityForm.get('startDate')?.value,
      endDate: this.subActivityForm.get('endDate')?.value,
      startTime: this.subActivityForm.get('startTime')?.value,
      endTime: this.subActivityForm.get('endTime')?.value,
      relatedCustomerId: parseInt(this.subActivityForm.get('companyId')?.value)
        ? parseInt(this.subActivityForm.get('companyId')?.value)
        : 0,
      relatedContactId: parseInt(this.subActivityForm.get('contact')?.value)
        ? parseInt(this.subActivityForm.get('contact')?.value)
        : 0,
      relatedTransactionId: parseInt(
        this.subActivityForm.get('relatedTransactionId')?.value
      )
        ? parseInt(this.subActivityForm.get('relatedTransactionId')?.value)
        : 0,
      message: this.subActivityForm.get('description')?.value,
      statusId: parseInt(this.subActivityForm.get('statusId')?.value)
        ? parseInt(this.subActivityForm.get('statusId')?.value)
        : 0,
      transactionType: this.subActivityForm.get('transactionType')?.value,
      participantIds: this.listParticipants,
      netSuiteId: 0,
      accountId: 0,
      createBy: '',
      createDate: new Date().toISOString(),
      updatedBy: '',
      updatedDate: new Date().toISOString(),
      dateCreated: new Date().toISOString(),
      phone: this.subActivityForm.get('phoneNumber')?.value.toString()
        ? this.subActivityForm.get('phoneNumber')?.value.toString()
        : '',
      id: this.subActivityForm.get('id')?.value
    };

    if (this.subActivityForm.get('id')?.value > 0) {
      params.netSuiteId = this.netSuiteId ? this.netSuiteId : 0;
      this.commonService
        .PutMethod(
          Apiurl.getAllActivties + '/' + this.subActivityForm.get('id')?.value,
          params,
          true,
          'Loading..',
          null
        )
        .then((resp: any) => {
          if (resp) {
            this.SpinnerService.hide();
            this.commonService.showSuccess('Activity Updated successfully');
            // Swal.fire(
            //   'Success',
            //   'Activity Updated successfully',
            //   'success'
            // ).then(() => {

            if (history && history.state.id) {
              let id = history.state.id;
              let param: NavigationExtras;
              if (history.state.qoute_id) {
                param = {
                  state: {
                    id: id,
                    activity_id: resp,
                    contact_id: history.state.contact_id,
                    qoute_id: history.state.qoute_id
                  }
                };
              } else {
                param = {
                  state: {
                    id: id,
                    activity_id: resp,
                    contact_id: history.state.contact_id
                  }
                };
              }
              if (history.state.from == 'company-detail') {
                this.router.navigate(['companies-details'], param);
              } else if (history.state.from == 'contact') {
                this.router.navigate(['contacts'], param);
              } else if (history.state.from == 'qoute') {
                this.router.navigate(['add-quotes'], param);
              }
            } else {
              let param: NavigationExtras = {
                state: {
                  type: history.state.type,
                  activity_id: history.state.activity_id
                  // from: 'activity-details'
                }
              };

              if (history.state.from == 'activity-details') {
                this.router.navigate(['activities']);
              } else if (history.state.from == 'dashboard') {
                this.router.navigate(['dashboard']);
              } else if (history.state.from == 'activities') {
                this.router.navigate(['activities']);
              } else if (history.state.from == 'opportunity') {
                this.router.navigate(['add-opportunities'], param);
              }
            }
            //});
          } else {
            this.commonService.showError('Failed to Update Activity');
            // Swal.fire(
            //   'Error',
            //   'Failed to Update Activity',
            //   'error'
            // ).then(() => { });
          }
        });
    } else {
      this.commonService
        .PostMethod(Apiurl.saveActivity, params, false, 'Loading.', null)
        .then((responsedata: any) => {
          this.SpinnerService.hide();
          if (responsedata > 0) {
            this.commonService.showSuccess('Activity Saved successfully');
            // Swal.fire('Success', 'Activity Save successfully', 'success').then(
            //   () => {
            // this.router.navigate(["activities"]);
            if (history && history.state.id) {
              let id = history.state.id;
              let param: NavigationExtras;
              if (history.state.qoute_id) {
                param = {
                  state: {
                    id: id,
                    activity_id: responsedata,
                    qoute_id: history.state.qoute_id,
                    from: 'add-activities',
                    isEdit: true
                  }
                };
              } else {
                param = {
                  state: {
                    id: id,
                    activity_id: responsedata,
                    contact_id: history.state.contact_id,
                    from: 'add-activities'
                  }
                };
              }
              if (history.state.from == 'company-detail') {
                this.router.navigate(['companies-details'], param);
              } else if (history.state.from == 'contact') {
                this.router.navigate(['contacts'], param);
              } else if (history.state.from == 'qoute') {
                this.router.navigate(['add-quotes'], param);
              } else if (history.state.from == 'opportunity') {
                this.router.navigate(['add-opportunities'], param);
              }
            } else {
              let param: NavigationExtras = {
                state: {
                  type: history.state.type,
                  activity_id: responsedata,
                  from: 'add-activities'
                  // from: 'activity-details'
                }
              };

              if (history.state.from == 'activity-details') {
                this.router.navigate(['activities']);
              } else if (history.state.from == 'dashboard') {
                this.router.navigate(['dashboard']);
              } else if (history.state.from == 'activities') {
                this.router.navigate(['activities']);
              }
            }
            //});
          } else {
            this.commonService.showError('Failed to Save Activity');
            // Swal.fire(
            //   'Error',
            //   'Failed to Save Activity',
            //   'error'
            // ).then(() => { });
          }
        });
    }
  }
  async ViewActivity() {
    let obj = {
      title: 'Are you sure you want to Cancel?',
      cancelButtonText: 'No',
      confirmButtonText: 'Yes'
    };
    await this.commonService.showConfirm(obj).then((resp: any) => {
      if (resp.isConfirmed) {
        let param: NavigationExtras;
        if (history.state.qoute_id) {
          param = {
            state: {
              id: history.state.id,
              activity_id: history.state.activity_id,
              contact_id: history.state.contact_id,
              qoute_id: history.state.qoute_id,
              from: 'add-activities',
              isEdit: true
            }
          };
        } else {
          param = {
            state: {
              id: history.state.id,
              activity_id: history.state.activity_id,
              contact_id: history.state.contact_id,
              from: 'add-activities'
            }
          };
        }
        // this.router.navigate(['activity-details'], param);
        if (history.state.from == 'activity-details') {
          this.router.navigate(['activities']);
        } else if (history.state.from == 'dashboard') {
          this.router.navigate(['dashboard']);
        } else if (history.state.from == 'activities') {
          this.router.navigate(['activities']);
        } else if (history.state.from == 'company-detail') {
          this.router.navigate(['companies-details'], param);
        } else if (history.state.from == 'contact') {
          this.router.navigate(['contacts'], param);
        } else if (history.state.from == 'qoute') {
          this.router.navigate(['add-quotes'], param);
        } else if (history.state.from == 'opportunity') {
          this.router.navigate(['add-opportunities'], param);
        }
      }
    });
  }
  async getActivityById() {
    if (this.listLookups) {
      if (
        this.listLookups?.company &&
        this.listLookups?.company?.companyList &&
        this.listLookups?.company?.companyList.length
      ) {
        this.listCompanies = this.listLookups?.company?.companyList;
        this.listCompanies = this.commonService.sortAlphabetically(
          this.listCompanies
        );
        this.companyListSearch = this.listLookups.company.companyList;
      }
    }
    if (this.activity_id) {
      await this.commonService
        .GetMethod(
          Apiurl.GetAllActivities + '/' + this.activity_id,
          null,
          true,
          'Loading'
        )
        .then(async (res: any) => {
          if (res) {
            this.activityDetailObj = res;
            this.companyId = res.relatedCustomerId;
            this.subActivityForm.controls['id'].setValue(res.id);
            this.subActivityForm.controls['title'].setValue(res.title);
            this.subActivityForm.controls['startDate'].setValue(
              new Date(res.startDate)
            );
            this.subActivityForm.controls['startTime'].setValue(
              new Date(res.startTime)
            );
            this.netSuiteId = res.netSuiteId;
            this.subActivityForm.controls['phoneNumber'].setValue(
              res.phone
                ? res.phone
                : this.subActivityForm.controls['phoneNumber'].value
            );
            this.subActivityForm.controls['endDate'].setValue(
              new Date(res.endDate)
            );
            this.subActivityForm.controls['endTime'].setValue(
              new Date(res.endTime)
            );
            this.subActivityForm.controls['companyId'].setValue(
              res.relatedCustomerId
            );
            this.subActivityForm.controls['transactionType'].setValue(
              res.transactionType
            );
            this.subActivityForm.controls['relatedTransactionId'].setValue(
              res.relatedTransactionId
            );
            this.getCompanyDetailById(res.relatedCustomerId, res, false);
            this.subActivityForm.controls['salesRepId'].setValue(
              res.salesRepId
            );

            // this.subActivityForm.controls['transaction'].setValue(
            //   res.transactionType + '-' + res.relatedTransactionId
            // );
            if (res.statusId > 0) {
              this.subActivityForm.controls['statusId'].setValue(res.statusId);
              if (res.statusId == 2) {
                this.isSheduled = true;
                this.isCompleted = false;
              }
              if (res.statusId == 1) {
                this.isSheduled = false;
                this.isCompleted = true;
              }
            }
            this.subActivityForm.controls['description'].setValue(res.message);
          }
        });
    } else {
      if (history.state.id != null && history.state.id != undefined) {
        let data = _.findWhere(this.listCompanies, {
          companyId: history.state.id
        });
        this.subActivityForm.controls['companyId'].setValue(history.state.id);
        this.subActivityForm.controls['companyId'].disable();
        this.subActivityForm.controls['companyId'].updateValueAndValidity();
        this.subActivityForm.controls['salesRepId'].setValue(data.salesRepId);
        this.subActivityForm.controls['salesRepId'].updateValueAndValidity();
        await this.getCompanyDetailById(history.state.id, null, true);
      }
    }
  }
  searchCompany(event: any) {
    if (event.target.value != '') {
      this.companyListSearch = this.listCompanies.filter(
        (s: any) =>
          s.companyName
            .toLowerCase()
            .indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.companyListSearch = this.listCompanies;
    }
  }
  async setCompany(obj: any) {
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    var userDetails = JSON.parse(this.userDetailsStr)
    // Set this.userDetails isAdmin validation 
    if (obj?.salesRepId !== userDetails.salesRepId && !userDetails.isAdmin) {
      let obj = {
        title: 'Error',
        text: "Activity SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => {
        this.subActivityForm.controls['companyId'].setValue('');
        this.subActivityForm.controls['salesRepId'].setValue('');
      });
    } else {
      this.listTransactions = [];
      this.listopportunities = [];
      this.listQutoes = [];
      this.subActivityForm.controls['companyId'].setValue(obj.companyId);
      let data = _.findWhere(this.listCompanies, { companyId: obj.companyId });
      if (data) {
        this.subActivityForm.controls['salesRepId'].setValue(data.salesRepId);
      }
      if (this.editflag) {
        this.getCompanyDetailById(obj.companyId, obj, false);
      } else {
        this.getCompanyDetailById(obj.companyId, obj, true);
      }
    }
  }

  getCompanyDetailById(id: any, obj: any, isReset: any = null) {
    this.listTransactions = [];
    this.SpinnerService.show();
    this.commonService
      .GetMethod(Apiurl.getAllCompanies + '/' + id, null, true, 'Loading')
      .then((res: any) => {
        if (res) {
          this.showProgressBar = false;
          this.SpinnerService.hide();
          if (res.opportunities && res.opportunities.length != 0) {
            if (res.opportunities.length == 1) {
              res.opportunities[0].type =
                'Opportunities' + '-' + res.opportunities[0].id;
              if (
                res.opportunities[0].documentNumber != '' &&
                res.opportunities[0].documentNumber != null
              ) {
                res.opportunities[0].typeval =
                  res.opportunities[0].documentNumber + ' - ' + 'Opportunities';
              } else {
                res.opportunities[0].typeval =
                  'OPP#' + res.opportunities[0].id + ' - ' + 'Opportunities';
              }
              this.listTransactions.push(res.opportunities[0]);
              this.subActivityForm.controls['relatedTransactionId'].setValue(
                res.opportunities[0].id
              );
            } else {
              for (let i = 0; i < res.opportunities.length; i++) {
                res.opportunities[i].type =
                  'Opportunities' + '-' + res.opportunities[i].id;
                if (
                  res.opportunities[i].documentNumber != '' &&
                  res.opportunities[i].documentNumber != null
                ) {
                  res.opportunities[i].typeval =
                    res.opportunities[i].documentNumber +
                    ' - ' +
                    'Opportunities';
                } else {
                  res.opportunities[i].typeval =
                    'OPP#' + res.opportunities[i].id + ' - ' + 'Opportunities';
                }
                this.listTransactions.push(res.opportunities[i]);
              }
            }
          }
          if (res.quotes && res.quotes.length != 0) {
            if (res.quotes.length == 1) {
              res.quotes[0].type = 'ESTBG' + '-' + res.quotes[0].id;
              if (
                res.quotes[0].documentNumber != '' &&
                res.quotes[0].documentNumber != null
              ) {
                res.quotes[0].typeval =
                  res.quotes[0].documentNumber + ' - ' + 'Quotes';
              } else {
                res.quotes[0].typeval =
                  'ESTBG' + res.quotes[0].id + ' - ' + 'Quotes';
              }
              this.listTransactions.push(res.quotes[0]);
              this.subActivityForm.controls['relatedTransactionId'].setValue(
                res.quotes[0].id
              );
            } else {
              for (let i = 0; i < res.quotes.length; i++) {
                res.quotes[i].type = 'Quotes' + '-' + res.quotes[i].id;
                if (
                  res.quotes[i].documentNumber != '' &&
                  res.quotes[i].documentNumber != null
                ) {
                  res.quotes[i].typeval =
                    res.quotes[i].documentNumber + ' - ' + 'Quotes';
                } else {
                  res.quotes[i].typeval =
                    'ESTBG' + res.quotes[i].id + ' - ' + 'Quotes';
                }
                this.listTransactions.push(res.quotes[i]);
              }
            }
          }
          if (this.listTransactions.length != 0) {
            if (history.state.qoute_id) {
              let data = _.findWhere(this.listTransactions, {
                id: history.state.qoute_id
              });

              if (data) {
                this.selectedTransectionType(data);
              }
            }
          }
          if (res.relatedContacts && res.relatedContacts.length != 0) {
            this.listParticipants = [];
            this.listContacts = [];
            this.listMainContacts = [];
            if (res.relatedContacts.length == 1) {
              res.relatedContacts[0].isPrimary = false;
              if (res.relatedContacts[0].role == 'Primary Contact') {
                res.relatedContacts[0].isPrimary = true;
              }
              this.listContacts.push(res.relatedContacts[0]);
              this.listMainContacts.push(res.relatedContacts[0]);
              this.listParticipants.push(res.relatedContacts[0].id);
              this.subActivityForm.controls['contact'].setValue(
                res.relatedContacts[0].id
              );
            } else {
              if (isReset) {
                for (let i = 0; i < res.relatedContacts.length; i++) {
                  res.relatedContacts[i].isPrimary = false;
                  if (res.relatedContacts[i].role == 'Primary Contact') {
                    this.subActivityForm.controls['contact'].setValue(
                      res.relatedContacts[i].id
                    );
                    this.subActivityForm.controls['phoneNumber'].setValue(
                      res.relatedContacts[i].phone
                        ? res.relatedContacts[i].phone
                        : this.subActivityForm.controls['phoneNumber'].value
                    );
                    res.relatedContacts[i].isPrimary = true;
                    this.listParticipants.push(res.relatedContacts[i].id);
                  }
                  this.listContacts.push(res.relatedContacts[i]);
                  this.listMainContacts.push(res.relatedContacts[i]);
                  if (this.listContacts.length) {
                    for (
                      let index = 0;
                      index < this.listContacts.length;
                      index++
                    ) {
                      if (
                        this.listContacts[index].id == history.state.contact_id
                      ) {
                        this.subActivityForm.controls['contact'].setValue(
                          this.listContacts[index].id
                        );
                        this.subActivityForm.controls['phoneNumber'].setValue(
                          this.listContacts[index].phone
                            ? this.listContacts[index].phone
                            : this.subActivityForm.controls['phoneNumber'].value
                        );
                        this.subActivityForm.controls['contact'].disable();
                      }
                    }
                  }
                }
              } else {
                if (res.relatedContacts.length) {
                  res.relatedContacts.forEach((element1: any) => {
                    element1.isPrimary = false;
                    if (this.activityDetailObj.participantIds.length) {
                      this.activityDetailObj.participantIds
                        .split(',')
                        .forEach((element: any) => {
                          element = parseInt(element);
                          if (element1.id == element) {
                            element1.isPrimary = true;
                            this.listParticipants.push(element1.id);
                            // this.listContacts.push(element1)
                          }
                        });
                    }
                    this.listContacts.push(element1);
                    this.listMainContacts.push(element1);
                    if (this.listContacts.length) {
                      for (
                        let index = 0;
                        index < this.listMainContacts.length;
                        index++
                      ) {
                        if (obj.relatedContactId && obj.relatedContactId != 0) {
                          if (
                            this.listMainContacts[index].id ==
                            obj.relatedContactId
                          ) {
                            this.subActivityForm.controls['contact'].setValue(
                              this.listMainContacts[index].id
                            );
                            this.subActivityForm.controls[
                              'phoneNumber'
                            ].setValue(
                              this.listMainContacts[index].phone
                                ? this.listMainContacts[index].phone
                                : this.subActivityForm.controls['phoneNumber']
                                  .value
                            );
                          }
                        } else if (
                          history.state.contact_id &&
                          history.state.contact_id != 0
                        ) {
                          if (
                            this.listMainContacts[index].id ==
                            history.state.contact_id
                          ) {
                            this.subActivityForm.controls['contact'].setValue(
                              this.listMainContacts[index].id
                            );
                          }
                        }
                      }
                    }
                  });
                }
              }
            }
          }
        }
      });
  }

  setActivityStatus(id: any) {
    this.subActivityForm.controls['statusId'].setValue(id);
    if (id == 2) {
      this.isSheduled = true;
      this.isCompleted = false;
    } else {
      this.isSheduled = false;
      this.isCompleted = true;
    }
  }
  clearCompanyDetails() {
    this.searchdropdown = '';
    this.companyListSearch = this.listCompanies;
  }

  datechange(event: any, from: any) {
    if (from == 'startDate') {
      this.subActivityForm.controls['startDate'].setValue(event._d);
      if (this.headerTitle == 'Edit Activity') {
        this.subActivityForm.controls['startTime'].setValue(
          this.setEndTime(
            this.subActivityForm.controls['startDate'].value,
            this.subActivityForm.controls['startTime'].value
          )
        );
        this.subActivityForm.controls['endTime'].setValue(
          this.setEndTimeOnePlus(
            this.subActivityForm.controls['startDate'].value,
            this.subActivityForm.controls['endTime'].value
          )
        );
      } else {
        this.subActivityForm.controls['startTime'].setValue(
          this.setEndTime(
            this.subActivityForm.controls['startDate'].value,
            this.subActivityForm.controls['startTime'].value
          )
        );
        this.subActivityForm.controls['endTime'].setValue(
          this.setEndTimeOnePlus(
            this.subActivityForm.controls['startDate'].value,
            this.subActivityForm.controls['startTime'].value
          )
        );
      }
    } else if (from == 'endDate') {
      this.subActivityForm.controls['endDate'].setValue(event._d);
      this.subActivityForm.controls['endTime'].setValue(
        this.setEndTime(
          this.subActivityForm.controls['endDate'].value,
          this.subActivityForm.controls['endTime'].value
        )
      );
      if (
        new Date(this.subActivityForm.controls['startDate'].value).setHours(
          0,
          0,
          0,
          0
        ) >
        new Date(this.subActivityForm.controls['endDate'].value).setHours(
          0,
          0,
          0,
          0
        )
      ) {
        this.endDateError = true;
      } else {
        this.endDateError = false;
      }
    }
  }
  timeChange(from: any) {
    if (from == 'endTime') {
      if (
        new Date(this.subActivityForm.controls['startTime'].value.getTime()) >
        new Date(this.subActivityForm.controls['endTime'].value.getTime())
      ) {
        this.endTimeError = true;
      } else {
        this.endTimeError = false;
      }
    }
  }
  setEndTime(startDate: any, startTime: any) {
    const endTime = new Date(startDate);
    endTime.setHours(startTime.getHours());
    endTime.setMinutes(startTime.getMinutes());
    endTime.setSeconds(startTime.getSeconds());
    return endTime;
  }
  setEndTimeOnePlus(startDate: any, startTime: any) {
    const endTime = new Date(startDate);
    endTime.setHours(startTime.getHours() + 1);
    endTime.setMinutes(startTime.getMinutes());
    endTime.setSeconds(startTime.getSeconds());
    return endTime;
  }
  //activity add
  onClickFab(event: any, isReset = false) {
    if (this.subActivityForm) {
      this.subActivityForm.controls['phoneNumber'].setValidators([]);
      this.subActivityForm.controls['phoneNumber'].updateValueAndValidity();
    }

    const endTime = new Date(this.selectedDate);
    const startTime = new Date(this.selectedDate);

    startTime.setHours(new Date().getHours());
    startTime.setMinutes(new Date().getMinutes());
    startTime.setSeconds(new Date().getSeconds());

    endTime.setHours(new Date().getHours() + 1);
    endTime.setMinutes(new Date().getMinutes());
    endTime.setSeconds(new Date().getSeconds());
    if (isReset) {
      if (this.subActivityForm) {
        this.subActivityForm.controls['id'].setValue(0);
        this.subActivityForm.controls['startDate'].setValue(
          new Date(this.selectedDate)
        );
        this.subActivityForm.controls['endDate'].setValue(
          new Date(this.selectedDate)
        );
        this.subActivityForm.controls['startTime'].setValue(startTime);
        this.subActivityForm.controls['endTime'].setValue(endTime);
        this.subActivityForm.controls['salesRepId'].setValue(0);
        this.subActivityForm.controls['statusId'].setValue(0);
        this.subActivityForm.controls['phoneNumber'].setValue('');
        this.subActivityForm.controls['transactionType'].setValue('');
      }
    }
    this.activeBtn = event.tooltip;
    if (event.tooltip == 'Task') {
      this.type = 'Task';
    } else if (event.tooltip == 'Event') {
      this.type = 'Event';
    } else if (event.tooltip == 'Phone') {
      this.type = 'Phone';
      if (this.subActivityForm) {
        this.subActivityForm.controls['phoneNumber'].setValidators(
          Validators.required
        );
        this.subActivityForm.controls['phoneNumber'].updateValueAndValidity();
      }
    }
  }

  selectedTransectionType(obj: any) {
    if (obj) {
      this.subActivityForm.controls['relatedTransactionId'].setValue(obj.id);
      if (obj.typeval.toLowerCase().includes('opp')) {
        this.subActivityForm.controls['transactionType'].setValue(
          'Opportunities'
        );
      }
      if (obj.typeval.toLowerCase().includes('quote')) {
        this.subActivityForm.controls['transactionType'].setValue('Quotes');
      }
    }
  }
}
