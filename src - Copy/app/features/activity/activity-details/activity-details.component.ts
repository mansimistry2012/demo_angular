import { Location } from '@angular/common';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { ThemePalette } from '@angular/material/core';
import { LayoutService } from '../../../features/layout/layout.service';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { CommonProvider } from '../../../shared/common';
import { Apiurl } from '../../../shared/route';
import * as _ from 'underscore';
import { Subject } from 'rxjs';
import {
  CalendarView,
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent
} from 'angular-calendar';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CalendarSchedulerViewComponent } from 'angular-calendar-scheduler';
import { isSameDay, isSameMonth } from 'date-fns';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-activity-details',
  templateUrl: './activity-details.component.html',
  styleUrls: ['./activity-details.component.scss']
})
export class ActivityDetailsComponent implements OnInit {
  // ContactForm: any;
  test = 'hello';
  CompanyList: any[] = [];
  RoleList: any[] = [];
  SubsidiariesList: any[] = [];
  StateList: any[] = [];
  CountryList: any[] = [];
  accountId: any;
  companyId: any;
  netSuiteId: any;
  openBlock: any = 1;
  ActivityId: any;
  activityDetails: any = [];
  IsEditClick: boolean = false;
  StateName: any;
  CountryName: any;
  phoneLength = 0;
  phoneData: any;
  postCodeLength = 0;
  postCodeData: any;
  filterTypeList: any = [];
  selectedView: any = 'Agenda';
  // dataSource: any;
  dataSource: any = null;
  listParticipants: any = [];
  isAllowEdit: any;
  userDetailsStr: any;
  displayedColumns: string[] = [
    'eventType',
    'eventTitle',
    'relatedCompany',
    'relatedTransaction',
    'relatedContact',
    'startDate',
    'startTime',
    'endTime'
    // 'salesRep'
    // 'phoneNumber'
  ];
  companyListSearch: any;
  companyType: any;
  activity_id: number;
  contactData: any = [];

  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;

  view: any = 'Agenda';
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  modalData: {
    action: string;
    event: CalendarEvent;
  };
  actions: CalendarEventAction[] = [
    {
      label: '<i class="fas fa-fw fa-pencil-alt"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fas fa-fw fa-trash-alt"></i>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();
  showFilter: boolean = false;
  statusType: any;
  relatedTransaction: any;
  events: CalendarEvent[];
  filterType: any;
  activeDayIsOpen: boolean = true;
  searchInput: any;
  statusList: any = [];
  lookup: any = [];
  salesrep: any;
  @ViewChild(CalendarSchedulerViewComponent)
  calendarScheduler: CalendarSchedulerViewComponent;
  showMoreCmnt = false;
  constructor(
    public common: CommonProvider,
    public router: Router,
    private modal: NgbModal,
    private layoutService: LayoutService,
    private location: Location,
    private commonService: CommonProvider
  ) { }
  customer_id: any;
  showProgressBar: boolean = false;
  color: ThemePalette = 'primary';
  mode: ProgressBarMode = 'indeterminate';
  value = 50;
  bufferValue = 75;

  ngOnInit(): void {
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();

    // this.FillDropDowns();
    if (history.state.activity_id) {
      this.ActivityId = history.state.activity_id;
      this.showProgressBar = true;
      this.getActivityDetailById();
      this.getCalenderEvent();
    } else {
      this.router.navigate(['activities']);
      setTimeout(() => {
        this.layoutService.activeRoute = this.router.url;
        this.layoutService.getcurrentActivateRoute(
          this.layoutService.activeRoute
        );
      }, 50);
    }
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }
  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }
  statusTypeChange(event: any) { }
  filterTypeChange(event: any) { }
  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
    this.showFilter = false;
  }
  clearSearch() {
    this.searchInput = '';
    this.filterType = null;
    this.statusType = null;
  }
  // getAllContactList() {
  //   this.listParticipants = [];
  //   this.getCompanyDetailById(this.activityDetails.relatedCustomerId)
  //   this.common.GetMethod(Apiurl.getContactList, null, true, 'Loading')
  //     .then((res: any) => {
  //       if (res) {
  //
  //         for (let i = 0; i < res.length; i++) {
  //           if (this.activityDetails.participantIds.length != 0) {
  //             for (let j = 0; j < this.activityDetails.participantIds.length; j++) {
  //               if (res[i].id == this.activityDetails.participantIds[j]) {
  //                 this.listParticipants.push(res[i]);
  //               }
  //             }
  //           }
  //         }
  //       }
  //     });
  // }

  getCompanyDetailById(id: any) {
    this.listParticipants = [];
    this.common
      .GetMethod(Apiurl.getAllCompanies + '/' + id, null, true, 'Loading')
      .then((res: any) => {
        if (res) {
          // for (let i = 0; i < res.relatedContacts.length; i++) {
          //   if (this.activityDetails.participantIds.split(",").length != 0) {
          //     for (let j = 0; j < this.activityDetails.participantIds.split(",").length; j++) {
          //       let element = parseInt(this.activityDetails.participantIds.split(",")[j])
          //
          //       if (res.relatedContacts[i].id == parseInt(this.activityDetails.participantIds[j])) {
          //
          //         this.listParticipants.push(res.relatedContacts[i]);
          //       }
          //     }
          //   }
          // }
          if (res.relatedContacts && res.relatedContacts.length) {
            res.relatedContacts.forEach((element1: any) => {
              element1.isPrimary = false;
              if (this.activityDetails.participantIds.length) {
                this.activityDetails.participantIds
                  .split(',')
                  .forEach((element: any) => {
                    element = parseInt(element);
                    if (element1.id == element) {
                      element1.isPrimary = true;
                      this.listParticipants.push(element1);
                      // this.listParticipants.push(element1)
                    }
                  });
              }
            });
          }
          this.showProgressBar = false;
        }
      });
  }

  // async FillDropDowns() {
  //   await this.common
  //     .GetMethod(Apiurl.getAllCompanies, null, true, 'Loading..')
  //     .then((resp: any) => {
  //       if (resp.length != 0) {
  //         this.CompanyList = [];
  //         for (var i = 0; i < resp.length; i++) {
  //           this.CompanyList.push({
  //             id: resp[i].id,
  //             text: resp[i].tradingName,
  //             netSuiteId: resp[i].netSuiteId,
  //             accountId: resp[i].accountId
  //           });
  //         }
  //         this.companyListSearch = this.CompanyList;
  //       }
  //     });
  // }

  isRequired(controlName: string) {
    if (!controlName) {
      return false;
    }
    return;
  }

  async ViewContact() {
    let obj = {
      title: 'Are you sure you want to Cancel?',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    };
    await this.common.showConfirm(obj).then((resp: any) => {
      if (resp.isConfirmed) {
        this.location.back();
        // let id = history.state.id;
        // let param: NavigationExtras;
        // if (history.state.qoute_id) {
        //   param = {
        //     state: {
        //       id: id,
        //       activity_id: history.state.activity_id,
        //       contact_id: history.state.contact_id,
        //       qoute_id: history.state.qoute_id,
        //       from: 'add-activities'
        //     }
        //   };
        // } else {
        //   param = {
        //     state: {
        //       id: id,
        //       activity_id: history.state.activity_id,
        //       contact_id: history.state.contact_id,
        //       from: 'add-activities'
        //     }
        //   };
        // }
        // if (history.state.from == "dashboard") {
        //   this.router.navigate(['dashboard']);
        // }
        // else if (history.state.from == 'company-detail') {
        //   this.router.navigate(['companies-details'], param);
        // }
        // else if (history.state.from == 'contacts') {
        //   this.router.navigate(['contacts'], param);
        // } else {
        //   this.router.navigate(['activities']);
        // }
      }
    });
  }

  goToPage() {
    let data = {
      id: this.activityDetails.id,
      netsuitid: this.activityDetails.netSuiteId,
      from: 'contacts'
    };

    let id: NavigationExtras = {
      state: {
        id: data
      }
    };
    // this.router.navigate(['add-company'], id);
  }
  async getActivityDetailById() {
    await this.common
      .GetMethod(
        Apiurl.GetAllActivities + '/' + this.ActivityId,
        null,
        true,
        'Loading'
      )
      .then((res: any) => {
        if (res) {
          this.activityDetails = [];
          this.activityDetails = res;
          if (res.salesRepId) {
            this.common.lookupList.subscribe((resp: any) => {
              this.lookup = resp;
              this.salesrep = this.lookup.opportunity.salesRep;
              let salesReps = this.salesrep.filter(
                (x: any) => x.id == this.activityDetails.salesRepId
              );
              if (salesReps && salesReps.length) {
                this.activityDetails.salesRep =
                  salesReps[0].salesRepDescription;
              }
            });
          }
          if (
            this.activityDetails &&
            this.activityDetails.relatedCustomerId != null &&
            this.activityDetails.relatedCustomerId != 0
          ) {
            this.getCompanyDetailById(this.activityDetails.relatedCustomerId);
          } else if (this.activityDetails.transactionType == 'Opportunities') {
            this.getOpportunityDetails();
          } else if (this.activityDetails.transactionType == 'Quotes') {
            this.getQuotesDetails();
          }
        }
      });
  }
  showMore() {
    this.showMoreCmnt = !this.showMoreCmnt;
  }
  getOpportunityDetails() {
    this.common
      .GetMethod(Apiurl.getAllOpportunityList, null, true, 'Loading')
      .then((res: any) => {
        if (res) {
          for (let i = 0; i < res.length; i++) {
            if (res[i].id == this.activityDetails.relatedTransactionId) {
              if (res[i].documentNumber != '') {
                this.relatedTransaction =
                  res[i].documentNumber +
                  ' - ' +
                  this.activityDetails.transactionType;
              } else {
                this.relatedTransaction =
                  'OPP' +
                  res[i].id +
                  ' - ' +
                  this.activityDetails.transactionType;
              }
            }
          }
          this.showProgressBar = false;
        }
      });
  }
  getQuotesDetails() {
    this.common
      .GetMethod(Apiurl.getAllQuotes, null, true, 'Loading')
      .then((res: any) => {
        if (res) {
          for (let i = 0; i < res.length; i++) {
            if (res[i].id == this.activityDetails.relatedTransactionId) {
              if (res[i].documentNumber != '') {
                this.relatedTransaction =
                  res[i].documentNumber +
                  ' - ' +
                  this.activityDetails.transactionType;
              } else {
                this.relatedTransaction =
                  'QUOTE' +
                  res[i].id +
                  ' - ' +
                  this.activityDetails.transactionType;
              }
            }
          }
          this.showProgressBar = false;
        }
      });
  }
  viewFilterField() {
    this.showFilter = !this.showFilter;
  }
  OpenBlock(id: any) {
    this.openBlock = id;
    this.StateName = this.StateList.filter(
      x => x.id == this.activityDetails.residentialAddress.stateId
    ).map(x => x.stateDescription)[0];
    this.CountryName = this.CountryList.filter(
      x => x.id == this.activityDetails.residentialAddress.countryId
    ).map(x => x.countryDescription)[0];
  }
  async EditClick() {
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    var userDetails = JSON.parse(this.userDetailsStr)
    // Set Admin and SalesrepId validation
    if (this.activityDetails.salesRepId !== userDetails.salesRepId && !userDetails.isAdmin) {
      let obj = {
        title: 'Error',
        text: "Activity SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => { });
    } else {
      let param: NavigationExtras = {
        state: {
          type: history.state.type,
          activity_id: history.state.activity_id,
          from: 'activity-details'
        }
      };
      this.router.navigate(['/sub-activity'], param);
    }
  }
  AddNewContact() {
    this.IsEditClick = true;
    this.ActivityId = 0;
  }
  viewAgenda() {
    this.view = 'Agenda';
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  gotoActivities() {
    let param: NavigationExtras = {
      state: {
        type: 'Event',
        from: 'contact',
        contact_id: history.state.contact_id,
        id: this.customer_id
      }
    };
    this.router.navigate(['sub-activity'], param);
  }
  searchCompany(event: any) {
    if (event.target.value != '') {
      this.companyListSearch = this.CompanyList.filter(
        (s: any) =>
          s.text.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.companyListSearch = this.CompanyList;
    }
  }

  getCalenderEvent() {
    this.common
      .GetMethod(
        Apiurl.getAllActivties + '/' + this.ActivityId,
        null,
        true,
        'Loading..'
      )
      .then((res: any) => {
        this.isAllowEdit =
          res.salesRepId == JSON.parse(this.userDetailsStr).salesRepId
            ? true
            : false;
        this.dataSource = new MatTableDataSource([{ ...res }]);

        // this.dataSource = res;
        // this.mainDataSouce = res;
        this.events = [];

        let end: any;
        if (
          new Date(res.endDate).setHours(0, 0, 0, 0) ==
          new Date().setHours(0, 0, 0, 0)
        ) {
          end = null;
        } else {
          end = new Date(res.endDate);
        }
        this.events.push({
          start: new Date(res.startDate),
          // end: end,
          // end: new Date(res.endTime),
          title: res.title,
          actions: this.actions,

          resizable: {
            beforeStart: true,
            afterEnd: true
          },
          draggable: true
        });
      });
  }
  async ViewActivity() {
    let obj = {
      title: 'Are you sure you want to Cancel?',
      cancelButtonText: 'No',
      confirmButtonText: 'Yes'
    };
    await this.common.showConfirm(obj).then((resp: any) => {
      if (resp.isConfirmed) {
        // this.location.back();
        this.router.navigate(['activities']);
        // let id = history.state.id;
        // let param: NavigationExtras;
        // if (history.state.qoute_id) {
        //   param = {
        //     state: {
        //       id: id,
        //       activity_id: history.state.activity_id,
        //       contact_id: history.state.contact_id,
        //       qoute_id: history.state.qoute_id,
        //       from: 'add-activities'
        //     }
        //   };
        // } else {
        //   param = {
        //     state: {
        //       id: id,
        //       activity_id: history.state.activity_id,
        //       contact_id: history.state.contact_id,
        //       from: 'add-activities'
        //     }
        //   };
        // }
        // if (history.state.from == "dashboard") {
        //   this.router.navigate(['dashboard']);
        // }
        // else if (history.state.from == 'company-detail') {
        //   this.router.navigate(['companies-details'], param);
        // }
        // else if (history.state.from == 'contacts') {
        //   this.router.navigate(['contacts'], param);
        // } else {
        //   this.router.navigate(['activities']);
        // }
      }
    });
  }

  //

  addContact() {
    let param: NavigationExtras = {
      state: {
        contact_id: 0,
        from: 'qoute'
        // id: this.rowData.customerId,
        // qoute_id: this.rowData.id
      }
    };
    // this.router.navigate(['contacts'], param);
  }
  gotoCompany(item: any) {
    let param: NavigationExtras = {
      state: {
        id: item.relatedCustomerId
      }
    };
    this.router.navigate(['companies-details'], param);
    setTimeout(() => {
      this.layoutService.activeRoute = this.router.url;
      this.layoutService.getcurrentActivateRoute(
        this.layoutService.activeRoute
      );
    }, 50);
  }
  gotoRelatedContact(item: any) {
    let param = {
      state: {
        id: item.relatedCustomerId,
        activity_id: history.state.activity_id,
        contact_id: item.relatedContactId,
        from: 'add-activities'
      }
    };
    this.router.navigate(['contacts'], param);
    setTimeout(() => {
      this.layoutService.activeRoute = this.router.url;
      this.layoutService.getcurrentActivateRoute(
        this.layoutService.activeRoute
      );
    }, 50);
  }
  gotoTranslation(item: any) {
    if (item.transactionType != '') {
      let param = {
        state: {
          qoute_id: item.relatedTransactionId,
          contact_id: 0,
          from: 'add-activities',
          isEdit: true
        }
      };
      if (item.transactionType == 'Opportunities') {
        this.router.navigate(['add-opportunities'], param);
      }
      if (item.transactionType == 'Quotes') {
        this.router.navigate(['add-quotes'], param);
      }
    }
    setTimeout(() => {
      this.layoutService.activeRoute = this.router.url;
      this.layoutService.getcurrentActivateRoute(
        this.layoutService.activeRoute
      );
    }, 50);
  }
  getSalesRep(id: any) {
    let salesRep = _.findWhere(this.lookup?.opportunity?.salesRep, { id: id });
    if (salesRep) {
      return salesRep.salesRepDescription;
    }
  }
}
