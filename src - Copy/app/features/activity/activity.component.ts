import { ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NavigationExtras, Router } from '@angular/router';
import * as _ from 'underscore';
import { CommonProvider } from '../../shared/common';
import { Apiurl } from '../../shared/route';
import { NgxSpinnerService } from 'ngx-spinner';
import { SpeedDialFabPosition } from '../../shared/speed-dial-fab/speed-dial-fab.component';
import { MatPaginator } from '@angular/material/paginator';
import { CalendarView, CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent } from 'angular-calendar';
import { Subject } from 'rxjs';
import { startOfDay, endOfDay, isSameDay, isSameMonth } from 'date-fns';
const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  },
  purpule: {
    primary: '#7E38FB  ',
    secondary: '#7E38FB '
  }
};
@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {
  CalendarView = CalendarView;
  pageSize = 1000;
  speedDialFabButtons = [
    {
      icon: 'note',
      tooltip: 'Task',
      class: 'note-color'
    },
    {
      icon: 'event',
      tooltip: 'Event',
      class: 'event-color'
    },
    {
      icon: 'phone',
      tooltip: 'Phone',
      class: 'phone-color'
    }
  ];

  SpeedDialFabPosition = SpeedDialFabPosition;
  speedDialFabColumnDirection = 'column';
  speedDialFabPosition = SpeedDialFabPosition.Top;
  speedDialFabPositionClassName = 'speed-dial-container-top';

  onPositionChange(position: SpeedDialFabPosition) {
    switch (position) {
      case SpeedDialFabPosition.Bottom:
        this.speedDialFabPositionClassName = 'speed-dial-container-bottom';
        this.speedDialFabColumnDirection = 'column-reverse';
        break;
      default:
        this.speedDialFabPositionClassName = 'speed-dial-container-top';
        this.speedDialFabColumnDirection = 'column';
    }
  }

  onSpeedDialFabClicked(btn: any) {
    this.GoToSubActivity(btn.tooltip);
    // this.gotoActivities(btn.tooltip)
  }

  displayedColumns: string[] = [
    'type',
    'title',
    'startDate',
    'duration',
    'salesRep',
    'statusId',
    'relatedCustomer',
    'relatedTransaction',
    'relatedContact',
    'phone'
    // 'startTime',
    // 'endTime'
    // 'phoneNumber'
  ];
  mainDataSouce: any = [];
  showFilter: boolean = false;
  @ViewChild(MatSort) sort: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  //scheduler
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;
  viewDate: Date = new Date();

  panelOpenState = false;
  dataSource: any;
  todayDate = new Date();
  Type: any;
  Status: any;
  searchText: any = '';
  listLookups: any = [];
  salesRepId: any;
  selectedView: any = 'Agenda';
  view: any = 'Agenda';
  events: CalendarEvent[];
  refresh: Subject<any> = new Subject();
  activeDayIsOpen: boolean = true;
  dayStartHour = 9;
  weekStartsOn = 1;
  // dayEndHour = 23

  modalData: {
    action: string;
    event: CalendarEvent;
  };
  actions: CalendarEventAction[] = [
    {
      label: '<i class="fas fa-fw fa-pencil-alt"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fas fa-fw fa-trash-alt"></i>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];
  constructor(
    private router: Router,
    public commonService: CommonProvider,
    private SpinnerService: NgxSpinnerService,
    private cdr: ChangeDetectorRef
  ) { }
  ngOnInit(): void {
    this.getAllActivityList();
    this.getAllLookups();
  }

  ngAfterViewInit() {
    this.cdr.detectChanges()
  }

  openFilter() {
    this.showFilter = !this.showFilter;
  }

  GoToSubActivity(getType: any) {

    // localStorage.setItem('ActivityType', getType);
    // this.router.navigate(["/sub-activity"]);
    let param: NavigationExtras = {
      state: {
        type: getType,
        from: 'activities'
      }
    };
    this.router.navigate(['/sub-activity'], param);
  }

  // getAllActivityList() {
  //   //this.SpinnerService.show();
  //   this.commonService
  //     .GetMethod(Apiurl.GetAllActivities, null, true, 'Loading')
  //     .then((res: any) => {
  //       if (res) {
  //         
  //         this.mainDataSouce = res;
  //         this.dataSource = new MatTableDataSource(res);
  //         this.dataSource.sort = this.sort;
  //         this.SpinnerService.hide();
  //       }
  //     });
  // }
  getAllActivityList() {
    //this.SpinnerService.show();
    this.commonService
      .GetMethod(Apiurl.GetAllActivities, null, true, 'Loading')
      .then((res: any) => {
        if (res) {
          this.mainDataSouce = res;
          this.events = [];
          this.dataSource = new MatTableDataSource(res);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          for (let index = 0; index < res.length; index++) {
            let end: any;
            if (
              new Date(res[index].endTime).setHours(0, 0, 0, 0) ==
              new Date().setHours(0, 0, 0, 0)
            ) {
              end = null;
            } else {
              end = new Date(res[index].endTime);
            }
            this.events.push({
              start: new Date(res[index].startTime),
              // end: end,
              end: new Date(res[index].endTime),
              title: res[index].title,
              id: res[index].id,
              color: colors.purpule,
              actions: this.actions,
              resizable: {
                beforeStart: true,
                afterEnd: true
              }
            });
          }
          this.SpinnerService.hide();
        }
      });
  }
  setView(view: CalendarView) {
    this.view = view;
  }
  viewAgenda() {
    this.view = 'Agenda';
  }
  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
    this.showFilter = false;
  }
  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }
  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }
  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };

    var response = _.filter(this.dataSource.filteredData, {
      id: this.modalData.event.id
    });

    this.selection(response[0]);
    //    this.modal.open(this.modalContent, { size: 'lg' });
  }
  addEvent(): void {
    this.events = [
      ...this.events,
      {
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: colors.purpule,

        resizable: {
          beforeStart: true,
          afterEnd: true
        }
      }
    ];
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter(event => event !== eventToDelete);
  }
  // getAllLookups() {
  //   this.listLookups = [];
  //   this.commonService
  //     .GetMethod(Apiurl.getAllLookup, null, true, 'Loading..')
  //     .then((res: any) => {
  //       this.listLookups = res;
  //     });
  // }
  getAllLookups() {
    let self = this;
    return new Promise(function (resolve, reject) {
      self.commonService.lookupList.subscribe((list: any) => {
        if (list) {
          self.listLookups = list;
          resolve(true)
        } else {
          reject(true);
        }
      });
    })
  }
  inputEvent(event: any) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filterTypeChange(filterValue);
  }
  selection(value: any) {
    let param: NavigationExtras = {
      state: {
        type: value.type,
        activity_id: value.id,
        from: 'activities'
      }
    };
    // this.router.navigate(['/sub-activity'], param);
    this.router.navigate(['/activity-details'], param);
  }
  clearFilterType(event: any) {
    this.Type = null;
    event.stopPropagation();
  }
  clearFilterStatus(event: any) {
    this.Status = null;
    event.stopPropagation();
    this.filterTypeChange(event);
  }
  clearFilterSalesRep(event: any) {
    this.salesRepId = null;
    event.stopPropagation();
    this.filterTypeChange(event);
  }
  filterTypeChange(from: any) {
    this.dataSource = new MatTableDataSource(this.mainDataSouce);

    if (this.searchText !== '') {
      this.dataSource.filter = this.searchText.trim().toLowerCase();
    }

    if (this.Type) {
      this.dataSource = new MatTableDataSource(
        _.filter(this.dataSource.filteredData, { type: this.Type })
      );
    }
    if (this.Status) {
      this.dataSource = new MatTableDataSource(
        _.filter(this.dataSource.filteredData, {
          statusId: parseInt(this.Status)
        })
      );
    }
    if (this.salesRepId) {
      this.dataSource = new MatTableDataSource(
        _.filter(this.dataSource.filteredData, { salesRepId: this.salesRepId })
      );
    }
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  clearSearch() {
    this.Type = null;
    this.Status = null;
    this.salesRepId = null;
    this.dataSource = new MatTableDataSource(this.mainDataSouce);
  }
  clear() {
    this.searchText = '';
    this.getAllActivityList();
  }
}
