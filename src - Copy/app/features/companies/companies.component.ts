import { Apiurl } from './../../shared/route';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { CommonProvider } from '../../shared/common';
import { Router, NavigationExtras } from '@angular/router';
import * as _ from 'underscore';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ThemePalette } from '@angular/material/core';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { PageChangeEvent } from '@progress/kendo-angular-grid';
import { SortDescriptor } from '@progress/kendo-data-query';
@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit {
  searchText: string = '';
  sortName: string = 'tradingName';
  sortBy: any = 'asc';
  filterShow: boolean = false;
  pageIndex: number = 1;
  pageLength: number = 100;
  state = {
    take: this.pageLength,
    skip: 0
  };
  totalCount: number;
  displayedColumns: string[] = [
    'stage',
    'tradingName',
    'legalName',
    'leadStatus',
    'phone',
    'email',
    'leadSource',
    'marketIndustry',
    'tier',
    'salesRep'
  ];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: any;
  TodayDate: any = new Date().toDateString();
  panelOpenState = false;
  showFilter: boolean = false;
  stageList: any = [];
  statusList: any = [];
  marketList: any = [];
  salesRepList: any = [];
  stageType: any = '';
  statusType: any = '';
  marketType: any = '';
  salesRepType: any;
  mainDataSouce: any = [];
  searchInput: any;
  isLoading = true;
  searchComapany = new Subject<string>();
  nodata: boolean = false;
  showProgressBar: boolean = false;
  color: ThemePalette = 'primary';
  mode: ProgressBarMode = 'indeterminate';
  value = 50;
  bufferValue = 75;
  listLookups: any = [];
  userIsAdmin: boolean = false;
  userDetails: any;
  userDetailsStr: any;
  salesRepId: any;
  public sort: SortDescriptor[] = [
    {
      field: 'tradingName',
      dir: 'asc'
    }
  ];
  defaultSalesRepType: any;
  constructor(
    private commonService: CommonProvider,
    private router: Router,
    private SpinnerService: NgxSpinnerService,
    private cdr: ChangeDetectorRef
  ) {
    this.searchComapany
      .pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(value => {
        this.filterCompanyList(value);
      });
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  ngOnInit(): void {
    /*--with status filter data--*/
    // if (history.state.stage && history.state.status) {
    //   this.stageType = history.state.stage;
    //   this.statusType = history.state.status;
    //   this.showFilter = true;
    // }
    /*--without status filter data--*/
    if (localStorage.getItem('pageLength')) {
      let pageLength = localStorage.getItem('pageLength') as string;
      this.pageLength = parseInt(pageLength);
    }

    this.getAllStages();
    this.getUserDetailsFromStorage();
    if (!history.state.stage) {
      this.getAllCompanies();
    }
  }
  getUserDetailsFromStorage() {
    let userDetailsStr: any = '';
    if (localStorage.getItem('UserDetails')) {
      console.log("LocalStorage", localStorage.getItem('UserDetails'))
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      this.userDetails = JSON.parse(userDetailsStr);
      this.userIsAdmin = this.userDetails.isAdmin;
      this.defaultSalesRepType = this.getSalesRep(this.userDetails.salesRepId);
      this.salesRepId = this.userDetails.salesRepId;
    }
  }
  filterCompanyList(val?: any) {
    // this.dataSource = [];
    this.salesRepId = val.id;
    this.getAllCompanies();
  }
  getAllCompanies() {
    //this.SpinnerService.show();
    this.showProgressBar = true;

    let param: any = {};
    if (this.stageType) {
      param.st = this.stageType;
    }
    if (this.marketType) {
      param.mi = this.marketType;
    }
    if (this.salesRepType) {
      param.sr = this.salesRepType;
    }
    if (this.searchText) {
      param.search = this.searchText;
    }
    if (this.sortBy) {
      param.sortBy = this.sortBy;
    }
    if (this.sortName) {
      param.sortName = this.sortName;
    }
    // if (this.userDetails && !this.userDetails.isAdmin) {
    param.subsidiaryId = this.userDetails.subsidiaryId;
    // param.salesRepId = this.userDetails.salesRepId;
    if (this.salesRepId != null) {
      param.salesRepId = this.salesRepId;
    }
    // }
    this.commonService
      .GetMethod(
        Apiurl.getAllCompanies + '/' + this.pageIndex + '/' + this.pageLength,
        param,
        true,
        'Loading'
      )
      .then((res: any) => {
        //
        if (res && res.length) {
          this.mainDataSouce = res;
          this.dataSource = this.mainDataSouce;
          this.totalCount = res[0].totalCount;
          this.dataSource = {
            data: this.mainDataSouce.slice(
              this.state.skip,
              this.state.skip + this.state.take
            ),
            total: this.totalCount
          };
          // this.dataSource.sort = this.sort;
          // this.dataSource.sort.matSortChange;

          this.nodata = false;
          this.SpinnerService.hide();
        } else {
          this.mainDataSouce = res;
          this.dataSource = res;
          this.totalCount = 0;
          this.dataSource = {
            data: this.mainDataSouce.slice(
              this.state.skip,
              this.state.skip + this.state.take
            ),
            total: this.totalCount
          };
          this.nodata = true;
        }
        this.showProgressBar = false;
      })
      .catch(error => {
        this.showProgressBar = true;
        this.isLoading = false;

        this.SpinnerService.hide();
      });
  }
  AddCompanies() {
    let param: NavigationExtras = {
      state: {
        from: 'company-list'
      }
    };
    this.router.navigate(['add-companies'], param);
  }
  showDetails(row: any) {
    let param: NavigationExtras = {
      state: {
        id: row.id,
        row
      }
    };
    this.router.navigate(['companies-details'], param);
  }

  getAllStages() {
    this.commonService.lookupList.subscribe((res: any) => {
      this.stageList = [];
      this.marketList = [];
      this.salesRepList = [];
      this.statusList = [];
      this.listLookups = res;
      if (res.length != 0) {
        this.stageList = res.stage.stage;
        this.statusList = res.company.leadStatus;
        this.marketList = res.company.marketIndustry;
        this.salesRepList = res.opportunity.salesRep;
        if (history.state.stage) {
          this.stageType = history.state.stage;
          this.showFilter = true;
          this.filterCompanyList();
        }
      }
    });
  }

  ClearFilterStageType(event: any) {
    this.stageType = null;
    event.stopPropagation();
    this.filterCompanyList(event);
  }
  clearFilterStatusType(event: any) {
    this.statusType = null;
    event.stopPropagation();
    this.filterCompanyList(event);
  }
  clearFilterMarketType(event: any) {
    this.marketType = null;
    event.stopPropagation();
    this.filterCompanyList(event);
  }
  clearFilterSalesRepType(event: any) {
    this.salesRepId = null;
    this.salesRepType = null;
    this.defaultSalesRepType = null;
    event.stopPropagation();
    this.filterCompanyList(event);
  }

  clearSearch() {
    if (
      this.stageType ||
      this.statusType ||
      this.marketType ||
      this.salesRepType
    ) {
      this.stageType = null;
      this.statusType = null;
      this.marketType = null;
      this.salesRepType = null;
      this.dataSource = this.mainDataSouce;
      // this.dataSource.sort = this.sort;
      // this.dataSource.sort.matSortChange;
      this.dataSource.paginator = this.paginator;
      this.filterCompanyList();
    }
  }
  openFilter() {
    this.showFilter = !this.showFilter;
  }

  goToAddContact() {
    let param: NavigationExtras = {
      state: {
        from: 'company-list'
      }
    };
    this.router.navigate(['add-company'], param);
  }

  clear() {
    this.searchText = '';
    this.getAllCompanies();
  }
  sortCompanyList(sort: any) {
    this.sort = sort;
    this.sortName = this.sort[0].field;
    this.sortBy = this.sort[0].dir;
    this.filterCompanyList();
  }

  onChangePage(ev: PageEvent) {
    this.pageIndex = ev.pageIndex + 1;
    localStorage.setItem('pageLength', ev.pageSize.toString());
    this.pageLength = ev.pageSize;
    this.getAllCompanies();
  }
  getSalesRep(id: any) {
    let salesRep = _.findWhere(this.listLookups?.opportunity?.salesRep, { id });
    if (salesRep) {
      return salesRep.salesRepDescription;
    }
  }
  public rowCallback(context: any) {
    return {
      dragging: context.dataItem.dragging
    };
  }
  public exportToExcel(grid: any): void {
    grid.saveAsExcel();
  }
  public pageChange(event: PageChangeEvent): void {
    this.pageIndex = (event.skip + this.state.take) / 100;
    this.pageLength = this.state.take;
    // this.getAllCompanies();
  }
}
