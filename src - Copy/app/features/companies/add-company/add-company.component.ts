import { LayoutService } from '../../../features/layout/layout.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { Apiurl } from '../../../shared/route';
import { CommonProvider } from '../../../shared/common';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, NgZone, OnInit, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import * as _ from 'underscore';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { ApiService, Maps } from '../../../api.service';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.scss']
})
export class AddCompanyComponent implements OnInit, AfterViewInit {
  @ViewChild('search')
  public searchElementRef: ElementRef;
  @ViewChild('searchBillingAdress')
  public searchElementRefBaddress: ElementRef;
  @ViewChild('searchShippingAdress')
  public searchElementRefSaddress: ElementRef;
  addCompanyForm: FormGroup;
  selectedIndex = 0;
  listLookups: any = [];
  companyList: any = [];
  companyListSearch: any = [];
  billingPostCodeLength = 0;
  billingPostCodeData: any;
  shippingPostCodeLength = 0;
  shippingPostCodeData: any;
  editflag: Boolean = false;
  parentId: any;
  id: any;
  phoneLength = 0;
  phoneData: any;
  alternateNumberLength = 0;
  alternateNumberData: any;
  faxNumberLength = 0;
  faxNumberData: any;
  submitted: boolean = false;
  allExpand: boolean = false;
  firstExpand: boolean = true;
  searchdropdown: any;
  Place: any;
  companyObject: any;
  netSuiteId: any = 0;
  userDetailsStr: any = '';
  userDetails: any;
  checked: boolean = false;
  userIsAdmin: any;
  defaultSalesRepId: any;
  constructor(
    private apiService: ApiService,
    private fb: FormBuilder,
    private commonService: CommonProvider,
    private router: Router,
    private route: ActivatedRoute,
    private layoutService: LayoutService,
    private ngZone: NgZone,
    private location: Location,
    private cdr: ChangeDetectorRef,
    private SpinnerService: NgxSpinnerService
  ) {
    if (localStorage.getItem('UserDetails')) {
      this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      this.userDetails = JSON.parse(this.userDetailsStr);
    }
  }

  initAutocomplete(maps: Maps) {
    //Search Region
    let autocomplete = new maps.places.Autocomplete(
      this.searchElementRef?.nativeElement
    );
    autocomplete.addListener('place_changed', () => {
      this.ngZone.run(() => {
        this.addCompanyForm.controls['companyName'].setValue(
          autocomplete.getPlace().name
        );
        this.addCompanyForm.controls['tradingName'].setValue(
          autocomplete.getPlace().name
        );
        this.setAddress(autocomplete);
        // this.addCompanyForm.controls['billingAddressLine1'].setValue(autocomplete.getPlace().address_components[1]['long_name'])
        // this.addCompanyForm.controls['billingAddressLine2'].setValue(autocomplete.getPlace().address_components[2]['long_name'])
        // this.addCompanyForm.controls['billingCity'].setValue(autocomplete.getPlace().address_components[3]['short_name'])
        // this.addCompanyForm.controls['billingPostCode'].setValue(autocomplete.getPlace().address_components[6]['long_name'] ? autocomplete.getPlace().address_components[6]['long_name'] : 0)
        // this.addCompanyForm.controls['web'].setValue(autocomplete.getPlace().website)
        // if (isNaN(parseInt(this.addCompanyForm.controls['billingPostCode'].value))) {
        //   this.addCompanyForm.controls['billingPostCode'].setValue(0)
        // }

        // if (autocomplete.getPlace().formatted_phone_number) {
        //   this.addCompanyForm.controls['phone'].setValue(autocomplete.getPlace().formatted_phone_number);
        // }
        // this.listLookups?.address.states.forEach((element: any) => {
        //   if (element.stateDescription == autocomplete.getPlace().address_components[4]['long_name']) {
        //     this.addCompanyForm.controls['billingState'].setValue(parseInt(element.id))
        //   }
        // });
        this.locationFromPlace(autocomplete.getPlace());
      });
    });

    //Billing Search Region
    let autocompleteBilling = new maps.places.Autocomplete(
      this.searchElementRefBaddress?.nativeElement
    );
    autocompleteBilling.addListener('place_changed', () => {
      this.ngZone.run(() => {
        this.setAddress(autocompleteBilling);
        // this.addCompanyForm.controls['billingAddressLine1'].setValue(autocompleteBilling.getPlace().address_components[0]['long_name'] + ', ' + autocompleteBilling.getPlace().address_components[1]['long_name'])
        // this.addCompanyForm.controls['billingAddressLine2'].setValue(autocompleteBilling.getPlace().address_components[2]['long_name'])
        // this.addCompanyForm.controls['billingCity'].setValue(autocompleteBilling.getPlace().address_components[3]['short_name'])
        // this.addCompanyForm.controls['billingPostCode'].setValue(autocompleteBilling.getPlace().address_components[6]['long_name'] ? autocomplete.getPlace().address_components[6]['long_name'] : 0)
        // if (isNaN(parseInt(this.addCompanyForm.controls['billingPostCode'].value))) {
        //   this.addCompanyForm.controls['billingPostCode'].setValue(0)
        // }

        // this.listLookups?.address.states.forEach((element: any) => {
        //   if (element.stateDescription == autocompleteBilling.getPlace().address_components[4]['long_name']) {
        //     this.addCompanyForm.controls['billingState'].setValue(parseInt(element.id))
        //   }
        // });
        this.locationFromPlace(autocompleteBilling.getPlace());
      });
    });
    let autocompleteShipping = new maps.places.Autocomplete(
      this.searchElementRefSaddress?.nativeElement
    );
    autocompleteShipping.addListener('place_changed', () => {
      this.ngZone.run(() => {
        this.setShippingAddress(autocompleteShipping);
        // this.addCompanyForm.controls['billingAddressLine1'].setValue(autocompleteBilling.getPlace().address_components[0]['long_name'] + ', ' + autocompleteBilling.getPlace().address_components[1]['long_name'])
        // this.addCompanyForm.controls['billingAddressLine2'].setValue(autocompleteBilling.getPlace().address_components[2]['long_name'])
        // this.addCompanyForm.controls['billingCity'].setValue(autocompleteBilling.getPlace().address_components[3]['short_name'])
        // this.addCompanyForm.controls['billingPostCode'].setValue(autocompleteBilling.getPlace().address_components[6]['long_name'] ? autocomplete.getPlace().address_components[6]['long_name'] : 0)
        // if (isNaN(parseInt(this.addCompanyForm.controls['billingPostCode'].value))) {
        //   this.addCompanyForm.controls['billingPostCode'].setValue(0)
        // }

        // this.listLookups?.address.states.forEach((element: any) => {
        //   if (element.stateDescription == autocompleteBilling.getPlace().address_components[4]['long_name']) {
        //     this.addCompanyForm.controls['billingState'].setValue(parseInt(element.id))
        //   }
        // });
        this.locationFromPlace(autocompleteShipping.getPlace());
      });
    });
  }
  setAddress(autocomplete: any) {
    var addressLine1 = '',
      addressLine2 = '';
    this.addCompanyForm.controls['billingAddressLine1'].setValue('');
    this.addCompanyForm.controls['billingAddressLine2'].setValue('');
    this.addCompanyForm.controls['billingCity'].setValue('');
    this.addCompanyForm.controls['billingPostCode'].setValue('');
    this.addCompanyForm.controls['billingState'].setValue(0);

    this.addCompanyForm.controls['web'].setValue(
      autocomplete.getPlace().website
    );
    if (autocomplete.getPlace().formatted_phone_number) {
      this.addCompanyForm.controls['phone'].setValue(
        autocomplete.getPlace().formatted_phone_number
      );
    }
    if (
      autocomplete.getPlace().address_components &&
      autocomplete.getPlace().address_components.length
    )
      var addressComponents = autocomplete.getPlace().address_components;
    addressComponents.forEach((addressComp: any) => {
      var addType = addressComp.types[0];
      if (addType) {
        if (addType == 'street_number') {
          addressLine1 += addressComp['long_name'] + ', ';
          this.addCompanyForm.controls['billingAddressLine1'].setValue(
            addressLine1
          );
        } else if (addType == 'premise') {
          addressLine1 += addressComp['long_name'] + ', ';
          this.addCompanyForm.controls['billingAddressLine1'].setValue(
            addressLine1
          );
        } else if (addType == 'route') {
          addressLine1 += addressComp['long_name'] + ', ';
          this.addCompanyForm.controls['billingAddressLine1'].setValue(
            addressLine1
          );
        } else if (addType == 'neighborhood') {
          addressLine1 += addressComp['long_name'] + ', ';
          this.addCompanyForm.controls['billingAddressLine1'].setValue(
            addressLine1
          );
        } else if (addType == 'sublocality_level_1') {
          addressLine2 += addressComp['long_name'] + ', ';
          this.addCompanyForm.controls['billingAddressLine2'].setValue(
            addressLine1
          );
        } else if (addType == 'sublocality_level_2') {
          addressLine2 += addressComp['long_name'] + ', ';
          this.addCompanyForm.controls['billingAddressLine2'].setValue(
            addressLine1
          );
        } else if (addType == 'postal_town') {
          addressLine1 += addressComp['long_name'] + ', ';
          this.addCompanyForm.controls['billingAddressLine1'].setValue(
            addressLine1
          );
        } else if (addType == 'locality') {
          // addressLine2 += addressComp['long_name'] + ", ";
          // this.addCompanyForm.controls['billingAddressLine2'].setValue(addressLine2);
          this.addCompanyForm.controls['billingCity'].setValue(
            addressComp['long_name']
          );
        } else if (addType == 'administrative_area_level_1') {
          // addressLine2 += addressComp['long_name'] + ", ";
          // this.addCompanyForm.controls['billingAddressLine2'].setValue(addressLine2);
          this.listLookups?.address.states.forEach((element: any) => {
            if (element.stateDescription == addressComp['long_name']) {
              this.addCompanyForm.controls['billingState'].setValue(
                parseInt(element.id)
              );
            }
          });
        } else if (addType == 'administrative_area_level_2') {
          // addressLine2 += addressComp['long_name'];
          // this.addCompanyForm.controls['billingAddressLine2'].setValue(addressLine2);
        } else if (addType == 'postal_code') {
          // addressLine2 += addressComp['long_name'];
          // this.addCompanyForm.controls['billingAddressLine2'].setValue(addressLine2);
          this.addCompanyForm.controls['billingPostCode'].setValue(
            addressComp['long_name']
          );
          this.billingPostCodeLength = addressComp['long_name'].length;
        } else if (
          isNaN(parseInt(this.addCompanyForm.controls['billingPostCode'].value))
        ) {
          this.addCompanyForm.controls['billingPostCode'].setValue(0);
        }
      }
    });
    this.setCompaniesShippingAddress();
  }
  setShippingAddress(autocomplete: any) {
    var addressLine1 = '',
      addressLine2 = '';
    this.addCompanyForm.controls['shippingAddressLine1'].setValue('');
    this.addCompanyForm.controls['shippingAddressLine2'].setValue('');
    this.addCompanyForm.controls['shippingCity'].setValue('');
    this.addCompanyForm.controls['shippingPostCode'].setValue('');
    this.addCompanyForm.controls['shippingState'].setValue(0);

    this.addCompanyForm.controls['web'].setValue(
      autocomplete.getPlace().website
    );
    if (autocomplete.getPlace().formatted_phone_number) {
      this.addCompanyForm.controls['phone'].setValue(
        autocomplete.getPlace().formatted_phone_number
      );
    }
    if (
      autocomplete.getPlace().address_components &&
      autocomplete.getPlace().address_components.length
    )
      var addressComponents = autocomplete.getPlace().address_components;
    addressComponents.forEach((addressComp: any) => {
      var addType = addressComp.types[0];
      if (addType) {
        if (addType == 'street_number') {
          addressLine1 += addressComp['long_name'] + ', ';
          this.addCompanyForm.controls['shippingAddressLine1'].setValue(
            addressLine1
          );
        } else if (addType == 'premise') {
          addressLine1 += addressComp['long_name'] + ', ';
          this.addCompanyForm.controls['shippingAddressLine1'].setValue(
            addressLine1
          );
        } else if (addType == 'route') {
          addressLine1 += addressComp['long_name'] + ', ';
          this.addCompanyForm.controls['shippingAddressLine1'].setValue(
            addressLine1
          );
        } else if (addType == 'neighborhood') {
          addressLine1 += addressComp['long_name'] + ', ';
          this.addCompanyForm.controls['shippingAddressLine1'].setValue(
            addressLine1
          );
        } else if (addType == 'sublocality_level_1') {
          addressLine2 += addressComp['long_name'] + ', ';
          this.addCompanyForm.controls['shippingAddressLine2'].setValue(
            addressLine1
          );
        } else if (addType == 'sublocality_level_2') {
          addressLine2 += addressComp['long_name'] + ', ';
          this.addCompanyForm.controls['shippingAddressLine2'].setValue(
            addressLine1
          );
        } else if (addType == 'postal_town') {
          addressLine1 += addressComp['long_name'] + ', ';
          this.addCompanyForm.controls['shippingAddressLine1'].setValue(
            addressLine1
          );
        } else if (addType == 'locality') {
          // addressLine2 += addressComp['long_name'] + ", ";
          // this.addCompanyForm.controls['shippingAddressLine2'].setValue(addressLine2);
          this.addCompanyForm.controls['shippingCity'].setValue(
            addressComp['long_name']
          );
        } else if (addType == 'administrative_area_level_1') {
          // addressLine2 += addressComp['long_name'] + ", ";
          // this.addCompanyForm.controls['shippingAddressLine2'].setValue(addressLine2);
          this.listLookups?.address.states.forEach((element: any) => {
            if (element.stateDescription == addressComp['long_name']) {
              this.addCompanyForm.controls['shippingState'].setValue(
                parseInt(element.id)
              );
            }
          });
        } else if (addType == 'administrative_area_level_2') {
          // addressLine2 += addressComp['long_name'];
          // this.addCompanyForm.controls['shippingAddressLine2'].setValue(addressLine2);
        } else if (addType == 'postal_code') {
          // addressLine2 += addressComp['long_name'];
          // this.addCompanyForm.controls['shippingAddressLine2'].setValue(addressLine2);
          this.addCompanyForm.controls['shippingPostCode'].setValue(
            addressComp['long_name']
          );
          this.shippingPostCodeLength = addressComp['long_name'].length;
        } else if (
          isNaN(
            parseInt(this.addCompanyForm.controls['shippingPostCode'].value)
          )
        ) {
          this.addCompanyForm.controls['shippingPostCode'].setValue(0);
        }
      }
    });
  }

  async ngOnInit() {
    this.addCompanyForm = this.createCompanyForm();
    this.getAllLookups();

    // this.route.queryParams.subscribe(params => {
    //   if (params.id != undefined) {
    //     this.id = params.id;
    //     this.setCompanyData(this.id);
    //     this.editflag = true
    //   }
    // });
    if (history.state.navigationId != 1) {
      if (history && history.state) {
        if (history.state.id && (history.state.from == "company-detail")) {
          this.id = history.state.id;
          await this.setCompanyData(this.id);
          this.editflag = true;
        }
      }
    } else {
      this.router.navigate(['dashboard']);
      setTimeout(() => {
        this.layoutService.activeRoute = this.router.url;
        this.layoutService.getcurrentActivateRoute(
          this.layoutService.activeRoute
        );
      }, 50);
    }
  }

  getAllLookups() {
    this.listLookups = [];
    this.commonService.lookupList.subscribe(async (res: any) => {
      this.listLookups = res;
      this.getUserDetailsFromStorage();
      await this.getAllCompaniesList();
    });
  }
  getUserDetailsFromStorage() {
    let userDetailsStr: any = '';
    if (localStorage.getItem('UserDetails')) {
      userDetailsStr = localStorage.getItem('UserDetails')?.toString();
      this.userDetails = JSON.parse(userDetailsStr);
      this.userIsAdmin = this.userDetails.isAdmin || false;
      if (!this.userIsAdmin) {
        this.defaultSalesRepId = this.userDetails.salesRepId;
        this.addCompanyForm.controls['salesRepId'].setValue(
          this.defaultSalesRepId
        );
      }
    }
  }
  ngAfterViewInit() {
    this.apiService.api.then((maps: any) => {
      this.initAutocomplete(maps);
    });
    this.cdr.detectChanges();
  }

  locationFromPlace(place: any) {
    const components = place.address_components;
    if (components === undefined) {
      return;
    }
    const areaLevel3 = this.getShort(components, 'administrative_area_level_3');
    const locality = this.getLong(components, 'locality');
    const cityName = locality || areaLevel3;
    const countryName = this.getLong(components, 'country');
    const countryCode = this.getShort(components, 'country');
    const stateCode = this.getShort(components, 'administrative_area_level_1');
    const name = place.name !== cityName ? place.name : null;
  }

  getLong(components: any, name: string) {
    const component = this.getComponent(components, name);
    return component && component.long_name;
  }

  getShort(components: any, name: string) {
    const component = this.getComponent(components, name);
    return component && component.short_name;
  }

  getComponent(components: any, name: string) {
    return components.filter(
      (component: any) => component.types[0] === name
    )[0];
  }

  createCompanyForm() {
    return this.fb.group({
      stageTypeId: [2, Validators.required],
      companyName: ['', Validators.required],
      parentId: [null],
      parentType: [''],
      tradingName: [''],
      leadSourceId: [0],
      leadStatusId: [11],
      salesRepId: [null],
      subsidiaryId: [
        this.userDetails && this.userDetails.subsidiaryId
          ? this.userDetails.subsidiaryId
          : 6
      ],
      comments: [''],
      email: [
        '',
        [
          Validators.email,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')
        ]
      ],
      phone: ['', [Validators.pattern('^[\\d() +-]+$')]],
      marketId: ['', Validators.required],
      tierId: ['', Validators.required],
      productCategoryId: ['', Validators.required],
      softwareId: [0],
      billingAddressLine1: [''],
      billingAddressLine2: [''],
      billingCity: [''],
      billingPostCode: [''],
      billingState: [0],
      billingCountry: [1],
      shippingAddressLine1: [''],
      shippingAddressLine2: [''],
      shippingCity: [''],
      shippingPostCode: [''],
      shippingState: [0],
      shippingCountry: [1],
      relatedContactIds: [],
      alternateNumber: ['', [Validators.pattern('^[\\d() +-]+$')]],
      faxNumber: ['', Validators.pattern('^[0-9]{10}$')],
      web: [''],
      linkedIn: [''],
      facebook: [''],
      google: ['']
    });
  }

  setCompanyData(id: any) {
    let self = this;
    return new Promise(function (resolve, reject) {
      self.commonService
        .GetMethod(Apiurl.getAllCompanies + '/' + id, null, true, 'Loading')
        .then((res: any) => {
          if (res) {
            if (!res.relatedContactIdsString) {
              res.relatedContactIdsString = '';
            }
            let ids = res.relatedContactIdsString.split(',');
            let relatedContactIds: any = [];
            if (ids) {
              ids.forEach((id: any) => {
                relatedContactIds.push(+id);
              });
            }
            self.netSuiteId = res.netSuiteId;
            self.companyObject = res;
            self.parentId = res.parentId;
            self.addCompanyForm.controls['companyName'].setValue(
              res.tradingName
            );
            self.addCompanyForm.controls['stageTypeId'].setValue(res.stageId);
            self.addCompanyForm.controls['parentId'].setValue(res.parentId);
            self.addCompanyForm.controls['tradingName'].setValue(
              res.tradingName
            );
            self.addCompanyForm.controls['leadSourceId'].setValue(
              res.leadSourceId
            );
            self.addCompanyForm.controls['leadStatusId'].setValue(
              res.leadStatusId
            );
            self.addCompanyForm.controls['salesRepId'].setValue(res.salesRepId);
            self.addCompanyForm.controls['subsidiaryId'].setValue(
              res.subsidiaryId
            );
            self.addCompanyForm.controls['comments'].setValue(res.comments);
            self.addCompanyForm.controls['email'].setValue(res.email);
            self.addCompanyForm.controls['phone'].setValue(res.phone);
            self.addCompanyForm.controls['marketId'].setValue(
              res.marketIndustryId
            );
            self.addCompanyForm.controls['tierId'].setValue(res.tierId);
            self.addCompanyForm.controls['productCategoryId'].setValue(
              res.productCategoryId
            );
            self.addCompanyForm.controls['softwareId'].setValue(res.softwareId);
            self.addCompanyForm.controls['billingAddressLine1'].setValue(
              res.billingAddress?.addressLine1
            );
            self.addCompanyForm.controls['billingAddressLine2'].setValue(
              res.billingAddress?.addressLine2
            );
            self.addCompanyForm.controls['billingCity'].setValue(
              res.billingAddress?.city
            );
            self.addCompanyForm.controls['billingPostCode'].setValue(
              res.billingAddress?.postCode
            );
            self.addCompanyForm.controls['billingState'].setValue(
              res.billingAddress?.stateId
            );
            self.addCompanyForm.controls['billingCountry'].setValue(
              res.billingAddress?.countryId
            );
            self.addCompanyForm.controls['shippingAddressLine1'].setValue(
              res.shippingAddress?.addressLine1
            );
            self.addCompanyForm.controls['shippingAddressLine2'].setValue(
              res.shippingAddress?.addressLine2
            );
            self.addCompanyForm.controls['shippingCity'].setValue(
              res.shippingAddress?.city
            );
            self.addCompanyForm.controls['shippingPostCode'].setValue(
              res.billingAddress?.postCode
            );
            self.addCompanyForm.controls['shippingState'].setValue(
              res.shippingAddress?.stateId
            );
            self.addCompanyForm.controls['shippingCountry'].setValue(
              res.shippingAddress?.countryId
            );
            self.addCompanyForm.controls['relatedContactIds'].setValue(
              relatedContactIds
            );
            self.addCompanyForm.controls['alternateNumber'].setValue(
              res.alternateNumber
            );
            self.addCompanyForm.controls['faxNumber'].setValue(res.faxNumber);
            self.addCompanyForm.controls['web'].setValue(res.web);
            self.addCompanyForm.controls['linkedIn'].setValue(res.linkedIn);
            self.addCompanyForm.controls['facebook'].setValue(res.facebook);
            self.addCompanyForm.controls['google'].setValue(res.google);
            resolve(true);
          } else {
            reject(true);
          }
          //self.companyList.filter((x  :any)=>x.id ==res.parentId ).map((x  :any)=>x.tradingName)[0];
          //
          // let name = _.findWhere(self.companyList, { id: res.parentId })
        });
    });
  }

  getAllCompaniesList() {
    new Promise((resolve: any, reject: any) => {
      let self = this;
      if (self.listLookups && self.listLookups.company) {
        self.companyList = self.listLookups.company?.companyList;
        self.companyList = self.commonService.sortAlphabetically(
          self.companyList
        );
        self.companyListSearch = JSON.parse(JSON.stringify(self.companyList));
        if (self.editflag) {
          self.addCompanyForm.controls['parentType'].setValue(
            self.companyList
              .filter((x: any) => x.companyId == self.parentId)
              .map((x: any) => x.companyName)[0]
          );
        }
        resolve(true);
      } else {
        reject(true);
      }
    });
  }
  saveCustomer(): any {
    this.submitted = true;
    this.firstExpand = true;
    this.allExpand = true;
    if (!this.addCompanyForm.valid) {
      if (this.addCompanyForm.controls.companyName.status == 'INVALID') {
        this.selectedIndex = 0;
      } else if (
        this.addCompanyForm.controls.email.status == 'INVALID' ||
        this.addCompanyForm.controls.phone.status == 'INVALID'
      ) {
        this.selectedIndex = 1;
      } else if (
        this.addCompanyForm.controls.marketId.status == 'INVALID' ||
        this.addCompanyForm.controls.tierId.status == 'INVALID'
      ) {
        this.selectedIndex = 2;
      } else if (
        this.addCompanyForm.controls.productCategoryId.status == 'INVALID'
      ) {
        this.selectedIndex = 3;
      } else if (
        this.addCompanyForm.controls.billingPostCode.status == 'INVALID'
      ) {
        this.selectedIndex = 5;
      }
      this.addCompanyForm.markAllAsTouched();
      return false;
    } else {
      let salesRep: any = '';
      let stage: any = '';
      if (
        this.addCompanyForm.value.salesRepId == 0 ||
        this.addCompanyForm.value.salesRepId == null
      ) {
        salesRep = '';
      } else {
        salesRep = _.findWhere(this.listLookups?.opportunity?.salesRep, {
          id: this.addCompanyForm.value.salesRepId
        });
        if (salesRep) {
          salesRep = salesRep.salesRepDescription;
        }
      }
      if (
        this.addCompanyForm.value.salesRepId == 0 ||
        this.addCompanyForm.value.salesRepId == null
      ) {
        stage = '';
      } else {
        stage = _.findWhere(this.listLookups?.stage?.stage, {
          id: this.addCompanyForm.value.stageTypeId
        }).stageDescription;
        if (stage) {
          stage = stage.stageDescription;
        }
      }
      if (
        !this.addCompanyForm.value.billingPostCode ||
        this.addCompanyForm.value.billingPostCode == ''
      ) {
        this.addCompanyForm.value.shippingPostCode = 0;
      }
      if (
        !this.addCompanyForm.value.billingPostCode ||
        this.addCompanyForm.value.billingPostCode == ''
      ) {
        this.addCompanyForm.value.billingPostCode = 0;
      }
      if (!this.addCompanyForm.value.parentId) {
        this.addCompanyForm.value.parentId = 0;
      }

      let paramObj = {
        netSuiteId: this.netSuiteId ? this.netSuiteId : 0,
        tradingName: this.addCompanyForm.value.companyName,
        legalName: this.addCompanyForm.value.tradingName,
        salesRepId: this.addCompanyForm.value.salesRepId,
        salesRep: salesRep,
        comments: this.addCompanyForm.value.comments,
        phone: this.addCompanyForm.value.phone,
        email: this.addCompanyForm.value.email,
        stage: stage,
        dateCreated: new Date(),
        tierId: this.addCompanyForm.value.tierId,
        marketIndustryId: this.addCompanyForm.value.marketId,
        productCategoryId: this.addCompanyForm.value.productCategoryId,
        leadStatusId: this.addCompanyForm.value.leadStatusId,
        leadSourceId: this.addCompanyForm.value.leadSourceId,
        softwareId: this.addCompanyForm.value.softwareId,
        subsidiaryId: this.addCompanyForm.value.subsidiaryId,
        parentId: this.addCompanyForm.value.parentId,
        accountId: 0,
        stageId: this.addCompanyForm.value.stageTypeId,
        shippingAddress: {
          id: 0,
          companyId: 0,
          addressLine1: this.addCompanyForm.value.shippingAddressLine1,
          addressLine2: this.addCompanyForm.value.shippingAddressLine2,
          city: this.addCompanyForm.value.shippingCity,
          stateId: this.addCompanyForm.value.shippingState,
          postCode: this.addCompanyForm.value.shippingPostCode
            ? parseInt(this.addCompanyForm.value.shippingPostCode)
            : 0,
          countryId: this.addCompanyForm.value.shippingCountry
        },
        billingAddress: {
          id: 0,
          companyId: 0,
          addressLine1: this.addCompanyForm.value.billingAddressLine1,
          addressLine2: this.addCompanyForm.value.billingAddressLine2,
          city: this.addCompanyForm.value.billingCity,
          stateId: this.addCompanyForm.value.billingState,
          postCode: this.addCompanyForm.value.billingPostCode
            ? parseInt(this.addCompanyForm.value.billingPostCode)
            : 0,
          countryId: this.addCompanyForm.value.billingCountry
        },
        createBy: '1',
        createDate: new Date(),
        updatedBy: '1',
        updatedDate: new Date(),
        alternateNumber: this.addCompanyForm.value.alternateNumber,
        faxNumber: this.addCompanyForm.value.faxNumber,
        web: this.addCompanyForm.value.web,
        linkedIn: this.addCompanyForm.value.linkedIn,
        facebook: this.addCompanyForm.value.facebook,
        google: this.addCompanyForm.value.google,
        relatedContactIds: this.addCompanyForm.value.relatedContactIds || []
      };
      if (this.editflag) {
        this.SpinnerService.show();
        paramObj.netSuiteId = this.netSuiteId ? this.netSuiteId : 0;
        this.commonService
          .PutMethod(
            Apiurl.saveCompany + '/' + this.id,
            paramObj,
            true,
            'Loading',
            null
          )
          .then((res: any) => {
            if (res) {
              this.commonService.getAllLookupList();
              this.SpinnerService.hide();
              if (res?.code?.includes('Duplicate Error')) {
                // Swal.fire(res?.code, res.message, "error").then(() => {
                // });
                this.commonService.showError(res.message);
                return;
              }
              this.commonService.showSuccess('Company Updated successfully');
              //Swal.fire("Success", "Company Updated successfully", "success").then(() => {
              if (history && history.state) {
                let param: NavigationExtras;
                param = {
                  state: {
                    // id: res,
                    id: this.id,
                    contact_id: history.state.contact_id,
                    Company_id: res,
                    from: 'add-company'
                  }
                };
                if (history.state.from == 'contacts') {
                  this.router.navigate(['contacts'], param);
                }
                // if (history.state.from == 'company-list') {
                //   this.router.navigate(['companies']);
                // }
                if (
                  history.state.from == 'company-detail' ||
                  history.state.from == 'company-list'
                ) {
                  this.router.navigate(['companies-details'], param);
                }
              } else {
                this.router.navigate(['companies']);
              }
              //});
            } else {
              // Swal.fire("Error", "Error updating contacts, Please try again", "error").then(() => {
              // });
              this.commonService.showError(
                'Error updating Company, Please try again'
              );
            }
          })
          .catch(err => { });
      } else {
        this.SpinnerService.show();
        this.commonService
          .PostMethod(Apiurl.saveCompany, paramObj, false, 'Loading.', null)
          .then((responsedata: any) => {
            this.SpinnerService.hide();
            if (responsedata) {
              this.commonService.getAllLookupList();
              if (responsedata?.code?.includes('Duplicate Error')) {
                // Swal.fire(responsedata?.code, responsedata.message, "error").then(() => {
                // });
                this.commonService.showError(responsedata.message);
                return;
              }
              this.commonService.showSuccess('Company Added successfully');
              //Swal.fire("Success", "Company Added successfully", "success").then(() => {
              if (history && history.state) {
                let param: NavigationExtras;
                param = {
                  state: {
                    id: responsedata,
                    Company_id: responsedata,
                    contact_id: history.state.contact_id,
                    from: 'add-company'
                  }
                };
                if (history.state.from == 'contacts') {
                  this.router.navigate(['contacts'], param);
                }
                // if (history.state.from == 'company-list') {
                //   this.router.navigate(['companies']);
                // }
                if (
                  history.state.from == 'company-detail' ||
                  history.state.from == 'company-list'
                ) {
                  this.router.navigate(['companies-details'], param);
                }
              } else {
                this.router.navigate(['companies']);
              }
              //});
            } else {
              this.commonService.showError(
                'Something went wrong!, Company was not saved'
              );
              // Swal.fire("Error", "Something went wrong!, Contact was not saved", "error").then(() => {
              // });
            }
          })
          .catch(err => { });
      }
    }
  }
  async goBackToListing() {
    let obj = {
      title: 'Are you sure you want to Cancel?',
      cancelButtonText: 'No',
      confirmButtonText: 'Yes'
    };
    await this.commonService.showConfirm(obj).then((resp: any) => {
      if (resp.isConfirmed) {
        if (history && history.state) {
          console.log("CompanyHistoryState", history)
          console.log("CompanyHistory", history.state)
          let param: NavigationExtras;
          param = {
            state: {
              id: history.state.id,
              Company_id: history.state.id,
              contact_id: history.state.contact_id,
              from: 'add-company'
            }
          };
          console.log("Params", param)
          if (history.state.from == 'contacts') {
            this.router.navigate(['contacts'], param);
          }
          if (history.state.from == 'company-list') {
            this.router.navigate(['companies']);
          }
          if (history.state.from == 'company-detail' && !history.state.isAdd) {
            this.router.navigate(['companies-details'], param);
          } else {
            this.location.back();
          }
        } else {
          this.router.navigate(['companies']);
        }
      }
    });
  }

  searchPhoneNumber(event: any, from: any) {
    if (from == 'billing') {
      if (event.target.value.length <= 6) {
        this.billingPostCodeLength = event.target.value.length;
        this.billingPostCodeData = event.target.value;
        return;
      }
      this.addCompanyForm.controls['billingPostCode'].setValue(
        parseInt(this.billingPostCodeData)
      );
    }
    if (from == 'shipping') {
      if (event.target.value.length <= 6) {
        this.shippingPostCodeLength = event.target.value.length;
        this.shippingPostCodeData = event.target.value;
        return;
      }
      this.addCompanyForm.controls['shippingPostCode'].setValue(
        parseInt(this.shippingPostCodeData)
      );
    }
    if (from == 'phone') {
      // if (event.target.value.length <= 10) {
      //
      //   this.phoneLength = event.target.value.length
      //   this.phoneData = event.target.value
      //   return;
      // }
      this.phoneData = event.target.value;
      this.addCompanyForm.controls['phone'].setValue(this.phoneData);
    }
    if (from == 'alternateNumber') {
      // if (event.target.value.length <= 10) {
      //   this.alternateNumberLength = event.target.value.length
      //   this.alternateNumberData = event.target.value
      //   return;
      // }
      this.alternateNumberData = event.target.value;
      this.addCompanyForm.controls['alternateNumber'].setValue(
        this.alternateNumberData
      );
    }
    if (from == 'faxNumber') {
      if (event.target.value.length <= 10) {
        this.faxNumberLength = event.target.value.length;
        this.faxNumberData = event.target.value;
        return;
      }
      this.addCompanyForm.controls['faxNumber'].setValue(this.faxNumberData);
    }
  }

  inputCompanyName(event: any) {
    if (event.target.value.length == 0) {
      this.addCompanyForm.controls['billingAddressLine1'].setValue('');
      this.addCompanyForm.controls['billingAddressLine2'].setValue('');
      this.addCompanyForm.controls['billingCity'].setValue('');
      this.addCompanyForm.controls['billingPostCode'].setValue('');
      this.addCompanyForm.controls['billingState'].setValue(0);
    }
    this.addCompanyForm.controls['tradingName'].setValue(event.target.value);
  }

  searchParent(event: any) {
    if (event.target.value != '') {
      this.companyListSearch = this.companyList.filter(
        (s: any) =>
          s.companyName
            .toLowerCase()
            .indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.companyListSearch = this.companyList;
    }
  }
  onTabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.selectedIndex = tabChangeEvent.index;
  }
  setPatient(obj: any) {
    if (!obj) return;
    this.addCompanyForm.controls['parentId'].setValue(obj.companyId);
    let data = _.findWhere(this.companyListSearch, {
      companyId: obj.companyId
    });
    this.addCompanyForm.controls['salesRepId'].setValue(data.salesRepId);
  }
  clearCompanyDetails() {
    this.searchdropdown = '';
    this.companyListSearch = this.companyList;
  }

  webAdd(event: any) {
    if (event.target.value.length == 1) {
      this.addCompanyForm.controls['web'].setValue(
        'https://' + event.target.value
      );
    }
  }
  // setCompaniesShippingAddress(checked: boolean) {
  //   this.checked = !this.checked;
  //   if (this.checked) {
  //     this.addCompanyForm.controls['shippingAddressLine1'].setValue(this.addCompanyForm.controls['billingAddressLine1'].value);
  //     this.addCompanyForm.controls['shippingAddressLine1'].disable();
  //     this.addCompanyForm.controls['shippingAddressLine2'].setValue(this.addCompanyForm.controls['billingAddressLine2'].value);
  //     this.addCompanyForm.controls['shippingAddressLine2'].disable();
  //     this.addCompanyForm.controls['shippingCity'].setValue(this.addCompanyForm.controls['billingCity'].value);
  //     this.addCompanyForm.controls['shippingCity'].disable();
  //     this.addCompanyForm.controls['shippingPostCode'].setValue(this.addCompanyForm.controls['billingPostCode'].value);
  //     this.addCompanyForm.controls['shippingPostCode'].disable();
  //     this.addCompanyForm.controls['shippingState'].setValue(this.addCompanyForm.controls['billingState'].value);
  //     this.addCompanyForm.controls['shippingState'].disable();
  //   } else {
  //     this.addCompanyForm.controls['shippingAddressLine1'].setValue('');
  //     this.addCompanyForm.controls['shippingAddressLine1'].enable();
  //     this.addCompanyForm.controls['shippingAddressLine2'].setValue('');
  //     this.addCompanyForm.controls['shippingAddressLine2'].enable();
  //     this.addCompanyForm.controls['shippingCity'].setValue('');
  //     this.addCompanyForm.controls['shippingCity'].enable();
  //     this.addCompanyForm.controls['shippingPostCode'].setValue('');
  //     this.addCompanyForm.controls['shippingPostCode'].enable();
  //     this.addCompanyForm.controls['shippingState'].setValue(0);
  //     this.addCompanyForm.controls['shippingState'].enable();
  //   }
  // }
  setCompaniesShippingAddress() {
    this.addCompanyForm.controls['shippingAddressLine1'].setValue(
      this.addCompanyForm.controls['billingAddressLine1'].value
    );
    this.addCompanyForm.controls['shippingAddressLine2'].setValue(
      this.addCompanyForm.controls['billingAddressLine2'].value
    );
    this.addCompanyForm.controls['shippingCity'].setValue(
      this.addCompanyForm.controls['billingCity'].value
    );
    this.addCompanyForm.controls['shippingPostCode'].setValue(
      this.addCompanyForm.controls['billingPostCode'].value
    );
    this.addCompanyForm.controls['shippingState'].setValue(
      this.addCompanyForm.controls['billingState'].value
    );
  }
}
