import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { CommonProvider } from '../../../shared/common';
import { async, Subject } from 'rxjs';
import { Apiurl } from '../../../shared/route';
import { LayoutService } from './../../layout/layout.service';
import { CalendarView, CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent } from 'angular-calendar';
import * as _ from 'underscore';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { startOfDay, endOfDay, isSameDay, isSameMonth, } from 'date-fns';
import { CalendarSchedulerViewComponent } from 'angular-calendar-scheduler';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { NgSelectComponent } from '@ng-select/ng-select';
@Component({
  selector: 'app-companies-details',
  templateUrl: './companies-details.component.html',
  styleUrls: ['./companies-details.component.scss']
})
export class CompaniesDetailsComponent implements OnInit {
  speedDialFabButtons = [
    {
      icon: 'note',
      tooltip: 'Task',
      class: 'note-color'
    },
    {
      icon: 'event',
      tooltip: 'Event',
      class: 'event-color'
    },
    {
      icon: 'phone',
      tooltip: 'Phone',
      class: 'phone-color'
    }
  ];
  getCompanyDetails: any;
  showCal: boolean = false;
  ContactList: any = [];
  contactListSearch: any = [];
  @ViewChild('ngSelectComponent') ngSelectComponent: NgSelectComponent;
  calendarScheduler: CalendarSchedulerViewComponent;
  filterTypeList: any = [];
  statusList: any = [];
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;
  view: any = 'Agenda';
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  events: CalendarEvent[];
  refresh: Subject<any> = new Subject();
  quoteData: any = [];
  oppertunityData: any = [];
  companyList: any = [];

  /*--LOADER FLAG--*/
  showActivitiesLoader = true;
  showActivitiesNoRecMsg = false;
  showContactsLoader = true;
  showContactsNoRecMsg = false;
  showOppertunitiesLoader = true;
  showOppertunitiesNoRecMsg = false;
  showQuoteLoader = true;
  showQuoteNoRecMsg = false;
  showMoreCmnt = false;

  addressData: any;
  modalData: {
    action: string;
    event: CalendarEvent;
  };
  actions: CalendarEventAction[] = [
    {
      label: '<i class="fas fa-fw fa-pencil-alt"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fas fa-fw fa-trash-alt"></i>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];
  listLookups: any = [];
  selectedView: any = 'Agenda';
  searchInput: any;
  showFilter: boolean = false;
  milestone_flag: any = 1;
  activeDayIsOpen: boolean = true;
  value: any;
  display_data: any;
  companydata: Array<any> = [];
  id: any;
  contactId: any;
  salesRepId: any;
  // filterType: any;
  dataSource: Array<any> = [];
  companymetadata: any = [];
  displayedColumns = [
    'firstName',
    'lastName',
    'jobTitle',
    'role',
    'email',
    'phone'
  ];

  shippingdetails = [
    'addressLine1',
    'addressLine2',
    'city',
    'postCode',
    'stateId',
    'countryId'
  ];
  productinfo = ['productCategory', 'software'];
  classicInfo = ['marketIndustry', 'tier', 'productCategory'];
  communiactionInfo = ['phone', 'email', 'subsidary'];
  primaryInfo = [
    'stage',
    'legalName',
    'parentName',
    'tradingName',
    'leadSource',
    'leadStatus',
    'salesRep',

    'comments'
  ];
  contact_id: any;
  agendaList: any = [];
  agendaTableColumn: string[] = [
    'type',
    'title',
    'startDate',
    'duration',
    'salesRep',
    'statusId',
    'relatedCustomer',
    'relatedTransaction',
    'relatedContact',
    'phone'
  ];
  subActivityForm: FormGroup;
  mainagendaList: any = [];
  relatedCompanyList: any = [];
  relatedContactList: any = [];
  listContacts: any = [];
  listContactIds: any = [];
  listParticipants: any = [];
  //serching & filteration
  @ViewChild(MatSort) sort: any;
  searchText: any = '';
  statusType: any;
  eventType: any;
  eventTitle: any;
  relatedCompany: any;
  relatedTransaction: any;
  relatedContact: any;
  dayStartHour = 9;
  weekStartsOn = 1;
  loadTime: boolean = false;
  // dayEndHour = 23
  selectedDate: Date = new Date();
  userDetailsStr: any;
  userDetail: any;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public commonService: CommonProvider,
    private modal: NgbModal,
    private layoutService: LayoutService,
    private SpinnerService: NgxSpinnerService,
    private fb: FormBuilder
  ) {
    setTimeout(() => {
      this.loadTime = true;
    }, 10000);
  }

  async ngOnInit(): Promise<void> {
    const myDateValue = this.selectedDate;
    myDateValue.setHours(myDateValue.getHours() + 1);
    this.subActivityForm = this.fb.group({
      id: [''],
      companyId: [''],
      contact: ['0'],
      title: ['', Validators.required],
      description: [''],
      phoneNumber: ['', [Validators.pattern('^[\\d() +-]+$')]],
      startDate: [this.selectedDate, Validators.required],
      endDate: [this.selectedDate, Validators.required],
      startTime: [this.selectedDate, Validators.required],
      endTime: [myDateValue, Validators.required],
      transactionType: [''],
      relatedTransactionId: [''],
      salesRepId: [0],
      statusId: [0]
    });

    // this.getContactList()
    if (history.state.navigationId != 1) {
      // this.getAllQuotes();
      // this.getAllOppertunities();

      this.id = history.state.id;

      if (this.id) {
        this.getData();
      }
      if (history.state.contact_id) {
        this.contact_id = history.state.contact_id;
      } else {
        this.contact_id = null;
      }
      if (history.state.activity_id) {
      }
      if (history.state.quote_id) {
        this.getAllQuotes();
      }
    } else {
      this.router.navigate(['dashboard']);
      setTimeout(() => {
        this.layoutService.activeRoute = this.router.url;
        this.layoutService.getcurrentActivateRoute(
          this.layoutService.activeRoute
        );
      }, 50);
    }
    this.filterTypeList = [
      {
        value: 1,
        name: 'Type'
      },
      {
        value: 1,
        name: 'Phone'
      },
      {
        value: 2,
        name: 'Event'
      },
      {
        value: 3,
        name: 'Task'
      },
      {
        value: 4,
        name: 'Note'
      }
    ];
    this.getAllLookupList();
    this.getStatusList();
    await this.getAllContacts();
  }

  //navigate to another page
  navigateTo(page: any) {
    let param: NavigationExtras = {
      state: {
        id: history.state.id,
        contact_id: 0,
        from: 'company-detail'
      }
    };
    switch (page) {
      case 'Contacts':
        this.router.navigate(['contacts'], param);
        break;
      case 'Quotes':
        this.router.navigate(['add-quotes'], param);
        setTimeout(() => {
          this.layoutService.activeRoute = this.router.url;
          this.layoutService.getcurrentActivateRoute(
            this.layoutService.activeRoute
          );
        }, 50);
        break;
      case 'Opportunities':
        this.router.navigate(['add-opportunities'], param);
        setTimeout(() => {
          this.layoutService.activeRoute = this.router.url;
          this.layoutService.getcurrentActivateRoute(
            this.layoutService.activeRoute
          );
        }, 50);
        break;

      default:
        break;
    }
  }
  showMore() {
    this.showMoreCmnt = !this.showMoreCmnt;
  }
  getStatusList() {
    this.statusList = [];
    this.commonService.lookupList.subscribe((res: any) => {
      if (res.length != 0) {
        this.statusList = res.activity.statuses;
      }
    });
  }

  async getData() {
    this.showContactsLoader = true;
    this.showContactsNoRecMsg = false;
    this.commonService
      .GetMethod(Apiurl.getAllCompanies + '/' + this.id, null, true, 'Loading')
      .then((response: any) => {
        this.getCompanyDetails = response;
        this.showCal = true;
        if (response) this.setCompanyDataInViewMode(response);
        this.getAllContactList();
      });
    // }
  }
  setCompanyDataInViewMode(res: any) {
    this.dataSource.push(res);
    this.companymetadata = res;
    this.getAllOppertunities();
    this.getAllQuotes();
    this.companydata = res.relatedContacts;
    this.showContactsLoader = false;
    if (!this.companydata || this.companydata.length == 0) {
      this.showContactsNoRecMsg = true;
    }
  }
  getAllContacts() {
    var companyId = this.subActivityForm.get('companyId')?.value;
    return new Promise((resolve: any, reject: any) => {
      this.commonService
        .GetMethod(Apiurl.getContactList, null, false, 'Loading..')
        .then((res: any) => {
          if (res) {
            resolve(true);
            for (let i = 0; i < res.length; i++) {
              if (res[i].companyId == companyId) {
                this.relatedContactList.push(res[i]);
              }
            }
            // }
            for (let i = 0; i < res.length; i++) {
              if (history.state.relatedContactIds) {
                let self = this;
                function forLoop(j: any) {
                  if (j >= history.state.relatedContactIds.length) return;
                  let id = history.state.relatedContactIds[j];
                  if (id === res[i].id) {
                    res[i].isSelect = false;
                    self.listContacts.push(res[i]);
                    self.listContactIds.push(res[i].id);
                    // if (!self.editflag) {
                    if (res[i].role == 'Primary Contact') {
                      self.subActivityForm.controls['contact'].setValue(
                        res[i].id
                      );
                      // }
                    }
                  } else {
                    forLoop(j + 1);
                  }
                }
                forLoop(0);
              } else if (res[i].companyId == companyId) {
                res[i].isSelect = false;
                this.listContacts.push(res[i]);
                this.listContactIds.push(res[i].id);
                // if (!this.editflag) {
                if (
                  history.state.contact_id != null &&
                  history.state.contact_id != undefined
                ) {
                  var getRefContactInfo: any;
                  getRefContactInfo = _.filter(this.listContacts, {
                    id: history.state.contact_id
                  });
                  if (getRefContactInfo[0]) {
                    var phoneNum = getRefContactInfo[0].phone;
                  }
                  if (phoneNum != '') {
                    this.subActivityForm.controls['phoneNumber'].setValue(
                      phoneNum
                    );
                  }
                }
                if (res[i].role == 'Primary Contact') {
                  this.subActivityForm.controls['contact'].setValue(res[i].id);
                }
                // }
              }
            }
            if (this.listContacts.length == 1) {
              this.subActivityForm.controls['contact'].setValue(
                this.listContacts[0].id
              );
            }
            this.SpinnerService.hide();
          } else {
            resolve(false);
          }
        });
    });
  }

  //addnew / edit details
  async addNewDetail(status: any) {
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    this.userDetail =
      JSON.parse(this.userDetailsStr).salesRepId ==
        this.companymetadata.salesRepId
        ? true
        : false;

    let param: NavigationExtras;
    if (status == 'editCurrent') {
      param = {
        state: {
          id: this.companymetadata.id,
          from: 'company-detail',
          contact_id: history.state.contact_id
        }
      };
      // Set isAdmin & SalesRepId Validation 
      if (JSON.parse(this.userDetailsStr).salesRepId !==
        this.companymetadata.salesRepId && !JSON.parse(this.userDetailsStr).isAdmin) {
        let obj = {
          title: 'Error',
          text: "Company SalesRep doesn't match with login SalesRep",
          cancelButtonText: 'ok'
        };
        await this.commonService.showErrorMsg(obj).then((resp: any) => { });
      } else {
        this.router.navigate(['add-company'], param);
      }
    } else {
      param = {
        state: {
          id: null,
          from: 'company-detail',
          contact_id: history.state.contact_id,
          isAdd: true
        }
      };
      this.router.navigate(['add-company'], param);
    }
  }

  //schedular start
  viewFilterField() {
    this.showFilter = !this.showFilter;
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    var response = _.filter(this.mainagendaList, {
      id: this.modalData.event.id
    });

    let param: NavigationExtras = {
      state: {
        type: response[0].type,
        activity_id: event.id,
        from: 'company-detail'
      }
    };
    this.router.navigate(['/activity-details'], param);
  }

  addEvent(): void {
    this.events = [
      ...this.events,
      {
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        // color: colors.purpule,

        resizable: {
          beforeStart: true,
          afterEnd: true
        }
      }
    ];
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter(event => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
    this.showFilter = false;
  }
  viewAgenda() {
    this.view = 'Agenda';
  }
  //schedular end

  filterTypeChange(event: any) {
    this.agendaList = new MatTableDataSource(this.mainagendaList);
    this.agendaList.sort = this.sort;
    if (this.searchText !== '') {
      this.agendaList.filter = this.searchText.trim().toLowerCase();
    }
    if (this.eventType) {
      this.agendaList = new MatTableDataSource(
        _.filter(this.agendaList.filteredData, {
          type: this.eventType
        })
      );
    }
    if (this.statusType) {
      this.agendaList = new MatTableDataSource(
        _.filter(this.agendaList.filteredData, {
          statusId: this.statusType
        })
      );
    }
    if (this.statusType) {
      this.agendaList = new MatTableDataSource(
        _.filter(this.agendaList.filteredData, {
          statusId: parseInt(this.statusType)
        })
      );
    }
    if (this.salesRepId) {
      this.agendaList = new MatTableDataSource(
        _.filter(this.agendaList.filteredData, { salesRepId: this.salesRepId })
      );
    }
    this.agendaList.sort = this.sort;
  }

  statusTypeChange(event: any) { }

  clearSearch() {
    if (this.eventType || this.statusType || this.salesRepId) {
      this.searchInput = '';
      this.eventType = null;
      this.statusType = null;
      this.salesRepId = null;
      this.agendaList = new MatTableDataSource(this.mainagendaList);
    }
  }

  gotoActivities(whereToGo: any) {
    let param: NavigationExtras = {
      state: {
        type: whereToGo,
        id: history.state.id,
        from: 'company-detail'
      }
    };
    this.router.navigate(['sub-activity'], param);
  }

  getAllQuotes() {
    this.showQuoteLoader = true;
    this.showQuoteNoRecMsg = false;
    this.quoteData = this.companymetadata?.quotes;
    if (this.quoteData) {
      this.quoteData.map((data: any) => {
        if (data.documentNumber) {
          data.opp = data.documentNumber;
        } else {
          data.opp = 'ESTBG' + data.id;
        }
      });
    }
    this.showQuoteLoader = false;
    if (!this.quoteData || this.quoteData?.length == 0) {
      this.showQuoteNoRecMsg = true;
    }
  }
  getAllOppertunities() {
    this.showOppertunitiesLoader = true;
    this.showOppertunitiesNoRecMsg = false;
    this.oppertunityData = [];
    this.oppertunityData = this.companymetadata.opportunities;
    if (this.oppertunityData) {
      this.oppertunityData.map((data: any) => {
        if (data.documentNumber) {
          data.opp = data.documentNumber;
        } else {
          data.opp = 'OPP#' + data.id;
        }
      });
    }
    this.showOppertunitiesLoader = false;
    if (!this.oppertunityData || this.oppertunityData?.length == 0) {
      this.showOppertunitiesNoRecMsg = true;
    }
  }

  inputEvent(event: any) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.filterTypeChange(filterValue);
  }

  clear() {
    this.searchText = '';
  }
  async gotoQuote(item: any) {
    //Set Admin and salesRepId validation
    let userDetailsStr: any = ''; userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    var userDetails = JSON.parse(userDetailsStr);
    if (this.companymetadata.salesRepId !==
      userDetails.salesRepId && !userDetails.isAdmin) {
      let obj = {
        title: 'Error',
        text: "Quotes SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => { });
    } else {
      let param: NavigationExtras = {
        state: {
          id: history.state.id,
          contact_id: history.state.contact_id,
          qoute_id: item.id,
          from: 'company-detail',
          isEdit: true,
          item
        }
      };
      this.router.navigate(['add-quotes'], param);
    }
  }
  async gotoOpportunity(item: any) {
    //Set Admin and salesRepId validation
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    var userDetails = JSON.parse(this.userDetailsStr)
    if (this.companymetadata.salesRepId !== userDetails.salesRepId && !userDetails.isAdmin) {
      let obj = {
        title: 'Error',
        text: "Opportunities SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => { });
    }
    else {
      let param: NavigationExtras = {
        state: {
          id: history.state.id,
          contact_id: history.state.contact_id,
          qoute_id: item.id,
          from: 'company-detail',
          isEdit: true,
          item
        }
      };
      this.router.navigate(['add-opportunities'], param);
    }

  }

  getAllLookupList() {
    let self = this;
    return new Promise(function (resolve, reject) {
      self.commonService.lookupList.subscribe((list: any) => {
        if (list) {
          self.listLookups = list;
          self.addressData = list.address;

          resolve(true);
        } else {
          reject(true);
        }
      });
    });
  }
  async gotoContact(data: any) {
    //Set Admin and salesRepId validation
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    var userDetails = JSON.parse(this.userDetailsStr)
    if (this.companymetadata.salesRepId !== userDetails.salesRepId && !userDetails.isAdmin) {
      let obj = {
        title: 'Error',
        text: "Opportunities SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => { });
    }
    else {
      let param: NavigationExtras = {
        state: {
          contact_id: data.id,
          id: this.companymetadata.id,
          from: 'company-detail'
        }
      };
      this.router.navigate(['contacts'], param);
    }
  }

  //clear individual filter
  clearIndividualFilter(filtername: any) {
    switch (filtername) {
      case 'eventType':
        this.eventType = null;
        this.filterTypeChange(null);
        break;
      case 'statusType':
        this.statusType = null;
        this.filterTypeChange(null);
        break;
      case 'salesRep':
        this.salesRepId = null;
        this.filterTypeChange(null);
        break;
      default:
        break;
    }
  }

  //display button
  showdetails(value: any) {
    let param: NavigationExtras = {
      state: {
        type: value.type,
        activity_id: value.id,
        from: 'company-detail',
        id: history.state.id
      }
    };
    this.router.navigate(['/activity-details'], param);
  }
  getCountry(id: any) {
    let data = _.findWhere(this.listLookups?.address?.countries, { id: id });
    if (data) {
      return data.countryDescription;
    } else {
      return;
    }
  }
  getParentName(id: any) {
    let data = _.findWhere(this.listLookups?.company?.companyList, {
      companyId: id
    });
    if (data) {
      return data.companyName;
    } else {
      return;
    }
  }

  getAllContactList() {
    this.showContactsLoader = true;
    this.showContactsNoRecMsg = false;
    this.commonService.lookupList.subscribe((resp: any) => {
      if (resp.length != 0) {
        this.ContactList = resp?.contact?.contactList;
        this.contactListSearch = Object.assign(this.ContactList);
        // if (this.getCompanyDetails && this.contactListSearch.length != 0 && this.getCompanyDetails.relatedContacts.length) {
        //   for (let i = 0; i < this.contactListSearch.length; i++) {
        //     // this.contactListSearch[i].isDisable = false
        //     for (let index = 0; index < this.getCompanyDetails.relatedContacts.length; index++) {
        //       if (this.getCompanyDetails.relatedContacts[index].id == this.contactListSearch[i].contactId) {
        //         // this.contactListSearch[i].isDisable = true
        //       }
        //     }
        //   }
        // }
      }
      this.showContactsLoader = false;
      if (!this.ContactList || !this.ContactList?.length) {
        this.showContactsNoRecMsg = true;
      }
    });
  }

  searchParent(event: any) {
    if (event.target.value != '') {
      this.contactListSearch = this.ContactList.filter(
        (s: any) =>
          s.name.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1
      );
    } else {
      this.contactListSearch = this.ContactList;
    }
  }

  updateContacts(obj: any) {
    if (
      obj &&
      this.getCompanyDetails &&
      !this.getCompanyDetails.relatedContactIds?.includes(
        obj.contactId.toString()
      )
    ) {
      if (obj.contactId && obj.contactId != 0 && obj.contactId != '') {
        if (!this.getCompanyDetails.relatedContactIdsString) {
          this.getCompanyDetails.relatedContactIdsString = '';
        }
        this.getCompanyDetails.relatedContactIdsString = this.getCompanyDetails.relatedContactIdsString.concat(
          ',' + obj.contactId.toString()
        );
        let data = this.getCompanyDetails.relatedContactIdsString.split(',');

        this.getCompanyDetails.relatedContactIds = [];
        data.forEach((element: any) => {
          element = parseInt(element);
          if (element) {
            this.getCompanyDetails.relatedContactIds.push(element);
          }
        });
      }

      this.commonService
        .PutMethod(
          Apiurl.saveCompany + '/' + this.id,
          this.getCompanyDetails,
          true,
          'Loading',
          null
        )
        .then((res: any) => {
          if (res) {
            this.commonService.getAllLookupList();
            this.getData();
          }
          this.ngSelectComponent.clearModel();
          this.ngSelectComponent.blur();
        });
    } else if (obj) {
      this.commonService.showError('This contact is already added');
      //Swal.fire('This contact is already added')
    }
  }

  getSalesRep(id: any) {
    let salesRep = _.findWhere(this.listLookups?.opportunity?.salesRep, {
      id: id
    });
    if (salesRep) {
      return salesRep.salesRepDescription;
    }
  }

  //redirct to add-quote page if login salesrepid is same as company salesrepid
  async setRedirctQuote() {
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    var userDetails = JSON.parse(this.userDetailsStr)
    if (
      !userDetails.isAdmin && this.companymetadata.salesRepId !==
      JSON.parse(this.userDetailsStr).salesRepId
    ) {
      let obj = {
        title: 'Error',
        text: "Quotes SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => { });
    } else {
      this.navigateTo('Quotes');
    }
  }

  //redirct to add-Opportunities page if login salesrepid is same as company salesrepid
  async setRedirctopportunity() {
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    var userDetails = JSON.parse(this.userDetailsStr)
    if (
      !userDetails.isAdmin && this.companymetadata.salesRepId !==
      JSON.parse(this.userDetailsStr).salesRepId
    ) {
      let obj = {
        title: 'Error',
        text: "Opportunities SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => { });
    }
    else {
      this.navigateTo('Opportunities');
    }
  }

  //redirct to add-contact page if login salesrepid is same as company salesrepid
  async setRedirectContact() {
    this.userDetailsStr = localStorage.getItem('UserDetails')?.toString();
    var userDetails = JSON.parse(this.userDetailsStr)
    if (this.companymetadata.salesRepId !== userDetails.salesRepId && !userDetails.isAdmin) {
      let obj = {
        title: 'Error',
        text: "Opportunities SalesRep doesn't match with login SalesRep",
        cancelButtonText: 'ok'
      };
      await this.commonService.showErrorMsg(obj).then((resp: any) => { });
    }
    else {
      this.navigateTo('Contacts');
    }
  }
}
