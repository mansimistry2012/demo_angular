import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class LayoutService {
    activeRoute: any;
    LoginToken: any = null;
    // LoginToken: any = "4F4F9EB5A3F86E7FAE5E431E179777AFB5FE4D07730219A66A263A332193AF15";

    constructor(
        private router: Router,
    ) {
        // localStorage.setItem("LoginToken", this.LoginToken);
    }

    private routeSelected = new Subject<any>();
    private sideMenuToggle = new Subject<any>();

    getcurrentActivateRoute(route: any) {
        this.routeSelected.next(route)
    }
    setObservable(): Subject<any> {
        return this.routeSelected;
    }

    sendToggleFlag(data: any) {
        this.sideMenuToggle.next(data)
    }

    setObservableforFlag(): Subject<any> {
        return this.sideMenuToggle;
    }

    ResetLoginToken() {
        localStorage.clear();
    }
}