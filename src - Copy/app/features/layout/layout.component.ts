import { HttpClient } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { CommonProvider } from 'src/app/shared/common';
import { AuthService } from 'src/app/shared/core/authentication/auth.service';
import { Apiurl } from 'src/app/shared/route';
import { LayoutService } from './layout.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})

export class LayoutComponent implements OnInit {
  sideBarOpen = true;
  activeRoute: any;
  miniSidemenu: boolean = false;

  constructor(
    private router: Router,
    private layoutService: LayoutService,
    private authService: AuthService
  ) {
    this.getUserDetails()
    this.layoutService.setObservable().subscribe((data: any) => {
      this.activeRoute = data
    })

    this.layoutService.setObservableforFlag().subscribe((data: any) => {
      if (data != undefined && data != null) {
        if (data == true) {
          this.miniSidemenu = true;
        } else {
          this.miniSidemenu = false;
        }
      }
    })
  }
  async getUserDetails() {
    var loginToken: any = localStorage.getItem("LoginToken") ? localStorage.getItem("LoginToken") : null;
    let userInfo: any;
    if (!userInfo && !loginToken) {
      userInfo = await this.authService.getUserDetails();
      if (!localStorage.getItem("Email") && userInfo) {
        localStorage.setItem("LoginToken", userInfo.access_token);
        localStorage.setItem("Email", userInfo.profile.email);
      }
    }
  }

  ngOnInit(): void {
    //code for auth login response
    this.layoutService.LoginToken = localStorage.getItem("LoginToken")
    if (this.layoutService.LoginToken) {
      this.router.navigate(['dashboard'])
    } else {
      localStorage.setItem("LoginToken", '')
      this.router.navigate(['login'])
    }
    if (this.layoutService.activeRoute != undefined && this.layoutService.activeRoute != null) {
      this.activeRoute = this.layoutService.activeRoute
    } else {
      this.activeRoute = this.router.url
    }
    this.manageRouteEvents();
  }

  private manageRouteEvents() {
    this.layoutService.activeRoute = this.router.url;
    this.layoutService.getcurrentActivateRoute(
      this.layoutService.activeRoute
    );
    this.activeRoute = this.layoutService.activeRoute
    // this.router.events.subscribe(val => {
    //   if (val instanceof NavigationEnd) {

    //   }
    // })
  }

  sideBarToggler(event: any) {
    this.sideBarOpen = !this.sideBarOpen;
  }

}
